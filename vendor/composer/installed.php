<?php return array(
    'root' => array(
        'name' => 'topthink/think',
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'reference' => NULL,
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'aferrandini/phpqrcode' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'reference' => '3c1c0454d43710ab5bbe19a51ad4cb41c22e3d46',
            'type' => 'library',
            'install_path' => __DIR__ . '/../aferrandini/phpqrcode',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'aliyuncs/oss-sdk-php' => array(
            'pretty_version' => 'v2.6.0',
            'version' => '2.6.0.0',
            'reference' => '572d0f8e099e8630ae7139ed3fdedb926c7a760f',
            'type' => 'library',
            'install_path' => __DIR__ . '/../aliyuncs/oss-sdk-php',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'bacon/bacon-qr-code' => array(
            'pretty_version' => '1.0.3',
            'version' => '1.0.3.0',
            'reference' => '5a91b62b9d37cee635bbf8d553f4546057250bee',
            'type' => 'library',
            'install_path' => __DIR__ . '/../bacon/bacon-qr-code',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'clue/stream-filter' => array(
            'pretty_version' => 'v1.7.0',
            'version' => '1.7.0.0',
            'reference' => '049509fef80032cb3f051595029ab75b49a3c2f7',
            'type' => 'library',
            'install_path' => __DIR__ . '/../clue/stream-filter',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'doctrine/cache' => array(
            'pretty_version' => 'v1.6.2',
            'version' => '1.6.2.0',
            'reference' => 'eb152c5100571c7a45470ff2a35095ab3f3b900b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/cache',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'endroid/qr-code' => array(
            'pretty_version' => '2.5.1',
            'version' => '2.5.1.0',
            'reference' => '6062677d3404e0ded40647b8f62ec55ff9722eb7',
            'type' => 'symfony-bundle',
            'install_path' => __DIR__ . '/../endroid/qr-code',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'guzzlehttp/guzzle' => array(
            'pretty_version' => '6.5.8',
            'version' => '6.5.8.0',
            'reference' => 'a52f0440530b54fa079ce76e8c5d196a42cad981',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/guzzle',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'guzzlehttp/promises' => array(
            'pretty_version' => '1.5.3',
            'version' => '1.5.3.0',
            'reference' => '67ab6e18aaa14d753cc148911d273f6e6cb6721e',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/promises',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'guzzlehttp/psr7' => array(
            'pretty_version' => '1.9.1',
            'version' => '1.9.1.0',
            'reference' => 'e4490cabc77465aaee90b20cfc9a770f8c04be6b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/psr7',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'guzzlehttp/uri-template' => array(
            'pretty_version' => 'v1.0.2',
            'version' => '1.0.2.0',
            'reference' => '61bf437fc2197f587f6857d3ff903a24f1731b5d',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/uri-template',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'jjonline/aliyun-dysms-php-sdk' => array(
            'pretty_version' => 'v2.0',
            'version' => '2.0.0.0',
            'reference' => '4155e585dd1c5668fce1afbbd8ac66c9de48cda8',
            'type' => 'library',
            'install_path' => __DIR__ . '/../jjonline/aliyun-dysms-php-sdk',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'jpush/jpush' => array(
            'pretty_version' => 'v3.6.8',
            'version' => '3.6.8.0',
            'reference' => 'ebb191e8854a35c3fb7a6626028b3a23132cbe2c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../jpush/jpush',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'khanamiryan/qrcode-detector-decoder' => array(
            'pretty_version' => '1',
            'version' => '1.0.0.0',
            'reference' => '96d5f80680b04803c4f1b69d6e01735e876b80c7',
            'type' => 'library',
            'install_path' => __DIR__ . '/../khanamiryan/qrcode-detector-decoder',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'lcobucci/jwt' => array(
            'pretty_version' => '3.3.1',
            'version' => '3.3.1.0',
            'reference' => 'a11ec5f4b4d75d1fcd04e133dede4c317aac9e18',
            'type' => 'library',
            'install_path' => __DIR__ . '/../lcobucci/jwt',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'lokielse/omnipay-alipay' => array(
            'pretty_version' => 'v3.1.2',
            'version' => '3.1.2.0',
            'reference' => '5ea0ff1e6240b3791164dc5c6cb5e5c4eaabab43',
            'type' => 'library',
            'install_path' => __DIR__ . '/../lokielse/omnipay-alipay',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'maniac/easemob-php' => array(
            'pretty_version' => '1.0.0',
            'version' => '1.0.0.0',
            'reference' => '36b550328c9911957becde2fd62b9379ba45865c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../maniac/easemob-php',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'moneyphp/money' => array(
            'pretty_version' => 'v3.3.3',
            'version' => '3.3.3.0',
            'reference' => '0dc40e3791c67e8793e3aa13fead8cf4661ec9cd',
            'type' => 'library',
            'install_path' => __DIR__ . '/../moneyphp/money',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'monolog/monolog' => array(
            'pretty_version' => '1.27.1',
            'version' => '1.27.1.0',
            'reference' => '904713c5929655dc9b97288b69cfeedad610c9a1',
            'type' => 'library',
            'install_path' => __DIR__ . '/../monolog/monolog',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'myclabs/php-enum' => array(
            'pretty_version' => '1.8.4',
            'version' => '1.8.4.0',
            'reference' => 'a867478eae49c9f59ece437ae7f9506bfaa27483',
            'type' => 'library',
            'install_path' => __DIR__ . '/../myclabs/php-enum',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'omnipay/common' => array(
            'pretty_version' => 'v3.2.1',
            'version' => '3.2.1.0',
            'reference' => '80545e9f4faab0efad36cc5f1e11a184dda22baf',
            'type' => 'library',
            'install_path' => __DIR__ . '/../omnipay/common',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'overtrue/socialite' => array(
            'pretty_version' => '1.3.0',
            'version' => '1.3.0.0',
            'reference' => 'fda55f0acef43a144799b1957a8f93d9f5deffce',
            'type' => 'library',
            'install_path' => __DIR__ . '/../overtrue/socialite',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'overtrue/wechat' => array(
            'pretty_version' => '3.7.4',
            'version' => '3.7.4.0',
            'reference' => '52441161eae93c97f46d1ea2a57141c77c327adc',
            'type' => 'library',
            'install_path' => __DIR__ . '/../overtrue/wechat',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'php-http/async-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '*',
                1 => '1.0',
            ),
        ),
        'php-http/client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '*',
                1 => '1.0',
            ),
        ),
        'php-http/discovery' => array(
            'pretty_version' => '1.19.2',
            'version' => '1.19.2.0',
            'reference' => '61e1a1eb69c92741f5896d9e05fb8e9d7e8bb0cb',
            'type' => 'composer-plugin',
            'install_path' => __DIR__ . '/../php-http/discovery',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'php-http/guzzle6-adapter' => array(
            'pretty_version' => 'v2.0.2',
            'version' => '2.0.2.0',
            'reference' => '9d1a45eb1c59f12574552e81fb295e9e53430a56',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-http/guzzle6-adapter',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'php-http/httplug' => array(
            'pretty_version' => '2.4.0',
            'version' => '2.4.0.0',
            'reference' => '625ad742c360c8ac580fcc647a1541d29e257f67',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-http/httplug',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'php-http/message' => array(
            'pretty_version' => '1.16.0',
            'version' => '1.16.0.0',
            'reference' => '47a14338bf4ebd67d317bf1144253d7db4ab55fd',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-http/message',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'php-http/message-factory' => array(
            'pretty_version' => '1.1.0',
            'version' => '1.1.0.0',
            'reference' => '4d8778e1c7d405cbb471574821c1ff5b68cc8f57',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-http/message-factory',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'php-http/message-factory-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'php-http/promise' => array(
            'pretty_version' => '1.2.1',
            'version' => '1.2.1.0',
            'reference' => '44a67cb59f708f826f3bec35f22030b3edb90119',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-http/promise',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'phpoffice/phpspreadsheet' => array(
            'pretty_version' => '1.3.1',
            'version' => '1.3.1.0',
            'reference' => 'aa5b0d0236c907fd8dba0883f3ceb97cc52e46ec',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpoffice/phpspreadsheet',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'pimple/pimple' => array(
            'pretty_version' => 'v3.5.0',
            'version' => '3.5.0.0',
            'reference' => 'a94b3a4db7fb774b3d78dad2315ddc07629e1bed',
            'type' => 'library',
            'install_path' => __DIR__ . '/../pimple/pimple',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/container' => array(
            'pretty_version' => '2.0.2',
            'version' => '2.0.2.0',
            'reference' => 'c71ecc56dfe541dbd90c5360474fbc405f8d5963',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/container',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-client' => array(
            'pretty_version' => '1.0.3',
            'version' => '1.0.3.0',
            'reference' => 'bb5906edc1c324c9a05aa0873d40117941e5fa90',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-client',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '*',
                1 => '1.0',
            ),
        ),
        'psr/http-factory-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '*',
            ),
        ),
        'psr/http-message' => array(
            'pretty_version' => '1.1',
            'version' => '1.1.0.0',
            'reference' => 'cb6ce4845ce34a8ad9e68117c10ee90a29919eba',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-message',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-message-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
                1 => '*',
            ),
        ),
        'psr/log' => array(
            'pretty_version' => '1.1.4',
            'version' => '1.1.4.0',
            'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/log',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/log-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0.0',
            ),
        ),
        'psr/simple-cache' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/simple-cache',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'ralouphie/getallheaders' => array(
            'pretty_version' => '3.0.3',
            'version' => '3.0.3.0',
            'reference' => '120b605dfeb996808c31b6477290a714d356e822',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ralouphie/getallheaders',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/http-foundation' => array(
            'pretty_version' => 'v3.4.47',
            'version' => '3.4.47.0',
            'reference' => 'b9885fcce6fe494201da4f70a9309770e9d13dc8',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-foundation',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/options-resolver' => array(
            'pretty_version' => 'v2.8.52',
            'version' => '2.8.52.0',
            'reference' => '7aaab725bb58f0e18aa12c61bdadd4793ab4c32b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/options-resolver',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-ctype' => array(
            'pretty_version' => 'v1.28.0',
            'version' => '1.28.0.0',
            'reference' => 'ea208ce43cbb04af6867b4fdddb1bdbf84cc28cb',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-ctype',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-idn' => array(
            'pretty_version' => 'v1.28.0',
            'version' => '1.28.0.0',
            'reference' => 'ecaafce9f77234a6a449d29e49267ba10499116d',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-idn',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-normalizer' => array(
            'pretty_version' => 'v1.28.0',
            'version' => '1.28.0.0',
            'reference' => '8c4ad05dd0120b6a53c1ca374dca2ad0a1c4ed92',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-normalizer',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.28.0',
            'version' => '1.28.0.0',
            'reference' => '42292d99c55abe617799667f454222c54c60e229',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php70' => array(
            'pretty_version' => 'v1.20.0',
            'version' => '1.20.0.0',
            'reference' => '5f03a781d984aae42cebd18e7912fa80f02ee644',
            'type' => 'metapackage',
            'install_path' => NULL,
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php72' => array(
            'pretty_version' => 'v1.28.0',
            'version' => '1.28.0.0',
            'reference' => '70f4aebd92afca2f865444d30a4d2151c13c3179',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php72',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php80' => array(
            'pretty_version' => 'v1.28.0',
            'version' => '1.28.0.0',
            'reference' => '6caa57379c4aec19c0a12a38b59b26487dcfe4b5',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php80',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/property-access' => array(
            'pretty_version' => 'v2.8.52',
            'version' => '2.8.52.0',
            'reference' => 'c8f10191183be9bb0d5a1b8364d3891f1bde07b6',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/property-access',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/psr-http-message-bridge' => array(
            'pretty_version' => 'v1.2.0',
            'version' => '1.2.0.0',
            'reference' => '9ab9d71f97d5c7d35a121a7fb69f74fee95cd0ad',
            'type' => 'symfony-bridge',
            'install_path' => __DIR__ . '/../symfony/psr-http-message-bridge',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'tencentcloud/tencentcloud-sdk-php' => array(
            'pretty_version' => '3.0.1074',
            'version' => '3.0.1074.0',
            'reference' => '564f9047a123d617ce3f9974e5ea210e795436b7',
            'type' => 'library',
            'install_path' => __DIR__ . '/../tencentcloud/tencentcloud-sdk-php',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'toohamster/ws-http' => array(
            'pretty_version' => '1.0.2',
            'version' => '1.0.2.0',
            'reference' => '79cf6ef17df8df80c0bb86451bd3e1d0adebd60b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../toohamster/ws-http',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/framework' => array(
            'pretty_version' => 'v5.1.42',
            'version' => '5.1.42.0',
            'reference' => 'ecf1a90d397d821ce2df58f7d47e798c17eba3ad',
            'type' => 'think-framework',
            'install_path' => __DIR__ . '/../../thinkphp',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'reference' => NULL,
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-captcha' => array(
            'pretty_version' => 'v2.0.2',
            'version' => '2.0.2.0',
            'reference' => '54c8a51552f99ff9ea89ea9c272383a8f738ceee',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-captcha',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-helper' => array(
            'pretty_version' => 'v1.0.7',
            'version' => '1.0.7.0',
            'reference' => '5f92178606c8ce131d36b37a57c58eb71e55f019',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-helper',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-image' => array(
            'pretty_version' => 'v1.0.7',
            'version' => '1.0.7.0',
            'reference' => '8586cf47f117481c6d415b20f7dedf62e79d5512',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-image',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'topthink/think-installer' => array(
            'pretty_version' => 'v2.0.5',
            'version' => '2.0.5.0',
            'reference' => '38ba647706e35d6704b5d370c06f8a160b635f88',
            'type' => 'composer-plugin',
            'install_path' => __DIR__ . '/../topthink/think-installer',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'wechatpay/wechatpay' => array(
            'pretty_version' => '1.4.9',
            'version' => '1.4.9.0',
            'reference' => '2cabc8a15136050c4ee61083cd24959114756a03',
            'type' => 'library',
            'install_path' => __DIR__ . '/../wechatpay/wechatpay',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'wechatpay/wechatpay-guzzle-middleware' => array(
            'pretty_version' => '0.2.2',
            'version' => '0.2.2.0',
            'reference' => '6782ac33ed8cf97628609a71cdc5e84a6a40677a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../wechatpay/wechatpay-guzzle-middleware',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'weiwei/api-doc' => array(
            'pretty_version' => '1.6.2',
            'version' => '1.6.2.0',
            'reference' => '6c2c3c03ce1139275cc5a5057677175ed8691e19',
            'type' => 'think-extend',
            'install_path' => __DIR__ . '/../weiwei/api-doc',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
