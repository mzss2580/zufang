<?php
if (!function_exists('get_region')) {
    /**
     * 获取行政区域数组
     * @param  integer $parent_id 上级行政区域id
     * @param  integer $level     行政区域级别
     * @return array              下级行政区域数组
     */
    function get_region($parent_id = 0, $level = 1)
    {
        return Db::name('system_region')->where('parent_id', $parent_id)->where('level', $level)->column('name', 'id');
    }
}
