<?php
namespace doorapi;

class Personnel extends Doorapi
{
    private static $url = 'http://dooropenapi.ymiot.net/Api/Personnel/';
    /**
     * 新增人员
     * @param  array $personnel 人员信息,必填内容:name姓名,id编号,idcard身份证号,sex性别(1男2女)
     * @return boolean          请求是否成功
     */
    public static function insert($personnel, $groupname = '', $community_id = 0)
    {
        if ($groupname === '') {
            $groupname = '益生菌';
        }
        $data = [
            'Personnel_Name'      => $personnel['name'],
            'Personnel_No'        => self::personnel_no($personnel['id'], $community_id),
            'Personnel_IDCard'    => $personnel['idcard'],
            'Personnel_Sex'       => $personnel['sex'],
            'Personnel_GroupName' => $groupname,
        ];
        if (isset($personnel['photo']) && $personnel['photo'] !== '') {
            $cont = self::image_base64($personnel['photo']);
            if ($cont !== '') {
                $data['Personnel_Photo'] = $cont;
            }
        }
        return self::request(self::$url . 'insert.do', ['data' => json_encode($data)], $community_id);
    }
    /**
     * 批量新增人员(批量新增无法新增人员照片)
     * @param  array $personnellist 人员信息,必填内容:name姓名,id编号,idcard身份证号,sex性别(1男2女)
     * @return boolean              请求是否成功
     */
    public static function insertlist($personnellist, $community_id = 0)
    {
        $data = [];
        foreach ($personnellist as $personnel) {
            $data[] = [
                'Personnel_Name'      => $personnel['name'],
                'Personnel_No'        => self::personnel_no($personnel['id'], $community_id),
                'Personnel_IDCard'    => $personnel['idcard'],
                'Personnel_Sex'       => $personnel['sex'],
                'Personnel_GroupName' => '益生菌',
            ];
        }
        return self::request(self::$url . 'insertlist.do', ['data' => json_encode($data)], $community_id);
    }
    /**
     * 编辑人员
     * @param  array $personnel 人员信息,必填内容:name姓名,id编号,idcard身份证号,sex性别(1男2女)
     * @return boolean          请求是否成功
     */
    public static function update($personnel, $groupname = '', $community_id = 0)
    {
        if ($groupname === '') {
            $groupname = '益生菌';
        }
        $data = [
            'Personnel_Name'      => $personnel['name'],
            'Personnel_No'        => self::personnel_no($personnel['id'], $community_id),
            'Personnel_IDCard'    => $personnel['idcard'],
            'Personnel_Sex'       => $personnel['sex'],
            'Personnel_GroupName' => '',
        ];
        if (isset($personnel['photo']) && $personnel['photo'] !== '') {
            $cont = self::image_base64($personnel['photo']);
            if ($cont !== '') {
                $data['Personnel_Photo'] = $cont;
            }
        }
        return self::request(self::$url . 'update.do', ['data' => json_encode($data)], $community_id);
    }
    /**
     * 删除人员
     * @param  array $personnel_id 人员id
     * @return boolean             请求是否成功
     */
    public static function delete($personnel_id, $community_id = 0)
    {
        return self::request(self::$url . 'delete.do', ['personnelno' => self::personnel_no($personnel_id, $community_id)], $community_id);
    }
    /**
     * 上传人员照片
     * @param  array  $personnel_id 人员id
     * @param  string $photo        人员照片
     * @return boolean              请求是否成功
     */
    public static function upload($personnel_id, $photo, $community_id = 0)
    {
        return self::request(self::$url . 'upload.do', ['personnelno' => self::personnel_no($personnel_id, $community_id), 'photo' => self::image_base64($photo)], $community_id);
    }
    /**
     * 人员授权设备
     * @param  array  $personnel_id 人员id
     * @param  string $deviceno     设备编号
     * @return boolean              请求是否成功
     */
    public static function auth($personnel_id, $deviceno, $community_id = 0)
    {
        return self::request(self::$url . 'auth.do', ['personnelno' => self::personnel_no($personnel_id, $community_id), 'deviceno' => $deviceno], $community_id);
    }

    protected static function image_base64($image)
    {
        $cont = '';
        $file = env('ROOT_PATH') . 'public' . str_replace('/', DIRECTORY_SEPARATOR, $image);
        if (file_exists($file)) {
            $open = fopen($file, "r");
            if ($open) {
                $size = filesize($file);
                $cont = urlencode(chunk_split(base64_encode(fread($open, $size))));
            }
            fclose($open);
        }
        return $cont;
    }
    private static function personnel_no($personnel_id, $community_id = 0)
    {
        if ($personnel_id > 5540) {
            $personnel_id = $personnel_id . '_' . $community_id;
        }
        return substr(strtoupper(md5($personnel_id)), 0, 16);
    }
}
