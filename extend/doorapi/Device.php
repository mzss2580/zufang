<?php
namespace doorapi;

class Device extends Doorapi
{
    private static $url = 'http://dooropenapi.ymiot.net/Api/device/';

    /**
     * 新增设备
     * @param  array $device 设备信息
     * @return boolean       请求是否成功
     */
    public static function insert($device, $community_id = 0)
    {
        $data = [
            'Device_AppID'           => self::getAppID($community_id),
            'Device_No'              => $device['deviceno'],
            'Device_Model'           => $device['modelno'],
            'Device_Name'            => $device['name'],
            'Device_Area'            => $device['area'],
            'Device_Type'            => $device['type'],
            'Device_Enable'          => $device['enable'],
            'Device_OutInType'       => $device['outintype'],
            'Device_Blacklist'       => $device['blacklist'],
            'Device_PictureRatio'    => $device['pictureratio'],
            'Device_Pwd'             => $device['pwd'],
            'Device_Volume'          => $device['volume'],
            'Device_LightBright'     => $device['light_bright'],
            'Device_LightMode'       => $device['light_mode'],
            'Device_LightOpenTime'   => $device['light_opentime'],
            'Device_LightCloseTime'  => $device['light_closetime'],
            'Device_RecogThreshold'  => $device['recog_threshold'],
            'Device_WhiteThreshold'  => $device['white_threshold'],
            'Device_OpenBroadcast'   => $device['broadcast'],
            'Device_RecogInterval'   => $device['recog_lnterval'],
            'Device_RecogSpace'      => $device['recog_space'],
            'Device_DoorMode'        => $device['door_mode'],
            'Device_DoorInterval'    => $device['door_lnterval'],
            'Device_DoorWiegandType' => $device['door_wiegand_type'],
            'Device_DoorWiegand'     => $device['door_wiegand'],
            'Device_OpenAdvert'      => $device['open_advert'],
            'Device_AutoRecord'      => 1,
            'Device_AutoRecordUrl'   => request()->domain() . '/api/record/index/device_id/' . $device['id'],
            'Device_IsLiving'        => $device['is_living'],
            'Device_LivingThreshold' => $device['living_threshold'],
            'Device_AdvertTime'      => $device['advert_time'],
            'Device_AdvertTitle'     => $device['advert_title'],
            'Device_AutoOpenDoor'    => $device['auto_open_door'],
            'Device_ResatrtDay'      => $device['restart_day'],
            'Device_ShowText'        => $device['show_text'],
            'Device_BroadcastText'   => $device['broadcast_text'],
            'Device_ResatrtTime'     => $device['restart_time'],
            'Device_DelayAlamValue'  => $device['delay_alam_value'],
        ];
        return self::request(self::$url . 'insert.do', ['deviceno' => $device['deviceno'], 'data' => json_encode($data)], $community_id);
    }
    /**
     * 编辑设备
     * @param  array $device 设备信息
     * @return boolean       请求是否成功
     */
    public static function update($device, $community_id = 0)
    {
        $data = [
            'Device_AppID'           => self::getAppID($community_id),
            'Device_No'              => $device['deviceno'],
            'Device_Model'           => $device['modelno'],
            'Device_Name'            => $device['name'],
            'Device_Area'            => $device['area'],
            'Device_Type'            => $device['type'],
            'Device_Enable'          => $device['enable'],
            'Device_OutInType'       => $device['outintype'],
            'Device_Blacklist'       => $device['blacklist'],
            'Device_PictureRatio'    => $device['pictureratio'],
            'Device_Pwd'             => $device['pwd'],
            'Device_Volume'          => $device['volume'],
            'Device_LightBright'     => $device['light_bright'],
            'Device_LightMode'       => $device['light_mode'],
            'Device_LightOpenTime'   => $device['light_opentime'],
            'Device_LightCloseTime'  => $device['light_closetime'],
            'Device_RecogThreshold'  => $device['recog_threshold'],
            'Device_WhiteThreshold'  => $device['white_threshold'],
            'Device_OpenBroadcast'   => $device['broadcast'],
            'Device_RecogInterval'   => $device['recog_lnterval'],
            'Device_RecogSpace'      => $device['recog_space'],
            'Device_DoorMode'        => $device['door_mode'],
            'Device_DoorInterval'    => $device['door_lnterval'],
            'Device_DoorWiegandType' => $device['door_wiegand_type'],
            'Device_DoorWiegand'     => $device['door_wiegand'],
            'Device_OpenAdvert'      => $device['open_advert'],
            'Device_AutoRecord'      => 1,
            'Device_AutoRecordUrl'   => request()->domain() . '/api/record/index/device_id/' . $device['id'],
            'Device_IsLiving'        => $device['is_living'],
            'Device_LivingThreshold' => $device['living_threshold'],
            'Device_AdvertTime'      => $device['advert_time'],
            'Device_AdvertTitle'     => $device['advert_title'],
            'Device_AutoOpenDoor'    => $device['auto_open_door'],
            'Device_ResatrtDay'      => $device['restart_day'],
            'Device_ShowText'        => $device['show_text'],
            'Device_BroadcastText'   => $device['broadcast_text'],
            'Device_ResatrtTime'     => $device['restart_time'],
            'Device_DelayAlamValue'  => $device['delay_alam_value'],
        ];
        return self::request(self::$url . 'update.do', ['deviceno' => $device['deviceno'], 'data' => json_encode($data)], $community_id);
    }
    /**
     * 删除设备
     * @param  array $deviceno 设备编号
     * @return boolean         请求是否成功
     */
    public static function delete($deviceno, $community_id = 0)
    {
        $data = [
            'Device_AppID' => self::getAppID($community_id),
            'Device_No'    => $deviceno,
        ];
        return self::request(self::$url . 'delete.do', ['deviceno' => $deviceno, 'data' => json_encode($data)], $community_id);
    }
    /**
     * 远程一键开门
     * @param  array $deviceno 设备编号
     * @return boolean         请求是否成功
     */
    public static function opendoor($deviceno, $community_id = 0)
    {
        $data = [
            'Device_AppID' => self::getAppID($community_id),
            'Device_No'    => $deviceno,
        ];
        return self::request(self::$url . 'opendoor', ['deviceno' => $deviceno, 'data' => json_encode($data)], $community_id);
    }
}
