<?php
namespace doorapi;

use Ws\Http\Request;
use Ws\Http\Request\Body;

class Doorapi
{
    protected static function request($url, $data = [], $community_id = 0)
    {
        $data = array_merge([
            'appid'     => self::getAppID($community_id),
            'timestemp' => date('YmdHis') . mt_rand(0, 999),
        ], $data);
        $body = Body::json(self::sign($data, $community_id));
        $http = Request::create();
        $resp = $http->post($url, ['content-type' => 'application/json'], $body);
        trace('doorapi请求:' . $resp->raw_body . ',appid:' . self::getAppID($community_id));
        if ($resp->body->code == '1') {
            return true;
        }
        return $resp->body->msg;
    }
    protected static function sign($data, $community_id)
    {
        ksort($data);
        $sign = '';
        foreach ($data as $k => $v) {
            $sign .= $k . '=' . $v . '&';
        }
        $sign .= 'key=' . self::getAppID($community_id, 'AppSecret');
        $data['sign'] = strtoupper(md5($sign));
        return $data;
    }
    protected static function getAppID($community_id, $key = 'AppID')
    {
        $appid = config('community.' . $key);
        if (!isset($appid[$community_id])) {
            $community_id = 0;
        }
        return $appid[$community_id];
    }
}
