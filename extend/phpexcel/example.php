<?php
// composer "phpoffice/phpspreadsheet": "1.3.1"
use phpexcel\PHPExcel;

// 需要下载的数据,第一行可设置为表头
$data = [
    ['student', 'teacher', 'class', 'lesson'],
    ['Tom', 'Spike', 'A Class', 'English'],
    ['Jerry', '', 'B Class', ''],
    ['Tyke', '', 'C Class', ''],
];

// 下载文件名,默认当前时间'YmdHis'
$filename = '';

// 设置列宽,一般为strlen() + 1
// 常用列宽
// 手机号:12
// 日期:11
// 日期时间:20
$widtharray = ['A' => 10, 'B' => 10, 'C' => 10, 'D' => 10];

// 其他设置
// eg:合并单元格,合并的单元格其他位置需填充''
$settings = [
    'mergeCells' => ['B2:B4', 'D2:D4'],
];

PHPExcel::download($data, $filename, $widtharray, $settings);
