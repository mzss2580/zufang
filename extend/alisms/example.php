<?php
// composer "jjonline/aliyun-dysms-php-sdk": "2.0"
use alisms\Alisms;

// https://ak-console.aliyun.com/#/accesskey 获取Access Key ID和Access Key Secret
// Access Key ID
$accessKeyId = '';
// Access Key Secret
$accessKeySecret = '';
// 手机号
$PhoneNumbers = '';
// 短信签名
$SignName = '';
// 短信模板CODE
$TemplateCode = '';
// 模板变量
$TemplateParam = ['code' => mt_rand(100000, 999999)];
// 发送成功时($response === true),否则打印失败或异常日志
$response = Alisms::send($accessKeyId, $accessKeySecret, $PhoneNumbers, $SignName, $TemplateCode, $TemplateParam);
