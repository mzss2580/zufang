<form class="layui-form layui-form-pane edit-form">{{$columns}}
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-filter="editform" lay-submit="">
                保存
            </button>
            <button class="layui-btn layui-btn-primary layui-btn-cancel" type="button">
                取消
            </button>
        </div>
    </div>
</form>
