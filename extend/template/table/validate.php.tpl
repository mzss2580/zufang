<?php
namespace app\common\validate;

use think\Validate;

class {{$tn_hump}} extends Validate
{
    private $tn = '{{$tn}}';

    protected $rule = [{{$rule}}
    ];

    protected $message = [{{$message}}
    ];
    
    protected $scene = [
        'edit' => [{{$scene}}
        ],
    ];
}
