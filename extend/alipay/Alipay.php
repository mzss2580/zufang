<?php
namespace alipay;

use Omnipay\Omnipay;

class Alipay
{
    public static function gateway($payway, $config)
    {
        $gateway = Omnipay::create($payway);
        $gateway->setSignType($config['sign_type']);
        $gateway->setAppId($config['app_id']);
        $gateway->setPrivateKey($config['private_key']);
        $gateway->setAlipayPublicKey($config['public_key']);
        if (isset($config['notify_url'])) {
            $gateway->setNotifyUrl($config['notify_url']);
        }
        if (isset($config['return_url']) && $config['return_url'] && in_array($payway, ['Alipay_AopPage', 'Alipay_AopWap'])) {
            $gateway->setReturnUrl($config['return_url']);
        }
        return $gateway;
    }

    /**
     * 当面付下单
     * @param  array  $config  配置信息
     * @param  string $orderno 订单编号
     * @param  number $total   总价
     * @param  string $subject 支付项目
     * @return string          跳转支付链接
     */
    public static function f2f($config, $orderno, $total, $subject)
    {
        $request = self::gateway('Alipay_AopF2F', $config)->purchase();
        $request->setBizContent([
            'out_trade_no' => $orderno,
            'total_amount' => $total,
            'subject'      => $subject,
        ]);
        try {
            $response = $request->send();
            return $response->getQrCode();
        } catch (\Exception $e) {
            trace('支付宝当面付下单异常:' . $e->getMessage());
            return false;
        }
    }

    /**
     * 当面付验签回调
     * @param  array  $config  配置信息
     * @param  array  $param   验签参数
     * @param  func   $success 支付成功回调,回传$param
     * @param  func   $fail    支付失败回调,回传$param
     */
    public static function f2f_sign($config, $param, $success = null, $fail = null)
    {
        $request = self::gateway('Alipay_AopF2F', $config)->completePurchase()->setParams($param);
        try {
            $response = $request->send();
            if ($response->isPaid()) {
                if (is_callable($success)) {
                    call_user_func($success, $param);
                }
            } else {
                if (is_callable($fail)) {
                    call_user_func($fail, $param);
                }
            }
        } catch (\Exception $e) {
            trace('支付宝当面付验签异常:' . $e->getMessage() . ',参数:' . json_encode($param));
        }
        die('success');
    }

    /**
     * APP支付下单
     * @param  array  $config  配置信息
     * @param  string $orderno 订单编号
     * @param  number $total   总价   
     * @param  string $subject 支付项目
     * @return string          跳转支付链接
     */
    public static function app($config, $orderno, $total, $subject)
    {
        $request = self::gateway('Alipay_AopApp', $config)->purchase();
        $request->setBizContent([
            'out_trade_no' => $orderno,
            'total_amount' => $total,
            'subject'      => $subject,
            'product_code' => 'QUICK_MSECURITY_PAY',
        ]);
        try {
            $response = $request->send();
            return $response->getOrderString();
        } catch (\Exception $e) {
            trace('支付宝APP支付下单异常:' . $e->getMessage());
            return false;
        }
    }

    /**
     * APP支付验签回调
     * @param  array  $config  配置信息
     * @param  array  $param   验签参数
     * @param  func   $success 支付成功回调,回传$param
     * @param  func   $fail    支付失败回调,回传$param
     */
    public static function app_sign($config, $param, $success = null, $fail = null)
    {
        $request = self::gateway('Alipay_AopApp', $config)->completePurchase()->setParams($param);
        try {
            $response = $request->send();
            if ($response->isPaid()) {
                if (is_callable($success)) {
                    call_user_func($success, $param);
                }
            } else {
                if (is_callable($fail)) {
                    call_user_func($fail, $param);
                }
            }
        } catch (\Exception $e) {
            trace('支付宝APP支付验签异常:' . $e->getMessage() . ',参数:' . json_encode($param));
        }
        die('success');
    }

    /**
     * APP支付申请退款
     * @param  array  $config   配置信息
     * @param  string $orderno  订单编号
     * @param  number $money    退款金额
     * @param  string $refundno 退款单号
     * @return boolean          申请退款结果
     */
    public static function app_refund($config, $orderno, $money, $refundno)
    {
        $request  = self::gateway('Alipay_AopApp', $config)->refund();
        $response = $request->setBizContent([
            'out_trade_no'   => $orderno,
            'refund_amount'  => $money,
            'out_request_no' => $refundno,
        ])->send();
        if ($response->getCode() == 10000) {
            return true;
        }
        $data = json_encode($response->getData());
        dump($data);
        trace('支付宝APP支付退款异常:' . $data);
        return false;
    }

    /**
     * 电脑网页支付下单
     * @param  array  $config  配置信息
     * @param  string $orderno 订单编号
     * @param  number $total   总价
     * @param  string $subject 支付项目
     * @return string          跳转支付链接
     */
    public static function page($config, $orderno, $total, $subject)
    {
        $request = self::gateway('Alipay_AopPage', $config)->purchase();
        $request->setBizContent([
            'out_trade_no' => $orderno,
            'total_amount' => $total,
            'subject'      => $subject,
            'product_code' => 'FAST_INSTANT_TRADE_PAY',
        ]);
        try {
            $response = $request->send();
            return $response->getRedirectUrl();
        } catch (\Exception $e) {
            trace('支付宝电脑网页支付下单异常:' . $e->getMessage());
            return false;
        }
    }

    /**
     * 电脑网页支付验签回调
     * @param  array  $config  配置信息
     * @param  array  $param   验签参数
     * @param  func   $success 支付成功回调,回传$param
     * @param  func   $fail    支付失败回调,回传$param
     */
    public static function page_sign($config, $param, $success = null, $fail = null)
    {
        $request = self::gateway('Alipay_AopPage', $config)->completePurchase()->setParams($param);
        try {
            $response = $request->send();
            if ($response->isPaid()) {
                if (is_callable($success)) {
                    call_user_func($success, $param);
                }
            } else {
                if (is_callable($fail)) {
                    call_user_func($fail, $param);
                }
            }
        } catch (\Exception $e) {
            trace('支付宝电脑网页支付验签异常:' . $e->getMessage() . ',参数:' . json_encode($param));
        }
        die('success');
    }

    /**
     * 手机网页支付下单
     * @param  array  $config  配置信息
     * @param  string $orderno 订单编号
     * @param  number $total   总价
     * @param  string $subject 支付项目
     * @return string          跳转支付链接
     */
    public static function wap($config, $orderno, $total, $subject)
    {
        $request = self::gateway('Alipay_AopWap', $config)->purchase();
        $request->setBizContent([
            'out_trade_no' => $orderno,
            'total_amount' => $total,
            'subject'      => $subject,
            'product_code' => 'QUICK_WAP_PAY',
        ]);
        try {
            $response = $request->send();
            return $response->getRedirectUrl();
        } catch (\Exception $e) {
            trace('支付宝手机网页支付下单异常:' . $e->getMessage());
            return false;
        }
    }

    /**
     * 手机网页支付验签回调
     * @param  array  $config  配置信息
     * @param  array  $param   验签参数
     * @param  func   $success 支付成功回调,回传$param
     * @param  func   $fail    支付失败回调,回传$param
     */
    public static function wap_sign($config, $param, $success = null, $fail = null)
    {
        $request = self::gateway('Alipay_AopWap', $config)->completePurchase()->setParams($param);
        try {
            $response = $request->send();
            if ($response->isPaid()) {
                if (is_callable($success)) {
                    call_user_func($success, $param);
                }
            } else {
                if (is_callable($fail)) {
                    call_user_func($fail, $param);
                }
            }
        } catch (\Exception $e) {
            trace('支付宝手机网页支付验签异常:' . $e->getMessage() . ',参数:' . json_encode($param));
        }
        die('success');
    }
}
