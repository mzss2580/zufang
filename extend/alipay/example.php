<?php
// composer "lokielse/omnipay-alipay": "3.0.3"
use alipay\Alipay;

// 配置信息
$config = [
    'sign_type'   => 'RSA2',
    'app_id'      => 'app_id',
    'private_key' => '商户私钥',
    'public_key'  => '支付宝公钥',
];
/**
 * 订单信息
 */
$orderno  = date('YmdHis') . mt_rand(1000, 9999); // 订单编号
$refundno = date('YmdHis') . mt_rand(1000, 9999); // 订单编号
$total    = round(floatval('0.02'), 2); // 支付金额 单位:元
$money    = round(floatval('0.01'), 2); // 实际退款金额,需小于等于支付金额 单位:元
$subject  = '支付宝支付'; // 支付项目

// 当面付,($str === false)下单失败,记录异常日志
$str = Alipay::f2f($config + ['notify_url' => '回调通知地址'], $orderno, $total, $subject);

// 当面付通知验签
Alipay::f2f_sign($config, $this->post, function ($param) {
    // 支付成功
    $param['out_trade_no']; // 订单编号
}, function ($param) {
    // 支付失败
    $param['out_trade_no']; // 订单编号
});

// APP支付,($str === false)下单失败,记录异常日志
$str = Alipay::app($config + ['notify_url' => '回调通知地址'], $orderno, $total, $subject);

// APP支付通知验签
Alipay::app_sign($config, $this->post, function ($param) {
    // 支付成功
    $param['out_trade_no']; // 订单编号
}, function ($param) {
    // 支付失败
    $param['out_trade_no']; // 订单编号
});

// APP支付退款申请,($res === false)申请退款失败,记录异常日志
$res = Alipay::app_refund($config, $orderno, $money, $refundno);

// 电脑网页支付,($url === false)下单失败,记录异常日志
$url = Alipay::page($config + ['notify_url' => '回调通知地址', 'return_url' => '支付成功跳转地址'], $orderno, $total, $subject);

// 电脑网页支付通知验签
Alipay::page_sign($config, $this->post, function ($param) {
    // 支付成功
    $param['out_trade_no']; // 订单编号
}, function ($param) {
    // 支付失败
    $param['out_trade_no']; // 订单编号
});

// 手机网页支付,($url === false)下单失败,记录异常日志
$url = Alipay::wap($config + ['notify_url' => '回调通知地址', 'return_url' => '支付成功跳转地址'], $orderno, $total, $subject);

// 手机网页支付通知验签
Alipay::wap_sign($config, $this->post, function ($param) {
    // 支付成功
    $param['out_trade_no']; // 订单编号
}, function ($param) {
    // 支付失败
    $param['out_trade_no']; // 订单编号
});
