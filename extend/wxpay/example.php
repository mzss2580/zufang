<?php
// composer "overtrue/wechat": "3.3.27"
// composer "aferrandini/phpqrcode": "1.0.1"  生成二维码

use EasyWeChat\Foundation\Application;
use EasyWeChat\Payment\Order;
use PHPQRCode\QRcode;

$config = [
    /**
     * Debug 模式，bool 值：true/false
     * 当值为 false 时，所有的日志都不会记录
     */
    'debug'        => true,
    /**
     * 账号基本信息，请从微信公众平台/开放平台获取
     */
    'app_id'       => 'AppID',
    'secret'       => 'AppSecret',
    'token'        => 'Token',
    'aes_key'      => 'EncodingAESKey，安全模式、兼容模式必填',
    /**
     * 小程序相关
     */
    'mini_program' => [
        'app_id'  => 'AppID',
        'secret'  => 'AppSecret',
        'token'   => 'Token',
        'aes_key' => 'EncodingAESKey，安全模式、兼容模式必填',
    ],
    /**
     * 微信支付
     */
    'payment'      => [
        'merchant_id' => '商户号',
        'key'         => 'API密钥',
        'cert_path'   => ROOT_PATH . 'public' . DS . 'cert' . DS . 'apiclient_cert.pem',
        'key_path'    => ROOT_PATH . 'public' . DS . 'cert' . DS . 'apiclient_key.pem',
    ],
    /**
     * OAuth 配置
     * scopes：公众平台（snsapi_userinfo / snsapi_base），开放平台：snsapi_login
     * callback：OAuth授权完成后的回调页地址
     */
    'oauth'        => [
        'scopes'   => ['snsapi_userinfo'],
        'callback' => '/weixin/oauth/callback',
    ],
    /**
     * 日志配置
     * level: 日志级别, 可选为：debug/info/notice/warning/error/critical/alert/emergency
     * permission：日志文件权限(可选)，默认为null（若为null值,monolog会取0644）
     * file：日志文件位置(绝对路径!!!)，要求可写权限
     */
    'log'          => [
        'level'      => 'debug',
        'permission' => 0777,
        'file'       => ROOT_PATH . 'runtime' . DS . 'easywechat' . DS . 'easywechat_' . date('Ymd') . '.log',
    ],
    /**
     * Guzzle 全局设置
     * 更多请参考： http://docs.guzzlephp.org/en/latest/request-options.html
     */
    'guzzle'       => [
        'timeout' => 3, // 超时时间（秒）
        //'verify' => false, // 关掉 SSL 认证（强烈不建议！！！）
    ],
];
/**
 * 订单信息
 */
$body    = '微信支付'; // 支付项目
$orderno = date('YmdHis'); //订单编号
$money   = '1';
$total   = intval(round(floatval($money) * 100)); // 支付金额,单位:分

/**
 * APP支付,生成APP支付配置内容
 */
$app = new Application($config);
$res = $app->payment->prepare(new Order([
    'trade_type'   => 'APP',
    'body'         => $body,
    'out_trade_no' => $orderno,
    'total_fee'    => $total,
    'notify_url'   => '订单回调地址',
]));
if ($res->return_code === 'SUCCESS') {
    if ($res->result_code === 'SUCCESS') {
        // APP支付配置内容
        $app_pay = $app->payment->configForAppPayment($res->prepay_id);
        die(dump($app_pay));
    } else {
        trace('微信下单失败:' . $res->err_code . ' ' . $res->err_code_des);
    }
} else {
    trace('微信下单配置有误:' . $res->return_msg);
}

/**
 * 当面付,根据支付url生成对应二维码
 */
$app = new Application($config);
$res = $app->payment->prepare(new Order([
    'trade_type'   => 'NATIVE',
    'body'         => $body,
    'out_trade_no' => $orderno,
    'total_fee'    => $total,
    'notify_url'   => '订单回调地址',
]));
if ($res->return_code === 'SUCCESS') {
    if ($res->result_code === 'SUCCESS') {
        @mkdir(ROOT_PATH . 'public' . DS . 'qrcode');
        $qrcode = md5(time() . mt_rand(100000, 999999)) . '.png';
        QRcode::png($res->code_url, ROOT_PATH . 'public' . DS . 'qrcode' . DS . $qrcode);
        $qrcode = $this->host . '/qrcode/' . $qrcode; // 生成的二维码,用来扫码支付
        die(dump($qrcode));
    } else {
        trace('微信下单失败:' . $res->err_code . ' ' . $res->err_code_des);
    }
} else {
    trace('微信下单配置有误:' . $res->return_msg);
}

/**
 * H5支付,生成支付url并拼接redirect_url(支付成功跳转页面)
 */
$app = new Application($config);
$res = $app->payment->prepare(new Order([
    'trade_type'   => 'MWEB',
    'body'         => $body,
    'out_trade_no' => $orderno,
    'total_fee'    => $total,
    'notify_url'   => '订单回调地址',
]));
if ($res->return_code === 'SUCCESS') {
    if ($res->result_code === 'SUCCESS') {
        $success = $this->host . '/weixin/pay/success'; // 支付成功跳转页面
        $pay_url = $res->mweb_url . '&redirect_url=' . urlencode($success); // 支付url,直接跳转
        die(dump($pay_url));
    } else {
        trace('微信下单失败:' . $res->err_code . ' ' . $res->err_code_des);
    }
} else {
    trace('微信下单配置有误:' . $res->return_msg);
}

/**
 * 公众号/小程序支付
 */
$app = new Application($config);
$res = $app->payment->prepare(new Order([
    'trade_type'   => 'JSAPI',
    'body'         => $body,
    'out_trade_no' => $orderno,
    'total_fee'    => $total,
    'openid'       => 'openid',
    'notify_url'   => '订单回调地址',
]));
if ($res->return_code === 'SUCCESS') {
    if ($res->result_code === 'SUCCESS') {
        // 调起支付配置 example.js data
        $data = $app->payment->configForJSSDKPayment($res->prepay_id);
        die(dump($data));
        // 小程序套壳网页调起支付 example.js param
        $param = '?timestamp=' . $data['timestamp'] . '&nonceStr=' . $data['nonceStr'] . '&package=' . str_replace('prepay_id=', '', $data['package']) . '&signType=' . $data['signType'] . '&paySign=' . $data['paySign'];
        die(dump($param));
    } else {
        trace('微信下单失败:' . $res->err_code . ' ' . $res->err_code_des);
    }
} else {
    trace('微信下单配置有误:' . $res->return_msg);
}

/**
 * 支付回调
 */
$app = new Application($config);
$res = $app->payment->handleNotify(function ($notify, $successful) {
    $notify['out_trade_no']; // 订单编号
    if ($successful) {
        // 支付成功
    } else {
        // 支付失败
    }
    return true;
});
$res->send();
