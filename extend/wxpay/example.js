/**
 * 公众号支付调起支付
 */
if (typeof WeixinJSBridge === 'undefined') {
    if (document.addEventListener) {
        document.addEventListener('WeixinJSBridgeReady', function() {
            onBridgeReady(data);
        }, false);
    } else if (document.attachEvent) {
        document.attachEvent('WeixinJSBridgeReady', function() {
            onBridgeReady(data);
        });
        document.attachEvent('onWeixinJSBridgeReady', function() {
            onBridgeReady(data);
        });
    } else {
        layer_msg('无法调起微信支付,请在微信中打开');
    }
} else {
    onBridgeReady(data);
}

function onBridgeReady(data) {
    WeixinJSBridge.invoke('getBrandWCPayRequest', {
        'appId': data.appId,
        'timeStamp': data.timestamp,
        'nonceStr': data.nonceStr,
        'package': data.package,
        'signType': data.signType,
        'paySign': data.paySign
    }, function(res) {
        if (res.err_msg == 'get_brand_wcpay_request:ok') {
            // 支付成功
        } else if (res.err_msg == 'get_brand_wcpay_request:cancel') {
            // 支付取消
        } else if (res.err_msg == 'get_brand_wcpay_request:fail') {
            // 支付失败
        }
    });
}
/**
 * 小程序调起支付
 */
wx.requestPayment({
    'timeStamp': data.timestamp,
    'nonceStr': data.nonceStr,
    'package': data.package,
    'signType': data.signType,
    'paySign': data.paySign,
    success: function(res) {
        // 支付成功
    },
    fail: function(res) {
        // 支付失败
    },
});
/**
 * 小程序套壳网页调起支付
 */
// 网页部分 小程序路径 /wxpay/wxpay
window.wx.miniProgram.navigateTo({
    url: '/wxpay/wxpay' + param
});
// 小程序部分 /wxpay/wxpay.js
Page({
    onLoad: function(param) {
        wx.requestPayment({
            timeStamp: param.timestamp,
            nonceStr: param.nonceStr,
            package: 'prepay_id=' + param.package,
            signType: param.signType,
            paySign: param.paySign,
            success: function(res) {
                // 支付成功
            },
            fail: function(res) {
                // 支付失败
            },
        });
    }
});
