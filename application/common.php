<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件

if (!function_exists('die_dump')) {
    /**
     * 遍历输出所有参数并die
     */
    function die_dump()
    {
        $vars = func_get_args();
        foreach ($vars as $var) {
            dump($var);
        }
        die();
    }
}

if (!function_exists('error_page')) {
    /**
     * 错误页面提示信息并关闭自身或者返回上一页
     * @param  string  $msg  提示信息
     * @param  boolean $back true:返回,false:关闭自身
     */
    function error_page($msg = '', $back = false)
    {
        $str = '<script src="/static/plugin/layui/layui.all.js">
</script>
<script src="/static/js/tool.js">
</script>
<script>
    ';
        if ($msg !== '') {
            $str .= 'layer.msg(\'' . $msg . '\', {
        icon: 0
    });
    setTimeout(function() {
        ';
        }
        if ($back === false) {
            $str .= 'layer_iframe_close();';
        } else {
            $str .= 'history.back();';
        }
        if ($msg !== '') {
            $str .= '
    }, 666);';
        }
        $str .= '
</script>';
        return $str;
    }
}

if (!function_exists('get_orderno')) {
    /**
     * 指定表生成订单编号
     * @param  string  $tn        表
     * @param  string  $prefix    前缀
     * @param  string  $field     字段
     * @param  string  $format    日期格式
     * @param  integer $length    补足长度
     * @param  array   $where     查询条件
     * @return string             订单编号
     */
    function get_orderno($tn, $prefix = '', $field = 'orderno', $format = 'Ymd', $length = 6, $where = [])
    {
        $orderno = $prefix . date($format);
        $db      = db($tn)->field([$field])->where($field, 'like', $orderno . '%');
        if (!empty($where)) {
            $db->where($where);
        }
        $result = $db->order('id', 'desc')->find();
        if (is_null($result)) {
            $num = 1;
        } else {
            $num = intval(str_replace($orderno, '', $result[$field])) + 1;
        }
        $orderno .= str_pad($num, $length, '0', STR_PAD_LEFT);
        return $orderno;
    }
}

if (!function_exists('get_status')) {
    /**
     * 获取系统状态
     * @param  string       $type   系统状态类型
     * @param  string       $status 默认'',非空获取具体状态
     * @return string|array         系统状态键值数组或者具体状态
     */
    function get_status($type, $status = '')
    {
        if ($status === '') {
            return db('system_status')->where('type', $type)->order('status', 'asc')->column('value', 'status');
        }
        return get_field('system_status', ['type' => $type, 'status' => $status], 'value');
    }
}

if (!function_exists('get_config')) {
    /**
     * 获取系统设置
     * @param  string $config 系统设置类型
     * @return string         系统设置内容
     */
    function get_config($config)
    {
        return get_field('system_config', ['config' => $config], 'value');
    }
}

if (!function_exists('get_field')) {
    /**
     * 获取指定表指定行指定字段
     * @param  string       $tn      完整表名
     * @param  string|array $where   参数数组或者id值
     * @param  string       $field   字段名,默认'name'
     * @param  string       $default 默认值,默认''
     * @param  array        $order   排序数组,默认id倒序
     * @return string                获取到的内容
     */
    function get_field($tn, $where, $field = 'name', $default = '', $order = ['id' => 'desc'])
    {
        if (!is_array($where)) {
            $where = ['id' => $where];
        }
        return db($tn)->where($where)->order($order)->value($field, $default);
    }
}

if (!function_exists('delete_dir')) {
    /**
     * 遍历删除文件夹所有内容
     * @param  string $dir 要删除的文件夹
     */
    function delete_dir($dir)
    {
        $dh = opendir($dir);
        while ($file = readdir($dh)) {
            if ($file != '.' && $file != '..') {
                $filepath = $dir . DIRECTORY_SEPARATOR . $file;
                if (is_dir($filepath)) {
                    delete_dir($filepath);
                } else {
                    @unlink($filepath);
                }
            }
        }
        closedir($dh);
        @rmdir($dir);
    }
}

if (!function_exists('deep_in_array')) {
    /**
     * 多维数组查询值是否存在
     * @param  string         $value  需要查询的值
     * @param  array          $array  需要查询的数组
     * @return string|boolean         查询到返回包含值的数组,查询不到返回false
     */
    function deep_in_array($value, $array)
    {
        foreach ($array as $v) {
            if (!is_array($v)) {
                if ($v == $value) {
                    return $v;
                }
                continue;
            }
            if (in_array($value, $v)) {
                return $v;
            } else {
                deep_in_array($value, $v);
            }

        }
        return false;
    }
}

if (!function_exists('simple_encrypt')) {
    /**
     * 简单可逆加密
     * @param  string $txtStream 需要加密的数据
     * @param  string $password  键值
     * @return string       加密结果
     */
    function simple_encrypt($txtStream, $password = '')
    {
        if ($password === '') {
            $password = config('encrypt_salt'); //BUzheng_123
        }
        //密锁串，不能出现重复字符，内有A-Z,a-z,0-9,=,+,_,
        $lockstream = '1234567890zxcvbnmZXCVBNM=asdfghjklASDFGHJKL+qwertyuiopQWERTYUIOP_';
        //密锁串长度
        $lockLen = strlen($lockstream);
        //随机找一个数字，并从密锁串中找到一个密锁值
        $lockCount = rand(0, $lockLen - 1);
        //截取随机密锁值
        $randomLock = $lockstream[$lockCount];
        //结合随机密锁值生成MD5后的密码
        $password = md5($password . $randomLock);
        //开始对字符串加密
        $txtStream = base64_encode($txtStream);
        $tmpStream = '';
        $i         = 0;
        $j         = 0;
        $k         = 0;
        for ($i = 0; $i < strlen($txtStream); $i++) {
            $k = ($k == strlen($password)) ? 0 : $k;
            $j = (strpos($lockstream, $txtStream[$i]) + $lockCount + ord($password[$k])) % ($lockLen);
            $tmpStream .= $lockstream[$j];
            $k++;
        }
        return $tmpStream . $randomLock;

    }
}

if (!function_exists('simple_decrypt')) {
    /**
     * 简单可逆解密
     * @param  string $txtStream 需要解密的数据
     * @param  string $password  键值
     * @return string       解密结果
     */
    function simple_decrypt($txtStream, $password = '')
    {
        if ($password === '') {
            $password = config('encrypt_salt');
        }
        //密锁串，不能出现重复字符，内有A-Z,a-z,0-9,=,+,_,
        $lockstream = '1234567890zxcvbnmZXCVBNM=asdfghjklASDFGHJKL+qwertyuiopQWERTYUIOP_';
        //密锁串长度
        $lockLen = strlen($lockstream);
        //获得字符串长度
        $txtLen = strlen($txtStream);
        //截取随机密锁值
        $randomLock = $txtStream[$txtLen - 1];
        //获得随机密码值的位置
        $lockCount = strpos($lockstream, $randomLock);
        //结合随机密锁值生成MD5后的密码
        $password = md5($password . $randomLock);
        //开始对字符串解密
        $txtStream = substr($txtStream, 0, $txtLen - 1);
        $tmpStream = '';
        $i         = 0;
        $j         = 0;
        $k         = 0;
        for ($i = 0; $i < strlen($txtStream); $i++) {
            $k = ($k == strlen($password)) ? 0 : $k;
            $j = strpos($lockstream, $txtStream[$i]) - $lockCount - ord($password[$k]);
            while ($j < 0) {
                $j = $j + ($lockLen);
            }
            $tmpStream .= $lockstream[$j];
            $k++;
        }
        return base64_decode($tmpStream);
    }
}

if (!function_exists('returnMsg')) {

    /**
     * 返回信息
     * @param int $status  状态值
     * @param string $msg 信息提示
     * @return array  $data 返回的数组
     *  @return string  $url 返回链接
     */
    function returnMsg($status, $msg='', $data='',$url='')
    {
        $datas = array();
        $datas['status'] = $status;
        $datas['msg']  = $msg;
        $datas['data'] = $data;
        $datas['url']  = $url;
        return $datas;
    }
}

    function bc_mul($arr = [], $accuracy = 2){
        if(count($arr) <= 0){
            return 0;
        }
        $res = 1;
        foreach($arr as $k=>$v){
            $res = bcmul($res, $v, $accuracy);
        }
        return $res;
    }
