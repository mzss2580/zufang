<?php
namespace app\user\controller;

class Config extends Base
{
    private $tn = 'system_config';

    public function _empty($action)
    {
        if ($this->post) {
            foreach ($this->post as $config => $value) {
                db($this->tn)->where('config', $config)->setField('value', $value);
            }
            $this->returnAPI('保存成功', 0);
        }
        return $this->fetch($action, ['configs' => db($this->tn)->column('value', 'config')]);
    }
}
