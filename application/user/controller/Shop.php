<?php
namespace app\user\controller;

use app\common\model\WechatTx;
use think\Db;

class Shop extends Base
{
    private $tn = 'shop';

    public function index()
    {
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $where = [];
            if(request()->get('seach_name') != ''){
                if(request()->get('seach_type') == 'name'){
                    $where['name'] = request()->get('seach_name');
                }else if(request()->get('seach_type') == 'tel'){
                    $where['tel'] = request()->get('seach_name');
                }
            }
            if(isset($this->get['is_tj'])){
                $where['is_tj'] = $this->get['is_tj'];
            }
            if(isset($this->get['status'])){
                $where['status'] = $this->get['status'];
            }
            $db    = db($this->tn);
            $count = $db->where($where)->count();
            $list  = $db->field(true)->where($where)->page($this->get['page'])->limit($this->get['limit'])->order('sort', 'asc')->select();
            foreach ($list as $k => $v) {
                $list[$k]['logo']        = '<img class="upload-image show-image" src="' . $v['logo'] . '"/>';
                $list[$k]['yyzz']        = '<img class="upload-image show-image" src="' . $v['yyzz'] . '"/>';
                $list[$k]['id_card_rx']        = '<img class="upload-image show-image" src="' . $v['id_card_rx'] . '"/>';
                $list[$k]['id_card_gh']        = '<img class="upload-image show-image" src="' . $v['id_card_gh'] . '"/>';
                $list[$k]['jyxkz']       = '<img class="upload-image show-image" src="' . $v['jyxkz'] . '"/>';
                $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
                $list[$k]['user_id']     = get_field('user', ['id' => $v['user_id']], 'nick');
            }
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit']]);
        }
        return $this->fetch();
    }

    public function edit($id = 0)
    {
        if ($this->post) {
            // dump($this->post);exit;
            if ($id === 0) {
                $model = model($this->tn);
            } else {
                $model = model($this->tn)->get($id);
                if (is_null($model)) {
                    $this->returnAPI('信息有误，请稍后重试');
                }
            }
            //推荐商家只能有三家，判断是否超过限制
            if(input('post.is_tj') == 1){
                $this->check_shop_tj($id,$model);
            }
            $validate = $this->validate(['id' => $id] + $this->post, 'user.login');
            if ($validate === true) {
                $user = model('user')->get(['phone' => $this->post['phone']]);
                if (!is_null($user)) {
                    $this->returnAPI('手机号已注册');
                }
                $validate = $this->validate(['id' => $id] + $this->post, 'shop.add');
                if ($validate === true) {
                    // echo $this->randnick();
                    // print_r($_POST);exit;
                    $userdata = [
                        'nick'  => mt_rand(100000, 999999),
                        'phone' => input('post.phone'),
                        'pwd'   => md5(input('post.pwd')),
                    ];
                    $usermodel = model('user');
                    $res       = $usermodel->allowField(true)->save($userdata);
                    if ($res === false) {
                        $this->returnAPI($usermodel->getError());
                    }
                    $shopdata = [
                        'user_id'     => $usermodel->id,
                        'name'        => input('post.name'),
                        'logo'        => input('post.logo'),
                        'tel'         => input('post.tel'),
                        'yyzz'        => input('post.yyzz'),
                        'jyxkz'       => input('post.jyxkz'),
                        'address'     => input('post.address'),
                        'lng'         => round(input('post.lng'), 6),
                        'lat'         => round(input('post.lat'), 6),
                        'status'      => '1',
                        'ziti'        => input('post.ziti', 0),
                        'fuwu'        => input('post.fuwu', 0),
                        'express_fee' => input('post.express_fee'),
                        'sort'        => input('post.sort'),
                        'is_tj'       => input('post.is_tj'),
                        'commission_rate' => input('post.commission_rate'),
                    ];
                    $shop_name = $model->get(['name' => $shopdata['name']]);
                    if (!is_null($shop_name)) {
                        $this->returnAPI('该店铺名称已存在', 1);
                    }
                    $result = $model->allowField(true)->save($shopdata);
                    if ($result === false) {
                        $this->returnAPI($model->getError());
                    }
                    $this->returnAPI('操作成功', 0);
                }
            }
            $this->returnAPI($validate);
        }
        $data = db('shop')->alias('s')->join('user u', 'u.id = s.user_id')->field('u.phone,u.pwd,u.head,u.nick,s.*')
            ->where(['s.id' => $id])->find();
        if (is_null($data)) {
            $user = $this->table_fields('user');
            $shop = $this->table_fields($this->tn);
            $data = array_merge($user, $shop);
        }
        return $this->fetch('', ['data' => $data]);
    }

    public function edits($id = 0)
    {
        if ($this->post) {
            // dump($this->post);exit;
            if ($id === 0) {
                $model = model($this->tn);
            } else {
                $model = model($this->tn)->get($id);
                if (is_null($model)) {
                    $this->returnAPI('信息有误，请稍后重试');
                }
            }
            //推荐商家只能有三家，判断是否超过限制
            if(input('post.is_tj') == 1){
                $this->check_shop_tj($id,$model);
            }
            $rule = [
                'phone'    => 'require|mobile',
                'sort'    => 'number',
            ];
            $message = [
                'phone.require' => '请输入手机号',
                'phone.mobile'  => '请输入正确的手机号',
                'sort.number'    => '请输入数字',
            ];
            $validate = $this->validate(['id' => $id] + $this->post, $rule,$message);
            if ($validate === true) {
                $user = model('user')->field('s.id')->alias('u')->join('shop s','s.user_id=u.id','left')->where(['u.phone' => $this->post['phone']])->find();
                // dump($user);exit;
                if (!is_null($user) && $id != $user['id']) {
                    $this->returnAPI('手机号已注册');
                }
                $validate = $this->validate(['id' => $id] + $this->post, 'shop.add');
                if ($validate === true) {
                    // echo $this->randnick();
                    // print_r($_POST);exit;
                    $userdata = [
                        'nick'  => mt_rand(100000, 999999),
                        'phone' => input('post.phone'),
                    ];
                    if(input('post.pwd')){
                        $userdata['pwd'] = md5(input('post.pwd'));
                    }
                    $user_id = $model->where('id = '.$id)->value('user_id');
                    $usermodel = model('user');
                    $res = $usermodel->allowField(true)->save($userdata,['id'=>$user_id]);
                    if ($res === false) {
                        $this->returnAPI($usermodel->getError());
                    }
                    $shopdata = [
                        'name'        => input('post.name'),
                        'logo'        => input('post.logo'),
                        'tel'         => input('post.tel'),
                        'yyzz'        => input('post.yyzz'),
                        'jyxkz'       => input('post.jyxkz'),
                        'address'     => input('post.address'),
                        'lng'         => round(input('post.lng'), 6),
                        'lat'         => round(input('post.lat'), 6),
                        'status'      => '1',
                        'ziti'        => input('post.ziti', 0),
                        'fuwu'        => input('post.fuwu', 0),
                        'express_fee' => input('post.express_fee'),
                        'sort'        => input('post.sort'),
                        'is_tj'       => input('post.is_tj'),
                        'commission_rate' => input('post.commission_rate'),
                        'category_id' => input('post.category_id'),
                    ];
                    $shop_name = $model->field('id')->get(['name' => $shopdata['name']]);
                    if (!is_null($shop_name) && $id != $shop_name['id']) {
                        $this->returnAPI('该店铺名称已存在', 1);
                    }
                    $result = $model->allowField(true)->save($shopdata);
                    if ($result === false) {
                        $this->returnAPI($model->getError());
                    }
                    $this->returnAPI('操作成功', 0);
                }
            }
            $this->returnAPI($validate);
        }
        $data = db('shop')->alias('s')->join('user u', 'u.id = s.user_id')->field('u.phone,u.pwd,u.head,u.nick,s.*')->where(['s.id' => $id])->find();
        if (is_null($data)) {
            $user = $this->table_fields('user');
            $shop = $this->table_fields($this->tn);
            $data = array_merge($user, $shop);
        }
        $shop_category = db('shop_category')->column('name', 'id');
        return $this->fetch('', ['data' => $data,'shop_category'=>$shop_category]);
    }
    public function editSort()
    {
        if(!$this->post['id'] || !is_numeric($this->post['sort'])){ return $this->returnAPI('更新失败',0);}
        $id = $this->post['id'];
        $sort = $this->post['sort'];
        $res = model('shop')->allowField('sort')->save(['sort'=>$sort],['id'=>$id]);
        if(!$res){return $this->returnAPI('更新失败',0);}else{return $this->returnAPI('更新成功',1);
        }
    }

    public function myshop()
    {
        if ($this->post) {
            $this->post['shop_create_time'] = strtotime($this->post['create_time']);
            $this->post['shop_ziti']        = input('post.shop_ziti', 0);
            $this->post['shop_fuwu']        = input('post.shop_fuwu', 0);
            // die_dump(input('post.shop_fuwu', 0));
            if ($this->post['shop_express_fee'] < 0) {
                $this->returnAPI('基础运费只能为数字');
            }
            foreach ($this->post as $config => $value) {
                db('system_config')->where('config', $config)->setField('value', $value);
            }
            $this->returnAPI('设置成功', 0);
        }
        $shop                     = db('system_config')->column('value', 'config');
        $shop['shop_create_time'] = date('Y-m-d H:i:s', $shop['shop_create_time']);
        return $this->fetch('', ['data' => $shop]);
    }

    public function check_shop_tj($id,$model){
        return 1;
        $db    = db($this->tn);
        $tj_count = $db->where(['is_tj'=>1])->count();
        if($tj_count >=3){
            //判断当前商家是否在推荐商家里
            if ($id === 0) {
                $this->returnAPI('推荐商家只能为3家哦，请取消其他推荐商家后再来设置吧');
            }else{
                if($model->is_tj == 0){
                    $this->returnAPI('推荐商家只能为3家哦，请取消其他推荐商家后再来设置吧');
                }
            }
        }
    }

    /*钱包记录*/
    public function wallet_log()
    {
        $type = input('type');
        $status = input('status');
        $name   = input('name');
        $tel    = input('tel');
        $start_time   = trim(input('start_time'));
        $end_time     = trim(input('end_time'));
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $db    = db('shop_wallet');
            $list  = $db
                ->alias('sw')
                ->join("shop s","s.id = sw.shop_id");
            if($type === '0' || $type === 0 || $type > 0){
                $list = $list
                    ->where('sw.type',$type);
            }
            if($status === '0' || $status === 0 || $status == 1){
                $list = $list
                    ->where('sw.status',$status);
            }
            if(!empty($name)){
                $list = $list
                    ->where('s.name',$name);
            }
            if(!empty($tel)){
                $list = $list
                    ->where('s.tel',$tel);
            }
            if ($start_time) {
                if ($end_time) {
                    $db->where('sw.create_time', 'between', [strtotime($start_time . ' 00:00:00'), strtotime($end_time . ' 23:59:59')]);
                } else {
                    $db->where('sw.create_time', '>=', strtotime($start_time . ' 00:00:00'));
                    $db_two->where('sw.create_time', '>=', strtotime($start_time . ' 00:00:00'));
                }
            } else if ($end_time) {
                $db->where('sw.create_time', '<=', strtotime($end_time . ' 23:59:59'));
            }
            $count = $db->count();
            $condition_money = $db->sum('sw.money');
            $list = $list
                ->page($this->get['page'])
                ->limit($this->get['limit'])
                ->field('sw.*,s.name as s_name,s.tel as s_tel')
                ->order('sw.id desc')
                ->select();
            foreach ($list as &$v) {

                //类型
                if($v['type'] == 1){
                    $v['type_name'] = '退款减额';
                }else if($v['type'] == 2){
                    $v['type_name'] = '下单加额';
                }else if($v['type'] == 3){
                    $v['type_name'] = '外卖费用减少';
                }

                //状态类型
                if($v['status'] == 1){
                    $v['status_name'] = '未结算';
                }else{
                    $v['status_name'] = '已结算';
                }

                //冻结状态类型
                if($v['freeze'] == 1){
                    $v['freeze_name'] = '已结算';
                }else{
                    $v['freeze_name'] = '未结算';
                }

                //查询订单号
                $v['order_sn'] =  '';
                if($v['order_id'] > 0){
                    $find_order = db('order')
                        ->where('id',$v['order_id'])
                        ->field('orderno')
                        ->find();
                    if($find_order){
                        $v['order_sn'] = $find_order['orderno'];
                    }
                }

                //查询退款订单号
                $v['order_refund_sn'] =  '';
                if($v['order_refund_id'] > 0){
                    $find_order_refund = db('order_refund')
                        ->where('id',$v['order_refund_id'])
                        ->field('orderno')
                        ->find();
                    if($find_order_refund){
                        $v['order_refund_sn'] = $find_order_refund['orderno'];
                    }
                }
                $v['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
            }
            $this->returnAPI('', 0, $list, ['count' => $count,'condition_money'=> $condition_money , 'limit' => $this->get['limit']]);
        }
        $this->assign('type',$type);
        return $this->fetch();
    }
    /*提现记录*/
    public function withdraw_log()
    {
        $status = input('status');
        $all_status = ['-1','0','1'];
        if(!in_array($status,$all_status)){
            $status = 'no';
        }
        $name   = input('name');
        $tel    = input('tel');
        $start_time   = trim(input('start_time'));
        $end_time     = trim(input('end_time'));
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $db    = db('shop_withdraw');
            $list  = $db
                ->alias('sw')
                ->join("shop s","s.id = sw.shop_id")
                ->where('s.is_del',0);
            if($status != 'no'){
                $list = $list
                    ->where('sw.status',$status);
            }
            if(!empty($name)){
                $list = $list
                    ->where('s.name',$name);
            }
            if(!empty($tel)){
                $list = $list
                    ->where('s.tel',$tel);
            }
            if ($start_time) {
                if ($end_time) {
                    $db->where('sw.create_time', 'between', [strtotime($start_time . ' 00:00:00'), strtotime($end_time . ' 23:59:59')]);
                } else {
                    $db->where('sw.create_time', '>=', strtotime($start_time . ' 00:00:00'));
                    $db_two->where('sw.create_time', '>=', strtotime($start_time . ' 00:00:00'));
                }
            } else if ($end_time) {
                $db->where('sw.create_time', '<=', strtotime($end_time . ' 23:59:59'));
            }
            $count = $db->count();
            $condition_money = $db->sum('sw.money');
            $list = $list
                ->page($this->get['page'])
                ->limit($this->get['limit'])
                ->field('sw.*,s.name as s_name,s.tel as s_tel')
                ->order('sw.id desc')
                ->select();
            foreach ($list as &$v) {
                //订单状态类型
                /*if($v['type'] == 1){
                    $v['type_name'] = '银行卡提现';
                }else{
                    $v['type_name'] = '支付宝提现';
                }
                //查询提现银行信息
                $find_bank = db('bank')
                    ->where('id',$v['bank_id'])
                    ->field('name as bank_name')
                    ->find();
                if($find_bank){
                    $v['bank_name'] = $find_bank['bank_name'];
                }else{
                    $v['bank_name'] = '';
                }*/
                //订单状态类型
                if($v['status'] == -1){
                    $v['status_name'] = '驳回';
                }else if($v['status'] == 1){
                    $v['status_name'] = '通过';
                }else{
                    $v['status_name'] = '等待审核';
                }

                $v['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
            }
            $this->returnAPI('', 0, $list, ['count' => $count,'condition_money'=> $condition_money, 'limit' => $this->get['limit']]);
        }
        $this->assign('status',$status);
        return $this->fetch();
    }
    //通过
    public function withdraw_ispass()
    {
        $id = input('id');
        if($id){
            //查询订单状态
            $find_log = db('shop_withdraw')
                ->where('id',$id)
                ->find();
            if(!$find_log){
                $this->returnAPI('申请不存在');
            }
            if($find_log['status'] != 0){
                $this->returnAPI('申请已审核过');
            }
            //查询商家信息
            $find_shop = Db::name('shop')
                ->alias('s')
                ->join('user u','u.id = s.user_id')
                ->where('s.id',$find_log['shop_id'])
                ->where('s.is_del',0)
                //->field('u.push_regid,s.status')
                ->find();
            if(!$find_shop){
                $this->returnAPI('通过失败,此商户不存在');
            }else{
                if($find_shop['status'] != 1){
                    $this->returnAPI('通过失败,此商户未审核通过');
                }else{
                    $up = db('shop_withdraw')
                        ->where('id',$id)
                        ->update(array('status'=>1,'status_time'=>time()));
                    if($up){
                        if(0 && !empty($find_shop['push_regid'])){
                            //推送商户消息
                            $this->push_user($find_shop['push_regid'],'提现通知：您好，你的提现申请已通过');
                        }
                        $this->returnAPI('通过成功',0);
                    }else{
                        $this->returnAPI('通过失败');
                    }
                    die;
                    /*用户条件不满足，直接提现到银行卡暂不能用，后期能用再放开
                    if($find_log['type'] == 1){
                        //查询银行卡信息
                        $find_bank = Db::name('bank')
                                ->where('id',$find_log['bank_id'])
                                ->field('name as bank_name,bank_code')
                                ->find();
                        if($find_bank){
                            $res = $this->payBank($find_log['money'],$find_log['order_sn'],$find_log['bank_card'],$find_log['name'],$find_bank['bank_code']);
                            if($res['status'] == 1){
                                $up = Db::name('rider_withdraw_log')
                                    ->where('id',$id)
                                    ->update(array('status'=>1,'payment_no'=>$res['data'],'update_time'=>time()));
                                if($up){
                                    if(!empty($find_rider['push_regid'])){
                                        //推送骑手消息
                                        $this->push_rider($find_rider['push_regid'],'提现通知：您好，你的提现申请已通过',$find_rider['is_voice']);
                                    }
                                    $this->returnAPI('通过成功',0);
                                }else{
                                    $this->returnAPI('通过失败');
                                }
                            }else{
                                $this->returnAPI($res['msg']);
                            }

                        }else{
                            $this->returnAPI('通过失败,无对应银行卡信息');
                        }
                    }else{
                        $up = Db::name('rider_withdraw_log')
                            ->where('id',$id)
                            ->update(array('status'=>1,'payment_no'=>$res['data'],'update_time'=>time()));
                        if($up){
                            if(!empty($find_rider['push_regid'])){
                                //推送骑手消息
                                $this->push_rider($find_rider['push_regid'],'提现通知：您好，你的提现申请已通过',$find_rider['is_voice']);
                            }
                            $this->returnAPI('通过成功',0);
                        }else{
                            $this->returnAPI('通过失败');
                        }
                    }*/
                }

            }
        }else{
            $this->returnAPI('通过失败');
        }
    }
    //企业付款到银行卡$money 100;$enc_bank_no '62***44';$enc_true_name '张**';$bank_code '银行编号';
    private function payBank($money,$order_sn,$enc_bank_no,$enc_true_name,$bank_code)
    {
        $appid      = 'wxf54941aae11824f6';
        $appsecret  = '18db677e764da6da7c5350e686c9e952';
        $mchid      = '1551628811';
        $key        = 'ccpt123456ccpt123456ccpt123456cc';
        $sslcert    = env('ROOT_PATH') . 'public' . DIRECTORY_SEPARATOR . 'cert' . DIRECTORY_SEPARATOR . '20191115_cert.pem';
        $sslkey     = env('ROOT_PATH') . 'public' . DIRECTORY_SEPARATOR . 'cert' . DIRECTORY_SEPARATOR . '20191115_key.pem';
        $out_trade_no =  $order_sn;
        $desc       = '企业付款到银行卡测试';
        $wxapi      = new WechatTx($appid,$appsecret,$mchid,$key,$sslcert,$sslkey);
        $payment_no = $wxapi->payForBank($out_trade_no,$money,$enc_bank_no,$enc_true_name,$bank_code,$desc);
        return $payment_no;
        if($payment_no){
            echo '微信付款到银行卡操作成功，微信订单号：'.$payment_no;
        } else {
            echo '微信付款到银行卡操作失败，请检查日记';
        }
    }
    //驳回
    public function withdraw_nopass()
    {
        $id = input('id');
        if($id){
            //查询订单状态
            $find_log = db('shop_withdraw')
                ->where('id',$id)
                ->find();
            if(!$find_log){
                $this->returnAPI('申请不存在');
            }
            if($find_log['status'] != 0){
                $this->returnAPI('已审核过');
            }
            $reason = input('reason');
            //查询商户信息
            $find_shop = Db::name('shop')
                ->alias('s')
                ->join('user u','u.id = s.user_id')
                ->where('s.id',$find_log['shop_id'])
                ->where('s.is_del',0)
                //->field('u.push_regid,s.status')
                ->find();
            if(!$find_shop){
                $this->returnAPI('驳回失败,此商户不存在');
            }else{
                if($find_shop['status'] != 1){
                    $this->returnAPI('通过失败,此商户未审核通过');
                }else{
                    $up = db('shop_withdraw')
                        ->where('id',$id)
                        ->update(array('status'=>-1,'reason'=>$reason,'status_time'=>time()));
                    if($up){

                        //查询用户余额
                        $find_money = db('shop')
                            ->where('id',$find_log['shop_id'])
                            ->where('is_del',0)
                            ->field('withdraw_money')
                            ->find();
                        //退回金额
                        $inc = db('shop')
                            ->where('id',$find_log['shop_id'])
                            //->where('is_del',0)
                            ->setInc('withdraw_money',$find_log['money']);
                        if($inc){
                            /*if(!empty($find_shop['push_regid'])){
                                //推送骑手消息
                                $this->push_user($find_shop['push_regid'],'提现通知：您好，你的提现申请已驳回');
                            }*/
                            $this->returnAPI('驳回成功',0);
                        }else{
                            $up = db('shop_withdraw')
                                ->where('id',$id)
                                ->update(array('status'=>0,'reason'=>$reason,'status_time'=>time()));
                            $this->returnAPI('驳回失败');
                        }

                    }else{
                        $this->returnAPI('驳回失败');
                    }
                }

            }

        }else{
            $this->returnAPI('驳回失败');
        }
    }
}
