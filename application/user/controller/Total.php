<?php
namespace app\user\controller;

use think\Db;

class Total extends Base
{
    private $tn = 'total';

    public function index()
    {
//        dump($today_user_num);
//        die();
        if (isset($this->get['page']) && isset($this->get['limit'])) {

            $list = [];
            $user = db('user')->where('is_delete', 0)->count();
            $list[] = ['title'=>'总用户', 'num'=>$user];
            //总订单数
            $all_order = db('order')->where('status', 'in', '1,2,3,4')->count();
            $list[] = ['title'=>'总订单数', 'num'=>$all_order];

            $all_money = db('order')->where('status', 'in', '1,2,3,4')->sum('pay_true');
            $list[] = ['title'=>'总营业额', 'num'=>$all_money];

            $today = strtotime(date("Y-m-d",time()));
            $this_month=strtotime(date('Y-m-1',time()));
            $today_money = db('order')->where('status', 'in', '1,2,3,4')->where('create_time', '>', $today)->sum('pay_true');
            $list[] = ['title'=>'今日营业额', 'num'=>$today_money];
            $moth_money = db('order')->where('status', 'in', '1,2,3,4')->where('create_time', '>', $this_month)->sum('pay_true');
            $list[] = ['title'=>'今月营业额', 'num'=>$moth_money];

            $on_sale = db('goods')->where('on_sale', 1)->where('is_del', 0)->count();
            $list[] = ['title'=>'在售商品', 'num'=>$on_sale];

            $no_sale = db('goods')->where('on_sale', 0)->where('is_del', 0)->count();
            $list[] = ['title'=>'下架商品', 'num'=>$no_sale];
            $list[] = ['title'=>'今日新增用户', 'num'=>\db('user')->where('create_time','>',$today)->where('is_delete',0)->count()];
            $list[] = ['title'=>'本月新增用户人数', 'num'=>\db('user')->where('create_time','>',$this_month)->where('is_delete',0)->count()];
//            $list[] = ['title'=>'易贝价格', 'num'=>$pond['price']];
//            $list[] = ['title'=>'易贝释放池', 'num'=>$pond['t_yb']];
//            $list[] = ['title'=>'易贝剩余释放池', 'num'=>$pond['credit2']];
//            $list[] = ['title'=>'易贝总销毁值', 'num'=>$pond['sum_credit2']];
//            $list[] = ['title'=>'今日易贝销毁值', 'num'=>$pond['t_credit2']];
//            $list[] = ['title'=>'全网总积分', 'num'=>$pond['credit1']];




            $this->returnAPI('', 0, $list, ['count' => 5, 'limit' => $this->get['limit']]);
        }
        return $this->fetch();
    }
    //易贝池
    public function pond(){
        $credit1=db('user')->sum('t_credit1');
        $credit2=get_field('distribute',['id'=>1],'surplus');
        $price=get_field('market_config',['id'=>1],'price');
        $pond=model('pond')->get(['id'=>1]);
        $date=strtotime(date('Y-m-d',time()));
        $mnum=db('market')->where('create_time','>',$date)->where('status','<>',4)->sum('num');
        $znum=db('market')->where('status','<>',4)->sum('num');
        $sxf=get_field('market_config',['id'=>1],'service');
        $mnum=floatval($mnum)*floatval($sxf);
        $znum=floatval($znum)*floatval($sxf);
//       dump($mnum);
        $data=[
            'numyb'=>get_field('distribute',['id'=>1],'green_points'),
            't_yb'=>$pond->t_yb,
            'credit1'=>$credit1,
            'credit2'=>$credit2,
            'price'=>$price,
            't_credit2'=>$pond->t_credi2+$mnum,
            'sum_credit2'=>$pond->sum_credit2+$znum
        ];
        return $data;
    }
    public function edit($id = 0)
    {
        if ($this->post) {
            if ($id === 0) {
                $model = model($this->tn);
            } else {
                $model = model($this->tn)->get($id);
                if (is_null($model)) {
                    $this->returnAPI('信息有误，请稍后重试');
                }
            }
            $validate = $this->validate(['id' => $id] + $this->post, $this->tn);
            if ($validate === true) {
                $result = $model->allowField(true)->save($this->post);
                if ($result === false) {
                    $this->returnAPI($model->getError());
                }
                $this->returnAPI('操作成功', 0);
            }
            $this->returnAPI($validate);
        }
        $model = model($this->tn)->get($id);
        if (is_null($model)) {
            $data = $this->table_fields($this->tn);
        } else {
            $data = $model->getData();
        }
        return $this->fetch('', ['data' => $data]);
    }
}
