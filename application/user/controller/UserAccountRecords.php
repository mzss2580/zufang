<?php
namespace app\user\controller;

use think\Db;
use app\common\model\User as Users;

class UserAccountRecords extends Base
{
    private $tn = 'user_account_records';
    private $wallet_type = [1=>'商城余额', 2=>'可提现余额'];
    private $from_type   = [1=>'商城订单', 2 =>'订单退还', 3 =>'提现', 4 =>'提现退还', 5 =>'余额充值',6 =>'后台修改'];

    public function index()
    {
        $uid = $this->get['id'];
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $where = [];
            $where['uid'] = $uid;
            if(isset($this->get['account_type'])){
                $where['account_type'] = $this->get['account_type'];
            }
            $db    = db($this->tn);
            $count = $db->where($where)->count();
            $list  = $db->where($where)->field(true)->page($this->get['page'])->limit($this->get['limit'])->order('id', 'desc')->select();
            foreach ($list as $k => $v) {
                $list[$k]['create_time']  = date('Y-m-d H:i:s', $v['create_time']);
                $list[$k]['account_type'] = $this->wallet_type[$v['account_type']]; //钱包类型
                $list[$k]['laiyuan']      = $this->from_type[$v['from_type']];
                if($v['from_type'] == 6){
                    $list[$k]['laiyuan'] .= '，操作人：'.get_field('admin',$v['admin_id'],'username'); //后台操作人
                }
            }
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit']]);
        }
        return $this->fetch('',['wallet_type'=>$this->wallet_type, 'uid'=>$uid]);
    }

    public function edit($id = 0)
    {
        $user_id = $this->get['user_id']; //用户id
        $user_model = model('user');
        $user_model = $user_model->get($user_id);
        if($user_id <= 0 || is_null($user_model)){
            $this->returnAPI('信息有误,请稍后重试');
        }
        $model = model($this->tn);
        if ($this->post) {
            $users = new Users;
            //$res = $users->add_user_account_records($uid, $account_type, $sign, $number, $order_amount, $from_type, $data_id, $typstr, $text, $union_orderno = '');
            $account_type = $this->post['account_type']; // 修改的金额类型
            $order_amount = $number = $this->post['number'];
            $sign = 0;
            if($number > 0){
                $sign = 1;
            }
            $from_type = 6; // 后台修改
            $data_id   = 0;
            $typstr    = '';
            $text      = '[后台操作]'.$this->post['text'];

            $res = $users->add_user_account_records($user_id, $account_type, $sign, $number, $order_amount, $from_type, $data_id, $typstr, $text, '', session('admin_id'));
            if($res['status']){
                $this->returnAPI('操作成功', 0);
            }else{
                $this->returnAPI($res['msg']);
            }
        }
        $model = $model->get($id);
        if (is_null($model)) {
            $data = $this->table_fields($this->tn);
        } else {
            $data = $model->getData();
        }
        return $this->fetch('', ['data' => $data, 'wallet_type' => $this->wallet_type]);
    }
}
