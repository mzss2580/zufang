<?php
namespace app\user\controller;

use think\Db;

class User extends Base
{
    private $tn = 'user';

    public function index()
    {
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $db = db($this->tn);
            if (isset($this->get['phone']) && $this->get['phone']) {
                $db->where('phone', 'like', '%' . $this->get['phone'] . '%');
            }
            if (isset($this->get['name']) && $this->get['name']) {
                $db->where('name', 'like', '%' . $this->get['name'] . '%');
            }
            if (isset($this->get['idcard']) && $this->get['idcard']) {
                $db->where('idcard', 'like', '%' . $this->get['idcard'] . '%');
            }
            $count = $db->count();
            $list  = $db->field(true)->page($this->get['page'])->limit($this->get['limit'])->order('id', 'desc')->select();
            foreach ($list as $k => $v) {
                $list[$k]['head']        = '<img class="upload-image show-image" src="' . $v['head'] . '"/>';
                $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
            }
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit']]);
        }
        return $this->fetch();
    }

    /*提现记录*/
    public function withdraw_log()
    {
        $status = input('status');
        $all_status = ['-1','0','1'];
        if(!in_array($status,$all_status)){
            $status = 'no';
        }
        $name   = input('name');
        $phone    = input('phone');
        $start_time   = trim(input('start_time'));
        $end_time     = trim(input('end_time'));
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $db    = db('user_withdraw');
            $list  = $db
                ->alias('uw')
                ->join("user u","u.id = uw.uid")
                ->where('u.is_delete',0);
            if($status != 'no'){
                $list = $list
                    ->where('uw.status',$status);
            }
            if(!empty($name)){
                $list = $list
                    ->where('u.name',$name);
            }
            if(!empty($phone)){
                $list = $list
                    ->where('u.phone',$phone);
            }
            if ($start_time) {
                if ($end_time) {
                    $db->where('uw.create_time', 'between', [strtotime($start_time . ' 00:00:00'), strtotime($end_time . ' 23:59:59')]);
                } else {
                    $db->where('uw.create_time', '>=', strtotime($start_time . ' 00:00:00'));
                    //$db_two->where('uw.create_time', '>=', strtotime($start_time . ' 00:00:00'));
                }
            } else if ($end_time) {
                $db->where('uw.create_time', '<=', strtotime($end_time . ' 23:59:59'));
            }
            $count = $db->count();
            $condition_money = $db->sum('uw.money');
            $list = $list
                ->page($this->get['page'])
                ->limit($this->get['limit'])
                ->field('uw.*,u.nick,u.phone')
                ->order('uw.id desc')
                ->select();
            foreach ($list as &$v) {
                //订单状态类型
                /*if($v['type'] == 1){
                    $v['type_name'] = '银行卡提现';
                }else{
                    $v['type_name'] = '支付宝提现';
                }
                //查询提现银行信息
                $find_bank = db('bank')
                    ->where('id',$v['bank_id'])
                    ->field('name as bank_name')
                    ->find();
                if($find_bank){
                    $v['bank_name'] = $find_bank['bank_name'];
                }else{
                    $v['bank_name'] = '';
                }*/
                //订单状态类型
                if($v['status'] == -1){
                    $v['status_name'] = '驳回';
                }else if($v['status'] == 1){
                    $v['status_name'] = '通过';
                }else{
                    $v['status_name'] = '等待审核';
                }

                $v['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
            }
            $this->returnAPI('', 0, $list, ['count' => $count,'condition_money'=> $condition_money, 'limit' => $this->get['limit']]);
        }
        $this->assign('status',$status);
        return $this->fetch();
    }

    //通过
    public function withdraw_ispass()
    {
        $id = input('id');
        if($id){
            //查询订单状态
            $find_log = db('user_withdraw')
                ->where('id',$id)
                ->find();
            if(!$find_log){
                $this->returnAPI('申请不存在');
            }
            if($find_log['status'] != 0){
                $this->returnAPI('申请已审核过');
            }
            //查询用户信息
            $find_user = Db::name('user')
                ->where('id',$find_log['uid'])
                ->where('is_delete',0)
                //->field('u.push_regid,u.status')
                ->find();
            if(!$find_user){
                $this->returnAPI('通过失败,此用户不存在');
            }else{
                if(0 && $find_user['status'] != 1){
                    $this->returnAPI('通过失败,此商户未审核通过');
                }else{
                    $up = db('user_withdraw')
                        ->where('id',$id)
                        ->update(array('status'=>1,'status_time'=>time()));
                    if($up){
                        if(0 && !empty($find_user['push_regid'])){
                            //推送商户消息
                            $this->push_user($find_user['push_regid'],'提现通知：您好，你的提现申请已通过');
                        }
                        $this->returnAPI('通过成功',0);
                    }else{
                        $this->returnAPI('通过失败');
                    }
                    die;
                    /*用户条件不满足，直接提现到银行卡暂不能用，后期能用再放开
                    if($find_log['type'] == 1){
                        //查询银行卡信息
                        $find_bank = Db::name('bank')
                                ->where('id',$find_log['bank_id'])
                                ->field('name as bank_name,bank_code')
                                ->find();
                        if($find_bank){
                            $res = $this->payBank($find_log['money'],$find_log['order_sn'],$find_log['bank_card'],$find_log['name'],$find_bank['bank_code']);
                            if($res['status'] == 1){
                                $up = Db::name('rider_withdraw_log')
                                    ->where('id',$id)
                                    ->update(array('status'=>1,'payment_no'=>$res['data'],'update_time'=>time()));
                                if($up){
                                    if(!empty($find_rider['push_regid'])){
                                        //推送骑手消息
                                        $this->push_rider($find_rider['push_regid'],'提现通知：您好，你的提现申请已通过',$find_rider['is_voice']);
                                    }
                                    $this->returnAPI('通过成功',0);
                                }else{
                                    $this->returnAPI('通过失败');
                                }
                            }else{
                                $this->returnAPI($res['msg']);
                            }

                        }else{
                            $this->returnAPI('通过失败,无对应银行卡信息');
                        }
                    }else{
                        $up = Db::name('rider_withdraw_log')
                            ->where('id',$id)
                            ->update(array('status'=>1,'payment_no'=>$res['data'],'update_time'=>time()));
                        if($up){
                            if(!empty($find_rider['push_regid'])){
                                //推送骑手消息
                                $this->push_rider($find_rider['push_regid'],'提现通知：您好，你的提现申请已通过',$find_rider['is_voice']);
                            }
                            $this->returnAPI('通过成功',0);
                        }else{
                            $this->returnAPI('通过失败');
                        }
                    }*/
                }

            }
        }else{
            $this->returnAPI('通过失败');
        }
    }

    //驳回
    public function withdraw_nopass()
    {
        $id = input('id');
        if($id){
            //查询订单状态
            $find_log = db('user_withdraw')
                ->where('id',$id)
                ->find();
            if(!$find_log){
                $this->returnAPI('申请不存在');
            }
            if($find_log['status'] != 0){
                $this->returnAPI('已审核过');
            }
            $reason = input('reason');
            //查询商户信息
            $find_user = Db::name('user')
                ->where('id',$find_log['uid'])
                ->where('is_delete',0)
                //->field('u.push_regid,u.status')
                ->find();
            if(!$find_user){
                $this->returnAPI('驳回失败,此用户不存在');
            }else{
                if(0 && $find_user['status'] != 1){
                    $this->returnAPI('通过失败,此商户未审核通过');
                }else{
                    Db::startTrans();
                    try{
                        $up = db('user_withdraw')
                            ->where('id',$id)
                            ->update(array('status'=>-1,'reason'=>$reason,'status_time'=>time()));
                        if(!$up){
                            throw new \Exception('审核失败，请重试');
                        }
                        //退回金额
                        $inc = db('user')
                            ->where('id',$find_log['uid'])
                            ->where('is_delete',0)
                            ->setInc('withdraw_money',$find_log['money']);
                        if(!$inc){
                            throw new \Exception('审核失败，请重试');
                        }
                        Db::commit();
                    }catch(\Exception $e){
                        //回滚事务
                        Db::rollback();
                        $this->returnAPI('123');
                    }
                    $this->returnAPI('驳回成功',0);
                }

            }

        }else{
            $this->returnAPI('驳回失败');
        }
    }
}
