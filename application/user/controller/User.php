<?php
namespace app\user\controller;

use http\Message\Body;
use think\Db;

class User extends Base
{
    private $tn = 'user';

    public function user_list()
    {
        $uid = $this->get['id'];
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $where = [];
            $where['invite_user_id'] = $uid;
            $db    = db($this->tn);
            $count = $db->where($where)->count();
            $list  = $db->where($where)->field(true)->page($this->get['page'])->limit($this->get['limit'])->order('id', 'desc')->select();
            foreach ($list as $k => $v) {
                $list[$k]['head'] = '<img class="upload-image show-image" src="' . $v['head'] . '"/>';
                $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
                $list[$k]['gid'] = get_field('grade', $v['gid']);
            }
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit']]);
        }
        return $this->fetch('',['uid'=>$uid]);
    }

    public function index()
    {
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $where = [];
            if(request()->get('seach_name') != ''){
                if(request()->get('seach_type') == 'name'){
                    $where['nick'] = request()->get('seach_name');
                }else if(request()->get('seach_type') == 'tel'){
                    $where['phone'] = request()->get('seach_name');
                }
            }
            $db    = db($this->tn);
            $order = input('order', '');

            if(empty($order)){
                $db->order('id', 'desc');
            }else{
                $db->order($order, 'desc');
            }
            if (request()->get('code')!=''){
                switch (request()->get('code')){
                    case 0;
                    $count=$db->where($where)->where('wechat_code','<>',null)->where('alipay_code','<>',null)->count();
                    $list=$db->where($where)->where('wechat_code','<>',null)->where('alipay_code','<>',null)->field(true)->page($this->get['page'])->limit($this->get['limit'])->select();
                        break;
                    case 1;
                        $count=$db->where($where)->where('wechat_code',null)->where('alipay_code','<>',null)->count();
                        $list=$db->where($where)->where('wechat_code',null)->where('alipay_code','<>',null)->field(true)->page($this->get['page'])->limit($this->get['limit'])->select();
                        break;
                    case 2;
                        $count=$db->where($where)->where('wechat_code','<>',null)->where('alipay_code',null)->count();
                        $list=$db->where($where)->where('wechat_code','<>',null)->where('alipay_code',null)->field(true)->page($this->get['page'])->limit($this->get['limit'])->select();

                        break;
                    case 3;
                        $count=$db->where($where)->where('wechat_code',null)->where('alipay_code',null)->count();
                        $list=$db->where($where)->where('wechat_code',null)->where('alipay_code',null)->field(true)->page($this->get['page'])->limit($this->get['limit'])->select();
                        break;
                }
            }else{
                $count = $db->where($where)->count();
                $list  = $db->where($where)->field(true)->page($this->get['page'])->limit($this->get['limit'])->select();

            }


            foreach ($list as $k => $v) {
                $list[$k]['head'] = '<img class="upload-image show-image" src="' . $v['head'] . '"/>';
                $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
                if($v['invite_user_id'] == 0){
                    $list[$k]['invite_user'] = '暂无推荐人';
                }else{
                    $invite_user = get_field('user',$v['invite_user_id'],'nick'); //后台操作人
                    $list[$k]['invite_user'] = $invite_user.'(ID:'.$v['invite_user_id'].')';
                }
//                $list[$k]['code'] = db('user_invite_code')->where('uid', $v['id'])->value('code', '-');
                //我的直推人数
                $list[$k]['invite_count'] = db('user')->where('invite_user_id', $v['id'])->count();
//                $list[$k]['gid'] = get_field('grade', $v['gid']);
            }
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit'],'password'=>get_field('system_config',['id'=>39],'value')]);
        }
        return $this->fetch('',['password'=>get_field('system_config',['id'=>39],'value')]);
    }

    public function remark($id = 0)
    {
        if ($this->post) {
            if ($id === 0) {
                $this->returnAPI('信息有误，请稍后重试');
            }
            $remark = input('remark', '');
            $result = db('user')->where('id', $id)->update(['remark'=>$remark]);

            if (!$result) {
                $this->returnAPI('修改失败');
            }
            $this->returnAPI('操作成功', 0);
        }
        $user = db('user')->where('id', $id)->find();
        return $this->fetch('', ['remark' => $user['remark']]);
    }

    public function edit_code($id = 0)
    {
        if ($this->post) {
            if ($id === 0) {
                $this->returnAPI('信息有误，请稍后重试');
            }
            $model = model('user_invite_code')->get(['uid'=>$id]);
            /*if (is_null($model)) {
                $this->returnAPI('信息有误，请稍后重试');
            }*/
            //判断当前邀请码是否有了
            $is_exist = db('user_invite_code')->where('code', $this->post['code'])->find();
            if(!empty($is_exist) && $is_exist['uid'] != $id){
                $this->returnAPI('当前邀请码已存在');
            }
            if(is_null($model)){
                //新增
                $result = db('user_invite_code')->insert(['uid'=>$id, 'code'=>$this->post['code'], 'create_time'=>time()]);
            }else{
                //修改
                $result = db('user_invite_code')->where('uid',$id)->update(['code'=>$this->post['code'], 'update_time'=>time()]);
            }

            if (!$result) {
                $this->returnAPI('修改失败');
            }
            $this->returnAPI('操作成功', 0);
        }
        $model = model('user_invite_code')->get(['uid'=>$id]);
        if (is_null($model)) {
            $data = $this->table_fields('user_invite_code');
        } else {
            $data = $model->getData();
        }
        return $this->fetch('', ['data' => $data]);
    }

    public function edit($id = 0)
    {
        $province=Db::name('city')->where('type',1)->field('id,pid,type,name')->select();

        if ($this->post) {
//            dump($this->post);
            $phone = input('phone');
            $pwd   = input('pwd');
            $nick  = input('nick');
            $head  = input('head');
//            $province=input('province');
//            $city=input('city');
//            $area=input('area');
            $street=input('street');
            //验证手机号
//            $check = '/^(1([3456789][0-9]))\d{8}$/';
//            if (!preg_match($check, $phone)) {
//                $this->returnAPI('手机号格式不正确', 1);
//            }

            $invite_user_id = input('invite_user_id', 0);

            //添加
            if ($id === 0) {
                $validate = $this->validate(['id' => $id] + $this->post, 'user.admin_register');
                //编辑
            } else {
                //查询用户
                $find_user = Db::name('user')
                    ->where('id',$id)
                    ->find();
                if (!$find_user) {
                    $this->returnAPI('信息有误，请稍后重试');
                }
                //是否修改密码
                if(!empty($pwd)){
                    $validate = $this->validate(['id' => $id] + $this->post, 'user.admin_register');
                }else{
                    $validate = true;
                }
            }

            if ($id === 0 || $find_user['phone'] != $phone) {
                //查询手机号是否有人使用
                $find_phone = Db::name('user')
                    ->where('phone',$phone)
                    //->where('is_del',0)
                    ->field('id')
                    ->find();
                if($find_phone){
                    $this->returnAPI('此手机号已经有会员注册', 1);
                }
            }

            if ($validate === true) {
                Db::startTrans();
                try{
                    $data = array();
                    $data['phone'] = $phone;
                    $data['head'] = $head;
//                    $data['province']=$province;
//                    $data['city']=$city;
//                    $data['area']=$area;
//                    $data['street']=$street;
                    if (!empty($pwd)) {
                        $data['pwd'] = md5($pwd);
                    }
                    $data['nick']  = $nick;
                    //$data['update_time'] = time();
                    $data['invite_user_id'] = $invite_user_id;
//                    $data['is_ban']=$this->post['is_ban'];

                    if ($id === 0) {

                        $data['create_time'] = time();
                        $up = Db::name('user')
                            ->insertGetId($data);
                        $uid = $up;
                    }else{
                        $time=date('Y-m-d H:i:s',time());
                        $user=model('user')->get(['id'=>$id]);
                        if ($user->invite_user_id!=$data['invite_user_id']){
//                            Db::name('typelog')->insert([
//                                'text'=>'后台与'.$time.'时间,将会员id:'.$id.',手机号为:'.$user->phone.'用户的邀请人,原邀请人为:id:'.$user->invite_user_id.',邀请人手机号为:'.get_field('user',$user->invite_user_id,'phone').'。现邀请人为:邀请人id:'.$data['invite_user_id'].'邀请人手机号为:'.get_field('user',$data['invite_user_id'],'phone'),
//                                'type'=>'修改用户邀请人',
//                                'time'=>$time
//                            ]);
                        }
                        $up = Db::name('user')
                            ->where('id',$id)
                            ->update($data);
                        $uid = $id;
                    }

                    Db::commit();
                    $this->returnAPI('操作成功', 0);

                } catch (\HttpResponseException $e) {
                    //回滚事务
                    Db::rollback();
                    $this->returnAPI('操作失败');
                }
            }else{
                $this->returnAPI($validate);
            }

        }else{

            $model = model($this->tn)->get($id);
            $user_list = db('user')->where('id', 'not in', $id)->select();

            if (is_null($model)) {
                $data = $this->table_fields($this->tn);
                $city=[];
                $area=[];
                $street=[];
            } else {
                $data = $model->getData();
//                dump($data);
//                $data['cityname']=get_field('city',['id'=>$data['city']]);
//                $city=Db::name('city')->where('pid',$data['province'])->select();
//                $area=Db::name('city')->where('pid',$data['city'])->select();
//                $street=Db::name('city')->where('pid',$data['area'])->select();
//
//                $data['areaname']=get_field('city',['id'=>$data['area']]);
//                $data['streetname']=get_field('city',['id'=>$data['street']]);
            }
//            if ($id){
//                $data = Db::name('user')
//                    ->where('id',$id)
//                    ->find();
//                $data['cityname']=get_field('city',['id'=>$data['city']]);
//                $city=Db::name('city')->where('pid',$data['province'])->select();
//                $area=Db::name('city')->where('pid',$data['city'])->select();
//                $street=Db::name('city')->where('pid',$data['area'])->select();
//
//                $data['areaname']=get_field('city',['id'=>$data['area']]);
//                $data['streetname']=get_field('city',['id'=>$data['street']]);
//                $user_list = db('user')->where('is_delete', 0)->where('id', 'not in', $id)->select();
//            }else{
//                $data = [
//                    'city'=>0,
//                    'area'=>0,
//                    'street'=>0,
//                    'cityname'=>null,
//                    'areaname'=>null,
//                    'streetname'=>null,
//                    'nick'=>null,
//                    'head'=>null,
//                    'phone'=>null,
//                    'invite_user_id'=>null,
//                    'sex'=>0,
//                    'province'=>0,
//                    'idcard'=>0,
//                    'real_phone'=>0
//                ];
////                $data['cityname']=get_field('city',['id'=>$data['city']]);
//                $city=[];
//                $area=[];
//                $street=[];
//
//
//                $user_list = db('user')->where('is_delete', 0)->where('id', 'not in', $id)->select();
//            }

            return $this->fetch('', ['data' => $data,'user_list'=>$user_list]);
        }

    }
    /*提现记录*/
    public function withdraw_log()
    {
        $status = input('status');
        $all_status = ['-1','0','1'];
        if(!in_array($status,$all_status)){
            $status = 'no';
        }
        $name   = input('name');
        $phone    = input('phone');
        $start_time   = trim(input('start_time'));
        $end_time     = trim(input('end_time'));
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $db    = db('user_withdraw');
            $list  = $db
                ->alias('uw')
                ->join("user u","u.id = uw.uid")
                ->where('u.is_delete',0);
            if($status != 'no'){
                $list = $list
                    ->where('uw.status',$status);
            }
            if(!empty($name)){
                $list = $list
                    ->where('u.name',$name);
            }
            if(!empty($phone)){
                $list = $list
                    ->where('u.phone',$phone);
            }
            if ($start_time) {
                if ($end_time) {
                    $db->where('uw.create_time', 'between', [strtotime($start_time . ' 00:00:00'), strtotime($end_time . ' 23:59:59')]);
                } else {
                    $db->where('uw.create_time', '>=', strtotime($start_time . ' 00:00:00'));
                    //$db_two->where('uw.create_time', '>=', strtotime($start_time . ' 00:00:00'));
                }
            } else if ($end_time) {
                $db->where('uw.create_time', '<=', strtotime($end_time . ' 23:59:59'));
            }
            $count = $db->count();
            $condition_money = $db->sum('uw.money');
            $list = $list
                ->page($this->get['page'])
                ->limit($this->get['limit'])
                ->field('uw.*,u.nick,u.phone')
                ->order('uw.id desc')
                ->select();
            foreach ($list as &$v) {
                //订单状态类型
                /*if($v['type'] == 1){
                    $v['type_name'] = '银行卡提现';
                }else{
                    $v['type_name'] = '支付宝提现';
                }
                //查询提现银行信息
                $find_bank = db('bank')
                    ->where('id',$v['bank_id'])
                    ->field('name as bank_name')
                    ->find();
                if($find_bank){
                    $v['bank_name'] = $find_bank['bank_name'];
                }else{
                    $v['bank_name'] = '';
                }*/
                //订单状态类型
                if($v['status'] == -1){
                    $v['status_name'] = '驳回';
                }else if($v['status'] == 1){
                    $v['status_name'] = '通过';
                }else{
                    $v['status_name'] = '等待审核';
                }

                $v['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
            }
            $this->returnAPI('', 0, $list, ['count' => $count,'condition_money'=> $condition_money, 'limit' => $this->get['limit']]);
        }
        $this->assign('status',$status);
        return $this->fetch();
    }

    //通过
    public function withdraw_ispass()
    {
        $id = input('id');
        if($id){
            //查询订单状态
            $find_log = db('user_withdraw')
                ->where('id',$id)
                ->find();
            if(!$find_log){
                $this->returnAPI('申请不存在');
            }
            if($find_log['status'] != 0){
                $this->returnAPI('申请已审核过');
            }
            //查询用户信息
            $find_user = Db::name('user')
                ->where('id',$find_log['uid'])
                ->where('is_delete',0)
                //->field('u.push_regid,u.status')
                ->find();
            if(!$find_user){
                $this->returnAPI('通过失败,此用户不存在');
            }else{
                if(0 && $find_user['status'] != 1){
                    $this->returnAPI('通过失败,此商户未审核通过');
                }else{
                    $up = db('user_withdraw')
                        ->where('id',$id)
                        ->update(array('status'=>1,'status_time'=>time()));
                    if($up){
                        if(0 && !empty($find_user['push_regid'])){
                            //推送商户消息
                            $this->push_user($find_user['push_regid'],'提现通知：您好，你的提现申请已通过');
                        }
                        $this->returnAPI('通过成功',0);
                    }else{
                        $this->returnAPI('通过失败');
                    }
                    die;
                    /*用户条件不满足，直接提现到银行卡暂不能用，后期能用再放开
                    if($find_log['type'] == 1){
                        //查询银行卡信息
                        $find_bank = Db::name('bank')
                                ->where('id',$find_log['bank_id'])
                                ->field('name as bank_name,bank_code')
                                ->find();
                        if($find_bank){
                            $res = $this->payBank($find_log['money'],$find_log['order_sn'],$find_log['bank_card'],$find_log['name'],$find_bank['bank_code']);
                            if($res['status'] == 1){
                                $up = Db::name('rider_withdraw_log')
                                    ->where('id',$id)
                                    ->update(array('status'=>1,'payment_no'=>$res['data'],'update_time'=>time()));
                                if($up){
                                    if(!empty($find_rider['push_regid'])){
                                        //推送骑手消息
                                        $this->push_rider($find_rider['push_regid'],'提现通知：您好，你的提现申请已通过',$find_rider['is_voice']);
                                    }
                                    $this->returnAPI('通过成功',0);
                                }else{
                                    $this->returnAPI('通过失败');
                                }
                            }else{
                                $this->returnAPI($res['msg']);
                            }

                        }else{
                            $this->returnAPI('通过失败,无对应银行卡信息');
                        }
                    }else{
                        $up = Db::name('rider_withdraw_log')
                            ->where('id',$id)
                            ->update(array('status'=>1,'payment_no'=>$res['data'],'update_time'=>time()));
                        if($up){
                            if(!empty($find_rider['push_regid'])){
                                //推送骑手消息
                                $this->push_rider($find_rider['push_regid'],'提现通知：您好，你的提现申请已通过',$find_rider['is_voice']);
                            }
                            $this->returnAPI('通过成功',0);
                        }else{
                            $this->returnAPI('通过失败');
                        }
                    }*/
                }

            }
        }else{
            $this->returnAPI('通过失败');
        }
    }

    //驳回
    public function withdraw_nopass()
    {
        $id = input('id');
        if($id){
            //查询订单状态
            $find_log = db('user_withdraw')
                ->where('id',$id)
                ->find();
            if(!$find_log){
                $this->returnAPI('申请不存在');
            }
            if($find_log['status'] != 0){
                $this->returnAPI('已审核过');
            }
            $reason = input('reason');
            //查询商户信息
            $find_user = Db::name('user')
                ->where('id',$find_log['uid'])
                ->where('is_delete',0)
                //->field('u.push_regid,u.status')
                ->find();
            if(!$find_user){
                $this->returnAPI('驳回失败,此用户不存在');
            }else{
                if(0 && $find_user['status'] != 1){
                    $this->returnAPI('通过失败,此商户未审核通过');
                }else{
                    Db::startTrans();
                    try{
                        $up = db('user_withdraw')
                            ->where('id',$id)
                            ->update(array('status'=>-1,'reason'=>$reason,'status_time'=>time()));
                        if(!$up){
                            throw new \Exception('审核失败，请重试');
                        }
                        //退回金额
                        $inc = db('user')
                            ->where('id',$find_log['uid'])
                            ->where('is_delete',0)
                            ->setInc('withdraw_money',$find_log['money']);
                        if(!$inc){
                            throw new \Exception('审核失败，请重试');
                        }
                        Db::commit();
                    }catch(\Exception $e){
                        //回滚事务
                        Db::rollback();
                        $this->returnAPI('123');
                    }
                    $this->returnAPI('驳回成功',0);
                }

            }

        }else{
            $this->returnAPI('驳回失败');
        }
    }


    //市
    public function city($pid){
        $city=Db::name('city')->where(['type'=>2,'pid'=>$pid])->select();
        return $city;
    }

    //区
    public function area($cid){
        $area=Db::name('city')->where(['type'=>3,'pid'=>$cid])->select();
        return $area;
    }

    //街道
    public function street($aid){
        $street=Db::name('city')->where(['type'=>4,'pid'=>$aid])->select();
        return $street;
    }


    public function credit1($id = 0)
    {
        $time=date('Y-m-d H:i:s',time());
        if ($this->post) {
            $user=model('user')->get(['id'=>$id]);
            $after_y=floatval($user->credit2);
            $after_t=floatval($user->t_credit1);
//            dump($this->post);
            if ($user->gid!=$this->post['gid']){
                Db::name('typelog')->insert([
                    'text'=>'后台在'.$time.'时间修改了,手机号为:'.$user->phone.'的等级,原始等级为:'.get_field('grade',['id'=>$user->gid],'name').',现在为:'.get_field('grade',['id'=>$this->post['gid']],'name'),
                    'time'=>$time,
                    'type'=>'修改等级'
                ]);
            }
            if ($after_t<$this->post['t_credit1']){
                $num=$this->post['t_credit1']-$after_t;
//
            }else{
                $num=-($after_t-$this->post['t_credit1']);
            }
            if ($after_y<$this->post['credit2']){
                $nums=$this->post['credit2']-$after_y;
//
            }else{
                $nums=-($after_y-$this->post['credit2']);
            }
//            dump($nums);
//            die();
            \app\common\model\User::capitalRecord($id,$nums,2,'后台播发易贝');

            $a=\app\common\model\User::capitalRecord($id,$num,3,'后台播发积分');
//            $a=\app\common\model\User::capitalRecord($id,$num,1,'后台播发积分');

            if ($id === 0) {
                $this->returnAPI('信息有误，请稍后重试');
            }
            $user=\app\common\model\User::get(['id'=>$id]);
            if (is_null($user)){
                $this->returnAPI('未查询到该用户');
            }else{
                $flag=$user->save(['credit1'=>$this->post['credit1'],'gid'=>$this->post['gid']]);
                if ($flag){
                    \app\common\model\User::capitalRecord($id,$num,1,'后台播发积分');
                    $this->returnAPI('操作成功',0);
                }else{
                    $this->returnAPI('操作失败');
                }
            }

        }
        $user=\app\common\model\User::get(['id'=>$id]);

        $data['credit1']=$user->credit1;
        $data['t_credit1']=$user->t_credit1;
        $data['credit2']=$user->credit2;
        $data['gid']=$user->gid;
        $grade=Db::name('grade')->select();
        return $this->fetch('', ['data' => $data,'grade'=>$grade]);
    }


    public function credit1_log(){
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $where = [];
            if(request()->get('seach_name') != ''){
                if(request()->get('seach_type') == 'name'){
                    $where['u.nick'] = request()->get('seach_name');
                }else if(request()->get('seach_type') == 'tel'){
                    $where['u.phone'] = request()->get('seach_name');
                }

            }
            if (request()->get('type')){
                $where['c.type']=intval(request()->get('type'));
            }
//            dump($where);
            $db    = db($this->tn)->alias('u')->join('capital_record c',"u.id=c.uid")->field('c.id,u.nick,u.phone,c.create_time,c.content,u.head,c.text') ;
            $count = $db->where($where)->count();
            $list  = $db->where($where)->order('id','desc')->page($this->get['page'])->limit($this->get['limit'])->select();
//            dump($list);
            foreach ($list as $k => $v) {
                $list[$k]['head'] = '<img class="upload-image show-image" src="' . $v['head'] . '"/>';
                $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
//                $list[$k]['gid'] = get_field('grade', $v['gid']);
            }
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit']]);
        }
        return $this->fetch();
    }

    public function collection_code($id){
        $user=model('user')->get($id);
        if (is_null($user)){
           $this->returnAPI('用户不存在');
        }
        $data=[
            'wechat'=>$user->wechat_code,
            'alipay'=>$user->alipay_code
        ];
        return $this->fetch('', ['data' => $data]);

    }




}
