<?php
namespace app\common\controller;

use OSS\OssClient;
use think\Controller;
use think\Db;
use think\exception\HttpResponseException;
use think\Image;
use think\Response;

class Ctrl extends Controller
{
    /**
     *删除oss上的文件
     * @param  string  $object      文件
     * @param  string  $bucket      你懂得
     */
    public function del_oss($object, $bucket = 1){
        $object = get_file_name($object);
        $config['accessKeyId'] = config('alioss.accessKeyId');
        $config['accessKeySecret'] = config('alioss.accessKeySecret');
        $config['endpoint'] = config('alioss.endpoint');

        if($bucket == 1){
            $buckets = config('alioss.bucket');
        }else{
            $buckets = config('alioss.bucket1');
            $object = 'uploads/' . $object;
        }

        // 填写文件完整路径，例如exampledir/exampleobject.txt。文档完整路径中不能包含Bucket名称。
        //$object = "62de63752be10.jpg";

        try{
            $OssClient = new OssClient($config['accessKeyId'], $config['accessKeySecret'], $config['endpoint']);

            $OssClient->deleteObject($buckets, $object);
        } catch(OssException $e) {
            //printf(__FUNCTION__ . ": FAILED\n");
            //printf($e->getMessage() . "\n");
            $this->returnAPI($e->getMessage());
            //return;
        }
        return 1;
        //$this->returnAPI('删除成功', 0);
    }

    public function del_objects($objects){
        $arr = [];
        foreach($objects as $k => $v){
            $arr[] = get_file_name($v);
        }
        $config['accessKeyId'] = config('alioss.accessKeyId');
        $config['accessKeySecret'] = config('alioss.accessKeySecret');
        $config['endpoint'] = config('alioss.endpoint');

        $bucket = config('alioss.bucket');

        try{
            $OssClient = new OssClient($config['accessKeyId'], $config['accessKeySecret'], $config['endpoint']);

            $OssClient->deleteObjects($bucket, $arr);
        } catch(OssException $e) {
            //printf(__FUNCTION__ . ": FAILED\n");
            //printf($e->getMessage() . "\n");
            $this->returnAPI($e->getMessage());
            //return;
        }
        return 1;
        //$this->returnAPI('删除成功', 0);
    }

    /**
     *上传到阿里OSS
     * @param  string  $file      文件地址
     * @param  string  $name      文件名
     */
    protected function upload_oss($file, $name)
    {
        //上传视频到阿里云OSS
        //$file = $_FILES['file'];

        //$name = $file['name'];
        $format = strrchr($name, '.');
        $fileName = uniqid() . $format;
        //获取配置
        $config['accessKeyId'] = config('alioss.accessKeyId');
        $config['accessKeySecret'] = config('alioss.accessKeySecret');
        $config['endpoint'] = config('alioss.endpoint');
        $OssClient = new OssClient($config['accessKeyId'], $config['accessKeySecret'], $config['endpoint']);
        if($format == 'mp4' || $format == 'avi' || $format == 'wma' || $format == 'rm' || $format == 'rmvb' || $format == '3GP'){
            $bucket = config('alioss.bucket1');
        }else{
            $bucket = config('alioss.bucket');
        }
        //$uploadToAliyunOss = $OssClient->uploadFile($bucket, $fileName, $file['tmp_name']);
        $uploadToAliyunOss = $OssClient->uploadFile($bucket, $fileName, $file);

        if ($uploadToAliyunOss) {
            $img =array('.jpg','.jpeg','.png','.bmp','mp4');
            $src='https://'. $bucket .'.'. $config['endpoint'].strrchr($uploadToAliyunOss['info']['url'], '/');
            $return = ['src' => $src];
            $return['thumb'] = $src;
            $img =array('.jpg','.jpeg','.png','.bmp','mp4');
            if(in_array($format,$img)){
                $thumb = $src.'?x-oss-process=image/auto-orient,1/resize,p_40/quality,q_90';
                $return['thumb'] = $thumb;
            }
            //删除源文件
            if(@file_exists($file)){
                @unlink($file);
            }
            return $return;
        } else {
            //删除源文件
            if(@file_exists($file)){
                @unlink($file);
            }
            // 上传失败，打印错误信息
            $this->returnAPI($uploadToAliyunOss);
        }
    }
    /**
     * 批量上传图片并生成缩略图
     * @param  string   $field      上传图片字段
     * @param  integer  $num        上传图片数量限制,0为不限制,否则只生成前$num张图片
     * @param  string   $num_error  超出数量限制是否报错
     * @return string               图片路径
     */
    protected function upload_thumbs($field, $num = 0, $num_error = false)
    {
        $images = [];
        if (isset($_FILES[$field])) {
            $files = $this->request->withFiles([$field => $_FILES[$field]])->file($field);
            if ($num > 0 && count($files) > $num && $num_error) {
                $this->returnAPI('最多上传' . $num . '张图片');
            }
            if (!empty($files)) {
                foreach ($files as $k => $file) {
                    if ($num > 0 && $k >= $num) {
                        break;
                    }
                    $move = env('ROOT_PATH') . 'public' . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR . 'images';
                    $info = $file->validate(['ext' => 'jpg,JPG,png,PNG'])->rule('md5')->move($move);
                    if ($info) {
                        $src = $move . DIRECTORY_SEPARATOR . $info->getSaveName();
                        $img = getimagesize($src);
                        if ($img !== false && $img['mime'] != 'image/gif') {
                            $thumb = str_replace('.', '_thumb.', $src);
                            $image = Image::open($src);
                            $image->thumb(150, 150)->save($thumb);
                        }
                        $images[] = str_replace('\\', '/', str_replace(env('ROOT_PATH') . 'public', '', $src));
                    }
                }
            }
        }
        return $images;
    }
    /**
     * 上传图片并生成缩略图
     * @param  string  $field      上传图片字段
     * @param  boolean $return     是否强制上传
     * @param  string  $field_name 图片字段说明,返回错误信息
     * @return string              图片路径
     */
    protected function upload_thumb($field, $return = false, $field_name = '', $file_size = 0)
    {
        $image = '';
        if (isset($_FILES[$field])) {
            if ($file_size > 0 && $_FILES[$field]['size'] > $file_size) {
                trace($field_name . '超出图片大小限制');
                $this->returnAPI($field_name . '超出图片大小限制');
            }
            $file = $this->request->withFiles([$field => $_FILES[$field]])->file($field);

            if ($file) {
                $return = $this->upload_oss($_FILES[$field], time().rand(1000,9999));
                $image = $return['src'];
                //$thumb = $oss_return['thumb'];
                /*$move = env('ROOT_PATH') . 'public' . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR . 'images';
                $info = $file->validate(['ext' => 'jpg,JPG,jpeg,JPEG,bmp,BMP,png,PNG'])->rule('md5')->move($move);
                if ($info) {
                    $src = $move . DIRECTORY_SEPARATOR . $info->getSaveName();
                    $img = getimagesize($src);
                    if ($img !== false && $img['mime'] != 'image/gif') {
                        $thumb = str_replace('.', '_thumb.', $src);
                        $image = Image::open($src);
                        $image->thumb(150, 150)->save($thumb);
                    }
                    $image = str_replace('\\', '/', str_replace(env('ROOT_PATH') . 'public', '', $src));
                } else {
                    if ($return) {
                        trace($file->getError());
                        $this->returnAPI($field_name . '上传失败');
                    }
                }*/
            } else {
                if ($return) {
                    trace('请上传' . $field_name);
                    $this->returnAPI('请上传' . $field_name);
                }
            }
        } else {
            if ($return) {
                trace('请上传' . $field_name);
                $this->returnAPI('请上传' . $field_name);
            }
        }
        return $image;
    }
    /**
     * 获取图片缩略图
     * @param  string $image 大图路径
     * @return string        缩略图路径
     */
    protected function image_thumb($image)
    {
        if(empty($image)){
            return '';
        }
        return $thumb = (strpos($image, '/') === 0) ? $this->request->domain() . $image : $image . '?x-oss-process=image/auto-orient,1/resize,p_40/quality,q_90';
        //return $image ? $this->request->domain() . $image : '';
        $thumb = '';
        if ($image) {
            $image = str_replace('.', '_thumb.', $image);
            $thumb = env('ROOT_PATH') . 'public' . str_replace('/', DIRECTORY_SEPARATOR, $image);
            if (is_file($thumb)) {
                $thumb = $this->request->domain() . $image;
            } else {
                $thumb = '';
            }
        }
        //
        return $thumb;
    }
    /**
     * 解析商品规格
     * @param  string  $specs          商品可选规格字符串
     * @param  boolean $get_specs_type 是否获取其类型
     * @return string                  解析后字符串
     */
    protected function parse_specs($specs, $get_specs_type = false)
    {
        if ($specs) {
            $specs = explode(',', $specs);
            $array = db('specs')->alias('s')->join('specs_type t', 's.type = t.id')->where('s.id', 'in', $specs)->column('s.name,t.name as type_name', 's.id');
            foreach ($specs as $k => $v) {
                if ($get_specs_type) {
                    $specs[$k] = $array[$v]['type_name'];
                } else {
                    $specs[$k] = $array[$v]['name'];
                }
            }
            $specs = implode(',', $specs);
        }
        return $specs;
    }
    /**
     * 获取表字段及其默认值
     * @param  string $tn 表名，不含前缀
     * @return array
     */
    protected function table_fields($tn = '')
    {
        $column = Db::query('SELECT COLUMN_NAME,COLUMN_DEFAULT FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = "' . config('database.database') . '" AND TABLE_NAME = "' . config('database.prefix') . $tn . '"');
        $data   = [];
        foreach ($column as $k => $v) {
            $data[$v['COLUMN_NAME']] = is_null($v['COLUMN_DEFAULT']) ? '' : $v['COLUMN_DEFAULT'];
        }
        return $data;
    }
    /**
     * 返回API
     * @access protected
     * @param  string  $msg    提示信息
     * @param  integer $code   状态码
     * @param  array   $data   对应数据
     * @param  array   $extend 扩展字段
     * @param  array   $header HTTP头信息
     * @return void
     * @throws HttpResponseException
     */
    protected function returnAPI($msg = '', $code = 1, $data = [], $extend = [], $header = [])
    {
        $return = ['msg' => $msg, 'code' => $code];
//        if (!empty($data)) {
        //            $return['data'] = $data;

//        }
                    $return['data'] = $data;

        if (!empty($extend)) {
            foreach ($extend as $k => $v) {
                $return[$k] = $v;
            }
        }
        $response = Response::create($return, 'json')->header($header);
        throw new HttpResponseException($response);
    }

    /**
     * POST严格验证
     * @param string $param  验证的参数
     */
    protected function StrictVerify($param,$msg='')
    {
        $data = input($param);
        //参数是否定义
        if(isset($data)){

            if($data === '0' || $data === 0){
                return $data;
                //判读是否为空
            }else if(empty($data)){
                if(!empty($msg)){
                    $this->returnAPI($msg."不能为空",1,'');
                }else{
                    $this->returnAPI("参数".$param."不能为空",1,'');
                }

            }else{
                return trim($data);
            }
        }else{
            $this->returnAPI("未传参数".$param, '444','');
        }
    }

    /**
     * POST非必须验证
     * @param string $param  验证的参数
     */
    protected function OptionalVerify($param,$msg='')
    {
        $data = input($param);
        if(!isset($data)){
            return $msg;
        }
        if($data === '0' || $data === 0){
            return $data;
        }else{
            //参数是否定义
            if(empty($data)){
                return $msg;
            }else{
                return trim($data);
            }
        }

    }
}
