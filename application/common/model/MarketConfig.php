<?php
namespace app\common\model;
use think\Model;
class MarketConfig extends Model
{
    protected $update = ['update_time'];
    protected function setUpdateTimeAttr()
    {
        return time();
    }
}
