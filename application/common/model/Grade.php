<?php
/**
 * Created by 项目名称.
 * User: 李磊
 * Date: 2022/9/6
 * Time: 11:45
 */

namespace app\common\model;
use think\Model;

class Grade extends Model
{
    protected $insert = ['create_time'];

    protected function setCreateTimeAttr()
    {
        return time();
    }
}