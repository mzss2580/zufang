<?php
namespace app\common\model;

use think\Model;

class Louti extends Model
{
    protected $insert = ['create_time'];

    protected function setCreateTimeAttr()
    {
        return time();
    }
}
