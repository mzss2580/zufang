<?php

namespace app\common\model;

use think\Loader;
use think\Model;
use think\Db;
use think\Exception;
use think\Request;
use think\Cache;

class AliPay extends Model
{

    // protected $appId = '';//支付宝AppId
    // protected $rsaPrivateKey = '';//支付宝私钥
    // protected $aliPayRsaPublicKey = '';//支付宝公钥
    private $seller = '';

    /*
     * 支付宝支付
     */
    public function aliPay($body, $total_amount, $product_code, $notify_url)
    {
        /**
         * 调用支付宝接口。
         */
        /*import('.Alipay.aop.AopClient', '', '.php');
        import('.Alipay.aop.request.AlipayTradeAppPayRequest', '', '.php');
        Loader::import('Alipay\aop\AopClient', EXTEND_PATH);
        Loader::import('Alipay\aop\request\AlipayTradeAppPayRequest', EXTEND_PATH);*/

        require_once $_SERVER["DOCUMENT_ROOT"]."/../vendor/alipay/aop/AopClient.php";
        require_once $_SERVER["DOCUMENT_ROOT"]."/../vendor/alipay/aop/request/AlipayTradeAppPayRequest.php";

        $aop = new \AopClient();

        $aop->gatewayUrl = "https://openapi.alipay.com/gateway.do";
        $aop->appId = config('alipay.appId'); //填写您的appid
        $aop->rsaPrivateKey = config('alipay.rsaPrivateKey'); //填写您的私钥
        $aop->format = "json";
        $aop->charset = "UTF-8";
        $aop->signType = "RSA2";
        $aop->alipayrsaPublicKey = config('alipay.aliPayRsaPublicKey');  // 填写您的支付宝公钥
        $request = new \AlipayTradeAppPayRequest();
        $arr['body'] = $body;
        $arr['subject'] = $body;
        $arr['out_trade_no'] = $product_code;
        $arr['timeout_express'] = '1d';
        $arr['total_amount'] = floatval($total_amount);
        $arr['product_code'] = 'QUICK_MSECURITY_PAY';

        $json = json_encode($arr);
        $request->setNotifyUrl($notify_url);
        $request->setBizContent($json);

        $response = $aop->sdkExecute($request);
        return $response;

    }


    function createLinkstring($para)
    {
        $arg = "";
        while (list ($key, $val) = each($para)) {
            $arg .= $key . "=" . $val . "&";
        }
        //去掉最后一个&字符
        $arg = substr($arg, 0, count($arg) - 2);

        //如果存在转义字符，那么去掉转义
        if (get_magic_quotes_gpc()) {
            $arg = stripslashes($arg);
        }

        return $arg;
    }


    function argSort($para)
    {
        ksort($para);
        reset($para);
        return $para;
    }
    /**
     * 调用支付宝app支付退款
     * @param $out_trade_no
     * @param $refund_fee
     * @return string
     */
    public function refund($out_trade_no,$refund_fee){

        require_once $_SERVER["DOCUMENT_ROOT"]."/../vendor/alipay/aop/AopClient.php";
        require_once $_SERVER["DOCUMENT_ROOT"]."/../vendor/alipay/aop/SignData.php";
        require_once $_SERVER["DOCUMENT_ROOT"]."/../vendor/alipay/aop/request/AlipayTradeRefundRequest.php";

        $aop = new \AopClient ();
        $aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
        $aop->appId = config('alipay.appId');//“应用ID,填写你的APPID”;
        $aop->rsaPrivateKey = config('alipay.rsaPrivateKey');//"商户私钥，您的原始格式RSA私钥()";
        $aop->alipayrsaPublicKey = config('alipay.aliPayRsaPublicKey');

        $aop->apiVersion = '1.0';
        $aop->signType = "RSA2";
        $aop->postCharset = 'UTF-8';
        $aop->format = "json";
        $request = new \AlipayTradeRefundRequest ();

        //"\"trade_no\":\"2014112611001004680073956707\"," . //支付宝交易号，和商户订单号不能同时为空 特殊可选
        $request->setBizContent("{" .
            "\"out_trade_no\":\"{$out_trade_no}\"," .  //订单支付时传入的商户订单号,不能和 trade_no同时为空。 特殊可选
            "\"refund_amount\":\"{$refund_fee}\"," . //需要退款的金额，该金额不能大于订单金额,单位为元，支持两位小数     必选
            "\"refund_reason\":\"订单取消-退款\"" . //退款的原因说明    正常退款 可选 最后的“，”逗号去掉
            "}");
        $result = $aop->execute($request);
        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        $resultCode = $result->$responseNode->code;
        if(!empty($resultCode) && $resultCode == 10000){
            return "SUCCESS";
        } else {
            return "FAIL";
        }
    }
}