<?php
/**
 * Created by 项目名称.
 * User: 李磊
 * Date: 2022/12/7
 * Time: 15:37
 */

namespace app\common\model;


use think\Model;

class OrderCredit1 extends Model
{
    protected $insert = ['time'];

    /**
     * @name 变更会员积余额
     * @param string $orderno 订单号
     * @param int|object $uid 会员对象或会员ID
     * @param float $money 变更金额
     * @param string $text 日志类型
     */
    static function write($orderno,$uid,$text,$money){
        $data=[
            'orderno'=>$orderno,
            'uid'=>$uid,
            'text'=>$text,
            'money'=>$money,
            'time'=>time()
        ];
        $node=db('OrderCredit1')->insert($data);
    }
}