<?php
namespace app\common\model;

use think\Model;

class SearchKey extends Model
{
    protected $update = ['update_time'];

    protected function setUpdateTimeAttr()
    {
        return time();
    }
}
