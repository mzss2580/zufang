<?php
namespace app\common\model;

use think\Model;

class CommunityNotice extends Model
{
    protected $insert = ['create_time'];

    protected function setCreateTimeAttr()
    {
        return time();
    }
}
