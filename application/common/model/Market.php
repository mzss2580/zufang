<?php
namespace app\common\model;

use think\Model;

class Market extends Model
{
    /**
     *
     */
    static public function change($id,$num){
        $market=self::get(['id'=>$id]);
        $after = $market->surplus + $num;
        if ($after<0){
            return returnMsg(0,'商家挂售剩余易贝不足'.abs($num));
        }else{
            $market->save(['surplus'=>$after]);
            if ($market->surplus==0){
                $market->save(['status'=>3,'true_time'=>time()]);
            }else{
                $market->save(['status'=>2]);

            }
            return  returnMsg(1);
        }

    }
}
