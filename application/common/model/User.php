<?php
namespace app\common\model;

use think\Db;
use think\Model;

class User extends Model
{
    protected $insert = ['create_time'];

    protected function setCreateTimeAttr()
    {
        return time();
    }

    /*
     * 获取上级用户，
     * @param layer:向上查找的层级
     * @param now_layer:目前的层级
     * @param user_arr:用户数组
     *
     * @return user_arr:用户数组
     * */
    static function getSuperiorUser($uid, $layer = 2, $now_layer = 1, $user_arr = [])
    {
        $user = Db::name("user")
            ->where("id", $uid)->find();
        if ($user['id'] > 0 && $layer >= $now_layer) {
            $user_arr[$now_layer] = $user;
            if ($user['invite_user_id'] > 0) {
                $new_layer = $now_layer + 1;
                return self::getSuperiorUser($user['invite_user_id'], $layer, $new_layer, $user_arr);
            } else {
                return $user_arr;
            }
        }
        return $user_arr;
    }

    //余额充值
    public function money_recharge($uid, $money, $pay_type)
    {
        $order_sn = "C" . date('Ymdhis') . rand(10000, 99999);
        //创建订单
        $data = array();
        $data['uid'] = $uid;
        $data['order_sn'] = $order_sn;
        $data['money'] = $money;
        $data['type'] = 1;
        $data['pay_type'] = $pay_type;
        $data['status'] = 0;
        $data['desc'] = '充值余额';
        $data['create_time'] = time();
        $data['update_time'] = time();

        $insert_id = Db::name('pay_log')
            ->insertGetId($data);
        if ($insert_id) {
            //查询订单参数
            $pay_res = $this->pay($order_sn, $money, $pay_type, $uid, $insert_id);
            if ($pay_res['status'] == 1) {
                return returnMsg(1, '下单成功', $pay_res['data']);
            } else {
                return returnMsg(0, $pay_res['msg'], '', $pay_res['status']);
            }
        } else {
            return returnMsg(0, '下单失败');
        }

    }

    //支付验证
    public function pay($order_sn, $money, $pay_type, $uid, $order_id)
    {

        //判断支付方式
        switch ($pay_type) {

            case '1';//如果支付方式为支付宝支付

                //实例化alipay类
                $ali = new AliPay();

                //异步回调地址
                $url = request()->domain() . '/api/notify/ac_notify';

                $res = $ali->alipay('余额充值', $money, $order_sn, $url);

                if ($res) {

                    return returnMsg(1, '下单成功', $res);

                } else {

                    return returnMsg(0, '对不起请检查相关参数!');
                }

                break;

            case '2';

                $wx = new WxPay();//实例化微信支付控制器

                $body = '余额充值';//支付说明

                $out_trade_no = $order_sn;//订单号

                $total_fee = $money * 100;//支付金额(乘以100)

                $notify_url = request()->domain() . '/api/notify/wc_notify';//回调地址

                $res = $wx->getPrePayOrder($body, $out_trade_no, $total_fee, $notify_url);//调用微信支付的方法

                if ($res) {//判断返回参数中是否有prepayid

                    return returnMsg(1, '下单成功', $res);

                } else {

                    return returnMsg(0, '下单失败');

                }

                break;

            case '3'; //用收益充值商城余额
                Db::startTrans();
                try {
                    //减去用户的收益余额
                    $sub = $this->add_user_account_records($uid, 2, 0, bcmul($money, -1, 2), $money, 5, $order_id, 'pay_log', '充值商城余额扣减收益余额');
                    if (!$sub['status']) {
                        throw new \Exception($sub['msg']);
                    }
                    //修改支付记录
                    $up_pay_log = Db::name('pay_log')
                        ->where('id', $order_id)
                        ->update(array('status' => 1, 'update_time' => time()));
                    if (!$up_pay_log) {
                        throw new \Exception('订单异常，请重试。');
                    }

                    $add = $this->add_user_account_records($uid, 1, 1, $money, $money, 5, $order_id, 'pay_log', '充值商城余额增加');
                    if (!$add['status']) {
                        throw new \Exception($add['msg']);
                    }
                    Db::commit();
                    //推送消息 查询推送人员并推送
                    //@$this->search_only_user($uid,'您好,你的充值已到账');

                } catch (\Exception $e) {
                    //回滚事务
                    Db::rollback();
                    trace($e->getMessage());

                    return returnMsg(0, $e->getMessage());
                }
                return returnMsg(1, '下单成功');
                break;
        }
    }

    //查询推送单个用户
    public function search_only_user($uid, $msg)
    {
        $find_user = Db::name('user')
            ->where('id', $uid)
            ->where('is_del', 0)
            ->where('is_back', 0)
            ->field("push_regid")
            ->find();
        if (!empty($find_user)) {
            if ($find_user['push_regid']) {
                //推送消息
                $this->push_user($find_user['push_regid'], $msg);
                //增加消息记录
                $data = array();
                $data['uid'] = $uid;
                $data['title'] = '消息通知';
                $data['content'] = $msg;
                $data['create_time'] = time();
                $inset = Db::name('run_notice')->insert($data);
            }

        }
    }

    /**
     * @param $uid :用户id
     * @param $account_type :账户类型1余额2可提现余额
     * @param $sign :1加0减
     * @param $number :数值
     * @param $order_amount :订单金额
     * @param $from_type :产生方式1.商城订单2.订单退还3.提现4.提现退还5余额充值
     * @param $data_id :相关表的id
     * @param $typstr :相关表名称
     * @param $text :备注
     * @param $union_orderno :合并支付的订单号
     *
     */
    public function add_user_account_records($uid, $account_type, $sign, $number, $order_amount, $from_type, $data_id, $typstr, $text, $union_orderno = '', $admin_id = 0)
    {
        $user = model('user')->get(['id' => $uid, 'is_delete' => 0]);
        if (is_null($user)) {
            return returnMsg(0, '用户信息有误。');
        }
        $balance = $user->balance;        //balance商城余额
        $all_balance = $user->all_balance;    //历史总余额

        $withdraw_money = $user->withdraw_money; //withdraw_money可提现余额
        $history_money = $user->history_money;  //历史总可提现余额

        Db::startTrans();
        try {
            //扣减时判断余额是否充足
            if ($sign == 0) {
                //判断是否够减的
                if ($account_type == 1) {
                    $bc = bcadd($balance, $number, 2);
                    if ($bc < 0) {
                        throw new \Exception("当前余额不足。");
                    }
                } else {
                    $bc = bcadd($withdraw_money, $number, 2);
                    if ($bc < 0) {
                        throw new \Exception("可提现收益余额不足");
                    }
                }
            }

            if ($account_type == 1) { //商城余额
                $user->balance = $now_money = bcadd($balance, $number, 2);
                if ($sign == 1) {
                    $user->all_balance = bcadd($all_balance, $number, 2);
                }
            } else { //可提现余额
                $user->withdraw_money = $now_money = bcadd($withdraw_money, $number, 2);
                if ($sign == 1) {
                    $user->history_money = bcadd($history_money, $number, 2);
                }
            }
            $ru = $user->save();
            if (!$ru) {
                throw new \Exception("用户余额更新失败，请重试。");
            }
            //增加余额变动记录
            $data = array();
            $data['uid'] = $uid;
            $data['account_type'] = $account_type;
            $data['sign'] = $sign;
            $data['number'] = $number;
            $data['now_number'] = $now_money;
            $data['order_amount'] = $order_amount;
            $data['from_type'] = $from_type;
            $data['data_id'] = $data_id;
            $data['typstr'] = $typstr;
            $data['text'] = $text;
            $data['create_time'] = time();
            $data['union_orderno'] = $union_orderno;
            $data['admin_id'] = $admin_id;
            //添加记录
            $insert = Db::name('user_account_records')->insert($data);
            if (!$insert) {
                throw new \Exception("用户余额记录表插入失败。");
            }
            Db::commit();
        } catch (\Exception $e) {
            Db::rollback();
            return returnMsg(0, $e->getMessage());
        }
        return returnMsg(1, '操作成功');
    }

    public function rebate($order)
    {
        $uid = $order->user_id;
        $order_id = $order->id;
        trace('-------------------二级返利开始uid:' . $uid . 'order_id:' . $order_id . '-------------------');
        trace(json_encode($order, true));
        $user = model('user')->get(['id' => $uid]);
        if (is_null($user)) {
            trace('-------------------返利结束：当前订单的用户不存在。-------------------');
            return false;
        }
        //$order = model('order')->get(['id'=>$order_id]);is_null($order) ||$order->user_id != $uid ||
        $order_money = bcsub(bcsub($order->pay_true, $order->refund_amount, 2), $order->express_fee, 2); //返利金额为支付金额减去退款金额再减去运费
        if ($order_money <= 0) {
            trace('-------------------返利结束：订单实付金额小于等于0。-------------------');
            return false;
        }

        if ($user->invite_user_id <= 0) {
            trace('-------------------返利结束：当前用户不存在推荐人。-------------------');
            return false;
        }
        //getSuperiorUser($uid, $layer = 2, $now_layer = 1, $user_arr = [])
        $recommend_arr = self::getSuperiorUser($user->invite_user_id, 2, 1);
        if (count($recommend_arr) < 0) {
            trace('-------------------返利结束：当前用户推荐人信息有误。-------------------');
            return false;
        }
        $one = get_config('one_rate');
        $two = get_config('two_rate');
        $rebate_rate = [1 => $one, 2 => $two];

        foreach ($recommend_arr as $k => $v) {
            //计算返利金额进行返利
            $rebate = bc_mul([$order_money, $rebate_rate[$k], 0.01], 2);
            if ($rebate > 0) {
                //            add_user_account_records($uid, $account_type, $sign, $number, $order_amount, $from_type, $data_id, $typstr, $text, $union_orderno = '')
                $user_phone = substr_replace($user->phone, '****', 3, 4);
                $res = $this->add_user_account_records($v['id'], 2, 1, $rebate, $order_money, 1, $order_id, 'bz_order', '用户(' . $user_phone . ')下单，获得返利.' . $rebate . '元，层级：' . $k);
                if (!$res['status']) {
                    trace('-------------------用户uid:' . $v['id'] . '返利失败，原因:' . $res['msg'] . '-------------------');
                }
            }
        }
        trace('-------------------返利结束-------------------');
        return true;
    }


    /**
     * @name 变更会员积余额
     * @param int|object $id 会员对象或会员ID
     * @param float $money 变更金额
     * @param string $type 变更类型
     * @param string $name 日志类型
     */
    static function capitalRecord($id, $money, $type, $name = '',$order=0)
    {

        $user = self::get(['id' => $id, 'is_delete' => 0]);
//        dump($user);
        if (is_null($user)) {
            return returnMsg(0, '用户信息有误。');
        }
        if ($money == 0) {
            return returnMsg(0, '请输入正确的积分');
        }
        switch ($type) {
            case 1;
                $msg = '购物积分';
                $zd = 'credit1';
                break;
            case 2;
                $msg = '易贝';
                $zd = 'credit2';
                break;
            case 3;
                $msg = '购物积分';
                $zd = 't_credit1';
                break;
            default;
                return returnMsg(0, '类型错误');
        }
        $before = $user->$zd;
        $after = $user->$zd + $money;
        if ($after < 0) {
            return returnMsg(0, '可用' . $msg . '不足');
        }
        $flag = $user->save([$zd => $after]);//变更
        if (!$flag) {
            return returnMsg('0', $msg . '变更失败');
        }


        //会员升级
        if ($type == 1) {
            if ($user->gid == 1 && $after > 2000) {
                $user->save(['gid' => 2]);
            }
            if ($user->gid == 2 && $after > 30000) {
                $user->save(['gid' => 3]);
            }
            //黄金会员及以上升级
            if ($user->gid > 2) {
//                if ($user->invite_user_id != '') {
                $teamss = self::teams([$user->id]);
//                    $teams = explode(',', $teamss);
                $gid = db('user')
                    ->whereIn('id', $teamss)
                    ->max('gid');
                $uid = db('user')
                    ->whereIn('id',$teamss)
                    ->where('gid', $gid)
                    ->value('id');
                $maxCredit = db('user')->where('id', $uid)->value('credit1');
                $credit1 = db('user')
                    ->whereIn('id', $teamss)
                    ->sum('credit1');


                $credit1 = $credit1 - $maxCredit;//减去最大等级会员所有的积分
                if ($credit1 >= 100000 && $user->gid == 3) {//白银晋升黄金
                    $user->save(['gid' => 4]);
                }
                if ($credit1 >= 800000 && $user->gid == 4) {//黄金晋升铂金
                    $user->save(['gid' => 5]);
                }
                if ($credit1 >= 5000000 && $user->gid == 5) {//铂金晋升钻石
                    $user->save(['gid' => 6]);
                }

            }

//            }
        }
        if ($type!=1){
            CapitalRecord::write($id, $msg, $type, $name, $money, $after);

        }
        return returnMsg(1);
    }
    /**
     * @name 获取当前用户的团队 查找上一级
     * @param array $arr 会员对象或会员ID
     *
     */
    public function team($arr = [])
    {
        if (empty(end($arr))) {
            $arr = implode(',', $arr);
            return $arr;
        } else {
            $iid = model('user')
                ->where('id', end($arr))
                ->value('invite_user_id');
            if ($iid) {
                array_push($arr, $iid);
                return self::team($arr);
            } else {
                $arr = array_reverse($arr);
                $arr = implode(',', $arr);
                return $arr;
            }
        }

    }
    /**
     * @name 获取当前用户的团队 查找下一级
     * @param array $arr 会员对象或会员ID
     *
     */
    static function teams($arr = [])
    {
        if (empty(end($arr))) {
            $arr = implode(',', $arr);
            return $arr;
        } else {
            $iid = model('user')
                ->where('invite_user_id', end($arr))
                ->value('id');
            if ($iid) {
                array_push($arr, $iid);
                return self::teams($arr);
            } else {
                $arr = array_reverse($arr);
                $arr = implode(',', $arr);
                return $arr;
            }
        }
    }


    /**
     * @name 获取当前用户的团队 查找上一级
     * @param array $arr 会员对象或会员ID
     *
     */
    public function up_team($arr = [])
    {
        if (empty(end($arr))) {
            $arr = implode(',', $arr);
            return $arr;
        } else {
            $iid = model('user')
                ->where('id', end($arr))
                ->value('invite_user_id');
            if ($iid) {
                array_push($arr, $iid);
                return self::up_team($arr);
            } else {
                return $arr;
            }
        }

    }

    /**
     * $id 消费者id
     * $money 产生积分
     * $type 获得团队奖类型
     */
    public function system($id, float $money,$type=1,$orderno){
        if ($type==1){
            $text='消费者';
        }else{
            $text='商家';
        }

        if ($money<0.5){
        }
        $user=model('user')->get(['id'=>$id]);
        $jf=$money;
        $user_phone=$user->phone;

        if ($user->invite_user_id){//判断有无上一级
            $teams=self::up_team([$user->id]);
            array_shift($teams);

            $team=[];
//                dump($teams);
            for ($i=0;$i<count($teams);$i++){
                $users=model('user')->get(['id'=>$teams[$i]]);

                $Grade =model('grade')->get(['id' => $users->gid]);
                $team[$i]['uid']=$users->id;
                $team[$i]['gid']=$users->gid;
                $team[$i]['dengji']=get_field('grade',['id'=>$users->gid],'name');
                $team[$i]['before']=get_field('grade',['id'=>$users->gid],'integral');//原始奖励百分比
                $team[$i]['after']=floatval($money* $team[$i]['before']/100);//默认之后奖励



                $team[$i]['jf']=floatval($money* $team[$i]['before']/100);
                $team[$i]['title']='直推';//默认奖励名称
                $team[$i]['会员等级']=$team[$i]['dengji'];
                $team[$i]['会员手机号']=$users->phone;
                $team[$i]['会员名称']=$users->nick;
                $team[$i]['pj']=0;//默认奖励名称
//                    $team[$i]['会员原本获得积分奖励']=$team[$i]['after'];
                $team[$i]['会员实际获得积分奖励']=$team[$i]['jf'];
//
            }
            //分发奖励

            for ($x=0;$x<count($team);$x++){
                for ($y=$x;$y<count($team)-1;$y++){
                    if ($team[$y]['gid']>=$team[$y+1]['gid']){//平级奖励
//                            $team[$y+1]['after']=number_format($money* $team[$y+1]['before']/100,2);//默认之后奖励
                        $team[$y+1]['title']='平级';//替换名称
                        $team[$y+1]['会员获得奖励']=$team[$y+1]['title'];
                        $team[$y+1]['pj']=$team[$y]['pj'];
                        if($team[$y+1]['gid']<3){
                            $team[$y+1]['jf']=  0;
                            $team[$y+1]['会员实际获得积分奖励']= $team[$y+1]['jf'];
                            $team[$y+1]['pj']=$team[$y]['pj'];
                        }else{
                            if ( $team[$y+1]['pj']==0){
                                $team[$y+1]['pj']= 1;
//                                    $team[$y+1]['jf']=number_format($money* $team[$y+1]['before']/100,2)*0.1;
                                $team[$y+1]['jf']=floatval($team[$y]['jf'])*0.1;
                                $team[$y+1]['会员实际获得积分奖励']= $team[$y+1]['jf'];


                            }else{
                                $team[$y+1]['pj']=  $team[$y]['pj'];
                                $team[$y+1]['jf']=0;
                                $team[$y+1]['会员实际获得积分奖励']= $team[$y+1]['jf'];
                            }
                        }
                    }else{
                        $team[$y+1]['pj']=$team[$y]['pj'];
//                            $team[$y]['after']=abs($team[$y+1]['before']-$team[$y]['before']);//极差奖励计算
//                            $team[$y+1]['jf']= $money* $team[$y+1]['before']/100-floatval($team[$y]['jf']);
                        if ( $team[$y]['jf']==0){
                            $team[$y+1]['jf']= $money*$team[$y+1]['before']/100;
                        }else{
                            $team[$y+1]['jf']= $money*($team[$y+1]['before']-$team[$y]['before'])/100;
                        }


                        $team[$y+1]['title']='极差'; //替换名称
                        $team[$y+1]['会员获得奖励']=$team[$y+1]['title'];
                        $team[$y+1]['会员实际获得积分奖励']= $team[$y+1]['jf'];
                    }
                }
            }
//                dump($team);
//                die();
            foreach ($team as $i=>$k){
                if($team[$i]['jf']>0){
//                        dump($team[$i]['jf']);

                    $team[$i]['jf']=floatval($team[$i]['jf']);
                    OrderCredit1::write($orderno,$team[$i]['uid'],'订单：'.$orderno.'支付获得'.'推荐'.$text.':'.$user_phone.',获得'.$team[$i]['title'].'奖励',$team[$i]['jf']);
                    self::capitalRecord($team[$i]['uid'],$team[$i]['jf'],1,'订单：'.$orderno.'支付获得'.'推荐'.$text.':'.$user_phone.',获得'.$team[$i]['title'].'奖励');
                    self::capitalRecord($team[$i]['uid'],$team[$i]['jf'],3,'订单：'.$orderno.'支付获得'.'推荐'.$text.':'.$user_phone.',获得'.$team[$i]['title'].'奖励');

                };
            }

        }

    }


    /**
     * 打赏or易债代理分佣奖励
     *
     * @name int $id 下单用户
     * @param float $money 积分
     * @param int $falg 类型默认为打赏
     */
    public function agent_gard($id, float $money, int $falg,$orderno=0){
        switch ($falg){
            case 1;
                $type='打赏';
                $text='商家';
                break;
            case 2;
                $type='打赏';
                $text='消费者';
                break;
            case 3;
                $type='易债';
                $text='债权人';
                break;
            case 4;
                $type='易债';
                $text='债务人';
                break;
            case 5;
                $type='购物';
                $text='消费者';
                break;
            case 6;
                $type='购物';
                $text='商家';
                break;
        }
        if ($type==5||$type==6){
            $order=model('order')->get(['orderno'=>$orderno]);
            $user=model('address')->get(['id'=>$order->address_id]);
            $street=$user->s_id;
            $area=$user->a_id;
            $city=$user->c_id;
            $province=$user->p_id;
        }else{
            $user=model('user')->get($id);
            $street=$user->street;
            $area=$user->area;
            $city=$user->city;
            $province=$user->province;

        }
//        $phone=$user
        $agent=[];
        //获取用户地址信息
        //街道代理
        $area_agent=  Db::name('agent')->where(['street'=>$street,'region'=>1])->field('uid,region,proportion,province,city,area,street')->select();
        if ($area_agent){
            foreach ($area_agent as $item){
                array_push($agent,$item);
            }
        }
        //区级代理
        $area_agent=  Db::name('agent')->where(['area'=>$area,'region'=>2])->field('uid,region,proportion,province,city,area,street')->select();
        if ($area_agent){
            foreach ($area_agent as $item){
                array_push($agent,$item);
            }
        }
        //市级代理
        $city_agent=  Db::name('agent')->where(['city'=>$city,'region'=>3])->field('uid,region,proportion,province,city,area,street')->select();
        if ($city_agent){
            foreach ($city_agent as $item){
                array_push($agent,$item);
            }
        }
        //1
        //省级级代理
        $province_agent=  Db::name('agent')->where(['province'=>$province,'region'=>4])->field('uid,region,proportion,province,city,area,street')->select();
        if ($province_agent){
            foreach ($province_agent as $item){
                array_push($agent,$item);
            }
        }
        //合并数组
        foreach ($agent as $item=>$k){
            $agent[$item]['jf']=$money*floatval($k['proportion'])/100;
            $agent[$item]['pj']=0;
            $agent[$item]['title']='';
            $agent[$item]['before']=floatval($k['proportion']);
            $agent[$item]['garde']=get_field('agent_grade',['id'=>$k['region']],'name');
            switch ($k['region']){
                case 1;
                    $agent[$item]['regions']=get_field('city',['id'=>$k['province']],'name').'-'.get_field('city',['id'=>$k['city']],'name').'-'.get_field('city',['id'=>$k['area']],'name').'-'.get_field('city',['id'=>$k['street']],'name');
                    break;
                case 2;
                    $agent[$item]['regions']=get_field('city',['id'=>$k['province']],'name').'-'.get_field('city',['id'=>$k['city']],'name').'-'.get_field('city',['id'=>$k['area']],'name');
                    break;
                case 3;
                    $agent[$item]['regions']=get_field('city',['id'=>$k['province']],'name').'-'.get_field('city',['id'=>$k['city']],'name');
                    break;
                case 4;
                    $agent[$item]['regions']=get_field('city',['id'=>$k['province']],'name');
                    break;
            }
        }
        //冒泡计算实际奖励
        for ($x=0;$x<count($agent);$x++){
            for ($y=$x;$y<count($agent)-1;$y++){
                if ($agent[$y]['region']<$agent[$y+1]['region']) {
//                        $agent[$y+1]['pj']=1;
                    $agent[$y + 1]['jf'] = $money*floatval( $agent[$y + 1]['proportion']-$agent[$y]['proportion'])/100;
                    $agent[$y + 1]['title'] = '级差';
                }
            }
        }

        foreach ($agent as $i=>$k){
            if($agent[$i]['jf']>0){
                $agent[$i]['jf']=floatval($agent[$i]['jf']);
                if ($orderno){
                    OrderCredit1::write($orderno,$agent[$i]['uid'],'通过'.$user->phone.$type.',该用户为'.$text.'区域的:'.$agent[$i]['regions'].'的'.$agent[$i]['garde'].'获得'.$agent[$i]['garde'].'的'.$agent[$i]['title'].'奖励',$agent[$i]['jf']);
                }
                \app\common\model\User::capitalRecord($agent[$i]['uid'],$agent[$i]['jf'],3,'通过'.$user->phone.$type.',该用户为'.$text.'区域的:'.$agent[$i]['regions'].'的'.$agent[$i]['garde'].'获得'.$agent[$i]['garde'].'的'.$agent[$i]['title'].'奖励');
                \app\common\model\User::capitalRecord($agent[$i]['uid'],$agent[$i]['jf'],1);
            };
        }



    }





    public function up($id,$after){
        //会员升级
//            if ($type == 1)
        $user=self::get(['id'=>$id]);
        if ($user->gid == 1 && $after > 2000) {
            $user->save(['gid' => 2]);
        }
        if ($user->gid == 2 && $after > 30000) {
            $user->save(['gid' => 3]);
        }
        //黄金会员及以上升级
        if ($user->gid > 2) {
            if ($user->invite_user_id != '') {
                $teamss = self::team([$user->invite_user_id]);
                $teams = explode(',', $teamss);
                $gid = db('user')
                    ->whereIn('id', $teamss)
                    ->max('gid');
                $uid = db('user')
                    ->where('gid', $gid)
                    ->value('id');
                $maxCredit = db('user')->where('id', $uid)->value('credit1');
                $credit1 = db('user')
                    ->whereIn('id', $teamss)
                    ->sum('credit1');
                $credit1 = $credit1 - $maxCredit;//减去最大等级会员所有的积分
                if ($credit1 >= 100000 && $user->gid == 3) {//白银晋升黄金
                    $user->save(['gid' => 4]);
                }
                if ($credit1 >= 800000 && $user->gid == 4) {//黄金晋升铂金
                    $user->save(['gid' => 5]);
                }
                if ($credit1 >= 5000000 && $user->gid == 5) {//铂金晋升钻石
                    $user->save(['gid' => 6]);
                }

            }

        }
    }

}
