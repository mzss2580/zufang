<?php
namespace app\common\model;

use think\Db;
use think\Model;

class Order extends Model
{
    protected $insert = ['create_time'];

    protected function setCreateTimeAttr()
    {
        return time();
    }

    static public function success_union($union_orderno, $pay_type)
    {
        $orders = Order::all(['union_orderno' => $union_orderno, 'pay_type' => $pay_type, 'status' => 0]);
        if (!empty($orders)) {
            foreach ($orders as $order) {
                Order::order_sale($order);
            }
        }
    }

    static public function success_order($orderno, $pay_type)
    {
        $order = Order::get(['orderno' => $orderno, 'pay_type' => $pay_type, 'status' => 0]);
        if (!is_null($order)) {
            self::order_sale($order, 1);
        }
    }
    static public function order_sale($order, $pay_result = 0)
    {
        $order->save(['status' => 1, 'pay_result' => $pay_result, 'pay_status' => 1, 'pay_time' => time()]);
        //根据订单反积分金额，进行反积分
        $user_model = new User();
        if($order->integral > 0){
            $user_model->capitalRecord($order->user_id,$order->integral,1,'订单：'.$order->orderno.'支付获得');
            $user_model->capitalRecord($order->user_id,$order->integral,3,'订单：'.$order->orderno.'支付获得');


            //购物成功消费者团队获得奖励11
            $user_model->system($order->user_id,$order->integral,1,$order->orderno);
            $user_model->agent_gard($order->user_id,$order->integral,5,$order->orderno);
        }
        //判断用户是否消费过，
        $user = db('user')->where('id', $order->user_id)->find();
        if($user['is_used'] == 0){
            db('user')->where('id', $order->user_id)->update(['is_used'=>1]); //记录用户已消费
            //赠送500积分
            $user_model->capitalRecord($order->user_id,500,1,'首次消费赠送积分');
            $user_model->capitalRecord($order->user_id,500,3,'首次消费赠送积分');
        }

        $order_goods = db('order_goods')->where('order_id', $order->id)->group('goods_id')->column('sum(amount) as amount', 'goods_id');
        foreach ($order_goods as $goods_id => $amount) {
            db('goods')->where('id', $goods_id)->setInc('sale', $amount);
        }
    }


    static public function order_finish_handle($order){
        trace('-------------------用户确认收货-------------------');
        trace(json_encode($order));
        Db::startTrans();
        try {
            if ($order->shop_id > 0) {
                //购买加盟商的商品，查询店铺抽成比例，扣除平台抽成
                $shop = model('shop')->get(['id' => $order->shop_id]);

                $order_money = bcsub($order->pay_true, $order->refund_amount, 2); //订单金额-退款金额=收益金额 100-20=80

                $platform_rate = $shop->commission_rate ? bcdiv($shop->commission_rate, 100, 2) : 0; //平台抽成比率整数 2/100 = 0.02 百分之2

                $platform_profit = bcmul($order_money, $platform_rate, 2); //平台抽成金额 订单金额x平台抽成比率小数 100x0.02 = 2元;
                //平台抽成start
                if($platform_profit > 0){
                    $platform_up = db('system_config')->where('config', 'in', ['shop_money', 'shop_history_money'])
                        ->update(['value' => Db::raw('value+' . $platform_profit)]);
                    if(!$platform_up){
                        throw new \Exception("平台收益修改失败。");
                    }
                    $shop_wallet = new ShopWallet();
                    $platform_wallet = $shop_wallet->save([
                        'user_id'      => $order->user_id,
                        'shop_id'      => 0,
                        'type'         => 1,
                        'money'        => $platform_profit,
                        'order_id'     => $order->id,
                    ]);
                    if(!$platform_wallet){
                        throw new \Exception("平台收益流水记录失败。");
                    }
                    //平台抽成end
                }

                $shop_profig = bcsub($order_money, $platform_profit, 2); //商家所得金额 订单金额-平台抽成金额 100-2 = 98元
                if($shop_profig > 0){
                    //商家所得start
                    $shop_up = db('shop')->where('id', $order->shop_id)->update([
                        'withdraw_money' => Db::raw('withdraw_money+' . $shop_profig),
                    ]);
                    if(!$shop_up){
                        throw new \Exception("商户收益修改失败。");
                    }
                    $shop_wallet = new ShopWallet();
                    $shop_w = $shop_wallet->save([
                        'user_id'      => $order->user_id,
                        'shop_id'      => $order->shop_id,
                        'type'         => 1,
                        'money'        => $shop_profig,
                        'order_id'     => $order->id,
                        'platform_profit' => $platform_profit, // 记录平台抽成金额
                    ]);
                    if(!$shop_w){
                        throw new \Exception("商户收益流水记录失败。");
                    }
                    //商家所得end
                }
                //购买的是加盟商的商品结束
            } else {
                //购买平台的商品
                $sys_up = db('system_config')->where('config', 'in', ['shop_money', 'shop_history_money'])
                    ->update(['value' => Db::raw('value+' . $order->pay_true)]);
                if(!$sys_up){
                    throw new \Exception("商户收益修改失败。");
                }
                $shop_wallet = new ShopWallet();
                $shop_w = $shop_wallet->save([
                    'user_id'      => $order->user_id,
                    'shop_id'      => $order->shop_id,
                    'type'         => 1,
                    'money'        => $order->pay_true,
                    'order_id'     => $order->id,
                ]);
                if(!$shop_w){
                    throw new \Exception("商户收益修改失败。");
                }
            }
            Db::commit();
        }catch(\Exception $e){
            Db::rollback();
            trace('失败：'.$e->getMessage());
            return returnMsg(0, '确认收货失败，请重试');
        }

        //二级分销返利
        $users = new User();
        $users->rebate($order);
        return returnMsg(1,'确认收货成功');
    }
}
