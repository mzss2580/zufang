<?php
/**
 * Created by 项目名称.
 * User: 李磊
 * Date: 2023/7/6
 * Time: 9:08
 */

namespace app\common\model;

use think\Model;

class HuanXin extends Model
{
    /**
     * 初始化参数
     *
     * @param array $options
     * @param $options['client_id']
     * @param $options['client_secret']
     * @param $options['org_name']
     * @param $options['app_name']
     */
    public function __construct() {
        if (! empty (config('huanxin.org_name') ) && ! empty (  config('huanxin.app_name') )) {
            $this->url = config('huanxin.url') .config('huanxin.org_name') . '/' .  config('huanxin.app_name') . '/';
        }
    }
    /**
     *获取token
     */
    public function getToken()
    {
        $options=array(
            "grant_type"=>"client_credentials",
            "client_id"=>config('huanxin.client_id'),
            "client_secret"=>config('huanxin.client_secret')
        );
        //json_encode()函数，可将PHP数组或对象转成json字符串，使用json_decode()函数，可以将json字符串转换为PHP数组或对象
        $body=json_encode($options);
        //使用 $GLOBALS 替代 global
        $url=$this->url.'token';
        //$url=$base_url.'token';
        $tokenResult = $this->postCurl($url,$body,$header=array());
        //return $tokenResult;
        return "Authorization:Bearer ".$tokenResult['access_token'];
    }
    /**
    授权注册
     */                     //123456
    //gt123456
    public function createUser($username,$password){
        $url=$this->url.'users';
        $options=array(
            "username"=>$username,
            "password"=>$password
        );
        $body=json_encode($options);
        $header=array($this->getToken());
        $result=$this->postCurl($url,$body,$header);
        return $result;
    }
    /*
    批量注册用户
    */
    public function createUsers($options){
        $url=$this->url.'users';
        $body=json_encode($options);
        $header=array($this->getToken());
        $result=$this->postCurl($url,$body,$header);
        return $result;
    }
    /*
    重置用户密码
    */
    public function resetPassword($username,$newpassword){
        $url=$this->url.'users/'.$username.'/password';
        $options=array(
            "newpassword"=>$newpassword
        );
        $body=json_encode($options);
        $header=array($this->getToken());
        $result=$this->postCurl($url,$body,$header,"PUT");
        return $result;
    }
    /*
            修改用户昵称
        */
    public function editNickname($username,$nickname){
        $url=$this->url.'users/'.$username;
        $options=array(
            "nickname"=>$nickname
        );
        $body=json_encode($options);
        $header=array($this->getToken());
        $result=$this->postCurl($url,$body,$header,'PUT');
        return $result;
    }
    /*
    获取单个用户
    */
    function getUser($username){
        $url=$this->url.'users/'.$username;
        $header=array($this->getToken());
        $result=$this->postCurl($url,'',$header,"GET");
        return $result;
    }
    /*
    删除单个用户
    */
    public function deleteUser($username){
        $url=$this->url.'users/'.$username;
        $header=array($this->getToken());
        $result=$this->postCurl($url,'',$header,'DELETE');
        return $result;
    }
    /*
    删除批量用户
    limit:建议在100-500之间，、
    注：具体删除哪些并没有指定, 可以在返回值中查看。
    */
    function deleteUsers($limit){
        $url=$this->url.'users?limit='.$limit;
        $header=array($this->getToken());
        $result=$this->postCurl($url,'',$header,'DELETE');
        return $result;
    }
    /*
    查看用户是否在线
    */
    public function isOnline($username){
        $url=$this->url.'users/'.$username.'/status';
        $header=array($this->getToken());
        $result=$this->postCurl($url,'',$header,'GET');
        return $result;
    }
    /*
    查看用户离线消息数
    */
    function getOfflineMessages($username){
        $url=$this->url.'users/'.$username.'/offline_msg_count';
        $header=array($this->getToken());
        $result=$this->postCurl($url,'',$header,'GET');
        return $result;
    }
    /*
    查看某条消息的离线状态
    ----deliverd 表示此用户的该条离线消息已经收到
    */
    function getOfflineMessageStatus($username,$msg_id){
        $url=$this->url.'users/'.$username.'/offline_msg_status/'.$msg_id;
        $header=array($this->getToken());
        $result=$this->postCurl($url,'',$header,'GET');
        return $result;
    }
    /*
     *禁用用户账号
     */
    public function deactiveUser($username){
        $url=$this->url.'users/'.$username.'/deactivate';
        $header=array($this->getToken());
        $result=$this->postCurl($url,'',$header);
        return $result;
    }
    /*
    解禁用户账号
    */
    public function activeUser($username){
        $url=$this->url.'users/'.$username.'/activate';
        $header=array($this->getToken());
        $result=$this->postCurl($url,'',$header);
        return $result;
    }
    /*
    强制用户下线
    */
    public function disconnectUser($username){
        $url=$this->url.'users/'.$username.'/disconnect';
        $header=array($this->getToken());
        $result=$this->postCurl($url,'',$header,'GET');
        return $result;
    }
//--------------------------------------------------------发送消息
    /*
    发送文本消息
    */
    public function sendText($from="admin",$target_type,$target,$content,$ext){
        $url=$this->url.'messages';
        $body['target_type']=$target_type;
        $body['target']=$target;
        $options['type']="txt";
        $options['msg']=$content;
        $body['msg']=$options;
        $body['action'] = 'senddiv';
        $body['from']=$from;
        $body['ext']=$ext;
        $b=json_encode($body);
        $header=array($this->getToken());
        $result=$this->postCurl($url,$b,$header);
//        $result=$this->postCurl($url,$body,$header);
        //dump($result);
        return $result;
    }
    public function postCurl($url,$body,$header,$type="POST"){
        //1.创建一个curl资源
        $ch = curl_init();
        //2.设置URL和相应的选项
        curl_setopt($ch,CURLOPT_URL,$url);//设置url
        //1)设置请求头
        //array_push($header, 'Accept:application/json');
        //array_push($header,'Content-Type:application/json');
        //array_push($header, 'http:multipart/form-data');
        //设置为false,只会获得响应的正文(true的话会连响应头一并获取到)
        curl_setopt($ch,CURLOPT_HEADER,0);
//        curl_setopt ( $ch, CURLOPT_TIMEOUT,5); // 设置超时限制防止死循环
        //设置发起连接前的等待时间，如果设置为0，则无限等待。
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,5);
        //将curl_exec()获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //2)设备请求体
        if (strlen($body)>0) {
            //$b=json_encode($body,true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);//全部数据使用HTTP协议中的"POST"操作来发送。
        }
        //设置请求头
        if(count($header)>0){
            curl_setopt($ch,CURLOPT_HTTPHEADER,$header);
        }
        //上传文件相关设置
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// 对认证证书来源的检查
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);// 从证书中检查SSL加密算
        //3)设置提交方式
        switch($type){
            case "GET":
                curl_setopt($ch,CURLOPT_HTTPGET,true);
                break;
            case "POST":
                curl_setopt($ch,CURLOPT_POST,true);
                break;
            case "PUT"://使用一个自定义的请求信息来代替"GET"或"HEAD"作为HTTP请求。这对于执行"DELETE" 或者其他更隐蔽的HTT
                curl_setopt($ch,CURLOPT_CUSTOMREQUEST,"PUT");
                break;
            case "DELETE":
                curl_setopt($ch,CURLOPT_CUSTOMREQUEST,"DELETE");
                break;
        }
        //4)在HTTP请求中包含一个"User-Agent: "头的字符串。-----必设
//        curl_setopt($ch, CURLOPT_USERAGENT, 'SSTS Browser/1.0');
//        curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
        curl_setopt ( $ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0)' ); // 模拟用户使用的浏览器
        //5)
        //3.抓取URL并把它传递给浏览器
        $res=curl_exec($ch);
        $result=json_decode($res,true);
        //4.关闭curl资源，并且释放系统资源
        curl_close($ch);
        if(empty($result))
            return $res;
        else
            return $result;
    }

}