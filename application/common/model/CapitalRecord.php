<?php
/**
 * Created by 项目名称.
 * User: 李磊
 * Date: 2022/9/6
 * Time: 16:14
 */

namespace app\common\model;

use think\Model;

class CapitalRecord extends Model
{
    //1
    protected $insert = ['create_time'];

    protected function setCreateTimeAttr()
    {
        return time();
    }

    static function write($id,$msg,$type,$name='',$money,$after){
        if ($money>0){
            $m=$money;
            $text='新增';
        }else{
            $m=abs($money);
            $text='减少';
        }
        $data=[
            'uid'=>$id,
            'content'=>$msg.'通知,当前账户'.$name.$text.$msg.':'.$m.',剩余'.$msg.':'.$after.'',
            'type'=>$type,
            'text'=>$name,
            'money'=>$money,
            'create_time'=>time()
        ];
        $node=model('CapitalRecord')->insert($data);
        if (!$node){
            return returnMsg('0','日志插入失败');
        }else{
            return returnMsg('1','日志插入失败');
        }
    }

}