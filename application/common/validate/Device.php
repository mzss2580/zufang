<?php
namespace app\common\validate;

use think\Validate;

class Device extends Validate
{
    private $tn = 'device';

    protected $rule = [
        'deviceno'         => 'require',
        'modelno'          => 'require|max:20',
        'name'             => 'require|max:20',
        'area'             => 'require|max:20',
        'pictureratio'     => 'require|between:0,100',
        'pwd'              => 'number|length:6',
        'volume'           => 'require|between:0,100',
        'light_bright'     => 'require|between:0,100',
        'light_opentime'   => 'requireIf:light_mode,3',
        'light_closetime'  => 'requireIf:light_mode,3',
        'recog_threshold'  => 'require|between:0,100',
        'white_threshold'  => 'require|between:30,100',
        'recog_lnterval'   => 'require|between:1,255',
        'door_lnterval'    => 'require|between:1,255',
        'living_threshold' => 'require|between:0,10',
        'advert_time'      => 'require|between:1,255',
        'advert_title'     => 'max:20',
        'restart_time'     => 'require',
        'delay_alam_value' => 'require|between:0,1800',
    ];

    protected $message = [
        'deviceno.require'          => '请填写设备编号',
        'modelno.require'           => '请填写设备型号',
        'modelno.max'               => '设备型号最长20位',
        'name.require'              => '请填写设备名称',
        'name.max'                  => '设备名称最长20位',
        'area.require'              => '请填写安装位置',
        'area.max'                  => '安装位置最长20位',
        'pictureratio.require'      => '请填写图片压缩比例',
        'pictureratio.between'      => '图片压缩比例需在0-100之间',
        'pwd.number'                => '设备密码只能为6位数字',
        'pwd.length'                => '设备密码只能为6位数字',
        'volume.require'            => '请填写设备音量',
        'volume.between'            => '设备音量需在0-100之间',
        'light_bright.require'      => '请填写设备补光灯亮度',
        'light_bright.between'      => '设备补光灯亮度需在0-100之间',
        'light_opentime.requireIf'  => '设备补光灯开启时间 "自定时间段"时必须设置',
        'light_closetime.requireIf' => '设备补光灯开启时间 "自定时间段"时必须设置',
        'recog_threshold.require'   => '请填写设备识别通用阈值',
        'recog_threshold.between'   => '设备识别通用阈值需在0-100之间',
        'white_threshold.require'   => '请填写设备识别白名单阈值',
        'white_threshold.between'   => '设备识别白名单阈值需在30-100之间',
        'recog_lnterval.require'    => '请填写设备识别间隔',
        'recog_lnterval.between'    => '设备识别间隔值需在1-255s之间',
        'door_lnterval.require'     => '请填写门禁开门延时',
        'door_lnterval.between'     => '门禁开门延时需在1-255s之间',
        'living_threshold.require'  => '请填写活体识别阈值',
        'living_threshold.between'  => '活体识别阈值需在0-10之间',
        'advert_time.require'       => '请填写广告切换时间',
        'advert_time.between'       => '广告切换时间需在1-255s之间',
        'advert_title.max'          => '广告标语最长20位',
        'restart_time.require'      => '请设置设备重启时间点',
        'delay_alam_value.require'  => '请填写门磁报警延时时间',
        'delay_alam_value.between'  => '门磁报警延时时间需在0~1800s之间',
    ];

    protected $scene = [
        'edit' => [
            'deviceno',
            'modelno',
            'name',
            'area',
            'pictureratio',
            'pwd',
            'volume',
            'light_bright',
            'light_opentime',
            'light_closetime',
            'recog_threshold',
            'white_threshold',
            'recog_lnterval',
            'door_lnterval',
            'living_threshold',
            'advert_time',
            'advert_title',
            'restart_time',
            'delay_alam_value',
        ],
    ];
}
