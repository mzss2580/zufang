<?php
namespace app\common\validate;

use think\Validate;

class Admin extends Validate
{
    private $tn = 'admin';

    protected $rule = [
        'username' => 'require|unique_username',
    ];

    protected $message = [
        'username.require' => '用户名为必填项',
    ];

    protected $scene = [
        'edit' => [
            'username',
        ],
    ];

    public function unique_username($value, $rule, $data = [])
    {
        $db = db($this->tn)->where('username', $value);
        if ($data['id'] > 0) {
            $db->where('id', '<>', $data['id']);
        }
        if (is_null($db->find())) {
            return true;
        }
        return '用户名已经存在';
    }
}
