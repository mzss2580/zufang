<?php
namespace app\common\validate;

use think\Validate;

class FalseShop extends Validate
{
    private $tn = 'false_shop';

    protected $rule = [
    ];

    protected $message = [
    ];
    
    protected $scene = [
        'edit' => [
        ],
    ];
}
