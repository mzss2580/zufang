<?php
namespace app\common\validate;

use think\Validate;

class Personnel extends Validate
{
    private $tn = 'personnel';

    protected $rule = [
        'name'   => 'require',
        'idcard' => 'require|idCard',
        'sex'    => 'require',
    ];

    protected $message = [
        'name.require'   => '请填写姓名',
        'idcard.require' => '请填写身份证号',
        'idcard.idCard'  => '请输入正确的身份证号',
        'sex.require'    => '请选择性别',
    ];

    protected $scene = [
        'edit'        => [
            'name',
            'idcard',
            'sex',
        ],
        'profile'     => [
            'name',
            'idcard',
        ],
        'family_edit' => [
            'name',
            'idcard',
            'sex',
        ],
        'excel'       => [
            'name',
            'idcard',
        ],
        'profile_new' => [
            'name',
            'sex',
        ],
    ];
}
