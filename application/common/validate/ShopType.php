<?php
namespace app\common\validate;

use think\Validate;

class ShopType extends Validate
{
    private $tn = 'shop_type';

    protected $rule = [
    ];

    protected $message = [
    ];
    
    protected $scene = [
        'edit' => [
        ],
    ];
}
