<?php
namespace app\common\validate;

use think\Validate;

class Market extends Validate
{
    private $tn = 'market';

    protected $rule = [
        'uid' => 'require',
        'num' => 'require',
        'price' => 'require|number',
        'phone' => 'require',
        'type' => 'require',
        'collection_code' => 'require',
//        'status' => 'require|number',
    ];

    protected $message = [
        'uid.require' => '挂卖用户id为必填项',
        'num.require' => '易贝数量为必填项',
        'price.require' => '挂卖价格为必填项',
        'price.number' => '挂卖价格只能填写数字',
        'phone.require' => '手机号为必填项',
        'type.require' => '二维码类型：1微信2支付宝为必填项',
        'collection_code.require' => '
收款码为必填项',
        'status.require' => '挂售状态：1挂售中，2已出售，3已完成为必填项',
        'status.number' => '挂售状态：1挂售中，2已出售，3已完成只能填写数字',
    ];
    
    protected $scene = [
        'edit' => [
            'uid',
            'num',
            'price',
            'phone',
//            'type',
//            'collection_code',
            'status',
        ],
    ];
}
