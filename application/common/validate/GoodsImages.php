<?php
namespace app\common\validate;

use think\Validate;

class GoodsImages extends Validate
{
    protected $rule = [
        'image' => 'require',
    ];
    protected $message = [
        'image.require' => '图片必须上传',
    ];
    protected $scene = [
        'edit' => [
            'image',
        ],
    ];
}
