<?php
namespace app\common\validate;

use think\Validate;

class SpecsType extends Validate
{
    protected $rule = [
        'name' => 'require',
    ];
    protected $message = [
        'name.require' => '类型名称为必填项',
    ];
    protected $scene = [
        'edit' => [
            'name',
        ],
    ];
}
