<?php
namespace app\common\validate;

use think\Validate;

class Specs extends Validate
{
    protected $rule = [
        'name' => 'require',
    ];
    protected $message = [
        'name.require' => '规格名称为必填项',
    ];
    protected $scene = [
        'edit' => [
            'name',
        ],
    ];
}
