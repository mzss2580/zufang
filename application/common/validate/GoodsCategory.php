<?php
namespace app\common\validate;

use think\Validate;

class GoodsCategory extends Validate
{
    private $tn = 'goods_category';

    protected $rule = [
        'name' => 'require',
    ];

    protected $message = [
        'name.require' => '名称为必填项',
    ];
    
    protected $scene = [
        'edit' => [
            'name',
        ],
    ];
}
