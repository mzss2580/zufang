<?php
namespace app\common\validate;

use think\Validate;

class OrderRefund extends Validate
{
    protected $rule = [
        'refund_type'     => 'require|in:0,1',
        'goods_state'     => 'require|in:0,1',
        'reason'          => 'require',
        'explain'         => 'requireIf:reason,其他',
        'express_company' => 'require',
        'expressno'       => 'require',
    ];
    protected $message = [
        'refund_type.require'     => '请选择退款方式',
        'refund_type.in'          => '请选择退款方式',
        'goods_state.require'     => '请选择货物状态',
        'goods_state.in'          => '请选择货物状态',
        'reason.require'          => '请选择退款原因',
        'explain.requireIf'       => '请填写退款说明',
        'express_company.require' => '请填写快递公司',
        'expressno.require'       => '请填写快递单号',
    ];
    protected $scene = [
        'apply' => [
            'refund_type',
            'goods_state',
            'reason',
            'explain',
        ],
        'send'  => [
            'express_company',
            'expressno',
        ],
    ];
}
