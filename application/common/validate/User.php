<?php
namespace app\common\validate;

use think\Validate;

class User extends Validate
{
    private $tn = 'user';

    protected $rule = [
        'phone'    => 'require|mobile',
        'pwd'      => 'require|length:6,20',
        'npwd'     => 'require|length:6,20',
        'code'     => 'require|number|length:6',
        'person'   => 'require',
        'province' => 'require',
        'city'     => 'require',
        'district' => 'require',
        'addr'     => 'require',
    ];

    protected $message = [
        'phone.require' => '请输入手机号',
        'phone.mobile'  => '请输入正确的手机号',
        'pwd.require'   => '请输入密码',
        'pwd.length'    => '请输入6-20位密码',
        'npwd.require'  => '请输入新密码',
        'npwd.length'   => '请输入6-20位新密码',
        'code.require'  => '请输入验证码',
        'code.number'   => '验证码只能为数字',
        'code.length'   => '验证码只能为6位数字',
        'person'        => '请填写收货人',
        'province'      => '请正确选择所在地区',
        'city'          => '请正确选择所在地区',
        'district'      => '请正确选择所在地区',
        'addr'          => '请填写详细地址',
    ];

    protected $scene = [
        'yzm'      => [
            'phone',
        ],
        'code'     => [
            'phone',
            'code',
        ],
        'login'    => [
            'phone',
            'pwd',
        ],
        'pwd'      => [
            'pwd',
            'npwd',
        ],
        'register' => [
            'phone',
            'pwd',
            'code',
        ],
        'address'  => [
            'person',
            'phone',
            'province',
            'city',
            'district',
            'addr',
        ],
        'admin_register' => [
            'phone',
            'pwd',
        ],
    ];
}
