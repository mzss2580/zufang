<?php
namespace app\common\validate;
use think\Validate;
class MarketConfig extends Validate
{
    private $tn = 'market_config';
    protected $rule = [
        'price' => 'require|float',
    ];
    protected $message = [
        'price.require' => '价格为必填项',
        'price.number' => '价格只能填写数字',
    ];

    protected $scene = [
        'edit' => [
            'price',
        ],
    ];
}