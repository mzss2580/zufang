<?php
namespace app\common\validate;

use think\Validate;

class MarketPond extends Validate
{
    private $tn = 'market_pond';

    protected $rule = [
        't_credi2' => 'require',
        'sum_credit2' => 'require',
        't_yb' => 'require',
//        'sum_yb' => 'require',
    ];

    protected $message = [
        't_credi2.require' => '今日销毁易贝数为必填项',
        't_credi2.number' => '今日销毁易贝数只能填写数字',
        'sum_credit2.require' => '全网销毁为必填项',
        'sum_credit2.number' => '全网销毁只能填写数字',
        't_yb.require' => '每天发放的易贝为必填项',
        't_yb.number' => '每天发放的易贝只能填写数字',
        'sum_yb.require' => '总共发放易贝量为必填项',
        'sum_yb.number' => '总共发放易贝量只能填写数字',
    ];
    
    protected $scene = [
        'edit' => [
            't_credi2',
            'sum_credit2',
            't_yb',
            'sum_yb',
        ],
    ];
}
