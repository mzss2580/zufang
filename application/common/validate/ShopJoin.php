<?php
namespace app\common\validate;

use think\Validate;

class ShopJoin extends Validate
{
    private $tn = 'shop_join';

    protected $rule = [
        'duration' => 'require',
        'price' => 'require|number',
    ];

    protected $message = [
        'duration.require' => '时长(月)为必填项',
        'price.require' => '价格为必填项',
        'price.number' => '价格只能填写数字',
    ];
    
    protected $scene = [
        'edit' => [
            'duration',
            'price',
        ],
    ];
}
