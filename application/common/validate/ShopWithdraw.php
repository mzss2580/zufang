<?php
namespace app\common\validate;

use think\Validate;

class ShopWithdraw extends Validate
{
    private $tn = 'shop_withdraw';

    protected $rule = [
        'money'       => 'require|float',
        'alipay_acc'  => 'require',
        'alipay_name' => 'require',
    ];

    protected $message = [
        'money.require'       => '请输入提现金额',
        'money.float'         => '请正确输入提现金额',
        'alipay_acc.require'  => '请输入支付宝账号',
        'alipay_name.require' => '请输入支付宝姓名',
    ];

    protected $scene = [
        'apply' => [
            'money',
            'alipay_acc',
            'alipay_name',
        ],
    ];
}
