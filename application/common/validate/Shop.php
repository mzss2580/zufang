<?php
namespace app\common\validate;

use think\Validate;

class Shop extends Validate
{
    private $tn = 'user';

    protected $rule = [
        'name'        => 'require',
        'logo'        => 'require',
        'tel'         => 'require',
        'yyzz'        => 'require',
        'jyxkz'       => 'require',
        'address'     => 'require',
        // 'desc'    => 'require',
        'lng'         => 'require',
        'lat'         => 'require',
        'notice'      => 'require',
        'express_fee' => 'require|float|>=:0',
        'id_card_rx'  => 'require',
        'id_card_gh'  => 'require',
        'leader'      => 'require',
        'leader_phone'=> 'require',
    ];

    protected $message = [
        'name.require'    => '请输入店铺名称',
        'logo.require'    => '请上传店铺logo',
        'tel.require'     => '请输入店铺电话',
        'yyzz.require'    => '请上传营业执照',
        'jyxkz.require'   => '请上传经营许可证',
        'address.require' => '请选择店铺地址',
        // 'desc.require'    => '请输入商家详情',
        'notice.require'  => '请填写店铺公告',
        'lng.require'     => '请选择店铺地址',
        'lat.require'     => '请选择店铺地址',
        'express_fee'     => '基础运费只能为数字',
        'id_card_rx'      => '请输入身份证人像面',
        'id_card_gh'      => '请输入身份证国徽面',
        'leader'          => '请输入负责人姓名',
        'leader_phone'    => '请输入负责人手机号',
    ];

    protected $scene = [
        'add'       => [
            'name',
            'logo',
            'tel',
            'yyzz',
            //'jyxkz',
            'address',
            'express_fee',
        ],
        'add_shop'  => [
            'name',
            'tel',
            'leader',
            'leader_phone',
        ],
        'shop_edit' => [
            'tel',
            'address',
            /*'lng',
            'lat',*/
            'notice',
        ],
        'distance'  => [
            'lng',
            'lat',
        ],
        'yzm' =>[
            'tel',
        ],
    ];
}
