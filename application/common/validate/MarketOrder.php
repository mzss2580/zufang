<?php
namespace app\common\validate;

use think\Validate;

class MarketOrder extends Validate
{
    private $tn = 'market_order';

    protected $rule = [
        'mid' => 'require|number',
        'pay_uid' => 'require',
        'voucher' => 'require',
        'pay_num' => 'require',
//        'status' => 'require|number',
    ];

    protected $message = [
        'mid.require' => '挂售id为必填项',
        'mid.number' => '挂售id只能填写数字',
        // 'pay_num.number' => '收购数量只能填写数字',
        'pay_uid.require' => '购买用户为必填项',
        'voucher.require' => '购买凭证为必填项',
        'pay_num.require' => '收购数量为必填项',
        'status.require' => '1商家待确认，2已完成
为必填项',
        'status.number' => '1商家待确认，2已完成
只能填写数字',
    ];
    
    protected $scene = [
        'edit' => [
            'mid',
            'pay_uid',
            'voucher',
            'pay_num',
        ],
    ];
}
