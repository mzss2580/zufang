<?php
namespace app\common\validate;

use think\Validate;

class Community extends Validate
{
    private $tn = 'community';

    protected $rule = [
        'name'    => 'require',
        'address' => 'require',
    ];

    protected $message = [
        'name.require'    => '小区名称为必填项',
        'address.require' => '小区地址为必填项',
    ];

    protected $scene = [
        'edit' => [
            'name',
            'address',
        ],
    ];
}
