<?php
namespace app\common\validate;

use think\Validate;

class AdminModule extends Validate
{
    private $tn = 'admin_module';

    protected $rule = [
        'name'  => 'require',
        'route' => 'unique_route',
    ];

    protected $message = [
        'name.require' => '模块名称为必填项',
    ];

    protected $scene = [
        'edit' => [
            'name',
            'route',
        ],
    ];

    public function unique_route($value, $rule, $data = [])
    {
        if ($value === '') {
            return true;
        }
        if (in_array($value, ['admin', 'ajax', 'common'])) {
            return '模块路径已存在';
        }
        $db = db($this->tn)->where('route', $value);
        if ($data['id'] > 0) {
            $db->where('id', '<>', $data['id']);
        }
        if (is_null($db->find())) {
            return true;
        }
        return '模块路径已存在';
    }
}
