<?php
namespace app\common\validate;
use think\Validate;
class Grade extends Validate
{
    private $tn = 'grade';
    protected $rule = [
        'name' => 'require',
        'integral' => 'require|number',
        'g_integral' => 'require|number',
    ];
    protected $message = [
        'name.require' => '等级名称为必填项',
        'integral.require' => '购物积分百分比为必填项',
        'integral.number' => '购物积分百分比只能填写数字',
        'g_integral.require' => '易贝百分比为必填项',
        'g_integral.number' => '易贝百分比只能填写数字',
    ];

    protected $scene = [
        'edit' => [
            'name',
            'integral',
            'g_integral',
        ],
    ];
}
