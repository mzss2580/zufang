<?php
namespace app\common\validate;

use think\Validate;

class AdminMenu extends Validate
{
    private $tn = 'admin_menu';

    protected $rule = [
        'name' => 'require',
    ];

    protected $message = [
        'name.require' => '菜单名称为必填项',
    ];

    protected $scene = [
        'edit' => [
            'name',
        ],
    ];
}
