<?php
namespace app\common\validate;

use think\Validate;

class Banner extends Validate
{
    private $tn = 'banner';

    protected $rule = [
        'image' => 'require',
    ];

    protected $message = [
        'image.require' => 'banner图片为必填项',
    ];
    
    protected $scene = [
        'edit' => [
            'image',
        ],
    ];
}
