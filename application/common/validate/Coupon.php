<?php
namespace app\common\validate;

use think\Validate;

class Coupon extends Validate
{
    private $tn = 'coupon';

    protected $rule = [
        'money'      => 'require',
        'money_max'  => 'require',
        'start_time' => 'require',
        'end_time'   => 'require',
        'type'       => 'require',

    ];

    protected $message = [
        'money.require'      => '请输入优惠金额',
        'money_max.require'  => '请输入满减金额',
        'start_time.require' => '请输入生效时间',
        'end_time.require'   => '请输入失效时间',
        'type.require'       => '请选择优惠券类型',
    ];

    protected $scene = [
        'edit' => [
            'money',
            'money_max',
            'start_time',
            'end_time',
            'type',
        ],

    ];
}
