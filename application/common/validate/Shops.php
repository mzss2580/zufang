<?php
namespace app\common\validate;

use think\Validate;

class Shops extends Validate
{
    private $tn = 'shops';

    protected $rule = [
        'head' => 'require',
        'name' => 'require',
        'contacts' => 'require',
        // 'phone' => 'require',
        // 'wechat' => 'require',
        'address' => 'require',
        'lng' => 'require',
        'lat' => 'require',
        'type' => 'require',
//        'qualifications' => 'require',
    ];

    protected $message = [
        'head.require' => '头像为必填项',
        'name.require' => '名称为必填项',
        'contacts.require' => '联系人为必填项',
        // 'phone.require' => '联系人电话为必填项',
        // 'wechat.require' => '联系人微信为必填项',
        'address.require' => '店家地址为必填项',
        'lng.require' => '经度为必填项',
        'lat.require' => '维度为必填项',
        'type.require' => '分类为必填项',
//        'type.number' => '分类只能填写数字',
//        'qualifications.require' => '资质证书为必填项',
    ];
    
    protected $scene = [
        'edit' => [
            'head',
            'name',
            'contacts',
            // 'phone',
            // 'wechat',
            'address',
            'lng',
            'lat',
            'type',
//            'qualifications',
        ],
    ];
}
