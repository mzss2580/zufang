<?php
namespace app\common\validate;

use think\Validate;

class Goods extends Validate
{
    protected $rule = [
        'name'      => 'require',
        'image'     => 'require',
        'price'     => 'require|float',
        'del_price' => 'require|float',
    ];
    protected $message = [
        'name.require'      => '名称为必填项',
        'image.require'     => '图片必须上传',
        'price.require'     => '价格为必填项',
        'price.number'      => '价格只能为数字',
        'del_price.require' => '原价为必填项',
        'del_price.number'  => '原价只能为数字',
    ];
    protected $scene = [
        'edit'            => [
            'name',
            'image',
            'price',
            'del_price',
        ],
        'shop_goods_edit' => [
            'name',
            'price',
        ],
    ];
}
