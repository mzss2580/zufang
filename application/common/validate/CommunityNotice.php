<?php
namespace app\common\validate;

use think\Validate;

class CommunityNotice extends Validate
{
    private $tn = 'community_notice';

    protected $rule = [
        'community_id' => 'require',
        'title'        => 'require',
        'image'        => 'require',
    ];

    protected $message = [
        'community_id.require' => '请选择小区',
        'title.require'        => '公告标题为必填项',
        'image.require'        => '公告图片为必填项',
    ];

    protected $scene = [
        'edit' => [
            'community_id',
            'title',
            'image',
        ],
    ];
}
