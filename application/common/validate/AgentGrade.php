<?php
namespace app\common\validate;

use think\Validate;

class AgentGrade extends Validate
{
    private $tn = 'agent_grade';

    protected $rule = [
        'name' => 'require',
        'proportion' => 'require',
    ];

    protected $message = [
        'name.require' => 'name为必填项',
        'proportion.require' => '抽成比例为必填项',
//        'proportion.number' => '抽成比例只能填写数字',
    ];
    
    protected $scene = [
        'edit' => [
            'name',
            'proportion',
        ],
    ];
}
