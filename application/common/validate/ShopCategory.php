<?php
namespace app\common\validate;

use think\Validate;

class ShopCategory extends Validate
{
    private $tn = 'shop_category';

    protected $rule = [
        'name' => 'require',
    ];

    protected $message = [
        'name.require' => '名称为必填项',
    ];
    
    protected $scene = [
        'edit' => [
            'name',
        ],
    ];
}
