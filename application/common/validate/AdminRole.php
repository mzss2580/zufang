<?php
namespace app\common\validate;

use think\Validate;

class AdminRole extends Validate
{
    private $tn = 'admin_role';

    protected $rule = [
        'name' => 'require|unique_name',
    ];

    protected $message = [
        'name.require' => '角色名称不能为空',
    ];

    protected $scene = [
        'edit' => [
            'name',
        ],
    ];

    public function unique_name($value, $rule, $data = [])
    {
        $db = db($this->tn)->where('name', $value);
        if ($data['id'] > 0) {
            $db->where('id', '<>', $data['id']);
        }
        if (is_null($db->find())) {
            return true;
        }
        return '角色名称已经存在';
    }
}
