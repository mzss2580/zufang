<?php
namespace app\common\validate;

use think\Validate;

class Agent extends Validate
{
    private $tn = 'agent';

    protected $rule = [
        'uid' => 'require|number',
        'province' => 'require|number',
        'city' => 'require|number',
        'area' => 'require|number',
    ];

    protected $message = [
        'uid.require' => '用户id为必填项',
        'uid.number' => '用户id只能填写数字',
        'province.require' => '省为必填项',
        'province.number' => '省只能填写数字',
        'city.require' => '市为必填项',
        'city.number' => '市只能填写数字',
        'area.require' => '区为必填项',
        'area.number' => '区只能填写数字',
    ];
    
    protected $scene = [
        'edit' => [
            'uid',
            'province',
            'city',
            'area',
        ],
    ];
}
