<?php
namespace app\common\validate;

use think\Validate;

class ShopShow extends Validate
{
    private $tn = 'shop_show';

    protected $rule = [
        'uid' => 'require|number',
        'shop_id' => 'require|number',
        'count' => 'require|number',
    ];

    protected $message = [
        'uid.require' => '用户id为必填项',
        'uid.number' => '用户id只能填写数字',
        'shop_id.require' => '店铺id为必填项',
        'shop_id.number' => '店铺id只能填写数字',
        'count.require' => '次数为必填项',
        'count.number' => '次数只能填写数字',
    ];
    
    protected $scene = [
        'edit' => [
            'uid',
            'shop_id',
            'count',
        ],
    ];
}
