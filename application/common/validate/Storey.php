<?php
namespace app\common\validate;

use think\Validate;

class Storey extends Validate
{
    private $tn = 'storey';

    protected $rule = [
        'name' => 'require',
    ];

    protected $message = [
        'name.require' => '楼层为必填项',
    ];
    
    protected $scene = [
        'edit' => [
            'name',
        ],
    ];
}
