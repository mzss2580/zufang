<?php
namespace app\common\validate;

use think\Validate;

class UserAccountRecords extends Validate
{
    private $tn = 'user_account_records';

    protected $rule = [
        'uid' => 'require|number',
        'account_type' => 'require|number',
        'sign' => 'require',
        'number' => 'require|number',
        'order_amount' => 'require|number',
        'from_type' => 'require',
        'data_id' => 'require',
        'typstr' => 'require',
        'text' => 'require',
    ];

    protected $message = [
        'uid.require' => '用户ID为必填项',
        'uid.number' => '用户ID只能填写数字',
        'account_type.require' => '账户类型1.余额2.可提现余额为必填项',
        'account_type.number' => '账户类型1.余额2.可提现余额只能填写数字',
        'sign.require' => '正负号1+ / 0-为必填项',
        'number.require' => '数量为必填项',
        'number.number' => '数量只能填写数字',
        'order_amount.require' => '订单支付金额为必填项',
        'order_amount.number' => '订单支付金额只能填写数字',
        'from_type.require' => '产生方式1.商城订单2.订单退还3.提现4.提现退还5余额充值为必填项',
        'data_id.require' => '相关表的数据ID为必填项',
        'typstr.require' => '相关数据表为必填项',
        'text.require' => '数据相关内容描述文本为必填项',
    ];
    
    protected $scene = [
        'edit' => [
            'uid',
            'account_type',
            'sign',
            'number',
            'order_amount',
            'from_type',
            'data_id',
            'typstr',
            'text',
        ],
    ];
}
