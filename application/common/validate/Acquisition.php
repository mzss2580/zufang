<?php
namespace app\common\validate;

use think\Validate;

class Acquisition extends Validate
{
    private $tn = 'acquisition';

    protected $rule = [
        'uid' => 'require|number',
        'num' => 'require|number',
        'price' => 'require',
//        'status' => 'require|number',
        'tel' => 'require',
    ];

    protected $message = [
        'uid.require' => '收购者id为必填项',
        'uid.number' => '收购者id只能填写数字',
        'num.require' => '求购数量为必填项',
        'num.number' => '求购数量只能填写数字',
        'price.require' => '价格为必填项',
        'price.number' => '价格只能填写数字',
        'status.require' => '状态 1发布中 2 已完成 3已取消为必填项',
        'status.number' => '状态 1发布中 2 已完成 3已取消只能填写数字',
        'tel.require' => '手机号为必填项',
    ];
    
    protected $scene = [
        'edit' => [
            'uid',
            'num',
            'price',
//            'status',
            'tel',
        ],
    ];
}
