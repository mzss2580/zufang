<?php
/**
 * Created by 项目名称.
 * User: 李磊
 * Date: 2022/9/6
 * Time: 13:57
 */

namespace app\common\validate;
use think\Validate;
class Distribute extends Validate
{
    private $tn = ' distribute';
    protected $rule = [
        'green_points' => 'require',
//        'surplus' => 'require',
        'probability' => 'require|number',
        'every' => 'require',
    ];
    protected $message = [
        'green_points.require' => '总额度为必填项',
//        'surplus.require' => '剩余数量为必填项',
        'probability.require' => '递减百分比
为必填项',
        'probability.number' => '递减百分比
只能填写数字',
        'every.require' => '每天发放为必填项',
    ];

    protected $scene = [
        'edit' => [
            'green_points',
            'surplus',
            'probability',
            'every',
        ],
    ];
}