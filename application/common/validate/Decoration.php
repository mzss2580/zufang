<?php
namespace app\common\validate;

use think\Validate;

class Decoration extends Validate
{
    private $tn = 'decoration';

    protected $rule = [
        'name' => 'require',
    ];

    protected $message = [
        'name.require' => 'name为必填项',
    ];
    
    protected $scene = [
        'edit' => [
            'name',
        ],
    ];
}
