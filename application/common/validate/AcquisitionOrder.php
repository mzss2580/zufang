<?php
namespace app\common\validate;

use think\Validate;

class AcquisitionOrder extends Validate
{
    private $tn = 'acquisition_order';

    protected $rule = [
        'aid' => 'require|number',
        'sell_uid' => 'require|number',
        'status' => 'require|number',
    ];

    protected $message = [
        'aid.require' => '求购id为必填项',
        'aid.number' => '求购id只能填写数字',
        'sell_uid.require' => '卖出用户id为必填项',
        'sell_uid.number' => '卖出用户id只能填写数字',
        'status.require' => '状态1已提交2求购者打款卖家待确认,3已完成为必填项',
        'status.number' => '状态1已提交2求购者打款卖家待确认,3已完成只能填写数字',
    ];
    
    protected $scene = [
        'edit' => [
            'aid',
            'sell_uid',
            'status',
        ],
    ];
}
