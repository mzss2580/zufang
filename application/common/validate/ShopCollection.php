<?php
namespace app\common\validate;

use think\Validate;

class ShopCollection extends Validate
{
    private $tn = 'shop_collection';

    protected $rule = [
        'shop_id' => 'require|number',
        'uid' => 'require|number',
    ];

    protected $message = [
        'shop_id.require' => '商家ｉｄ为必填项',
        'shop_id.number' => '商家ｉｄ只能填写数字',
        'uid.require' => '用户ｉｄ为必填项',
        'uid.number' => '用户ｉｄ只能填写数字',
    ];
    
    protected $scene = [
        'edit' => [
            'shop_id',
            'uid',
        ],
    ];
}
