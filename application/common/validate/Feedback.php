<?php
namespace app\common\validate;

use think\Validate;

class Feedback extends Validate
{
    private $tn = 'feedback';

    protected $rule = [
        'uid' => 'require|number',
        'type' => 'require',
        'content' => 'require',
    ];

    protected $message = [
        'uid.require' => 'uid为必填项',
        'uid.number' => 'uid只能填写数字',
        'type.require' => '类型为必填项',
        'content.require' => '内容为必填项',
    ];
    
    protected $scene = [
        'edit' => [
            'uid',
            'type',
            'content',
        ],
    ];
}
