<?php
namespace app\api\controller;

/**
 * @title 商品
 * @description 接口说明
 */
class Goods extends Base
{
    /**
     * @title 商品详情
     * @description 接口说明
     * @author MOON
     * @url /api/goods/detail
     * @method POST
     *
     * @param name:token type:string require:0 default:1 other: desc:用户登录token
     * @param name:goods_id type:string require:1 default:1 other: desc:商品id
     *
     * @return video:视频
     * @return extension:推广内容@
     * @extension logo:平台logo my_link:二维码地址
     * @return score_list:评价列表@
     * @score_list head:头像 nick:昵称 score:分数 content:内容 images:图片数组@
     * @images images_thumb:缩略图 image:图片
     */
    public function detail()
    {
        $goods = model('goods')->get(['id' => input('post.goods_id'), 'type' => 0, 'on_sale' => 1, 'is_del'=>0]);
        if (is_null($goods)) {
            $this->returnAPI('商品信息有误或已下架,请确认后重试');
        }
        $gs_thumb = $this->image_thumb($goods->image);
        $gs_img   = (strpos($goods->image, '/') === 0) ? $this->request->domain() . $goods->image : $goods->image;
        $images   = db('goods_images')->where('goods_id', $goods->id)->order(['sort' => 'desc', 'id' => 'desc'])->column('image');
        foreach ($images as $k => $v) {
            if ($v) {
                $images[$k] = (strpos($v, '/') === 0) ? $this->request->domain() . $v : $v;
            } else {
                unset($images[$k]);
            }
        }
        if (empty($images)) {
            $images = [$gs_img];
        }
        // 所有规格选择可能
        $specs_choose = [];
        // 当前商品所有上架规格
        $goods_specs = db('goods_specs')->field(['specs', 'kucun', 'price', 'image'])->where('goods_id', $goods->id)->where('on_sale', 1)->select();
        foreach ($goods_specs as $k => $v) {
            if ($v['specs']) {
                $specs       = explode(',', $v['specs']);
                $price       = floatval($v['price'] ? $v['price'] : $goods->price);
                $image_thumb = $v['image'] ? $this->image_thumb($v['image']) : $gs_thumb;
                $image       = $v['image'] ? ((strpos($goods->image, '/') === 0) ? $this->request->domain() . $v['image'] : $v['image']) : $gs_img;
                if (count($specs) === 1) {
                    $specs_choose[$specs[0]] = [
                        'price'       => $price,
                        'kucun'       => $v['kucun'],
                        'image'       => $image,
                        'image_thumb' => $image_thumb,
                    ];
                } else {
                    $specs_choose[$specs[0]][] = [
                        'price' => $price,
                        'kucun' => $v['kucun'],
                    ];
                    $specs_choose[$specs[1]][] = [
                        'price' => $price,
                        'kucun' => $v['kucun'],
                    ];
                    $specs_choose[$v['specs']] = [
                        'price'       => $price,
                        'kucun'       => $v['kucun'],
                        'image'       => $image,
                        'image_thumb' => $image_thumb,
                    ];
                }
            }
        }
        foreach ($specs_choose as $k => $v) {
            if (isset($v[0])) {
                $min = 0;
                $max = 0;
                foreach ($v as $key => $value) {
                    if ($min === 0) {
                        $min = $value['price'];
                    } else if ($min > $value['price']) {
                        $min = $value['price'];
                    }
                    if ($max === 0) {
                        $max = $value['price'];
                    } else if ($max < $value['price']) {
                        $max = $value['price'];
                    }
                }
                $kucun = array_sum(array_column($v, 'kucun'));
                if ($min == $max) {
                    $specs_choose[$k] = [
                        'price' => $min,
                        'kucun' => $kucun,
                    ];
                } else {
                    $specs_choose[$k] = [
                        'price' => $min . '-' . $max,
                        'kucun' => $kucun,
                    ];
                }
            }
        }
        $kucun = $goods->kucun;
        if (!empty($specs_choose)) {
            $kucun = array_sum(array_column($specs_choose, 'kucun'));
        }
        // 所有可选规格
        $specs_type = [];
        $specs_list = db('specs')->alias('s')->field(['s.id' => 'specs', 's.name', 't.id' => 'type', 't.name' => 'type_name'])->join('specs_type t', 's.type = t.id')->where('s.id', 'in', array_keys($specs_choose))->order(['t.sort' => 'desc', 't.id' => 'desc', 's.sort' => 'desc', 's.id' => 'desc'])->select();
        foreach ($specs_list as $k => $v) {
            if (isset($specs_type[$v['type']])) {
                $specs_type[$v['type']]['specs'][] = ['specs' => $v['specs'], 'name' => $v['name']];
            } else {
                $specs_type[$v['type']] = [
                    'type'  => $v['type'],
                    'name'  => $v['type_name'],
                    'specs' => [['specs' => $v['specs'], 'name' => $v['name']]],
                ];
            }
        }
        $collect = 0;
        $coupon  = db('coupon')->field(['id' => 'coupon_id', 'money', 'money_max', 'start_time', 'end_time'])->where('type', 0)->where('shop_id', $goods->shop_id)->where('start_time', '<=', time())->where('end_time', '>=', time())->select();
        $user    = $this->check_token();
        $logo = get_config('shop_logo');
        $logos = (strpos($logo, '/') === 0) ? $this->request->domain() . $logo : $logo;
        if (is_null($user)) {
            $user_coupon = [];
            $link = get_config('sign_up_link');
            $extension = ['logo'=>$logos, 'my_link'=>$this->request->domain() . $link];
        } else {
            if (db('user_collect')->where('user_id', $user->id)->where('type', 1)->where('other_id', $goods->id)->count() > 0) {
                $collect = 1;
            }
            db('user_coupon')->where('user_id', $user->id)->where('status', 0)->where('end_time', '<=', time())->setField('status', 2);
            $user_coupon = db('user_coupon')->distinct(true)->where('type', 0)->where('shop_id', $goods->shop_id)->where('user_id', $user->id)->where('status', 0)->column('start_time,end_time', 'coupon_id');

            $extension = ['logo'=>$logos, 'my_link'=>$user->my_link];
        }
        foreach ($coupon as $k => $v) {
            if (array_key_exists($v['coupon_id'], $user_coupon)) {
                $status     = 1;
                $start_time = $user_coupon[$v['coupon_id']]['start_time'];
                $end_time   = $user_coupon[$v['coupon_id']]['end_time'];
            } else {
                $status     = 0;
                $start_time = $v['start_time'];
                $end_time   = $v['end_time'];
            }
            $coupon[$k]['status']     = $status;
            $coupon[$k]['start_time'] = date('Y-m-d H:i:s', $start_time);
            $coupon[$k]['end_time']   = date('Y-m-d H:i:s', $end_time);
        }
        $score_list = db('goods_comment')->alias('c')->join('user u', 'c.user_id = u.id')
            ->field(['u.head', 'u.nick', 'c.score', 'c.content', 'c.images', 'c.create_time'])
            ->where('c.goods_id', $goods->id)->where('c.comment_del', 0)->order('c.create_time', 'desc')->limit(3)->select();
        foreach ($score_list as $k => $v) {
            if (is_null($v['images']) || $v['images'] == '') {
                $score_images = [];
            } else {
                $score_images = json_decode($v['images'], true);
            }
            if (!empty($score_images)) {
                foreach ($score_images as $key => $value) {
                    $score_images[$key] = [
                        'images_thumb' => $this->image_thumb($value),
                        'image'        => (strpos($value, '/') === 0) ? $this->request->domain() . $value : $value,
                    ];
                }
            }
            $score_list[$k]['images']      = $score_images;
            $score_list[$k]['head']        = (strpos($v['head'], '/') === 0) ? $this->request->domain() . $v['head'] : $v['head'];
            $score_list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
        }
        if($goods->shop_id == 0){
            $kf_url = 'http://kefu.ebeizi.net.cn/index/index/home?visiter_id=&visiter_name=&avatar=&business_id=1&groupid=0&special=1';
        }else{
            $shop = db('shop')->where('id', $goods->shop_id)->find();
            $kf_url = $shop['kf_url'];
        }
        
        $this->returnAPI('', 0, [
            'shop_id'     => $goods->shop_id,
            'images'      => $images,
            'name'        => $goods->name,
            'kucun'       => $kucun,
            'price'       => $goods->price,
            'del_price'   => $goods->del_price,
            'collect'     => $collect,
            'coupon'      => $coupon,
            'desc'        => $goods->desc,
            'content'     => $goods->content,
            'video'       => $goods->video,
            'score'       => round($goods->score * 20, 2) . '%',
            'score_num'   => db('goods_comment')->where('goods_id', $goods->id)->count(),
            'score_list'  => $score_list,
            'goods_specs' => [
                'specs_type'   => array_values($specs_type),
                'specs_choose' => $specs_choose,
            ],
            'extension'  => $extension, //推广信息
            'kf_url'     => $kf_url,
        ]);
    }
    public function kanjia_list()
    {
        $user = $this->check_token();
        if (is_null($user)) {
            $user_id = 0;
        } else {
            $user_id = $user->id;
        }
        $list = db('goods')->field(['id' => 'goods_id', 'name', 'image', 'price', 'del_price'])->where('type', 1)->where('on_sale', 1)->order('sort', 'desc')->order('create_time', 'desc')->page(input('post.page', 1))->limit(input('post.limit', 10))->select();
        foreach ($list as $k => $v) {
            $kanjia = 0;
            if ($user_id !== 0 && db('kanjia')->where('goods_id', $v['goods_id'])->where('user_id', $user_id)->count() > 0) {
                $kanjia = 1;
            }
            $list[$k]['kanjia']      = $kanjia;
            $list[$k]['image']       = $v['image'] ? $this->request->domain() . $v['image'] : '';
            $list[$k]['image_thumb'] = $this->image_thumb($v['image']);
        }
        $this->returnAPI('', 0, ['list' => $list]);
    }
    public function detail_kj()
    {
        $goods = model('goods')->get(['id' => input('post.goods_id'), 'type' => 1, 'on_sale' => 1]);
        if (is_null($goods)) {
            $this->returnAPI('商品信息有误或已下架,请确认后重试');
        }
        $gs_img = $goods->image ? $this->request->domain() . $goods->image : '';
        $images = db('goods_images')->where('goods_id', $goods->id)->order(['sort' => 'desc', 'id' => 'desc'])->column('image');
        foreach ($images as $k => $v) {
            if ($v) {
                $images[$k] = $this->request->domain() . $v;
            } else {
                unset($images[$k]);
            }
        }
        if (empty($images)) {
            $images = [$gs_img];
        }
        $kanjia  = 0;
        $collect = 0;
        $user    = $this->check_token();
        if (!is_null($user)) {
            if (db('kanjia')->where('goods_id', $goods->id)->where('user_id', $user->id)->count() > 0) {
                $kanjia = 1;
            }
            if (db('user_collect')->where('user_id', $user->id)->where('type', 1)->where('other_id', $goods->id)->count() > 0) {
                $collect = 1;
            }
        }
        $score_list = db('goods_comment')->alias('c')->join('user u', 'c.user_id = u.id')
            ->field(['u.head', 'u.nick', 'c.score', 'c.content', 'c.images', 'c.create_time'])
            ->where('c.goods_id', $goods->id)->where('c.comment_del', 0)->order('c.create_time', 'desc')->limit(3)->select();
        foreach ($score_list as $k => $v) {
            if (is_null($v['images']) || $v['images'] == '') {
                $score_images = [];
            } else {
                $score_images = json_decode($v['images'], true);
            }
            if (!empty($score_images)) {
                foreach ($score_images as $key => $value) {
                    $score_images[$key] = [
                        'images_thumb' => $this->image_thumb($value),
                        'image'        => $value ? $this->request->domain() . $value : '',
                    ];
                }
            }
            $score_list[$k]['images']      = $score_images;
            $score_list[$k]['head']        = (strpos($v['head'], '/') === 0) ? $this->request->domain() . $v['head'] : $v['head'];
            $score_list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
        }
        $this->returnAPI('', 0, [
            'images'     => $images,
            'name'       => $goods->name,
            'kucun'      => $goods->kucun,
            'price'      => $goods->price,
            'del_price'  => $goods->del_price,
            'content'    => $goods->content,
            'kanjia'     => $kanjia,
            'collect'    => $collect,
            'score'      => round($goods->score * 20, 2) . '%',
            'score_num'  => db('goods_comment')->where('goods_id', $goods->id)->count(),
            'score_list' => $score_list,
        ]);
    }
    public function miaosha_time_list()
    {
        $list = [];
        for ($i = 0; $i < 24; $i++) {
            if (db('goods')->where('type', 2)->where('on_sale', 1)->where('ms_start_time', strtotime(date('Y-m-d ' . $i . ':00:00')))->count() > 0) {
                $list[] = ($i < 10 ? '0' . $i : $i) . ':00';
            }
        }
        $this->returnAPI('', 0, ['list' => $list]);
    }
    public function miaosha_list()
    {
        $list = db('goods')->field(['id' => 'goods_id', 'name', 'image', 'price', 'del_price', 'kucun', 'sale'])
            ->where('type', 2)->where('on_sale', 1)->where('ms_start_time', strtotime(date('Y-m-d ' . input('post.start_time', date('H:00')) . ':00')))->where('ms_end_time', '>', time())
            ->order('sort', 'desc')->order('create_time', 'desc')->page(input('post.page', 1))->limit(input('post.limit', 10))->select();
        foreach ($list as $k => $v) {
            $list[$k]['image']       = (strpos($v['image'], '/') === 0) ? $this->request->domain() . $v['image'] : $v['image'];
            $list[$k]['image_thumb'] = $this->image_thumb($v['image']);
        }
        $this->returnAPI('', 0, ['list' => $list]);
    }
    public function detail_ms()
    {
        $goods = model('goods')->get(['id' => input('post.goods_id'), 'type' => 2, 'on_sale' => 1]);
        if (is_null($goods)) {
            $this->returnAPI('商品信息有误或已下架,请确认后重试');
        }
        $gs_img = $goods->image ? $this->request->domain() . $goods->image : '';
        $images = db('goods_images')->where('goods_id', $goods->id)->order(['sort' => 'desc', 'id' => 'desc'])->column('image');
        foreach ($images as $k => $v) {
            if ($v) {
                $images[$k] = $this->request->domain() . $v;
            } else {
                unset($images[$k]);
            }
        }
        if (empty($images)) {
            $images = [$gs_img];
        }
        $collect = 0;
        $coupon  = db('coupon')->field(['id' => 'coupon_id', 'money', 'money_max', 'start_time', 'end_time'])->where('type', 0)->where('shop_id', $goods->shop_id)->where('start_time', '<=', time())->where('end_time', '>=', time())->select();
        $user    = $this->check_token();
        if (is_null($user)) {
            $user_coupon = [];
        } else {
            if (db('user_collect')->where('user_id', $user->id)->where('type', 1)->where('other_id', $goods->id)->count() > 0) {
                $collect = 1;
            }
            db('user_coupon')->where('user_id', $user->id)->where('status', 0)->where('end_time', '<=', time())->setField('status', 2);
            $user_coupon = db('user_coupon')->distinct(true)->where('type', 0)->where('shop_id', $goods->shop_id)->where('user_id', $user->id)->where('status', 0)->column('start_time,end_time', 'coupon_id');
        }
        foreach ($coupon as $k => $v) {
            if (array_key_exists($v['coupon_id'], $user_coupon)) {
                $status     = 1;
                $start_time = $user_coupon[$v['coupon_id']]['start_time'];
                $end_time   = $user_coupon[$v['coupon_id']]['end_time'];
            } else {
                $status     = 0;
                $start_time = $v['start_time'];
                $end_time   = $v['end_time'];
            }
            $coupon[$k]['status']     = $status;
            $coupon[$k]['start_time'] = date('Y-m-d H:i:s', $start_time);
            $coupon[$k]['end_time']   = date('Y-m-d H:i:s', $end_time);
        }
        $score_list = db('goods_comment')->alias('c')->join('user u', 'c.user_id = u.id')
            ->field(['u.head', 'u.nick', 'c.score', 'c.content', 'c.images', 'c.create_time'])
            ->where('c.goods_id', $goods->id)->where('c.comment_del', 0)->order('c.create_time', 'desc')->limit(3)->select();
        foreach ($score_list as $k => $v) {
            if (is_null($v['images']) || $v['images'] == '') {
                $score_images = [];
            } else {
                $score_images = json_decode($v['images'], true);
            }
            if (!empty($score_images)) {
                foreach ($score_images as $key => $value) {
                    $score_images[$key] = [
                        'images_thumb' => $this->image_thumb($value),
                        'image'        => $value ? $this->request->domain() . $value : '',
                    ];
                }
            }
            $score_list[$k]['images']      = $score_images;
            $score_list[$k]['head']        = (strpos($v['head'], '/') === 0) ? $this->request->domain() . $v['head'] : $v['head'];
            $score_list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
        }
        $this->returnAPI('', 0, [
            'images'        => $images,
            'name'          => $goods->name,
            'kucun'         => $goods->kucun,
            'price'         => $goods->price,
            'del_price'     => $goods->del_price,
            'sale'          => $goods->sale,
            'content'       => $goods->content,
            'ms_start_time' => $goods->ms_start_time ? date('Y-m-d H:i:s', $goods->ms_start_time) : '',
            'ms_end_time'   => $goods->ms_end_time ? date('Y-m-d H:i:s', $goods->ms_end_time) : '',
            'coupon'        => $coupon,
            'collect'       => $collect,
            'score'         => round($goods->score * 20, 2) . '%',
            'score_num'     => db('goods_comment')->where('goods_id', $goods->id)->where('score', '>', 0)->count(),
            'score_list'    => $score_list,
        ]);
    }

    /**
     * @title 收藏商品
     * @description 接口说明
     * @author MOON
     * @url /api/goods/collect
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:goods_id type:string require:1 default:1 other: desc:商品id
     *
     */
    public function collect()
    {
        $user = $this->check_token();
        if (is_null($user)) {
            $this->returnAPI('登录信息有误,请重新登录', 10000);
        }
        $goods_id = input('post.goods_id');
        if (!is_null($goods_id)) {
            $data = [
                'user_id'  => $user->id,
                'type'     => 1,
                'other_id' => $goods_id,
            ];
            $model = model('user_collect')->get($data);
            if (is_null($model)) {
                $result = model('user_collect')->save($data);
            } else {
                $result = true;
            }
            if ($result) {
                $this->returnAPI('收藏成功', 0);
            }
            $this->returnAPI('收藏失败,请稍后重试');
        }
    }
    /**
     * @title 取消收藏商品
     * @description 接口说明
     * @author MOON
     * @url /api/goods/collect_cancel
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:goods_id type:string require:1 default:1 other: desc:商品id
     *
     */
    public function collect_cancel()
    {
        $user = $this->check_token();
        if (is_null($user)) {
            $this->returnAPI('登录信息有误,请重新登录', 10000);
        }
        $goods_id = input('post.goods_id');
        if (!is_null($goods_id)) {
            $data = [
                'user_id'  => $user->id,
                'type'     => 1,
                'other_id' => $goods_id,
            ];
            $model = model('user_collect')->get($data);
            if (is_null($model)) {
                $result = true;
            } else {
                $result = $model->delete();
            }
            if ($result) {
                $this->returnAPI('取消收藏成功', 0);
            }
            $this->returnAPI('取消收藏失败,请稍后重试');
        }
    }
    /**
     * @title 评价列表
     * @description 接口说明
     * @author MOON
     * @url /api/goods/comment_list
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:goods_id type:string require:1 default:1 other: desc:商品id
     * @param name:score type:string require:1 default:1 other: desc:1好评，2中评，3差评
     * @param name:page type:string require:1 default:1 other: desc:页码
     * @param name:limit type:string require:1 default:1 other: desc:个数
     *
     */
    public function comment_list()
    {
        $db = db('goods_comment')->alias('c')->join('user u', 'c.user_id = u.id')
            ->field(['u.head', 'u.nick', 'c.score', 'c.content', 'c.images', 'c.create_time']);
        $goods_id = input('post.goods_id');
        $score    = input('post.score');
        if (!is_null($score) && $score > 0) {
            if ($score == 1) {
                $db->where('c.score', 'in', [4, 5]);
            } else if ($score == 2) {
                $db->where('c.score', 'in', [2, 3]);
            } else if ($score == 3) {
                $db->where('c.score', 1);
            }
        }
        $list = $db->where('c.goods_id', $goods_id)->where('c.comment_del', 0)
            ->order('c.create_time', 'desc')->page(input('post.page', 1))->limit(input('post.limit', 10))->select();
        foreach ($list as $k => $v) {
            if (is_null($v['images']) || $v['images'] == '') {
                $score_images = [];
            } else {
                $score_images = json_decode($v['images'], true);
            }
            if (!empty($score_images)) {
                foreach ($score_images as $key => $value) {
                    $score_images[$key] = [
                        'images_thumb' => $this->image_thumb($value),
                        'image'        => (strpos($value, '/') === 0) ? $this->request->domain() . $value : $value,
                    ];
                }
            }
            $list[$k]['images']      = $score_images;
            $list[$k]['head']        = (strpos($v['head'], '/') === 0) ? $this->request->domain() . $v['head'] : $v['head'];
            $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
        }
        $this->returnAPI('', 0, [
            'score_0' => db('goods_comment')->where('goods_id', $goods_id)->count(),
            'score_1' => db('goods_comment')->where('goods_id', $goods_id)->where('score', 'in', [4, 5])->count(),
            'score_2' => db('goods_comment')->where('goods_id', $goods_id)->where('score', 'in', [2, 3])->count(),
            'score_3' => db('goods_comment')->where('goods_id', $goods_id)->where('score', 1)->count(),
            'list'    => $list,
        ]);
    }
}
