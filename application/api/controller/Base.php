<?php
namespace app\api\controller;

use alisms\Alisms;
use app\common\controller\Ctrl;
use app\common\model\TLSSigAPIv2;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use think\Db;
use Ws\Http\Request;

class Base extends Ctrl
{

    /**
     * 初始化参数
     */
    protected function initialize()
    {
        $this->validate = 'app\common\validate\\';
    }
    /**
     * 根据配置项获取对应的key和secret
     * @return array key和secret
     */
    function get_rong_key_secret(){
        // 判断是需要开发环境还是生产环境的key
        if (C('RONG_IS_DEV')) {
            $key=C('RONG_DEV_APP_KEY');
            $secret=C('RONG_DEV_APP_SECRET');
        }else{
            $key=C('RONG_PRO_APP_KEY');
            $secret=C('RONG_PRO_APP_SECRET');
        }
        $data=array(
            'key'=>$key,
            'secret'=>$secret
        );
        return $data;
    }


    public function drive($lng1,$lat1,$lng2,$lat2)
    {
        //固定好的key值，用的是高德地图的api接口
        $key = "4161853f85a7cda34fc4abfcb449637c";
//        $local = "$longitude,$latitude";
        $origin="$lng1,$lat1";
        $destination="$lng2,$lat2";
        $regeo_url = "https://restapi.amap.com/v5/direction/driving";
        $address_location = $regeo_url . "?output=JSON&origin=$origin&destination=$destination&key=$key";
        $data_location = file_get_contents($address_location);
        $result_local = json_decode($data_location, true);

        if ($result_local['status']==1){
            $arr=$result_local['route']['paths'][0];//默认获取第一条

//            die_dump( $arr);
            return $arr['distance'];
        }

    }

    /**
     * 模拟post进行url请求 * @param string $url * @param array $postData */
    function https_request($url, $data = null)
    {
        $curl = curl_init();//初始化
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);//允许 cURL 函数执行的最长秒数。
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/json;charset=UTF-8'));
        //设置请求目标url头部信息
        if (!empty($data)) {
            //$data不为空，发送post请求
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data); //$data：数组
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);//执行命令
        $error = curl_error($curl);//错误信息
        if ($error || $output == FALSE) {        //报错信息
            return 'ERROR ' . curl_error($curl);
        }
        curl_close($curl);
        return $output;
    }

    /**
     * 获取融云token
     * @param  integer $uid 用户id
     * @return integer      token
     */
    function get_rongcloud_token($uid){
        // 从数据库中获取token
        $token=D('OauthUser')->getToken($uid,1);
        // 如果有token就返回
        if ($token) {
            return $token;
        }
        // 获取用户昵称和头像
        $user_data=M('Users')->field('username,avatar')->getById($uid);
        // 用户不存在
        if (empty($user_data)) {
            return false;
        }
        // 获取头像url格式
        $avatar=get_url($user_data['avatar']);
        // 获取key和secret
        $key_secret=get_rong_key_secret();
        // 实例化融云
        $rong_cloud=new \Org\Xb\RongCloud($key_secret['key'],$key_secret['secret']);
        // 获取token
        $token_json=$rong_cloud->getToken($uid,$user_data['username'],$avatar);
        $token_array=json_decode($token_json,true);
        // 获取token失败
        if ($token_array['code']!=200) {
            return false;
        }
        $token=$token_array['token'];
        $data=array(
            'uid'=>$uid,
            'type'=>1,
            'nickname'=>$user_data['username'],
            'head_img'=>$avatar,
            'access_token'=>$token
        );
        // 插入数据库
        $result=D('OauthUser')->addData($data);
        if ($result) {
            return $token;
        }else{
            return false;
        }
    }

    /**
     * 更新融云头像
     * @param  integer $uid 用户id
     * @return boolear      操作是否成功
     */
    function refresh_rongcloud_token($uid){
        // 获取用户昵称和头像
        $user_data=M('Users')->field('username,avatar')->getById($uid);
        // 用户不存在
        if (empty($user_data)) {
            return false;
        }
        $avatar=get_url($user_data['avatar']);
        // 获取key和secret
        $key_secret=get_rong_key_secret();
        // 实例化融云
        $rong_cloud=new \Org\Xb\RongCloud($key_secret['key'],$key_secret['secret']);
        // 更新融云用户头像
        $result_json=$rong_cloud->userRefresh($uid,$user_data['username'],$avatar);
        $result_array=json_decode($result_json,true);
        if ($result_array['code']==200) {
            return true;
        }else{
            return false;
        }
    }

















    protected function check_token()
    {
        $user  = null;
        $token = input('param.token');
        if (!is_null($token)) {
            try {
                $token  = simple_decrypt($token);
                $parser = new Parser();
                $token  = $parser->parse($token);
                // dump($token);exit;
                // dump($token);exit;
                $sha256 = new Sha256();
                if ($token->verify($sha256, config('encrypt_salt'))) {
                    $user = model('user')->get($token->getClaim('user_id'));
                    if (!is_null($user) && $user->pwd !== $token->getClaim('user_pwd')) {
                        $user = null;
                    }else{
                        //判断我有没有推荐码
                        $invite_code = db('user_invite_code')->where('uid', $user->id)->find();
//                        if(!$invite_code){
//                            $user_code = $this->randomkeys(6);
//                            db('user_invite_code')->insert(['uid'=>$user->id, 'code'=>$user_code,'create_time'=>time()]);
//                        }else{
//                            $user_code = $invite_code['code'];
//                        }
//                        $user->invite_code = $user_code;
                        $link = get_config('sign_up_link');
//                        $user->my_link     = $this->request->domain() . $link.'?code='.$user_code;
                    }
                    if($user->is_ban == 1){
                        $user = null;
                    }
                }
            } catch (\Exception $e) {
                // echo 1;exit;
            }
        }
        return $user;
    }

    /*发送验证码  腾域短信*/
    public function sendtyu($mobile,$mobile_code){
        if($mobile!=''){
            $content = "尊敬的用户您好！您正在进行手机验证，验证码：{$mobile_code}，有效期5分钟，如非本人操作，请勿泄露！";
            $url="http://47.96.136.50:8001/sms/interface/sendmess.htm?";
            $curlPost = "username=助果农&userpwd=372158&mobiles={$mobile}&content={$content}&sendTime=&extno=";
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL,$url);//抓取指定网页
            curl_setopt($ch, CURLOPT_HEADER, 0);//设置header
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //要求结果为字符串且输出到屏幕上
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);  //允许curl提交后,网页重定向
            curl_setopt($ch, CURLOPT_POST, 1);//post提交方式
            curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
            $data = curl_exec($ch);//运行curl
            $xml = simplexml_load_string($data);
            $result = json_decode(json_encode($xml),TRUE);
            if($data){
                //$_SESSION['mobile_code']=$mobile_code;
                if($result['errorcode'] == 1){
                    return true;
                }else{
                    //Log::write('发送验证码失败，手机号：'.$mobile.json_encode($result));
                    return false;
                }

            }else{
                return false;
            }
        }else{
            //$b = false; '没电话！';
            return false;
        }
    }
    protected function sendyzm($phone, $prefix)
    {
        $time  = time();
        $again = config('alisms.yzm_again');
        if (cache($phone . '_' . $prefix . '_yzm') >= $time - $again) {
            $this->returnAPI('请' . $again . '秒后再次发送');
        }
        $code = mt_rand(100000, 999999);
        $resp = Alisms::send(config('alisms.accessKeyId'), config('alisms.accessKeySecret'), $phone, config('alisms.SignName'), config('alisms.TemplateCode'), ['code' => $code]);
        // $code = 123456;
         //$resp = true;
        if ($resp === true) {
            cache($phone . '_' . $prefix, $code, config('alisms.yzm_indate') * 60);
            cache($phone . '_' . $prefix . '_yzm', $time, $again);
            $this->returnAPI('验证码发送成功', 0,['code'=>$code]);
        }
        $this->returnAPI('验证码发送失败',$resp);
    }
    /*protected function sendyzm($phone, $prefix)
    {
        $time  = time();
        $again = config('alisms.yzm_again');
        // if (cache($phone . '_' . $prefix . '_yzm') >= $time - $again) {
        //     $this->returnAPI('请' . $again . '秒后再次发送');
        // }
        $code = mt_rand(100000, 999999);
        //$resp = Alisms::send(config('alisms.accessKeyId'), config('alisms.accessKeySecret'), $phone, config('alisms.SignName'), config('alisms.TemplateCode'), ['code' => $code]);
        $resp = $this->sendtyu($phone,$code);
        // $code = 123456;
         //$resp = true;
        if ($resp === true) {
            cache($phone . '_' . $prefix, $code, bcmul(config('alisms.yzm_indate'),60));
            cache($phone . '_' . $prefix . '_yzm', $time); //, $again
            $this->returnAPI('验证码发送成功', 0); //.$code
        }
        $this->returnAPI('验证码发送失败');
    }*/
    protected function checkyzm($phone, $code, $prefix)
    {
        if (!cache($phone . '_' . $prefix . '_yzm')) {
            $this->returnAPI('请先发送验证码');
        }
        if (intval(cache($phone . '_' . $prefix . '_yzm')) <= (time() - config('alisms.yzm_indate') * 60)) {
            $this->returnAPI('验证码已失效');
        }
        if ($code != cache($phone . '_' . $prefix)) {
            $this->returnAPI('验证码有误');
        }
        /*cache($phone . '_' . $prefix, null);
        cache($phone . '_' . $prefix . '_yzm', null);*/
    }
    /**
     * 设置定时器
     * @param  integer $time 执行时间
     * @param  string  $url  执行方法
     */
    protected function timer($time, $url)
    {
        $http = Request::create();
        $http->get('http://0.0.0.0:2345', [], [
            'type' => 'create',
            'time' => $time,
            'url'  => $this->request->domain() . $url,
        ]);
    }
    protected function cancel_order($order, $time)
    {
        Db::startTrans();
        try {
            $order->save(['status' => 9, 'cancel_time' => $time]);
            if ($order->user_coupon_id > 0) {
                $user_coupon = model('user_coupon')->get($order->user_coupon_id);
                if (!is_null($user_coupon) && $user_coupon->status == 1) {
                    if ($user_coupon->end_time <= time()) {
                        $user_coupon->status = 2;
                    } else {
                        $user_coupon->status = 0;
                    }
                    $user_coupon->save(['used_time' => null]);
                }
            }
            $order_goods = db('order_goods')->field(['goods_id', 'specs_id', 'amount'])->where('order_id', $order->id)->select();
            foreach ($order_goods as $v) {
                if ($v['specs_id'] === '') {
                    db('goods')->where('id', $v['goods_id'])->setInc('kucun', $v['amount']);
                } else {
                    db('goods_specs')->where('goods_id', $v['goods_id'])->where('specs', $v['specs_id'])->setInc('kucun', $v['amount']);
                }
            }
            // 提交事务
            Db::commit();
            $result = true;
        } catch (\Exception $e) {
            trace('取消订单失败:' . $e->getMessage());
            // 回滚事务
            Db::rollback();
            $result = false;
        }
        return $result;
    }
    protected function refund_num_back($refund)
    {
        $refund_cont = json_decode($refund->refund_cont, true);
        foreach ($refund_cont as $v) {
            db('order_goods')->where('id', $v['id'])->setDec('refund_num', $v['refund_num']);
        }
    }
    public function user_score($user_id, $score, $type)
    {
        $user = model('user')->get($user_id);
        if (!is_null($user)) {
            if (in_array($type, [1])) {
                $user->score -= $score;
            } else {
                $user->score += $score;
                $user->allscore += $score;
            }
            $user->save();
            model('user_score')->save([
                'user_id' => $user->id,
                'type'    => $type,
                'score'   => $score,
                'balance' => $user->score,
            ]);
        }
    }
    //根据经纬度计算距离
    protected function getdistance($lng1, $lat1, $lng2, $lat2)
    {
        //将角度转为狐度
        $radLat1 = deg2rad($lat1);
        $radLat2 = deg2rad($lat2);
        $radLng1 = deg2rad($lng1);
        $radLng2 = deg2rad($lng2);
        $a       = $radLat1 - $radLat2; //两纬度之差,纬度<90
        $b       = $radLng1 - $radLng2; //两经度之差纬度<180
        $s       = 2 * asin(sqrt(pow(sin($a / 2), 2) + cos($radLat1) * cos($radLat2) * pow(sin($b / 2), 2))) * 6371 * 1000;
        return $s;
    }

//    protected function randomkeys($length){
//        $info="";
//        $pattern = '1234567890ABCDEFGHIJKLMNOPQISTUVWXYZ';
//        for($i=0;$i<$length;$i++) {
//            $info .= $pattern{mt_rand(0,35)};    //生成php随机数
//        }
//        $user = db('user_invite_code')->where('code',$info)->find(); //model('user_invite_code')->get(['code' => $info]);
//        if (is_null($user)) {
//            return $info;
//        } else {
//            return $this->randomkeys($length);
//        }
//    }


    protected function createCode($userId)
    {
        static $sourceString = [
            0,1,2,3,4,5,6,7,8,9,10,
            'a','b','c','d','e','f',
            'g','h','i','j','k','l',
            'm','n','o','p','q','r',
            's','t','u','v','w','x',
            'y','z'
        ];
        /*static $sourceString = [
            0,1,2,3,4,5,6,7,8,9,10,
            'A','B','C','D','E','F',
            'G','H','I','J','K','L',
            'M','N','O','P','Q','I',
            'S','T','U','V','W','X',
            'Y','Z'
        ];*/

        $num = $userId;
        $code = '';
        while($num)
        {
            $mod = $num % 36;
            $num = (int)($num / 36);
            $code = "{$sourceString[$mod]}{$code}";
        }

        //判断code的长度
        if( empty($code[4]))
            str_pad($code,5,'0',STR_PAD_LEFT);

        return $code;
    }
    protected function format_distance($juli){
        return $juli < 1000 ? $juli . 'm' : bcdiv($juli, 1000, 2) . 'km';
    }

//    //获取IM
//    public function gen_User_Sig($user_id, $UserSig, $sig_time){
//        if(empty($UserSig) || time() > $sig_time){
//            $tlss = new TLSSigAPIv2(config('txim.sdkappid'), config('txim.key'));
//            $UserSig = $tlss->genUserSig($user_id);
//            $expire_time = time() + 86400*180 - 100;
//            $data = [
//                'UserSig'  => $UserSig,
//                'sig_time' => $expire_time,
//            ];
//            db('user')->where('phone', $user_id)->update($data);
//        }
//        return ['UserSig'=>$UserSig, 'sig_time'=>$sig_time];
//
//    }

    public function citys($lng,$lat){


        $url = "https://restapi.amap.com/v3/geocode/regeo?location={$lng},{$lat}&key=bbe66d87b7156cb11752d4c3bdd3308d";
        $result = json_decode($this->https_request($url));
//        dump($result->regeocode->addressComponent);
        $city=$result->regeocode->addressComponent;
        $area=Db::name('citys')->where('merger_name','like','%'.$city->province.'%'."%".$city->district."%")->find();
        $city=Db::name('citys')->where('id',$area['pid'])->find();
        $province=Db::name('citys')->where('id',$city['pid'])->find();

        return [
            'province'=>$province['id'],
            'city'=>$city['id'],
            'area'=>$area['id'],
        ];
    }
}
