<?php
/**
 * ============================================================================
 * Create by PhpStorm
 * @Author：MOON
 * @Data：2022/4/13 上午9:24
 * @Description: rui
 * ============================================================================
 */

namespace app\api\controller;


use alipay\Alipay;
use think\Controller;
use think\Db;
use think\Model;

class Payback extends Base
{

    /**
     * 微信card
     */
    public function ShopNotify()
    {

        $xml = file_get_contents("php://input");

        if (empty($xml)) {
            Db::name('pay_error_log')->insert(['type'=>'wx','content'=>'微信回调没有数据','create_time'=>date('Y-m-d H:i:s')]);
            # 如果没有数据，直接返回失败
            return false;
        }
        //转义xml
        libxml_disable_entity_loader(true);
        $data = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);


        Db::name('pay_error_log')->insert(['type'=>'wx','content'=>json_encode($data),'create_time'=>date('Y-m-d H:i:s')]);



        if (!array_key_exists('sign',$data)){
            Db::name('pay_error_log')->insert(['type'=>'wx','content'=>'微信回调未含有sign','create_time'=>date('Y-m-d H:i:s')]);
            return false;
        }
        Db::name('pay_error_log')->insert(['type'=>'wx','content'=>'验签通过','create_time'=>date('Y-m-d H:i:s')]);
        //TODO 1、验签
        // if (!$this->wxCheckSign($data)){
        //     Db::name('pay_error_log')->insert(['type'=>'wx','content'=>'签名错误','create_time'=>date('Y-m-d H:i:s')]);
        //     return false;
        // }
        // Db::name('pay_error_log')->insert(['type'=>'wx','content'=>'签名通过','create_time'=>date('Y-m-d H:i:s')]);

        // //TODO 2、进行参数校验
        // if(!array_key_exists("return_code", $data)||(array_key_exists("return_code", $data) && $data['return_code'] != "SUCCESS")) {
        //     //TODO失败,不是支付成功的通知
        //     //如果有需要可以做失败时候的一些清理处理，并且做一些监控
        //     Db::name('pay_error_log')->insert(['type'=>'wx','content'=>'参数异常','create_time'=>date('Y-m-d H:i:s')]);
        //     return false;
        // }
        // Db::name('pay_error_log')->insert(['type'=>'wx','content'=>'参数通过','create_time'=>date('Y-m-d H:i:s')]);

//         if(!array_key_exists("transaction_id", $data)){
//             Db::name('pay_error_log')->insert(['type'=>'wax','content'=>'输入参数不正确','create_time'=>date('Y-m-d H:i:s')]);
// //                $msg = "输入参数不正确";
//             return false;
//         }
        //TODO 3、处理业务逻辑
//        $model = new \app\api\model\Order();
//        $datas['order_sn'] = $data['out_trade_no'];
//        $datas['out_trade_no'] = $data['transaction_id'];
//        $user_id =$model->where(['order_sn'=>$datas['order_sn']])->value('user_id');
//        $datas['pay_code'] = 2;
//        $datas['user_id'] = $user_id;
//        $rs = $model->compeltePay($datas);


        // $model=model('shops')->get(['shopsderno '=>$data['out_trade_no']]);
        $shop=model('shops')->get(['shopsderno'=>$data['out_trade_no']]);

        Db::name('pay_error_log')->insert(['type'=>'信息','content'=>json_encode($shop),'create_time'=>date('Y-m-d H:i:s')]);

//        $data['time']=$model=->time;
//        if ($model->time>998){
//            $data['time']=0;
//            $data['end_time']='永久';
//        }else{
//            $data['time']=$time;
//            $data['end_time']=date("Y-m-d H:i:s",time()+86400*30*$time);
//        }
        $res=$shop->save(['is_show'=>1,'pay_status'=>1,'pay_time'=>time()]);





        if($res){
//            $card=model('card_config')->get($model->cid);
//            $time=time();
//            Db::name('card')->insert([
//                'uid'=>$model->uid,
//                'cid'=>$model->cid,
//                'num'=>$card->num*$model->num,
//                'create_time'=>$time,
//                'end_time'=>$time+($card->num*86400)
//            ]);

            $xml = <<<XML
<xml>
  <return_code><![CDATA[SUCCESS]]></return_code>
  <return_msg><![CDATA[OK]]></return_msg>
</xml>
XML;
        }else{
            $xml = <<<XML
<xml>
  <return_code><![CDATA[FAIL]]></return_code>
  <return_msg><![CDATA[FAIL]]></return_msg>
</xml>
XML;
        }
        return $xml;
    }
    /**
     * 微信验证签名
     * @param $data
     * @return bool
     */
    public function wxCheckSign($data)
    {
        //生成签名
        //todo 1、签名步骤一：按字典序排序参数
        ksort($data);
        //格式化参数格式化成url参数
        $string = "";
        foreach ($data as $k => $v)
        {
            if($k != "sign" && $v != "" && !is_array($v)){
                $string .= $k . "=" . $v . "&";
            }
        }
        $string = trim($string, "&");
        //todo 2、签名步骤二：在string后加入KEY
        $wxPay = new \wxpay\Wxpay();
        $string = $string . "&key=".$wxPay->config['api_key'];
        //todo 3、签名步骤三：MD5加密或者HMAC-SHA256
//        if($this->config->GetSignType() == "MD5"){
        $string = md5($string);
//        } else if($this->config->GetSignType() == "HMAC-SHA256") {
//            $string = hash_hmac("sha256",$string ,$wxPay->config['api_key']);
//        } else {
////            throw new WxPayException("签名类型不支持！");
//            return false;
//        }
        $sign = strtoupper($string);
        if ($sign != $data['sign']){
            //签名错误
            return false;
        }
        return true;
    }
}
