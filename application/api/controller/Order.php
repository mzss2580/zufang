<?php
namespace app\api\controller;

use alipay\Alipay;
use app\common\model\GoodsComment;
use app\common\model\Order as user_order;
use app\common\model\User as Users;
use EasyWeChat\Foundation\Application;
use EasyWeChat\Payment\Order as wx_order;
use think\Db;

/**
 * @title 订单接口
 * @description 接口说明
 */
class Order extends Base
{
    protected $beforeActionList = ['check_login'];

    protected function check_login()
    {
        $user = $this->check_token();
        if (is_null($user)) {
            $this->returnAPI('登录信息有误,请重新登录', 10000);
        }
        $this->user = $user;
    }

    /**
     * @title 我的订单列表
     * @description status 0待付款,1待发货,2待收货,3待评价,4已评价,8已关闭,9已取消
     * @author 开发者
     * @url /api/order/order_list
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:page type:int require:0 default:1 other: desc:页数
     * @param name:limit type:float require:1 default:10 other: desc:一页查询数量
     * @param name:status type:string require:1 default:10 other: desc:全部不传或传"",0待付款,1待发货,2待收货,3待评价,4已评价
     *
     * @return list:列表@
     * @list order_id:订单id shop_id:店铺id shop_name:店铺名称 shop_tel:联系电话 shop_logo:店铺logo logo_thumb:logo缩略图 status:订单状态 amount:购买商品数量  refund_power:是否可申请退款0可以申请1不可申请隐藏退款申请按钮
     */
    public function order_list()
    {
        $orders = user_order::all(function ($query) {
            $query->where('user_id', $this->user->id)->where('status', 0)->where('create_time', '<=', time() - config('api.order_timeout'));
        });
        if (!empty($orders)) {
            foreach ($orders as $order) {
                $this->cancel_order($order, $order->create_time + config('api.order_timeout'));
            }
        }
        $shop = db('system_config')->where('config', 'in', ['shop_name', 'shop_tel', 'shop_logo'])->column('value', 'config');
        $db   = db('order')->alias('o')->join('shop s', 'o.shop_id = s.id', 'left')
            ->field(['o.id' => 'order_id', 'o.shop_id', 'ifnull(s.name,"' . $shop['shop_name'] . '")' => 'shop_name', 'ifnull(s.tel,"' . $shop['shop_tel'] . '")' => 'shop_tel', 'ifnull(s.logo,"' . $shop['shop_logo'] . '")' => 'shop_logo', 'o.status', 'o.amount', 'o.pay_true', 'o.finish_time', 'o.user_refund', 'o.refund_power'])
            ->where('o.user_id', $this->user->id)->where('o.user_del', 0);
        $status = input('post.status', '');
        if ($status !== '') {
            if($status == 5){
                $db->where('o.user_refund', 1);
            }else{
                $db->where('o.status', $status);
            }
        }
        $order_status = get_status('order_status');

        $list = $db->order('o.id', 'desc')->page(input('post.page', 1))->limit(input('post.limit', 10))->select();
        foreach ($list as $k => $v) {
            $goods_list = db('order_goods')->field(['goods_id', 'type', 'name', 'specs_id', 'specs', 'image', 'price', 'amount', 'refund_num', 'pay_price'])->where('order_id', $v['order_id'])->select();
            foreach ($goods_list as $key => $value) {
                $goods_list[$key]['image_thumb'] = $this->image_thumb($value['image']);
                $goods_list[$key]['image']       = (strpos($value['image'], '/') === 0) ? $this->request->domain() . $value['image'] : $value['image'];
            }
            $list[$k]['goods_list']  = $goods_list;
            $list[$k]['logo_thumb']  = $this->image_thumb($v['shop_logo']);
            $list[$k]['shop_logo']   = (strpos($v['shop_logo'], '/') === 0) ? $this->request->domain() . $v['shop_logo'] : $v['shop_logo'];
            $list[$k]['status_desc'] = $order_status[$v['status']];
            $list[$k]['finish_time'] = $v['finish_time'] ? date('Y-m-d H:i:s', $v['finish_time']) : '';
        }
        $this->returnAPI('', 0, ['list' => $list]);
    }

    /**
     * @title 订单详情
     * @description 接口说明
     * @author 刘宇
     * @url /api/order/detail
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:order_id type:int require:1 default:1 other: desc:订单id
     *
     * @return shop_id:店铺id
     * @return shop_name:店铺名称
     * @return shop_tel:店铺联系电话
     * @return shop_logo:店铺logo
     * @return logo_thumb:logo缩略图
     * @return shop_lng:店铺经度
     * @return shop_lat:店铺纬度
     * @return person:收货人
     * @return phone:收货人手机号
     * @return province:省
     * @return city:市
     * @return district:区
     * @return street:街道
     * @return addr:详细地址
     * @return orderno:订单id订单状态
     * @return status:订单状态
     * @return status_desc:订单状态说明
     * @return total:商品总价
     * @return express_type:配送方式，0快递，1自提，2上门
     * @return express_fee:运费
     * @return coupon:优惠金额
     * @return pay_true:实付金额
     * @return pay_type:支付方式，1支付宝，2微信，3用户余额
     * @return create_time:下单时间
     * @return pay_time:支付时间
     * @return cancel_time:取消时间
     * @return send_time:发货时间
     * @return finish_time:完成时间
     * @return finish_time_stamp:确认收货时间戳
     * @return kefu_phone:客服电话
     * @return goods_list:商品列表@
     * @goods_list goods_id:商品id type:类型，0普通/秒杀，1砍价 name:商品名称 specs:购买规格 image:商品图片 price:购买价格 amount:购买数量 refund_num:退款数量 pay_price:退款单价 image_thumb:图片缩略图 refund:退款数组@
     * @refund refund_id:退款id status_desc:退款状态描述
     * @return remark:备注
     *
     */
    public function detail()
    {
        $order = model('order')->get(['id' => input('post.order_id'), 'user_id' => $this->user->id, 'user_del' => 0]);
        if (is_null($order)) {
            $this->returnAPI('订单信息查询失败,请稍后重试');
        }
        if ($order->shop_id == 0) {
            $shop = db('system_config')->where('config', 'in', ['shop_name', 'shop_logo', 'shop_tel', 'shop_lng', 'shop_lat'])->column('value', 'config');

            $shop['shop_id']    = 0;
            $shop['shop_logo']  = (strpos($shop['shop_logo'], '/') === 0) ? $this->request->domain() . $shop['shop_logo'] : $shop['shop_logo'];
            $shop['logo_thumb'] = $this->image_thumb($shop['shop_logo']);
        } else {
            $shop = model('shop')->get($order->shop_id);
            if (is_null($shop)) {
                $this->returnAPI('订单信息查询失败,请稍后重试');
            }
            $shop = [
                'shop_id'    => $shop->id,
                'shop_name'  => $shop->name,
                'shop_tel'   => $shop->tel,
                'shop_logo'  => (strpos($shop->logo, '/') === 0) ? $this->request->domain() . $shop->logo : $shop->logo,
                'logo_thumb' => $this->image_thumb($shop->logo),
                'shop_lng'   => $shop->lng ? $shop->lng : '',
                'shop_lat'   => $shop->lat ? $shop->lat : '',
            ];
        }
        $refund_status = get_status('order_refund_status');
        $refund_list   = db('order_refund')->field(['id' => 'refund_id', 'status', 'refund_cont'])->where('order_id', $order->id)->select();
        $goods_list    = db('order_goods')->field(['id', 'goods_id', 'type', 'name', 'specs_id', 'specs', 'image', 'price', 'amount', 'refund_num', 'pay_price'])->where('order_id', $order->id)->select();
        foreach ($goods_list as $k => $v) {
            $refund = [];
            if (!empty($refund_list)) {
                foreach ($refund_list as $key => $value) {
                    $refund_cont = json_decode($value['refund_cont'], true);
                    if (in_array($v['id'], array_column($refund_cont, 'id'))) {
                        $refund[] = [
                            'refund_id'   => $value['refund_id'],
                            'status_desc' => $refund_status[$value['status']],
                        ];
                    }
                }
            }
            $goods_list[$k]['image_thumb'] = $this->image_thumb($v['image']);
            $goods_list[$k]['image']       = (strpos($v['image'], '/') === 0) ? $this->request->domain() . $v['image'] : $v['image'];
            $goods_list[$k]['refund']      = $refund;
            unset($goods_list[$k]['id']);
        }
        $this->returnAPI('', 0, $shop + [
            'person'          => $order->person,
            'phone'           => $order->phone,
            'province'        => $order->province,
            'city'            => $order->city,
            'district'        => $order->district,
            'street'          => $order->street,
            'addr'            => $order->addr,
            'orderno'         => $order->orderno,
            'status'          => $order->status,
            'status_desc'     => get_status('order_status', $order->status),
            'total'           => $order->total,
            'express_type'    => $order->express_type,
            'express_fee'     => $order->express_fee,
            'coupon'          => $order->coupon,
            'pay_true'        => $order->pay_true,
            'pay_type'        => $order->pay_type,
            'express_company' => $order->express_company,
            'expressno'       => $order->expressno,
            'create_time'     => $order->create_time ? date('Y-m-d H:i:s', $order->create_time) : '',
            'pay_time'        => $order->pay_time ? date('Y-m-d H:i:s', $order->pay_time) : '',
            'cancel_time'     => $order->cancel_time ? date('Y-m-d H:i:s', $order->cancel_time) : '',
            'send_time'       => $order->send_time ? date('Y-m-d H:i:s', $order->send_time) : '',
            'finish_time'     => $order->finish_time ? date('Y-m-d H:i:s', $order->finish_time) : '',
            'kefu_phone'      => get_config('kefu_phone'),
            'refund_power'    => $order->refund_power,
            'goods_list'      => $goods_list,
            'user_refund'     => $order->user_refund,
        ]);
    }

    /**
     * @title 订单支付
     * @description 接口说明
     * @author 开发者
     * @url /api/order/order_pay
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:order_id type:string require:1 default:1 other: desc:订单id
     * @param name:pay_type type:string require:1 default:1 other: desc:支付方式1.支付宝2.微信3.商城余额
     *
     * @return pay_str:微信支付为微信支付配置信息,支付宝支付为pay_str属性，商城余额支付不返回值
     */
    public function order_pay()
    {
        $order = model('order')->get(['id' => input('post.order_id'), 'user_id' => $this->user->id, 'user_del' => 0]);
        if (is_null($order)) {
            $this->returnAPI('订单信息查询失败,请稍后重试');
        }
        if ($order->status > 0) {
            $this->returnAPI('订单当前不可支付');
        }
        $pay_type = intval(input('post.pay_type'));
        if ($pay_type !== 1 && $pay_type !== 2) { //  && $pay_type !== 3
            $this->returnAPI('当前只能使用支付宝付款');
        }
        $order->save(['pay_type' => $pay_type]);
        if ($pay_type === 1) {
            $this->returnAPI('', 0, [
                'pay_str' => Alipay::app(config('alipay.') + [
                    'notify_url' => $this->request->domain() . '/api/notify/alipay_order',
                ], $order->orderno, $order->pay_true, '玖赢商城'),
            ]);
        } else if ($pay_type === 2) {
            $app = new Application(config('wechat.'));
            $res = $app->payment->prepare(new wx_order([
                'trade_type'   => 'APP',
                'body'         => '玖赢商城',
                'out_trade_no' => $order->orderno,
                'total_fee'    => $order->pay_true * 100,
                'notify_url'   => $this->request->domain() . '/api/notify/wxpay_order',
            ]));
            if ($res->return_code === 'SUCCESS') {
                if ($res->result_code === 'SUCCESS') {
                    $this->returnAPI('', 0, $app->payment->configForAppPayment($res->prepay_id));
                } else {
                    trace('微信下单失败:' . $res->err_code . ' ' . $res->err_code_des);
                }
            } else {
                trace('微信下单配置有误:' . $res->return_msg);
            }
            $this->returnAPI('微信支付提交失败,请稍后重试');
        }else if($pay_type == 3){
            //商城余额支付
            $Users = new Users();
            $res = $Users->add_user_account_records($this->user->id, 1, 0, bcmul($order->pay_true, -1, 2), $order->amount, 1, $order->id, 'bz_order', '订单余额支付');
            if($res['status'] == 1){
                user_order::success_order($order->orderno, 3);
                $this->returnAPI('支付成功', 0);
            }else{
                $this->returnAPI($res['msg']);
            }
        }
    }

    /**
     * @title 取消订单
     * @description 接口说明
     * @author 刘宇
     * @url /api/order/order_cancel
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:order_id type:int require:1 default:1 other: desc:订单id
     *
     */
    public function order_cancel()
    {
        $order = model('order')->get(['id' => input('post.order_id'), 'user_id' => $this->user->id, 'user_del' => 0]);
        if (is_null($order)) {
            $this->returnAPI('订单信息查询失败,请稍后重试');
        }
        if ($order->status > 0) {
            $this->returnAPI('订单当前不可取消');
        }
        if ($this->cancel_order($order, time())) {
            $this->returnAPI('取消订单成功', 0);
        }
        $this->returnAPI('取消订单失败,请稍后重试');
    }
    /**
     * @title 用户删除订单
     * @description 接口说明
     * @author 刘宇
     * @url /api/order/user_del
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:order_id type:int require:1 default:1 other: desc:订单id
     *
     */
    public function user_del()
    {
        $order = model('order')->get(['id' => input('post.order_id'), 'user_id' => $this->user->id, 'user_del' => 0]);
        if (!is_null($order)) {
            if ($order->status <= 2) {
                $this->returnAPI('订单当前不可删除');
            }
            $order->save(['user_del' => 1]);
        }
        $this->returnAPI('删除订单成功', 0);
    }

    /**
     * @title 订单确认收货
     * @description 接口说明
     * @author 刘宇
     * @url /api/order/order_finish
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:order_id type:int require:1 default:1 other: desc:订单id
     *
     */
    public function order_finish()
    {
        $order = model('order')->get(['id' => input('post.order_id'), 'user_id' => $this->user->id, 'user_del' => 0]);
        if (is_null($order)) {
            $this->returnAPI('订单信息查询失败,请稍后重试');
        }
        if ($order->status !== 2) {
            $this->returnAPI('订单当前不可确认收货');
        }

        $order->save(['status' => 3, 'finish_time' => time()]);

        $this->returnAPI('确认收货成功', 0);
    }

    /**
     * @title 订单评价
     * @description 接口说明
     * @author 刘宇
     * @url /api/order/order_score
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:order_id type:int require:1 default:1 other: desc:订单id
     * @param name:score type:int require:1 default:1 other: desc:店铺评分,1-5
     * @param name:list['goods_id'] type:int require:1 default:1 other: desc:商品id
     * @param name:list['specs_id'] type:string require:1 default:1 other: desc:规格id
     * @param name:list['score'] type:int require:1 default:1 other: desc:评分,1-5
     * @param name:list['content'] type:string require:1 default:1 other: desc:评价内容
     * @param name:list['image'] type:string require:1 default:1 other: desc:图片数组
     */
    public function order_score()
    {
        $order = model('order')->get(['id' => input('post.order_id'), 'user_id' => $this->user->id, 'user_del' => 0]);
        if (is_null($order)) {
            $this->returnAPI('订单信息查询失败,请稍后重试');
        }
        if ($order->status !== 3) {
            $this->returnAPI('订单当前不可提交评价');
        }
        /*$a = [
          [
              'goods_id'=>1,
              'specs_id'=> 1,
              'score' => 1,
              'content'=>1,
              'images'=>[]
          ]
        ];
        echo json_encode($a);*/
        $list = json_decode(input('post.list'), true);
        /*dump(input('post.list'));
        dump($list);*/
        if (is_null($list) || empty($list)) {
            $this->returnAPI('请设置评价内容');
        }
        // 预设更新商品评价数组
        $goods_id    = [];
        $order_goods = db('order_goods')->field(['goods_id', 'specs_id', 'specs', 'amount'])->where('order_id', $order->id)->select();
        foreach ($list as $v) {
            if (!isset($goods_id[$v['goods_id']])) {
                $goods_id[] = $v['goods_id'];
            }
            if ($v['content'] === '') {
                $this->returnAPI('请填写评价内容');
            }
            $order_goods = model('order_goods')->get([
                'user_id'  => $this->user->id,
                'order_id' => $order->id,
                'goods_id' => $v['goods_id'],
                'specs_id' => $v['specs_id'],
            ]);
            if (!is_null($order_goods)) {
                //$images = $this->upload_thumbs('images_' . $v['goods_id'] . '_' . $v['specs_id'], 5);
                $model  = new GoodsComment();
                $model->save([
                    'user_id'  => $order_goods->user_id,
                    'order_id' => $order_goods->order_id,
                    'shop_id'  => $order_goods->shop_id,
                    'goods_id' => $order_goods->goods_id,
                    'specs_id' => $order_goods->specs_id,
                    'type'     => $order_goods->type,
                    'name'     => $order_goods->name,
                    'image'    => $order_goods->image,
                    'specs'    => $order_goods->specs,
                    'price'    => $order_goods->price,
                    'amount'   => $order_goods->amount,
                    'score'    => $v['score'],
                    'content'  => $v['content'],
                    'images'   => json_encode($v['images']),
                ]);
            }
        }
        foreach ($goods_id as $id) {
            db('goods')->where('id', $id)->setField('score', db('goods_comment')->where('goods_id', $id)->avg('score'));
        }
        model('shop_comment')->save([
            'user_id'  => $this->user->id,
            'order_id' => $order->id,
            'shop_id'  => $order->shop_id,
            'score'    => input('post.score'),
        ]);
        $score = db('shop_comment')->where('shop_id', $order->shop_id)->avg('score');
        if ($order->shop_id > 0) {
            db('shop')->where('id', $order->shop_id)->setField('score', $score);
        } else {
            db('system_config')->where('config', 'shop_score')->setField('value', $score);
        }
        $order->save(['status' => 4]);
        $this->returnAPI('订单评价成功', 0);
    }

    /**
     * @title 退款/售后订单列表
     * @description 接口说明
     * @author 刘宇
     * @url /api/order/refund_list
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:page type:int require:1 default:1 other: desc:当前页码
     * @param name:limit type:int require:1 default:10 other: desc:当页数据量
     * @param name:goods_name type:string require:1 default:10 other: desc:搜索商品
     *
     * @return shop_id:店铺id
     * @return refund_id:退款id
     * @return shop_name:店铺名称
     * @return shop_logo:店铺logo
     * @return logo_thumb:logo缩略图
     * @return status:状态0待审核，1待结算，2已拒绝，3已完成，4已撤销
     * @return status_desc:状态说明
     * @return refund_type:退款方式
     * @return refund_type_desc:退款方式说明
     * @return goods_list:订单商品列表@
     * @goods_list goods_id:商品id type:类型,0普通,1砍价 name:商品名称 specs:规格 image:图片 image_thumb:图片缩略图 refund_num:已退款数量 pay_price:退款金额
     */
    public function refund_list()
    {
        $shop = db('system_config')->where('config', 'in', ['shop_name', 'shop_logo'])->column('value', 'config');
        $list = db('order_refund')->alias('o')->join('shop s', 'o.shop_id = s.id', 'left')
            ->field(['o.id' => 'refund_id', 'o.order_id', 'o.shop_id', 'ifnull(s.name,"' . $shop['shop_name'] . '")' => 'shop_name', 'ifnull(s.logo,"' . $shop['shop_logo'] . '")' => 'shop_logo', 'o.status', 'o.refund_type', 'o.refund_cont'])
            ->where('o.user_id', $this->user->id)->order('o.id', 'desc')->page(input('post.page', 1))->limit(input('post.limit', 10))->select();
        $order_refund_type   = get_status('order_refund_type');
        $order_refund_status = get_status('order_refund_status');
        foreach ($list as $k => $v) {
            $refund_cont    = json_decode($v['refund_cont'], true);
            $order_goods_id = array_column($refund_cont, 'id');
            $order_goods    = array_combine($order_goods_id, array_column($refund_cont, 'refund_num'));
            $goods_list     = db('order_goods')->field(['id', 'goods_id', 'type', 'name', 'specs', 'image', 'pay_price'])->where('id', 'in', $order_goods_id)->where('order_id', $v['order_id'])->select();
            foreach ($goods_list as $key => $value) {
                $goods_list[$key]['refund_num']  = $order_goods[$value['id']];
                $goods_list[$key]['image_thumb'] = $this->image_thumb($value['image']);
                $goods_list[$key]['image']       = (strpos($value['image'], '/') === 0) ? $this->request->domain() . $value['image'] : $value['image'];
                unset($goods_list[$key]['id']);
            }
            $list[$k]['goods_list']       = $goods_list;
            $list[$k]['logo_thumb']       = $this->image_thumb($v['shop_logo']);
            $list[$k]['shop_logo']        = (strpos($v['shop_logo'], '/') === 0) ? $this->request->domain() . $v['shop_logo'] : $v['shop_logo'];
            $list[$k]['status_desc']      = $order_refund_status[$v['status']];
            $list[$k]['refund_type_desc'] = $order_refund_type[$v['refund_type']];
            unset($list[$k]['order_id']);
            unset($list[$k]['refund_cont']);
        }
        $this->returnAPI('', 0, ['list' => $list]);
    }

    /**
     * @title 提交退款申请
     * @description 退款相关接口需实际支付过的订单才可以申请 status[1,2,3,4]时才可申请退款 [3,4]时finish_time在15天之内才可申请退款
     * @author 刘宇
     * @url /api/order/refund_apply
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:order_id type:int require:1 default:1 other: desc:订单id
     * @param name:refund_type type:int require:1 default:10 other: desc:退款方式,0只退款,1退款退货
     * @param name:goods_state type:int require:1 default:10 other: desc:货物状态,0未收到货,1已收到货
     * @param name:reason type:string require:1 default:10 other: desc:退款原因
     * @param name:explain type:string require:1 default:10 other: desc:退款说明,reason=="其他"时必填
     * @param name:refund_goods type:string require:1 default:10 other: desc:退款商品数组,需JSON化
     * @param name:refund_goods['goods_id'] type:string require:1 default:10 other: desc:商品id
     * @param name:refund_goods['specs_id'] type:string require:1 default:10 other: desc:规格id
     * @param name:refund_goods['refund_num'] type:string require:1 default:10 other: desc:退款数量
     * @param name:images type:file require:1 default:10 other: desc:app上传图片
     *
     */
    public function refund_apply()
    {
        $order = model('order')->get(['id' => input('post.order_id'), 'user_id' => $this->user->id, 'user_del' => 0]);
        if (is_null($order)) {
            $this->returnAPI('订单信息查询失败,请稍后重试');
        }
        if (!in_array($order->status, [1, 2, 3, 4])) { //, 3, 4 已收货不能退款
            $this->returnAPI('订单当前不可退款');
        }
        //判断订单是否同意过退款，同意过的不能再退款了 0
        $count  = db('order_refund')->where(['shop_id' => $order->shop_id, 'order_id' => $order->id])->where('status', 'not in', [0, 2, 9])->count();
        if($count > 0){
            $this->returnAPI('每个订单只能申请退款一次。');
        }
        /*if (in_array($order->status, [3, 4]) && $order->finish_time <= (time() - config('api.order_refund'))) {
            $this->returnAPI('订单当前不可退款');
        }*/
        //print_r(json_encode([['goods_id'=>'80', 'specs_id'=>"", 'refund_num'=>"1"]]));die;
        $refund_goods = json_decode(input('post.refund_goods'), true);
        if (is_null($refund_goods) || empty($refund_goods)) {
            $this->returnAPI('请设置退款商品');
        }
        $order_goods  = [];
        $refund_cont  = [];
        $refund_money = 0;
        $order_goods_id   = [];
        $order_goods_nums = [];
        foreach ($refund_goods as $goods) {
            $model = model('order_goods')->get(['order_id' => $order->id, 'goods_id' => $goods['goods_id'], 'specs_id' => $goods['specs_id']]);
            if (is_null($model)) {
                $this->returnAPI('退款商品选择有误,请重新设置');
            }
            if (intval($goods['refund_num']) > $model->amount) { // ($model->amount - $model->refund_num
                $this->returnAPI('超出退款商品可选数量');
            }
            $order_goods[] = ['id' => $model->id, 'refund_num' => intval($goods['refund_num'])]; //$model->refund_num + intval($goods['refund_num'])
            $refund_cont[] = ['id' => $model->id, 'refund_num' => intval($goods['refund_num'])];
            $refund_money += round($goods['refund_num'] * $model->pay_price, 2);

            $order_goods_id[] = $model->id;
            $order_goods_nums[$model->id] = intval($goods['refund_num']);
        }
        //判断退款金额是否要加上运费
        $express_fee = $this->check_express_fee($order, $order_goods_id, $order_goods_nums);
        $refund_money = bcadd($refund_money, $express_fee,2);

        $refund_type = input('post.refund_type');
        $goods_state = input('post.goods_state');
        $reason      = input('post.reason');
        $explain     = input('post.explain');
        $images      = input('images');//json_encode($this->upload_thumbs('images', 3, true));
        $data        = [
            'user_id'        => $order->user_id,
            'order_id'       => $order->id,
            'shop_id'        => $order->shop_id,
            'orderno'        => $order->orderno,
            'refund_orderno' => $order->pay_result ? $order->orderno : $order->union_orderno,
            'order_money'    => $order->pay_result ? $order->pay_true : $order->union_pay_true,
            'pay_type'       => $order->pay_type,
            'refund_type'    => $refund_type,
            'goods_state'    => $goods_state,
            'reason'         => $reason,
            'explain'        => $explain,
            'images'         => $images,
            'refund_cont'    => json_encode($refund_cont),
            'refundno'       => get_orderno('order_refund', 'REFUND', 'refundno'),
            'refund_money'   => $refund_money,
        ];
        $result = $this->validate($data, $this->validate . 'OrderRefund.apply');
        if ($result !== true) {
            $this->returnAPI($result);
        }
        $refund_type = get_status('order_refund_type', $refund_type);
        $goods_state = get_status('order_refund_goods_state', $goods_state);
        $content     = <<<CONTENT
退款类型 $refund_type
货物状态 $goods_state
退款原因 $reason
退款金额 ￥ $refund_money
退款说明 $explain
CONTENT;
        Db::startTrans();
        try {
            if ($order->user_refund == 0) {
                $order->save(['user_refund' => 1]);
            }
            model('order_goods')->saveAll($order_goods);
            $order_refund = model('order_refund')->get(['order_id'=>$order->id]);
            if (is_null($order_refund)) {
                $order_refund = model('order_refund');
            }else{
                if($order_refund->status == 2){
                    $data['status'] = 0;
                    $data['refuse_reason'] = '';
                    $data['refuse_images'] = '';
                    $data['create_time']   = time();
                }
            }

            $order_refund->save($data);
            model('order_refund_history')->save([
                'order_refund_id' => $order_refund->id,
                'user_id'         => $order->user_id,
                'content'         => $content,
                'images'          => $images,
            ]);
            /*if ($order->shop_id > 0) { 确认收货才到账
                $wallet = model('shop_wallet')->get(['shop_id' => $order->shop_id, 'order_id' => $order->id, 'type' => 1]);
                if (!is_null($wallet) && $wallet->freeze == 0) {
                    $wallet->save(['freeze' => 1]);
                }
            }*/
            // 提交事务
            Db::commit();
            $result = true;
        } catch (\Exception $e) {
            trace('退款提交失败:' . $e->getMessage());
            // 回滚事务
            Db::rollback();
            $result = false;
        }
        if ($result) {
            $this->returnAPI('退款提交成功', 0);
        }
        $this->returnAPI('退款提交失败,请稍后重试', 0);
    }

    /**
     * @title 退款详情
     * @description
     * @author 刘宇
     * @url /api/order/refund_detail
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:refund_id type:int require:1 default:1 other: desc:退款id
     *
     * @return order_id:订单id
     * @return refund_id:退款id
     * @return status:退款状态
     * @return status_desc:状态说明
     * @return refund_money:退款金额
     * @return reason:退款原因
     * @return explain:退款说明
     * @return refund_type:退款方式，0只退款，1退款退货
     * @return create_time:退款申请时间
     * @return refundno:退款单号
     * @return goods_list:商品列表@
     * @goods_list goods_id:商品id name:商品名称 specs:商品规格 image:商品图片 pay_price:退款单价 refund_num:退款数量 image_thumb:图片缩略图
     * @return address:订单的收货地址@
     * @address address_id:地址id person:姓名 phone:手机号 province:省 city:市 district:区 addr:详细
     */
    public function refund_detail()
    {
        $refund = model('order_refund')->get(['id' => input('post.refund_id'), 'user_id' => $this->user->id]);
        if (is_null($refund)) {
            $this->returnAPI('退款信息查询失败,请稍后重试');
        }
        $refund_cont    = json_decode($refund->refund_cont, true);
        $order_goods_id = array_column($refund_cont, 'id');
        $order_goods    = array_combine($order_goods_id, array_column($refund_cont, 'refund_num'));
        $goods_list     = db('order_goods')->field(['id', 'goods_id', 'type', 'name', 'specs', 'image', 'pay_price'])->where('id', 'in', $order_goods_id)->where('order_id', $refund->order_id)->select();
        foreach ($goods_list as $key => $value) {
            $goods_list[$key]['refund_num']  = $order_goods[$value['id']];
            $goods_list[$key]['image_thumb'] = $this->image_thumb($value['image']);
            $goods_list[$key]['image']       = (strpos($value['image'], '/') === 0) ? $this->request->domain() . $value['image'] : $value['image'];
            unset($goods_list[$key]['id']);
        }
        //查询下单的id
        $order = db('order')->where('id', $refund->order_id)->find();
        $this->returnAPI('', 0, [
            'refund_id'    => $refund->id,
            'order_id'     => $refund->order_id,
            'status'       => $refund->status,
            'status_desc'  => get_status('order_refund_status', $refund->status),
            'refund_money' => $refund->refund_money,
            'reason'       => $refund->reason,
            'create_time'  => $refund->create_time ? date('Y-m-d H:i:s', $refund->create_time) : '',
            'refundno'     => $refund->refundno,
            'refund_type'  => $refund->refund_type,
            'goods_list'   => $goods_list,
            'shop_tel'     => get_field('shop', $refund->shop_id, 'tel'),
            'kefu_phone'   => get_config('kefu_phone'),
            'shop_address' => [
                'person'   => $refund->person,
                'phone'    => $refund->phone,
                'province' => $refund->province,
                'city'     => $refund->city,
                'district' => $refund->district,
                'addr'     => $refund->addr,
            ],
            'address'   => [
                'address_id'    => $order['address_id'],
                'person'        => $order['person'],
                'phone'         => $order['phone'],
                'province'      => $order['province'],
                'city'          => $order['city'],
                'district'      => $order['district'],
                'addr'          => $order['addr']
            ],
        ]);
    }
    /**
     * @title 退款协商历史
     * @description
     * @author 刘宇
     * @url /api/order/refund_history
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:refund_id type:int require:1 default:1 other: desc:退款id
     *
     * @return content:内容
     * @return images:图片数组@
     * @images images_thumb:图片缩略图 image:图片
     * @return create_time:时间
     * @return user_id:用户id
     * @return head:头像
     * @return nick:昵称
     *
     */
    public function refund_history()
    {
        $shop = db('system_config')->where('config', 'in', ['shop_name', 'shop_logo'])->column('value', 'config');
        $list = db('order_refund_history')->alias('o')->join('user u', 'o.user_id = u.id', 'left')->join('shop s', 'o.shop_id = s.id', 'left')
            ->field(['o.content', 'o.images', 'o.create_time', 'o.user_id', 'u.head', 'u.nick', 'o.shop_id', 'ifnull(s.name,"' . $shop['shop_name'] . '")' => 'shop_name', 'ifnull(s.logo,"' . $shop['shop_logo'] . '")' => 'shop_logo'])
            ->where('o.order_refund_id', input('post.refund_id'))->order('o.create_time', 'desc')->select();
        foreach ($list as $k => $v) {
            if (is_null($v['images']) || $v['images'] == '') {
                $images = [];
            } else {
                $images = json_decode($v['images'], true);
            }
            if (!empty($images)) {
                foreach ($images as $key => $value) {
                    $images[$key] = [
                        'images_thumb' => $this->image_thumb($value),
                        'image'        => (strpos($value, '/') === 0) ? $this->request->domain() . $value : $value,
                    ];
                }
            }else{
                $images = [];
            }
            $list[$k]['images']      = $images;
            $list[$k]['head']        = (strpos($v['head'], '/') === 0) ? $this->request->domain() . $v['head'] : $v['head'];
            $list[$k]['logo_thumb']  = $this->image_thumb($v['shop_logo']);
            $list[$k]['shop_logo']   = (strpos($v['shop_logo'], '/') === 0) ? $this->request->domain() . $v['shop_logo'] : $v['shop_logo'];
            $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
        }
        $this->returnAPI('', 0, ['list' => $list]);
    }
    /**
     * @title 取消退款申请
     * @description
     * @author 刘宇
     * @url /api/order/refund_cancel
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:refund_id type:int require:1 default:1 other: desc:退款id
     *
     */
    public function refund_cancel()
    {
        $refund = model('order_refund')->get(['id' => input('post.refund_id'), 'user_id' => $this->user->id]);
        if (is_null($refund)) {
            $this->returnAPI('退款信息查询失败,请稍后重试');
        }
        if ($refund->status > 0) {
            $this->returnAPI('退款当前不可取消');
        }
        $refund->save(['status' => 9]);
        model('order_refund_history')->save(['order_refund_id' => $refund->id, 'user_id' => $this->user->id, 'content' => '买家取消退款申请']);
        $this->refund_num_back($refund);
        $this->returnAPI('退款取消成功', 0);
    }

    /**
     * @title 退货发货
     * @description
     * @author 刘宇
     * @url /api/order/refund_send
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:refund_id type:int require:1 default:1 other: desc:退款id
     * @param name:express_company type:int require:1 default:1 other: desc:退款公司
     * @param name:expressno type:int require:1 default:1 other: desc:快递单号
     *
     */
    public function refund_send()
    {
        $refund = model('order_refund')->get(['id' => input('post.refund_id'), 'user_id' => $this->user->id]);
        if (is_null($refund)) {
            $this->returnAPI('退款信息查询失败,请稍后重试');
        }
        if ($refund->refund_type == 0) {
            $this->returnAPI('退款类型为只退款');
        }
        if ($refund->status !== 3) {
            $this->returnAPI('退款当前不可发货');
        }
        $express_company = input('post.express_company');
        $expressno       = input('post.expressno');
        $data            = [
            'express_company' => $express_company,
            'expressno'       => $expressno,
            'status'          => 4,
        ];
        $result = $this->validate($data, $this->validate . 'OrderRefund.send');
        if ($result !== true) {
            $this->returnAPI($result);
        }
        $refund->save($data);
        $content = <<<CONTENT
买家发货
物流公司 $express_company
快递单号 $expressno
CONTENT;
        model('order_refund_history')->save(['order_refund_id' => $refund->id, 'user_id' => $this->user->id, 'content' => $content]);
        $this->returnAPI('退款订单发货成功', 0);
    }

    /**
     * @title 查询退款金额
     * @description 选择退款商品后调用，返回可能退多少钱，金额不可修改
     * @author 开发者
     * @url /api/order/refund_amount
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:order_id type:string require:1 default:1 other: desc:订单id
     * @param name:refund_goods type:json require:1 default:1 other: desc:退款商品[{"goods_id":"商品id","specs_id":"规格id","refund_num":"退款数量"},{"goods_id":"86","specs_id":"","refund_num":"8"} ]
     *
     * @return refund_money:退款金额
     */
    public function refund_amount(){
        $order = model('order')->get(['id' => input('post.order_id'), 'user_id' => $this->user->id, 'user_del' => 0]);
        if (is_null($order)) {
            $this->returnAPI('订单信息查询失败,请稍后重试');
        }
        $refund_goods = json_decode(input('post.refund_goods'), true);
        if (is_null($refund_goods) || empty($refund_goods)) {
            $this->returnAPI('请设置退款商品');
        }

        $order_goods_id   = [];
        $order_goods_nums = [];
        $refund_money     = 0;
        foreach ($refund_goods as $goods) {
            $model = model('order_goods')->get(['order_id' => $order->id, 'goods_id' => $goods['goods_id'], 'specs_id' => $goods['specs_id']]);
            if (is_null($model)) {
                $this->returnAPI('退款商品选择有误,请重新设置');
            }
            if (intval($goods['refund_num']) > $model->amount) { // ($model->amount - $model->refund_num
                $this->returnAPI('超出退款商品可选数量');
            }
            $order_goods_id[] = $model->id;
            $order_goods_nums[$model->id] = intval($goods['refund_num']);

            $refund_money += round($goods['refund_num'] * $model->pay_price, 2);
        }
        $express_fee = $this->check_express_fee($order, $order_goods_id, $order_goods_nums);
        $refund_money = bcadd($refund_money, $express_fee, 2);
        $this->returnAPI('获取成功', 0, ['refund_money' => $refund_money]);

    }

    private function check_express_fee($order, $order_goods_id, $order_goods_nums){

        $count    = db('order_goods')
                            ->where('order_id', $order->id)
                            ->where('id', 'not in', $order_goods_id)
                            ->count();
        if($count > 0){
            return 0;
        }
        $express_fee = $order->express_fee;
        //全部商品都退款了，判断每个商品退款的数量，是否和购买的一致
        $goods_list = db('order_goods')->field(['id', 'amount'])->where('order_id', $order->id)->select();
        foreach($goods_list as $goods){
            if($goods['amount'] > $order_goods_nums[$goods['id']]){
                $express_fee = 0;
                break;
            }
        }
        return $express_fee;
    }
}
