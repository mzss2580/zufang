<?php
namespace app\api\controller;

/**
 * @title 购物车
 * @description 接口说明
 */
class Cart extends User
{
    /**
     * @title 加入购物车
     * @description 接口说明
     * @author 刘宇
     * @url /api/cart/intocart
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:goods_id type:int require:1 default:1 other: desc:商品id
     * @param name:amount type:int require:1 default:10 other: desc:选择数量
     * @param name:specs type:int require:1 default:10 other: desc:选择规格
     *
     * @return cart_id:购物车id
     */
    public function intocart()
    {
        $goods = model('goods')->get(['id' => input('post.goods_id'), 'type' => 0, 'on_sale' => 1, 'is_del'=>0]);
        if (is_null($goods)) {
            $this->returnAPI('商品信息有误或已下架,请确认后重试');
        }
        $goods_specs = db('goods_specs')->where('goods_id', $goods->id)->where('on_sale', 1)->column('specs');
        if (empty($goods_specs)) {
            $specs = '';
        } else {
            $specs = input('post.specs');
            if (!in_array($specs, $goods_specs)) {
                $this->returnAPI('商品规格选择有误,请确认后重试');
            }
        }
        $data = [
            'user_id'  => $this->user->id,
            'shop_id'  => $goods->shop_id,
            'goods_id' => $goods->id,
            'specs'    => $specs,
        ];
        $amount = input('post.amount', 1);
        $cart   = model('cart')->get($data);
        if (is_null($cart)) {
            $cart = model('cart');
            $cart->data($data + ['amount' => $amount]);
        } else {
            $cart->amount += $amount;
        }
        $cart->save();
        $this->returnAPI('加入购物车成功', 0, ['cart_id' => $cart->id]);
    }

    /**
     * @title 查看购物车列表
     * @description
     * @author 开发者
     * @url /api/cart/cartlist
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     *
     * @return goods_list:商品列表@
     * @goods_list goods_specs:商品规格@
     * @goods_specs specs_type:所有可选规格类型数组@ specs_choose:规格选择数组@
     * @specs_type type:规格类型id name:规格类型名称 specs:规格类型下规格明细数组@
     * @specs specs:规格id name:规格名称
     * @specs_choose price:具体价格 image:对应图片 image_thumb:图片缩略图 kucun:对应库存
     */
    public function cartlist()
    {
        $shop = db('system_config')->where('config', 'in', ['shop_name', 'shop_logo'])->column('value', 'config');
        $data = db('cart')->alias('c')->join('shop s', 'c.shop_id = s.id', 'left')->join('goods g', 'c.goods_id = g.id')->join('goods_specs gs', 'c.goods_id = gs.goods_id and c.specs = gs.specs', 'left')
            ->field(['c.id' => 'cart_id', 'c.shop_id', 's.name' => 'shop_name', 's.logo', 'c.goods_id', 'c.specs', 'c.amount', 'g.name', 'if(gs.price = "" || gs.price is null,g.price,gs.price)' => 'price', 'if(gs.image = "" || gs.image is null,g.image,gs.image)' => 'image', 'g.on_sale' => 'goods_on_sale', 'gs.on_sale' => 'goods_specs_on_sale', 'g.is_del'])
            ->where('c.user_id', $this->user->id)->order('c.create_time', 'asc')->select();

        $list = [];
        foreach ($data as $k => $v) {
            if ($v['shop_id'] === 0) {
                $v['shop_name'] = $shop['shop_name'];
                $v['logo']      = $shop['shop_logo'];
            }
            $v['logo_thumb']  = $this->image_thumb($v['logo']);
            $v['logo']        = (strpos($v['logo'], '/') === 0) ? $this->request->domain() . $v['logo'] : $v['logo'];
            $v['image_thumb'] = $this->image_thumb($v['image']);
            $v['image']       = (strpos($v['image'], '/') === 0) ? $this->request->domain() . $v['image'] : $v['image'];
            $v['specs']       = $this->parse_specs($v['specs']);
            if (!isset($list[$v['shop_id']])) {
                $list[$v['shop_id']] = [
                    'shop_id'    => $v['shop_id'],
                    'shop_name'  => $v['shop_name'],
                    'logo'       => $v['logo'],
                    'logo_thumb' => $v['logo_thumb'],
                    'check'      => false,
                    'goods_list' => [],
                ];
            }
            $on_sale = 0;
            if ($v['specs'] == '') {
                if ($v['goods_on_sale'] == 1) {
                    $on_sale = 1;
                }
            } else {
                if ($v['goods_specs_on_sale'] == 1) {
                    $on_sale = 1;
                }
            }
            if($v['is_del'] == 0){
                $on_sale = 0;
            }

            $gs_thumb = $this->image_thumb($v['image']);
            $gs_img   = (strpos($v['image'], '/') === 0) ? $this->request->domain() . $v['image'] : $v['image'];
            // 所有规格选择可能
            $specs_choose = [];
            // 当前商品所有上架规格
            $goods_specs = db('goods_specs')->field(['specs', 'kucun', 'price', 'image'])->where('goods_id', $v['goods_id'])->where('on_sale', 1)->select();
            foreach ($goods_specs as $k => $s) {
                if ($s['specs']) {
                    $specs       = explode(',', $s['specs']);
                    $price       = floatval($s['price'] ? $s['price'] : $v['price']);
                    $image_thumb = $s['image'] ? $this->image_thumb($s['image']) : $gs_thumb;
                    $image       = $s['image'] ? ((strpos($s['image'], '/') === 0) ? $this->request->domain() . $s['image'] : $s['image']) : $gs_img;
                    if (count($specs) === 1) {
                        $specs_choose[$specs[0]] = [
                            'price'       => $price,
                            'kucun'       => $s['kucun'],
                            'image'       => $image,
                            'image_thumb' => $image_thumb,
                        ];
                    } else {
                        $specs_choose[$specs[0]][] = [
                            'price' => $price,
                            'kucun' => $s['kucun'],
                        ];
                        $specs_choose[$specs[1]][] = [
                            'price' => $price,
                            'kucun' => $s['kucun'],
                        ];
                        $specs_choose[$s['specs']] = [
                            'price'       => $price,
                            'kucun'       => $s['kucun'],
                            'image'       => $image,
                            'image_thumb' => $image_thumb,
                        ];
                    }
                }
            }
            foreach ($specs_choose as $k => $c) {
                if (isset($c[0])) {
                    $min = 0;
                    $max = 0;
                    foreach ($c as $key => $value) {
                        if ($min === 0) {
                            $min = $value['price'];
                        } else if ($min > $value['price']) {
                            $min = $value['price'];
                        }
                        if ($max === 0) {
                            $max = $value['price'];
                        } else if ($max < $value['price']) {
                            $max = $value['price'];
                        }
                    }
                    $kucun = array_sum(array_column($v, 'kucun'));
                    if ($min == $max) {
                        $specs_choose[$k] = [
                            'price' => $min,
                            'kucun' => $kucun,
                        ];
                    } else {
                        $specs_choose[$k] = [
                            'price' => $min . '-' . $max,
                            'kucun' => $kucun,
                        ];
                    }
                }
            }
            /*$kucun = $v['kucun'];
            if (!empty($specs_choose)) {
                $kucun = array_sum(array_column($specs_choose, 'kucun'));
            }*/
            // 所有可选规格
            $specs_type = [];
            $specs_list = db('specs')->alias('s')->field(['s.id' => 'specs', 's.name', 't.id' => 'type', 't.name' => 'type_name'])->join('specs_type t', 's.type = t.id')->where('s.id', 'in', array_keys($specs_choose))->order(['t.sort' => 'desc', 't.id' => 'desc', 's.sort' => 'desc', 's.id' => 'desc'])->select();
            foreach ($specs_list as $k => $c) {
                if (isset($specs_type[$c['type']])) {
                    $specs_type[$c['type']]['specs'][] = ['specs' => $c['specs'], 'name' => $c['name']];
                } else {
                    $specs_type[$c['type']] = [
                        'type'  => $c['type'],
                        'name'  => $c['type_name'],
                        'specs' => [['specs' => $c['specs'], 'name' => $c['name']]],
                    ];
                }
            }

            $list[$v['shop_id']]['goods_list'][] = [
                'cart_id'     => $v['cart_id'],
                'goods_id'    => $v['goods_id'],
                'specs'       => $v['specs'],
                'amount'      => $v['amount'],
                'name'        => $v['name'],
                'price'       => $v['price'],
                'image'       => $v['image'],
                'image_thumb' => $v['image_thumb'],
                'on_sale'     => $on_sale,
                'check'       => false,
                'goods_specs' => [
                    'specs_type'   => array_values($specs_type),
                    'specs_choose' => $specs_choose,
                ],
            ];
        }
        $this->returnAPI('', 0, ['list' => array_values($list)]);
    }
    /**
     * @title 购物车移除
     * @description 接口说明
     * @author 刘宇
     * @url /api/cart/cart_remove
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:cart_id type:int require:1 default:1,2,3 other: desc:购物车id以','拼接,不传为清空
     *
     */
    public function cart_remove()
    {
        $db      = db('cart')->where('user_id', $this->user->id);
        $cart_id = input('post.cart_id');
        if (!is_null($cart_id) && $cart_id !== '') {
            $db->where('id', 'in', $cart_id);
        }
        $db->delete();
        $this->returnAPI('删除成功', 0);
    }

    /**
     * @title 购物车数量
     * @description 接口说明
     * @author 刘宇
     * @url /api/cart/cart_amount
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:cart_id type:int require:1 default:1,2,3 other: desc:购物车id以','拼接,不传为清空
     * @param name:amount type:int require:1 default:1 other: desc:更改后数量
     */
    public function cart_amount()
    {
        $cart = model('cart')->get(['id' => input('post.cart_id'), 'user_id' => $this->user->id]);
        if (!is_null($cart)) {
            $cart->save(['amount' => input('post.amount', 1)]);
        }
        $this->returnAPI('', 0);
    }

    /**
     * @title 购物车修改规格
     * @description
     * @author 开发者
     * @url /api/cart/cart_up_specs
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:cart_id type:int require:0 default:1 other: desc:购物车id
     * @param name:goods_id type:float require:1 default:10 other: desc:商品id
     * @param name:specs type:string require:1 default:10 other: desc:规格id
     *
     */

    public function cart_up_specs(){
        $cart = model('cart')->get(['id' => input('post.cart_id'), 'user_id' => $this->user->id]);
        if (is_null($cart)) {
            $this->returnAPI('购物车修改失败');
        }

        $goods = model('goods')->get(['id' => input('post.goods_id'), 'type' => 0, 'on_sale' => 1, 'is_del'=> 0]);
        if (is_null($goods)) {
            $this->returnAPI('商品信息有误或已下架,请确认后重试');
        }

        $specs = input('post.specs');
        $goods_specs = db('goods_specs')->where('goods_id', $goods->id)->where('specs',$specs)->where('on_sale', 1)->find();
        if (empty($goods_specs)) {
            $this->returnAPI('商品规格选择有误,请确认后重试');
        }

        $cart->specs = $specs;

        $cart->save();
        $this->returnAPI('修改规格成功', 0, ['cart_id' => $cart->id]);
    }
}
