<?php
namespace app\api\controller;

use app\common\model\User as Users;
use app\common\model\WechatTx;
use app\common\model\WxPay;
use app\user\controller\Config;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key;
use think\Db;
use think\Image;
use PHPQRCode\QRcode;
use Ws\Http\Request;

/**
 * @title 商城首页
 * @description 接口说明
 */
class Index extends Base
{
    private function user_token($user_id, $pwd)
    {
        $builder = new Builder();
        $sha256  = new Sha256();
        $key     = new Key(config('encrypt_salt'));
        $token   = $builder->withClaim('user_id', $user_id)->withClaim('user_pwd', $pwd)->getToken($sha256, $key);
        return simple_encrypt($token->__toString());
    }

    public function test(){
        $arr = Users::getSuperiorUser(10,2,1);
        print_r($arr);
    }
    protected $beforeActionList = ['check_login'];

    protected function check_login()
    {
        $user = $this->check_token();
        if (is_null($user)) {
            // $this->user=model('user')->get(['id'=>1]);
            $this->returnAPI('登录信息有误,请重新登录', 10000);
        }else{
            $this->user = $user;

        }
    }
    /**
     * @title 编辑信息
     * @description 接口说明
     * @author MOON
     * @url /api/index/edit_user
     * @method POST
     *
     * @param name:code type:string require:1 default:1 other: desc:code
     *
     */

    public function edit_user(){
        $data=[
            'head'=>input('head'),
            'nick'=>input('nick'),
        ];
        $this->user->save($data);
        $this->returnAPI('编辑成功');
    }









    /**
     * @title 获取区名
     * @description 接口说明
     * @author MOON
     * @url /api/index/city
     * @method POST
     *
     * @param name:lng type:string require:1 default:1 other: desc:经度
     * @param name:lat type:string require:1 default:1 other: desc:维度
     *
     * @return content:轮播图详情
     */
    public function city(){

        $lng=input('lng');
        $lat=input('lat');
        $url = "https://restapi.amap.com/v3/geocode/regeo?location={$lng},{$lat}&key=bbe66d87b7156cb11752d4c3bdd3308d";
        $result = json_decode($this->https_request($url));
//        dump($result->regeocode->addressComponent);
        $city=$result->regeocode->addressComponent;
        $city=$result->regeocode->addressComponent;
        $area=Db::name('citys')->where('merger_name','like','%'.$city->province.'%'."%".$city->district."%")->find();
        $city=Db::name('citys')->where('id',$area['pid'])->find();
        $province=Db::name('citys')->where('id',$city['pid'])->find();
        $this->returnAPI('获取成功',0,[
            'province'=>$province['name'],
            'pid'=>$province['id'],
            'city'=>$city['name'],
            'cid'=>$city['id'],
            'area'=>$area['name'],
            'aid'=>$area['id'],
        ]);
    }



    /**
     * @title 轮播图
     * @description 接口说明
     * @author MOON
     * @url /api/index/banner
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:id type:string require:1 default:1 other: desc:banner的id
     *
     * @return content:轮播图详情
     */
    public function banner(){
//        $id = input('post.id');
        $info = db('banner')->select();
        foreach ($info as $k=>$item){
            $info[$k]['create_time']=date('Y-m-d H:i:s',$item['create_time']);
            $info[$k]['imag']= (strpos($item['image'], '/') === 0) ? $this->request->domain() . $item['image'] : $item['image'];
        }
        $this->returnAPI('', 0, $info);
    }

    /**
     * @title 用户协议
     * @description 接口说明
     * @author MOON
     * @url /api/index/yonghu
     * @method POST
     **
     * @return content:轮播图详情
     */
    public function yonghu(){
        $yonghu=get_field('system_config',['config'=>'yonghuxieyi'],'value');
//        dump($yonghu);
        $this->returnAPI('获取成功',0,$yonghu);
    }
    /**
     * @title  隐私声明
     * @description 接口说明
     * @author MOON
     * @method POST
     **
     * @return content:轮播图详情
     */
    public function yinsishengming(){
        $yonghu=get_field('system_config',['config'=>'yinsishengming'],'value');
//        dump($yonghu);
        $this->returnAPI('获取成功',0,$yonghu);
    }

    /**
     * @title 上传图片
     * @description 接口说明
     * @author MOON
     * @url /api/index/upload
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:file type:file require:1 default:1 other: desc:文件格式图片
     *     */
    public function upload()
    {
        $path = $this->request->post('path', 'api_images');
        $file = $this->request->file($this->request->post('name', 'file'));
        $file_info = $file->getInfo();
        if ($file) {
//            $return = $this->upload_oss($file_info['tmp_name'], $file_info['name']);
//            $this->returnAPI('上传成功', 0, $return);
            $move = env('ROOT_PATH') . 'public' . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR . $path;
            $info = $file->rule('md5')->move($move);
            if ($info) {
                $src = $move . DIRECTORY_SEPARATOR . $info->getSaveName();
                $img = getimagesize($src);
                if ($img !== false && $img['mime'] != 'image/gif') {
                    $thumb = str_replace('.', '_thumb.', $src);
                    $image = Image::open($src);
                    $image->thumb(150, 150)->save($thumb);
                }
                if ($this->request->post('domain', 1)) {
                    $domain = $this->request->domain();
                } else {
                    $domain = '';
                }
                $return = ['src' => str_replace('\\', '/', str_replace(env('ROOT_PATH') . 'public', $domain, $src))];
                if (isset($thumb)) {
                    $return['thumb'] = str_replace('\\', '/', str_replace(env('ROOT_PATH') . 'public', $domain, $thumb));
                }
                $this->returnAPI('上传成功', 0, $return);
            }
            $this->returnAPI($file->getError());
        }
        $this->returnAPI('上传失败,请稍后重试');
    }
    /**
     * @title 店铺分类
     * @description 接口说明
     * @author MOON
     * @url /api/index/shop_type
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     *
     * @return id:类型id
     * @return name:类型名称
     * @return create_time:创建时间
     */


    public function shop_type(){
        $list=Db::name('shop_type')->select();
        foreach ($list as $Key=>$item){
            $list[$Key]['create_time']=date('Y-m-d H:i:s',$item['create_time']);
        }
        $this->returnAPI('获取成功',0,$list);
    }

    /**
     * @title 入驻费用
     * @description 接口说明
     * @author MOON
     * @url /api/index/price
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     *
     * @return id:类型id
     * @return duration:时长
     * @return price:价格
     * @return create_time:创建时间
     */

    public function price(){
        $list=Db::name('shop_join')->select();
        foreach ($list as $Key=>$item){
            $list[$Key]['create_time']=date('Y-m-d H:i:s',$item['create_time']);
        }
        $this->returnAPI('获取成功',0,$list);
    }


    /**
     * @title 商家入驻
     * @description 接口说明
     * @author MOON
     * @url /api/index/shop_add
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:head type:string require:1 default:1 other: desc:头像
     * @param name:name type:string require:1 default:1 other: desc:小区名称
     * @param name:contacts type:string require:1 default:1 other: desc:面积
     * @param name:zujin type:string require:1 default:1 other: desc:租金
     * @param name:zujin type:string require:1 default:1 other: desc:租金
     * @param name:phone type:string require:1 default:1 other: desc:联系人手机号
     * @param name:address type:string require:1 default:1 other: desc:地址
     * @param name:lng type:string require:1 default:1 other: desc:精度
     * @param name:lat type:string require:1 default:1 other: desc:维度
     * @param name:type type:int require:1 default:1 other: desc:房型
     * @param name:zhuangxiu type:int require:1 default:1 other: desc:装修
     * @param name:louti type:int require:1 default:1 other: desc:楼梯
     * @param name:louceng type:int require:1 default:1 other: desc:楼层
     * @param name:introduce type:int require:1 default:1 other: desc:简介
     * @param name:qualifications type:int require:1 default:1 other: desc:照片
     * @param name:time type:int require:1 default:1 other: desc:购买时长
    //     * @param name:province type:int require:1 default:1 other: desc:省
    //     * @param name:city type:int require:1 default:1 other: desc:市
    //     * @param name:area type:int require:1 default:1 other: desc:区
     *

     */


    public function shop_add(){


        $citys=$this->citys(input('lng'),input('lat'));
        $orderno = get_orderno('shops', 'Shopas', 'shopsderno');

        $data=[
            'shopsderno'=>$orderno,
            'uid'=>$this->user->id,
            'head'=>input('head'),
            'name'=>input('name'),
            'contacts'=>input ('contacts'),
            'phone'=>input('phone'),
            'louceng'=>input('louceng'),
            'louti'=>input('louti'),
            'zhuangxiu'=>input('zhuangxiu'),
            // 'wechat'=>input('wechat'),
            'address'=>input('address'),
            'lng'=>input('lng'),
            'lat'=>input('lat'),
            'type'=>input('type'),
            'introduce'=>input('introduce'),
            'introduce_img'=>input('introduce_img'),
            'qualifications'=>input('qualifications'),
            'province'=>$citys['province'],
            'city'=>$citys['city'],
            'area'=>$citys['area'],
            'zujin'=>input('zujin'),
        ];
//          die_dump($data);
        $time=input('time');
        $time=get_field('shop_join',$time,'duration');
        
    
        $price=get_field('shop_join',input('time'),'price');
        // dump($price);
        $result = $this->validate($data, $this->validate . 'Shops.edit');
        
        // dump($result);
        if ($result===true){
            $data['create_time']=time();
////            $data['pay_time']=time();
///
///

            if ($time=='永久'){
                $data['time']=0;
                $data['end_time']='永久';
            }else{
                $data['time']=$time;
                $data['end_time']=date("Y-m-d H:i:s",time()+86400*30*$time);
            }

            $res=Db::name('shops')->insert($data);
            if ($res){
                $wxpay=new Wechat();
//        $wxpay = new ();
                $data1 = [
                    'subject' => '入驻商家',
                    'total_fee' =>$price*100,
//                    'total_fee' =>0.01*100,
                    'notify_url' => request()->domain() . '/api/payback/ShopNotify',
                    'out_trade_no' =>$orderno,
                    'openid' =>$this->user->wx_openid,
                ];
                $res = $wxpay->wxAppPay($data1);


                $this->returnAPI('支付信息',0,$res);




//                $this->returnAPI('入驻成功',0);
            }else{
                $this->returnAPI('入驻失败');
            }

        }else{
            $this->returnAPI($result);
        }
    }


    /**
     * @title 店铺收藏
     * @description 接口说明
     * @author MOON
     * @url /api/index/shop_collection
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:shop_id type:string require:1 default:1 other: desc:店铺id
     */
    public function shop_collection(){
        $data=[
            'uid'=>$this->user->id,
            'shop_id'=>input('shop_id')
        ];

        $flag=Db::name('shop_collection')->where($data)->find();
        if ($flag){
            $res=Db::name('shop_collection')->where('id',$flag['id'])->delete();
            if ($res){
                $this->returnAPI('取消收藏成功',0);
            }else{
                $this->returnAPI('取消收藏失败');
            }
        }else{
            $res=Db::name('shop_collection')->insert($data);
            if ($res){
                $this->returnAPI('收藏成功',0);
            }else{
                $this->returnAPI('收藏失败');
            }
        }
    }


    /**
     * @title 店铺列表
     * @description 接口说明
     * @author MOON
     * @url /api/index/shop_list
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:lng type:string require:1 default:1 other: desc:当前经度
     * @param name:lat type:string require:1 default:1 other: desc:当前维度
     * @param name:type type:string require:1 default:1 other: desc:类型id
     * @param name:sort type:string require:1 default:1 other: desc:1最新2推荐3距离,主页默认不穿
     * @param name:keyword type:string require:1 default:1 other: desc:关键词
     * @param name:status type:string require:1 default:1 other: desc:1入驻2收藏3浏览,主页默认不穿
     * @param name:province type:string require:0 default:1 other: desc:省
     * @param name:city type:string require:0 default:1 other: desc:市
     * @param name:area type:string require:0 default:1 other: desc:区
     */

    public function shop_list(){
        $db=Db::name('shops')->where('is_show',1)->where('pay_status',1);
        $type=input('type');
        $status=input('status');
        if (!empty($type)){
            $db->where('type',$type);
        }
        if (!empty(input('keyword'))){
            $db->where('name','like',"%".input('keyword')."%");
        }
        if (!empty(input('province'))){
            $db->where('province',input('province'));
        }
        if (!empty(input('city'))){
            $db->where('city',input('city'));
        }
        if (!empty(input('area'))){
            $db->where('area',input('area'));
        }

        switch ($status){
            case 1;//入驻
                $db->where('uid',$this->user->id);
                break;
            case 2;//收藏
                $ids=Db::name('shop_collection')->where('uid',$this->user->id)->field('shop_id')->select();
                $arr=[];
                foreach ($ids as $key=>$item){
                    array_push($arr,$item['shop_id']);
                }
                $arr=implode(',',$arr);
                $db->whereIn('id',$arr)->where('status',1);
                break;
            case 3;//浏览
                $ids=Db::name('shop_show')->where('uid',$this->user->id)->field('shop_id')->select();
                $arr=[];
                foreach ($ids as $key=>$item){
                    array_push($arr,$item['shop_id']);
                }
                $arr=implode(',',$arr);
                break;
        }

        if (input('sort')==1){//最新
            $db->order('id','desc');
        }
        if (input('sort')==2){//是否推荐
            $db->where('is_recommend',1);
        }
        $list=$db->field('id,head,create_time,name,phone,address,status,lng,lat,uid')->select();

        foreach ($list as $key=>$item){
//            $juli=floatval($this->drive(input('lng'),input('lat'),$item['lng'],$item['lat']));
//            $list[$key]['juli']=$juli/1000;
//
////            $list[$key]['juli']=$juli;
//            $list[$key]['julis']=round($juli/1000,1) . '公里';
            $list[$key]['create_time']=date('Y-m-d H:i:s',$item['create_time']);
            $list[$key]['create_time']=date('Y-m-d H:i:s',$item['create_time']);
        }
        if (input('sort')==3){//距离
            $arr=$list;
            $len = count($arr); // 数组长度
            for ($i=0; $i < $len-1; $i++) {
                for ($j=0; $j < $len-1-$i; $j++) {
//                    if($arr[$j]['juli'] > $arr[$j+1]['juli']){ // 相邻两个值作比较，选出大的那个值，交换位置，然后继续往后做比较直到数组最后一个值
//                        $temp = $arr[$j];
//                        $arr[$j] = $arr[$j+1];
//                        $arr[$j+1] = $temp;
//                    }
                }
            }
            $list=$arr;
        }
        $this->returnAPI('获取成功',0,$list);
    }





    /**
     * @title 商品编辑
     * @description 接口说明
     * @author MOON
     * @url /api/index/shop_edit
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:id type:string require:1 default:1 other: desc:店铺id
     * @param name:head type:string require:1 default:1 other: desc:头像
     * @param name:name type:string require:1 default:1 other: desc:商家名称
     * @param name:contacts type:string require:1 default:1 other: desc:联系人
     * @param name:phone type:string require:1 default:1 other: desc:联系人手机号
     * @param name:wechat type:string require:1 default:1 other: desc:联系人微信
     * @param name:address type:string require:1 default:1 other: desc:地址
     * @param name:lng type:string require:1 default:1 other: desc:精度
     * @param name:lat type:string require:1 default:1 other: desc:维度
     * @param name:type type:int require:1 default:1 other: desc:分类id
     * @param name:introduce type:int require:1 default:1 other: desc:简介
     * @param name:qualifications type:int require:1 default:1 other: desc:资质
     * @param name:time type:int require:1 default:1 other: desc:购买时长
     * @param name:province type:int require:1 default:1 other: desc:省
     * @param name:city type:int require:1 default:1 other: desc:市
     * @param name:area type:int require:1 default:1 other: desc:区
     *

     */
    public function shop_edit(){
        $citys=$this->citys(input('lng'),input('lat'));

        $data=[
            'uid'=>$this->user->id,

            'head'=>input('head'),
            'name'=>input('name'),
            'contacts'=>input ('contacts'),
            'phone'=>input('phone'),
            'louceng'=>input('louceng'),
            'louti'=>input('louti'),
            'zhuangxiu'=>input('zhuangxiu'),
            // 'wechat'=>input('wechat'),
            'address'=>input('address'),
            'lng'=>input('lng'),
            'lat'=>input('lat'),
            'type'=>input('type'),
            'introduce'=>input('introduce'),
            'introduce_img'=>input('introduce_img'),
            'qualifications'=>input('qualifications'),
            'province'=>$citys['province'],
            'city'=>$citys['city'],
            'area'=>$citys['area'],
            'zujin'=>input('zujin'),

        ];

        $id=input('id');
        $shop=model('shops')->get(['uid'=>$this->user->id,'id'=>$id]);
        if (empty($shop)){
            $this->returnAPI('未查询到店铺');
        }
        $shop->save($data);


        $this->returnAPI('修改成功',0);
    }

    /**
     * @title 我的入驻
     * @description 接口说明
     * @author MOON
     * @url /api/index/shop_my
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @return id:店铺id
     * @return head:头像
     * @return create_time:入驻时间
     * @return name:店铺名
     * @return phone:手机号
     * @return address:地址
     * @return status:状态
     */

    public function shop_my(){
        $list=Db::name('shops')->where([])->field('id,head,create_time,name,phone,address,status')->select();
        foreach ($list as $key=>$item){
            $list[$key]['head']=(strpos($item['head'], '/') === 0) ? $this->request->domain() . $item['head'] : $item['head'];
            $list[$key]['create_time']=date('Y-m-d H:i:s',$item['create_time']);
            $list[$key]['head']=(strpos($item['head'], '/') === 0) ? $this->request->domain() . $item['head'] : $item['head'];

        }
        $this->returnAPI('获取成功',0,$list);

    }

    /**
     * @title 店铺详情
     * @description 接口说明
     * @author MOON
     * @url /api/index/shop_detail
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:id type:string require:1 default:1 other: desc:店铺id
     * @return id:店铺id
     * @return head:头像
     * @return create_time:入驻时间
     * @return name:店铺名
     * @return phone:手机号
     * @return address:地址
     * @return status:状态
     * @return contacts:联系人
     * @return wechat:联系人微信
     * @return type:分类id
     * @return type_name:分类名称
     * @return introduce:介绍
     * @return qualifications:资质证书
     * @return flag:1已收藏0未收藏
     */
    public function shop_detail(){
        $list=Db::name('shops')->where('id',input('id'))->select();
        if (empty($list)){
            $this->returnAPI('未查询到指定订单');
        }
        $this->show($this->user->id,input('id'));
        foreach ($list as $key=>$item){
            $list[$key]['flag']=model('shop_collection')->get(['uid'=>$this->user->id,'shop_id'=>$item['id']])?1:0;
            $list[$key]['head']=(strpos($item['head'], '/') === 0) ? $this->request->domain() . $item['head'] : $item['head'];
            $list[$key]['qualifications']=(strpos($item['qualifications'], '/') === 0) ? $this->request->domain() . $item['qualifications'] : $item['qualifications '];
            $list[$key]['introduce_img']=(strpos($item['introduce_img'], '/') === 0) ? $this->request->domain() . $item['introduce_img'] : $item['introduce_img '];

            $list[$key]['type_name']=get_field('shop_type',$item['type'],'name');
            $list[$key]['create_time']=date('Y-m-d H:i:s',$item['create_time']);
        }
        $this->returnAPI('获取成功',0,$list);
    }


    public function show($uid,$shop_id){
        $model=model('shop_show');
        $data=['uid'=>$uid,'shop_id'=>$shop_id];
        $show=$model->get($data);
        if (empty($show)){
            $model->insert($data);
        }else{

            $show->save(['count'=>$show->count+1]);
        }
    }
    /**
     * @title 店铺删除
     * @description 接口说明
     * @author MOON
     * @url /api/index/shop_del
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:id type:string require:1 default:1 other: desc:店铺id
     */

    public function shop_del(){
        $id=input('id');
        $shop=model('shops')->get(['id'=>$id]);
        if (empty($shop)){
            $this->returnAPI('为查询到店铺');
        }
        $shop->delete();
        $this->returnAPI('删除成功',0);
    }
    /**
     * @title 店铺操作
     * @description 接口说明
     * @author MOON
     * @url /api/index/shop_status
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:id type:string require:1 default:1 other: desc:店铺id
     */
    public function shop_status(){
        $id=input('id');
        $shop=model('shops')->get(['id'=>$id]);
        if (empty($shop)){
            $this->returnAPI('为查询到店铺');
        }
        if ($shop->status==1){
            $shop->save(['status'=>0]);
        }else{
            $shop->save(['status'=>1]);
        }

        $this->returnAPI('操作成功',0);
    }

    /**
     * @title ces
     * @description 接口说明
     * @author MOON
     * @url /api/index/ces
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:id type:string require:1 default:1 other: desc:店铺id
     */
    public function ces(){
        $wxpay=new Wechat();
//        $wxpay = new ();
        $data1 = [
            'subject' => '购买洗车卡',
            'total_fee' =>1,
            'notify_url' => request()->domain() . '/api/payback/ShopNotify',
            'out_trade_no' =>'11111111111',
            'openid' =>'oOu2m5G3HZpARmNCDelCu9uknMwQ'
        ];
        $res = $wxpay->wxAppPay($data1);
        die_dump($res);
    }

    /**
     * @title 用户信息修改
     * @description 接口说明
     * @author MOON
     * @url /api/index/user_edit
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:head type:string require:1 default:1 other: desc:头像
     * @param name:nick type:string require:1 default:1 other: desc:昵称
     */
    public function user_edit(){
        $data=[
            'head'=>input('head'),
            'nick'=>input('nick')
        ];

        $this->user->save($data);
        $this->returnAPI('修改成功',0);
    }
    /**
     * @title 虚假店铺
     * @description 接口说明
     * @author MOON
     * @url /api/index/false_shop
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     *
     */

    public function false_shop(){
        $list=Db::name('false_shop')->select();
        foreach ($list as $key=>$item){
            $list[$key]['create']=date('Y-m-d H:i:s',$item['create_time']);
        }
        $ruzhu=get_field('system_config',['config'=>'count'],'value');
        $fuwu=get_field('system_config',['config'=>'browse'],'value');
        $this->returnAPI('获取成功',0,['list'=>$list,'ruzhu'=>$ruzhu,'fuwu'=>$fuwu]);
    }

    /**
     * @title 用户反馈
     * @description 接口说明
     * @author MOON
     * @url /api/index/feedback
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:type type:string require:1 default:1 other: desc:类型
     * @param name:content type:string require:1 default:1 other: desc:内容
     */
    public function feedback(){
        $data=[
            'uid'=>$this->user->id,
            'type'=>input('type'),
            'content'=>input('content'),
            'create_time'=>time()
        ];

        $res=Db::name('feedback')->insert($data);

        if ($res){
            $this->returnAPI('反馈成功',0);
        }else{
            $this->returnAPI('反馈失败');
        }
    }
}
