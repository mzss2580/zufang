<?php

namespace app\api\controller;

use think\Db;

class Wechat extends Base
{
    /*
配置参数
*/
    public $config = array(
        'appid' => "wxff323aae2a6bc752",
        'mch_id' => "1666826706",   /*微信申请成功之后邮件中的商户id*/
        'api_key' => "81Bd1fddfbb02ab287be0f7e27e1B4dC"    /*在微信商户平台上自己设定的api密钥 32位*/
    );

    public function wxAppPay($data)
    {
        if (empty($data['subject'])){
            $arr=['status'=>201,'msg'=>'服务标题不能为空'];
            return $arr;
        }
        if (empty($data['out_trade_no'])){
            $arr=['status'=>201,'msg'=>'订单号不能为空'];
            return $arr;
        }
        if (empty($data['total_fee'])){
            $arr=['status'=>201,'msg'=>'订单总金额不能为空'];
            return $arr;
        }
        if (empty($data['notify_url'])){
            $arr=['status'=>201,'msg'=>'回调url不能为空'];
            return $arr;
        }
        if (!isset($data['attach']))
            $data['attach'] = 1;


        header('Content-Type: text/html; charset=utf-8');
        $url = "https://api.mch.weixin.qq.com/pay/unifiedorder";
        $onoce_str = $this->getRandChar(32);
        $d["appid"] = $this->config["appid"];
        $d["body"] = $data['subject'];
        $d["mch_id"] = $this->config['mch_id'];
        $d["nonce_str"] = $onoce_str;
        $d["notify_url"] = !empty($data['notify_url'])?$data['notify_url']:$this->config['notify'];
        $d["out_trade_no"] = $data['out_trade_no'];
        $d["spbill_create_ip"] = $this->get_client_ip();
        $d["total_fee"] = $data['total_fee'];
        $d["trade_type"] = "JSAPI";
        $d["openid"] = $data['openid'];
        $d = array_filter($d);//过滤函数
        ksort($d);
        $str ='';
        foreach($d as $k=>$v) {
            $str.=$k.'='.$v.'&';
        }
        $str.='key='.$this->config['api_key'];;//把秘钥和字符串拼接起来
        $d['sign'] = strtoupper(md5($str));//用md5得到sign 转换成大写


//        dump($d);/**/
        // $s = $this->getSign($d);
        // $d["sign"] = $s;
        // $time = time();
        // $d['timeStamp'] = "$time";
        // p($d);die;
        $xml = $this->arrayToXml($d);
        $response = $this->postXmlCurl($xml, $url);
//        dump($response);
        $response=$this->xmlToArray($response);



        // dump($response);
//        die();
        // p($response);die;

        if($response['return_code'] == 'SUCCESS'){

//            dump($response);
            $time = time();
            $tmp=[];//临时数组用于签名
            $tmp['appId'] = $this->config["appid"];
            $tmp['nonceStr'] = $response['nonce_str'];
            $tmp['package'] = 'prepay_id='.$response['prepay_id'];
            $tmp['signType'] = 'MD5';
            $tmp['timeStamp'] = "$time";


            $tmp = array_filter($tmp);//过滤函数
            ksort($tmp);
            $str_s ='';
            foreach($tmp as $k=>$v) {
                $str_s.=$k.'='.$v.'&';
            }
            $str_s.='key='.$this->config['api_key'];//把秘钥和字符串拼接起来
            $tmp['sign']=strtoupper(md5($str_s));


            // $data1['timeStamp'] = "$time";//时间戳
            // $data1['nonce_str'] = $onoce_str;//随机字符串
            // $data1['signType'] = 'MD5';//签名算法，暂支持 MD5
            // $data1['package'] = 'prepay_id='.$response['prepay_id'];//统一下单接口返回的 prepay_id 参数值，提交格式如：prepay_id=*

            // $data1['sign'] = $this->getSign($tmp);//签名,具体签名方案参见微信公众号支付帮助文档;
            // $data1['out_trade_no'] = $data['out_trade_no'];;
        }else{
            $tmp['RETURN_CODE'] = $response['RETURN_CODE'];
            $tmp['RETURN_MSG'] = $response['RETURN_MSG'];
        }

        return $tmp;
    }


    // 退款接口

    public function payBack($id) {

        $orderInfo = Db::name('Order')->where('id', '=', $id)->find();
        $url = 'https://api.mch.weixin.qq.com/secapi/pay/refund';
        $data = array(
            'appid' => $this->config["appid"],
            'mch_id' => $this->config['mch_id'],
            'nonce_str' => $this->getRandChar(32),
            'out_refund_no' => time(),
            'transaction_id' => $orderInfo['out_trade_no'],
            'total_fee' => $orderInfo['pay_price'] * 100,
            'refund_fee' => $orderInfo['pay_price'] * 100
        );
        $data['sign'] = $this->getSign($data);//生成签名
        $xml = $this->arrayToXml($data);
        $response = $this->postXmlSSLCurl($xml, $url);
        $response = $this->xmlToArray($response);


        // dump($response);
        return $response;
    }


    //需要使用证书的请求
    public function postXmlSSLCurl($xml,$url,$second=30)
    {
        $ch = curl_init();
        //超时时间
        curl_setopt($ch,CURLOPT_TIMEOUT,$second);
        //这里设置代理，如果有的话
        //curl_setopt($ch,CURLOPT_PROXY, '8.8.8.8');
        //curl_setopt($ch,CURLOPT_PROXYPORT, 8080);
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,FALSE);
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,FALSE);
        //设置header
        curl_setopt($ch,CURLOPT_HEADER,FALSE);
        //要求结果为字符串且输出到屏幕上wxAppPay
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);
        //设置证书
        //使用证书：cert 与 key 分别属于两个.pem文件
        //默认格式为PEM，可以注释
        curl_setopt($ch,CURLOPT_SSLCERTTYPE,'PEM');
        curl_setopt($ch,CURLOPT_SSLCERT, realpath('/www/wwwroot/xiche/public/apiclient_cert.pem'));
        // curl_setopt($ch,CURLOPT_SSLCERT, realpath('/home/wwwroot/wuhaiyun-php/apiclient_cert.pem'));
        //默认格式为PEM，可以注释
        curl_setopt($ch,CURLOPT_SSLKEYTYPE,'PEM');
        curl_setopt($ch,CURLOPT_SSLKEY,realpath('/www/wwwroot/xiche/public/apiclient_key.pem'));
        // curl_setopt($ch,CURLOPT_SSLKEY,realpath('/home/wwwroot/wuhaiyun-php/apiclient_key.pem'));
        //post提交方式



        // dump(realpath('/www/wwwroot/xiche/public/apiclient_cert.pem'));
        curl_setopt($ch,CURLOPT_POST, true);
        curl_setopt($ch,CURLOPT_POSTFIELDS,$xml);
        $data = curl_exec($ch);
        //返回结果






        if($data){
            curl_close($ch);
            return $data;
        }
        else {
            $error = curl_errno($ch);
            $errpr = curl_getinfo($ch);
            return $errpr;
            echo "curl出错，错误码:$error"."<br>";
            curl_close($ch);
            return false;
        }
    }


    public function getOrder($prepayId)
    {
        $data["appid"] = $this->config["appid"];
        $data["noncestr"] = $this->getRandChar(32);;
        $data["package"] = "Sign=WXPay";
        $data["partnerid"] = $this->config['mch_id'];
        $data["prepayid"] = $prepayId;
        $data["timestamp"] = time();
        $s = $this->getSign($data, false);
        $data["sign"] = $s;
        return $data;
    }

    /*
        生成签名
    */
    function getSign($Obj)
    {
        foreach ($Obj as $k => $v) {
            $Parameters[strtolower($k)] = $v;
        }
        //签名步骤一：按字典序排序参数
        ksort($Parameters);
//        $String = $this->formatBizQueryParaMap($Parameters, true);
        $String = $this->ToUrlParams($Parameters);
        //echo "【string】 =".$String."</br>";
        //签名步骤二：在string后加入KEY
        $String = $String . "&key=" . $this->config['api_key'];
//        echo "<textarea style='width: 50%; height: 150px;'>$String</textarea> <br />";
        //签名步骤三：MD5加密
        $result = strtoupper(md5($String));
//        $result = hash_hmac("sha256",$String ,$this->config['api_key']);
//        $result = strtoupper($result);
        return $result;
    }

    /**
     * 格式化参数格式化成url参数
     */
    public function ToUrlParams($d)
    {
        $buff = "";
        foreach ($d as $k => $v)
        {
            if($k != "sign" && $v != "" && !is_array($v)){
                $buff .= $k . "=" . $v . "&";
            }
        }

        $buff = trim($buff, "&");
        return $buff;
    }

    //将数组转成uri字符串
    function formatBizQueryParaMap($paraMap, $urlencode){
        $buff = "";
        ksort($paraMap);
        foreach ($paraMap as $k => $v){
            if($urlencode){
                $v = urlencode($v);
            }
            $buff .= strtolower($k) . "=" . $v . "&";
        }
        $reqPar='';
        if (strlen($buff) > 0){
            $reqPar = substr($buff, 0, strlen($buff)-1);
        }
        return $reqPar;
    }


    //获取指定长度的随机字符串
    function getRandChar($length)
    {
        $str = null;
        $strPol = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
        $max = strlen($strPol) - 1;
        for ($i = 0; $i < $length; $i++) {
            $str .= $strPol[rand(0, $max)];//rand($min,$max)生成介于min和max两个数之间的一个随机整数
        }
        return $str;
    }

    /*
        获取当前服务器的IP
    */
    function get_client_ip()
    {
        if ($_SERVER['REMOTE_ADDR']) {
            $cip = $_SERVER['REMOTE_ADDR'];
        } elseif (getenv("REMOTE_ADDR")) {
            $cip = getenv("REMOTE_ADDR");
        } elseif (getenv("HTTP_CLIENT_IP")) {
            $cip = getenv("HTTP_CLIENT_IP");
        } else {
            $cip = "unknown";
        }
        return $cip;
    }

    //数组转xml
    function arrayToXml($arr)
    {
        $xml = "<xml>";
        foreach ($arr as $key => $val) {
            if (is_numeric($val)) {
                $xml .= "<" . $key . ">" . $val . "</" . $key . ">";
            } else
                $xml .= "<" . $key . "><![CDATA[" . $val . "]]></" . $key . ">";
        }
        $xml .= "</xml>";
        return $xml;
    }
    /*xml转数组*/
    function xmlToArray($xml) {


        // $array_data = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
        // return $array_data;


        $result = is_string($xml) ? simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA) : $xml;
        return json_decode(json_encode($result, JSON_UNESCAPED_UNICODE), true);

        // //禁止引用外部xml实体
        // libxml_disable_entity_loader(true);
        // $xmlstring = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);
        // $val = json_decode(json_encode($xmlstring), true);
        // return $val;
    }
    //post https请求，CURLOPT_POSTFIELDS xml格式
    function postXmlCurl($xml, $url, $second = 30)
    {
        //初始化curl
        $ch = curl_init();
        //超时时间
        curl_setopt($ch, CURLOPT_TIMEOUT, $second);
        //这里设置代理，如果有的话
        //curl_setopt($ch,CURLOPT_PROXY, '8.8.8.8');
        //curl_setopt($ch,CURLOPT_PROXYPORT, 8080);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        //设置header
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        //要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        //post提交方式
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        //运行curl
        $data = curl_exec($ch);
        //返回结果
        if ($data) {
            curl_close($ch);
            return $data;
        } else {
            $error = curl_errno($ch);
            echo "curl出错，错误码:$error" . "<br>";
            echo "<a href='http://curl.haxx.se/libcurl/c/libcurl-errors.html'>错误原因查询</a></br>";
            curl_close($ch);
            return false;
        }
    }
}