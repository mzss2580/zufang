<?php
namespace app\api\controller;

use alipay\Alipay;
use app\common\model\Kanjia;
use app\common\model\Order;
use app\common\model\User as Users;
use EasyWeChat\Foundation\Application;
use EasyWeChat\Payment\Order as wx_order;
use think\Db;

/**
 * @title 购买商品
 * @description 接口说明
 */
class Buy extends Base
{
    protected $beforeActionList = ['check_login'];

    protected function check_login()
    {
        $user = $this->check_token();
        if (is_null($user)) {
            $this->returnAPI('登录信息有误,请重新登录', 10000);
        }
        $this->user = $user;
    }

    public function kanjia_create()
    {
        $goods = model('goods')->get(['id' => input('post.goods_id'), 'type' => 1]);
        if (is_null($goods)) {
            $this->returnAPI('商品信息有误或已下架,请确认后重试');
        }
        if (db('kanjia')->where('goods_id', $goods->id)->where('user_id', $this->user->id)->where('status', 0)->count() > 0) {
            $this->returnAPI('您已发起砍价');
        }
        $address = model('address')->get(['id' => input('post.address_id'), 'user_id' => $this->user->id]);
        if (is_null($address)) {
            $this->returnAPI('请选择收货地址');
        }
        $money = mt_rand($goods->kj_min_80, $goods->kj_max_80);
        if ($money > ($goods->del_price - $goods->price)) {
            $money = $goods->del_price - $goods->price;
        }
        $time   = time();
        $kanjia = model('kanjia');
        $kanjia->save([
            'user_id'     => $this->user->id,
            'goods_id'    => $goods->id,
            'name'        => $goods->name,
            'image'       => $goods->image,
            'price'       => $goods->price,
            'del_price'   => $goods->del_price,
            'now_price'   => $goods->del_price - $money,
            'express_fee' => $goods->express_fee,
            'kj_rule'     => $goods->kj_rule,
            'kj_max_80'   => $goods->kj_max_80,
            'kj_min_80'   => $goods->kj_min_80,
            'kj_max_60'   => $goods->kj_max_60,
            'kj_min_60'   => $goods->kj_min_60,
            'kj_max_40'   => $goods->kj_max_40,
            'kj_min_40'   => $goods->kj_min_40,
            'kj_max_20'   => $goods->kj_max_20,
            'kj_min_20'   => $goods->kj_min_20,
            'kj_max_0'    => $goods->kj_max_0,
            'kj_min_0'    => $goods->kj_min_0,
            'address_id'  => $address->id,
            'person'      => $address->person,
            'phone'       => $address->phone,
            'province'    => $address->province,
            'city'        => $address->city,
            'district'    => $address->district,
            'addr'        => $address->addr,
            'indate_time' => $time + $goods->kj_time,
        ]);
        $this->returnAPI('发起砍价成功', 0, [
            'kanjia_id'     => $kanjia->id,
            'goods_id'      => $kanjia->goods_id,
            'image_thumb'   => $this->image_thumb($kanjia->image),
            'image'         => $kanjia->image ? $this->request->domain() . $kanjia->image : '',
            'name'          => $kanjia->name,
            'price'         => $kanjia->price,
            'del_price'     => $kanjia->del_price,
            'now_price'     => $kanjia->now_price,
            'kj_rule'       => $kanjia->kj_rule,
            'indate_second' => $kanjia->indate_time - $time,
            'money'         => $money,
        ]);
    }
    public function kanjia_detail()
    {
        $user_id = $this->user->id;
        $kanjia  = Kanjia::all(function ($query) use ($user_id) {
            $query->where('goods_id', input('post.goods_id'))->where('user_id', $user_id)->order('status');
        });
        if (empty($kanjia)) {
            $this->returnAPI('砍价信息查询失败,请确认后重试');
        }
        $kanjia = $kanjia[0];
        if ($kanjia->indate_time <= time()) {
            $kanjia->save(['status' => 2]);
        }
        $this->returnAPI('', 0, [
            'kanjia_id'     => $kanjia->id,
            'goods_id'      => $kanjia->goods_id,
            'image_thumb'   => $this->image_thumb($kanjia->image),
            'image'         => $kanjia->image ? $this->request->domain() . $kanjia->image : '',
            'name'          => $kanjia->name,
            'price'         => $kanjia->price,
            'del_price'     => $kanjia->del_price,
            'now_price'     => $kanjia->now_price,
            'kj_rule'       => $kanjia->kj_rule,
            'status'        => $kanjia->status,
            'indate_second' => $kanjia->indate_time - time(),
            'bangkan_list'  => db('kanjia_user')->alias('k')->join('weixin_user u', 'k.weixin_user_id = u.id')->where('kanjia_id', $kanjia->id)->field(['k.money', 'u.headimgurl'])->select(),
        ]);
    }
    public function kanjia_finish()
    {
        $kanjia = model('kanjia')->get(['goods_id' => input('post.goods_id'), 'user_id' => $this->user->id, 'status' => 0]);
        if (is_null($kanjia)) {
            $this->returnAPI('砍价信息查询失败,请确认后重试');
        }
        if ($kanjia->indate_time <= time()) {
            $kanjia->save(['status' => 2]);
            $this->returnAPI('砍价已过期');
        }
        $shop = db('system_config')->where('config', 'in', ['shop_name', 'shop_ziti', 'shop_fuwu', 'shop_logo', 'shop_express_fee', 'shop_address', 'shop_lng', 'shop_lat'])->column('value', 'config');
        $list = [
            [
                'kanjia_id'   => $kanjia->id,
                'shop_id'     => 0,
                'shop_name'   => $shop['shop_name'],
                'ziti'        => $shop['shop_ziti'],
                'fuwu'        => $shop['shop_fuwu'],
                'express_fee' => $shop['shop_express_fee'],
                'address'     => $shop['shop_address'],
                'lng'         => $shop['shop_lng'],
                'lat'         => $shop['shop_lat'],
                'logo'        => $shop['shop_logo'],
                'goods_list'  => [
                    [
                        'goods_id'    => $kanjia->goods_id,
                        'type'        => 2,
                        'specs'       => '',
                        'amount'      => 1,
                        'name'        => $kanjia->name,
                        'image'       => $kanjia->image,
                        'price'       => $kanjia->now_price,
                        'express_fee' => $kanjia->express_fee,
                    ],
                ],
            ],
        ];
        $this->order_preview($list);
    }

    /**
     * @title 立即购买
     * @description 接口说明
     * @author 刘宇
     * @url /api/buy/buy_now
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:goods_id type:int require:1 default: other: desc:商品id
     * @param name:specs type:int require:1 default: other: desc:选择规格
     * @param name:amount type:int require:1 default: other: desc:选择数量
     *
     * @return address:收货地址信息@
     * @address address_id:收货地址id person:收货人 phone:收货手机号 province:省 city:市 district:区 addr:详细地址
     * @return list:订单内容列表@
     * @list shop_name:店铺名称 address:店铺地址 lng:店铺经度 lat:店铺纬度 logo:平台logo goods_list:商品列表@ total:店铺订单小计 amount:数量小计
     * @goods_list cart_id:购物车id goods_id:商品id specs:购买规格 amount:购买数量 name:商品名称 price:价格 image:图片 image_thumb:图片缩略图 express_fee:商品运费
     * @return total:总价
     * @return express_fee:总运费
     * @return pay_true:实付金额
     */
    public function buy_now()
    {
        $goods = model('goods')->get(['id' => input('post.goods_id'), 'on_sale' => 1, 'is_del'=>0]);
        if (is_null($goods)) {
            $this->returnAPI('商品信息有误或已下架,请确认后重试');
        }
        $goods_specs = db('goods_specs')->where('goods_id', $goods->id)->where('on_sale', 1)->column('price', 'specs');
        if (empty($goods_specs)) {
            $specs = '';
            $price = $goods->price;
        } else {
            $specs = input('post.specs');
            if (!array_key_exists($specs, $goods_specs)) {
                $this->returnAPI('商品规格选择有误,请确认后重试');
            }
            $price = $goods_specs[$specs];
        }
        if ($goods->type === 1) {
            $this->returnAPI('砍价商品请先进行砍价');
        }
        if ($goods->type === 2 && $goods->ms_end_time < time()) {
            $this->returnAPI('商品秒杀活动已结束');
        }
        if ($goods->shop_id == 0) {
            $shop = db('system_config')->where('config', 'in', ['shop_name', 'shop_ziti', 'shop_fuwu', 'shop_logo', 'shop_express_fee', 'shop_address', 'shop_lng', 'shop_lat'])->column('value', 'config');
        } else {
            $shop = db('shop')->field(['name' => 'shop_name', 'logo' => 'shop_logo', 'ziti' => 'shop_ziti', 'fuwu' => 'shop_fuwu', 'express_fee' => 'shop_express_fee', 'address' => 'shop_address', 'lng' => 'shop_lng', 'lat' => 'shop_lat'])->where('id', $goods->shop_id)->find();
            if (is_null($shop)) {
                $this->returnAPI('商品所属店铺信息有误,请确认后重试');
            }
        }
        $amount = input('post.amount', 1);
        $list   = [
            [
                'shop_id'     => $goods->shop_id,
                'shop_name'   => $shop['shop_name'],
                'ziti'        => $shop['shop_ziti'],
                'fuwu'        => $shop['shop_fuwu'],
                'express_fee' => $shop['shop_express_fee'],
                'address'     => $shop['shop_address'] ? $shop['shop_address'] : '',
                'lng'         => $shop['shop_lng'] ? $shop['shop_lng'] : '',
                'lat'         => $shop['shop_lat'] ? $shop['shop_lat'] : '',
                'logo'        => $shop['shop_logo'],
                'goods_list'  => [
                    [
                        'goods_id'    => $goods->id,
                        'type'        => $goods->type,
                        'specs'       => $specs,
                        'amount'      => $amount,
                        'name'        => $goods->name,
                        'image'       => $goods->image,
                        'price'       => $price,
                        'express_fee' => $goods->express_fee,
                        'integral'    => $goods->integral,
                    ],
                ],
            ],
        ];
        $this->order_preview($list);
    }
    /**
     * @title 购物车结算
     * @description 接口说明
     * @author MOON
     * @url /api/buy/cart_settle
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:cart_id type:string require:1 default: other: desc:购物车id拼接(1,2,3)
     *
     * @return address:收货地址信息@
     * @address address_id:收货地址id person:收货人 phone:收货手机号 province:省 city:市 district:区 addr:详细地址
     * @return list:订单内容列表@
     * @list shop_name:店铺名称 address:店铺地址 lng:店铺经度 lat:店铺纬度 logo:平台logo goods_list:商品列表@ total:店铺订单小计 amount:数量小计
     * @goods_list cart_id:购物车id goods_id:商品id specs:购买规格 amount:购买数量 name:商品名称 price:价格 image:图片 image_thumb:图片缩略图 express_fee:商品运费
     * @return total:总价
     * @return express_fee:总运费
     * @return pay_true:实付金额
     */
    public function cart_settle()
    {
        $shop = db('system_config')->where('config', 'in', ['shop_name', 'shop_ziti', 'shop_fuwu', 'shop_logo', 'shop_express_fee', 'shop_address', 'shop_lng', 'shop_lat'])->column('value', 'config');
        $data = db('cart')->alias('c')->join('shop s', 'c.shop_id = s.id', 'left')->join('goods g', 'c.goods_id = g.id')->join('goods_specs gs', 'c.goods_id = gs.goods_id and c.specs = gs.specs', 'left')
            ->field(['c.id' => 'cart_id', 'c.shop_id', 's.name' => 'shop_name', 's.express_fee' => 'shop_express_fee', 's.ziti', 's.fuwu', 's.address', 's.lng', 's.lat', 's.logo', 'c.goods_id', 'c.specs', 'c.amount','g.integral', 'g.type', 'g.name', 'if(gs.price = "" || gs.price is null,g.price,gs.price)' => 'price', 'if(gs.image = "" || gs.image is null,g.image,gs.image)' => 'image', 'g.express_fee', 'g.on_sale' => 'goods_on_sale', 'gs.on_sale' => 'goods_specs_on_sale'])
            ->where('c.id', 'in', input('post.cart_id'))->where('c.user_id', $this->user->id)->order('c.create_time', 'asc')->select();
        if (empty($data)) {
            $this->returnAPI('请选择购物车内容进行结算');
        }
        $list = [];
        foreach ($data as $k => $v) {
            if ($v['specs'] == '') {
                if ($v['goods_on_sale'] == 0) {
                    continue;
                }
            } else {
                if ($v['goods_specs_on_sale'] == 0) {
                    continue;
                }
            }
            if ($v['shop_id'] === 0) {
                $v['shop_name']        = $shop['shop_name'];
                $v['ziti']             = $shop['shop_ziti'];
                $v['fuwu']             = $shop['shop_fuwu'];
                $v['logo']             = $shop['shop_logo'];
                $v['shop_express_fee'] = $shop['shop_express_fee'];
                $v['address']          = $shop['shop_address'];
                $v['lng']              = $shop['shop_lng'];
                $v['lat']              = $shop['shop_lat'];
            }
            if (!isset($list[$v['shop_id']])) {
                $list[$v['shop_id']] = [
                    'shop_id'     => $v['shop_id'],
                    'shop_name'   => $v['shop_name'],
                    'ziti'        => $v['ziti'],
                    'fuwu'        => $v['fuwu'],
                    'logo'        => $v['logo'],
                    'express_fee' => $v['shop_express_fee'],
                    'address'     => $v['address'],
                    'lng'         => $v['lng'],
                    'lat'         => $v['lat'],
                    'goods_list'  => [],
                ];
            }
            $list[$v['shop_id']]['goods_list'][] = [
                'cart_id'     => $v['cart_id'],
                'goods_id'    => $v['goods_id'],
                'type'        => $v['type'],
                'specs'       => $v['specs'],
                'amount'      => $v['amount'],
                'name'        => $v['name'],
                'price'       => $v['price'],
                'image'       => $v['image'],
                'express_fee' => $v['express_fee'],
                'integral'    => $v['integral'],
            ];
        }
        if (empty($list)) {
            $this->returnAPI('请选择购物车内容进行结算');
        }
        $this->order_preview(array_values($list));
    }
    private function order_preview($list)
    {
        $address = model('address')->get(['user_id' => $this->user->id, 'is_default' => 1]);
        if (is_null($address)) {
            $address = [
                'address_id' => 0,
                'person'     => '',
                'phone'      => '',
                'province'   => '',
                'city'       => '',
                'district'   => '',
                'addr'       => '',
                'street'    => ''
            ];
        } else {
            $address = [
                'address_id' => $address->id,
                'person'     => $address->person,
                'phone'      => $address->phone,
                'province'   => $address->province,
                'city'       => $address->city,
                'district'   => $address->district,
                'addr'       => $address->addr,
                'street'    => $address->street
            ];
        }
        $all_total       = 0;
        $all_express_fee = 0;
        foreach ($list as $k => $v) {
            $amount      = 0;
            $total       = 0;
            $express_fee = floatval($v['express_fee']);
            foreach ($v['goods_list'] as $goods) {
                $amount += $goods['amount'];
                $total += round($goods['price'] * $goods['amount'], 2);
                $express_fee += round($goods['express_fee'] * $goods['amount'], 2);
            }
            $list[$k]['amount']      = $amount;
            $list[$k]['total']       = $total;
            $list[$k]['express_fee'] = $express_fee;
            $all_total += $total;
            $all_express_fee += $express_fee;
        }
        $order = [
            'address'     => $address,
            'list'        => $list,
            'total'       => $all_total,
            'express_fee' => $all_express_fee,
            'pay_true'    => $all_total + $all_express_fee,
        ];
        cache('order_preview_' . $this->user->id, json_encode($order, JSON_UNESCAPED_UNICODE));
        foreach ($order['list'] as $k => $v) {
            foreach ($v['goods_list'] as $key => $value) {
                $order['list'][$k]['goods_list'][$key]['image_thumb'] = $this->image_thumb($value['image']);
                $order['list'][$k]['goods_list'][$key]['image']       = (strpos($value['image'], '/') === 0) ? $this->request->domain() . $value['image'] : $value['image'] ;
                $order['list'][$k]['goods_list'][$key]['specs']       = $this->parse_specs($value['specs']);
            }
            $order['list'][$k]['logo_thumb'] = $this->image_thumb($v['logo']);
            $order['list'][$k]['logo']       = (strpos($v['logo'], '/') === 0) ? $this->request->domain() . $v['logo'] : $v['logo'];
            unset($order['list'][$k]['kanjia_id']);
        }
        $this->returnAPI('', 0, $order);
    }
    /**
     * @title 可选优惠券列表
     * @description 接口说明
     * @author 刘宇
     * @url /api/buy/coupon_list
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:total type:int require:1 default: other: desc:该店铺商品总价,不包括运费
     *
     * @return user_coupon_id:用户优惠券id
     * @return money:优惠金额
     * @return money_max:满当前金额可用
     * @return start_time:有效期开始时间
     * @return end_time:有效期结束时间
     */
    public function coupon_list()
    {
        db('user_coupon')->where('user_id', $this->user->id)->where('status', 0)->where('end_time', '<=', time())->setField('status', 2);
        $shop_id = input('post.shop_id');
        $total   = input('post.total');
        $list    = db('user_coupon')->field(['id' => 'user_coupon_id', 'type', 'money', 'money_max', 'start_time', 'end_time'])->where('user_id', $this->user->id)->where('status', 0)
            ->where(function ($query) use ($total) {
                $query->whereOr('money_max', 0)->whereOr('money_max', '<=', $total);
            })
            ->where(function ($query) use ($shop_id) {
                $query->whereOr('type', 1)->whereOr('shop_id', $shop_id);
            })->page(input('post.page', 1))->limit(input('post.limit', 10))->select();
        foreach ($list as $k => $v) {
            $list[$k]['start_time'] = date('Y-m-d H:i:s', $v['start_time']);
            $list[$k]['end_time']   = date('Y-m-d H:i:s', $v['end_time']);
        }
        $this->returnAPI('', 0, ['list' => $list]);
    }

    /**
     * @title 提交订单
     * @description 接口说明
     * @author 开发者
     * @url /api/buy/confirm_order
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:address_id type:string require:1 default:1 other: desc:地址id
     * @param name:pay_type type:string require:1 default:1 other: desc:支付方式1.支付宝2.微信3.商城余额
     * @param name:list  type:string require:1 default:1 other: desc:各订单配置信息,需json后传json字符串,至少需传"[]"[{"shop_id":"0","express_type":"0","user_coupon_id":"0","remark":"text"},{"shop_id":"0","express_type":"0","user_coupon_id":"0","remark":"text"},]
     *
     * @return pay_str:微信支付为微信支付配置信息,支付宝支付为pay_str属性，商城余额支付不返回值
     */
    public function confirm_order()
    {
        $data = cache('order_preview_' . $this->user->id);
        if (!is_null($data)) {
            $data = json_decode($data, true);
        }
        if (is_null($data) || empty($data)) {
            $this->returnAPI('订单信息查询失败');
        }
        $shop     = $this->shop_list();
        $address  = $this->user_address();
        $pay_type = intval(input('post.pay_type'));
        if ($pay_type !== 1 && $pay_type !== 2) { // && $pay_type !== 2 && $pay_type !== 3
            $this->returnAPI('当前只能使用支付宝付款');
        }
        // 预设订单信息
        list($orderlist, $goods_specs, $order_coupon) = $this->order_list($data['list'], $shop, $address, $pay_type);
        // 验证商品库存及优惠券
        list($orderlist, $usedlist) = $this->check_kucun_coupon($orderlist, $goods_specs, $order_coupon);
        Db::startTrans();
        try {
            if (!empty($usedlist['coupon'])) {
                model('user_coupon')->saveAll($usedlist['coupon']);
            }
            if (!empty($usedlist['goods_kucun'])) {
                model('goods')->saveAll($usedlist['goods_kucun']);
            }
            if (!empty($usedlist['goods_specs_kucun'])) {
                model('goods_specs')->saveAll($usedlist['goods_specs_kucun']);
            }
            // 联合付款金额
            $union_pay_true = round(array_sum(array_column($orderlist, 'pay_true')), 2);
            // 联合付款订单号
            $union_orderno = get_orderno('order', 'UNION', 'union_orderno');
            // 预存购物车id
            $cart_id = [];
            $order_str = '';
            foreach ($orderlist as $key => $order) {
                $orderno = get_orderno('order', 'ORDER', 'orderno');
                $model   = new Order();
                $model->allowField(true)->save($order + ['union_pay_true' => $union_pay_true, 'orderno' => $orderno, 'union_orderno' => $union_orderno]);
                foreach ($order['goods_list'] as $k => $v) {
                    if ($v['cart_id'] > 0) {
                        $cart_id[] = $v['cart_id'];
                    }
                    $order['goods_list'][$k]['user_id']  = $this->user->id;
                    $order['goods_list'][$k]['order_id'] = $model->id;
                    $order['goods_list'][$k]['shop_id']  = $order['shop_id'];
                    $order_str .= $model->id.',';
                }
                $order_str = rtrim($order_str,',');
                db('order_goods')->insertAll($order['goods_list']);
                if ($order['kanjia_id'] > 0) {
                    db('kanjia')->where('id', $order['kanjia_id'])->where('user_id', $this->user->id)->update(['status' => 1, 'buy_time' => time()]);
                }
            }
            if (!empty($cart_id)) {
                db('cart')->where('id', 'in', $cart_id)->where('user_id', $this->user->id)->delete();
            }
            // 提交事务
            Db::commit();
            $result = true;
        } catch (\Exception $e) {
            trace('提交订单失败:' . $e->getMessage());
            // 回滚事务
            Db::rollback();
            $result = false;
        }
        if ($result === false) {
            $this->returnAPI('订单提交失败,请稍后重试');
        }
        cache('order_preview_' . $this->user->id, null);
        //$this->timer(time() + config('api.order_timeout'), '/api/timer/order_cancel/orderno/' . $union_orderno);
        if ($pay_type === 1) {
            $this->returnAPI('', 0, [
                'pay_str' => Alipay::app(config('alipay.') + [
                    'notify_url' => $this->request->domain() . '/api/notify/alipay_union',
                ], $union_orderno, $union_pay_true, '玖赢商城'),
            ]);
        } else if ($pay_type === 2) {
            $app = new Application(config('wechat.'));
            $res = $app->payment->prepare(new wx_order([
                'trade_type'   => 'APP',
                'body'         => '玖赢商城',
                'out_trade_no' => $union_orderno,
                'total_fee'    => $union_pay_true * 100,
                'notify_url'   => $this->request->domain() . '/api/notify/wxpay_union',
            ]));
            if ($res->return_code === 'SUCCESS') {
                if ($res->result_code === 'SUCCESS') {
                    $this->returnAPI('', 0, $app->payment->configForAppPayment($res->prepay_id));
                } else {
                    trace('微信下单失败:' . $res->err_code . ' ' . $res->err_code_des);
                }
            } else {
                trace('微信下单配置有误:' . $res->return_msg);
            }
            $this->returnAPI('微信支付提交失败,请稍后重试');
        } else if($pay_type == 3){
            //商城余额支付
            $Users = new Users();
            $res = $Users->add_user_account_records($this->user->id, 1, 0, bcmul($union_pay_true, -1, 2), $union_pay_true, 1, $order_str, 'bz_order', '订单余额支付', $union_orderno);
            if($res['status'] == 1){
                Order::success_union($union_orderno, 3);
                $this->returnAPI('支付成功', 0);
            }else{
                $this->returnAPI($res['msg']);
            }
        }
    }
    // 用户设置的订单相关信息
    private function shop_list()
    {
        //print_r(json_encode(input('post.list', '[]')));die;
        $list = json_decode(input('post.list', '[]'), true);
        $shop = [];
        foreach ($list as $v) {
            if (!isset($shop[$v['shop_id']])) {
                $shop[$v['shop_id']] = [
                    'express_type'   => (isset($v['express_type']) && in_array($v['express_type'], [0, 1, 2])) ? $v['express_type'] : 0,
                    'user_coupon_id' => (isset($v['user_coupon_id']) && $v['user_coupon_id'] > 0) ? $v['user_coupon_id'] : 0,
                    'remark'         => isset($v['remark']) ? $v['remark'] : '',
                    'shop_id'        => $v['shop_id'],
                ];
            }
        }
        return $shop;
    }
    // 用户收货地址
    private function user_address()
    {
        $address = db('address')->where('id', input('post.address_id'))->where('user_id', $this->user->id)->find();
        if (is_null($address)) {
            $this->returnAPI('请选择收货地址');
        }
        $address = [
            'user_id'    => $this->user->id,
            'address_id' => $address['id'],
            'person'     => $address['person'],
            'phone'      => $address['phone'],
            'province'   => $address['province'],
            'city'       => $address['city'],
            'district'   => $address['district'],
            'addr'       => $address['addr'],
            'street'    =>  $address['street']
        ];
        return $address;
    }
    private function order_list($list, $shop, $address, $pay_type)
    {
        // 店铺运费信息
        $shop_list        = db('shop')->where('id', 'in', array_column($shop, 'shop_id'))->column('express_fee', 'id');

        $shop_express_fee = get_config('shop_express_fee');
        // 订单列表
        $orderlist = [];
        // 预存商品及规格,验证库存
        $goods_specs = [];
        // 预存订单优惠券,验证优惠券
        $order_coupon = [];
        foreach ($list as $k => $v) {
            $total        = 0;
            $express_fee  = 0;
            $integral     = 0; //购物返积分，要加起来
            $express_type = isset($shop[$v['shop_id']]) ? $shop[$v['shop_id']]['express_type'] : 0;
            $goods_list   = [];
            foreach ($v['goods_list'] as $goods) {
                $goods_specs[] = [
                    'goods_id' => $goods['goods_id'],
                    'specs'    => $goods['specs'],
                    'amount'   => $goods['amount'],
                ];
                $goods_list[] = [
                    'cart_id'     => isset($goods['cart_id']) ? $goods['cart_id'] : 0,
                    'goods_id'    => $goods['goods_id'],
                    'type'        => $goods['type'],
                    'name'        => $goods['name'],
                    'image'       => $goods['image'],
                    'specs'       => $goods['specs'],
                    'price'       => $goods['price'],
                    'amount'      => $goods['amount'],
                    'express_fee' => $goods['express_fee'],
                ];
                if ($express_type == 0) {
                    $express_fee += round($goods['express_fee'] * $goods['amount'], 2);
                }
                $total += round($goods['price'] * $goods['amount'], 2);
                //订单一共反多少积分
                $integral += round($goods['integral'] * $goods['amount'], 2);
            }
            $user_coupon_id = isset($shop[$v['shop_id']]) ? $shop[$v['shop_id']]['user_coupon_id'] : 0;
            $order_coupon[] = [
                'user_coupon_id' => $user_coupon_id,
                'shop_id'        => $v['shop_id'],
                'total'          => $total,
            ];
            $orderlist[] = $address + [
                'shop_id'        => $v['shop_id'],
                'kanjia_id'      => isset($v['kanjia_id']) ? $v['kanjia_id'] : 0,
                'remark'         => isset($shop[$v['shop_id']]) ? $shop[$v['shop_id']]['remark'] : '',
                'amount'         => $v['amount'],
                'total'          => $total,
                'express_type'   => $express_type,
                'express_fee'    => $express_fee + floatval($v['shop_id'] == 0 ? $shop_express_fee : (isset($shop_list[$v['shop_id']]) ? $shop_list[$v['shop_id']] : 0)),
                'integral'       => $integral,
                'user_coupon_id' => $user_coupon_id,
                'pay_type'       => $pay_type,
                'goods_list'     => $goods_list,
            ];
        }
        return [$orderlist, $goods_specs, $order_coupon];
    }
    private function check_kucun_coupon($orderlist, $goods_specs, $order_coupon)
    {
        // 预设使用优惠券,扣除库存
        $usedlist = [
            'coupon'            => [],
            'goods_kucun'       => [],
            'goods_specs_kucun' => [],
        ];
        $specsarray = [];
        foreach (array_column($goods_specs, 'specs') as $specs) {
            if ($specs !== '') {
                foreach (explode(',', $specs) as $v) {
                    array_push($specsarray, $v);
                }
            }
        }
        // 规格对应名称
        $all_specs = db('specs')->where('id', 'in', array_unique($specsarray))->column('name', 'id');
        // 所有选择规格库存销售情况
        $specslist = db('goods_specs')->where('goods_id', 'in', array_column($goods_specs, 'goods_id'))->where('specs', 'in', array_column($goods_specs, 'specs'))->column('on_sale,kucun,id', 'specs');
        // 所有选择商品库存销售情况
        $goodslist = db('goods')->where('id', 'in', array_column($goods_specs, 'goods_id'))->column('is_del,on_sale,kucun,name', 'id');
        db('user_coupon')->where('user_id', $this->user->id)->where('status', 0)->where('end_time', '<=', time())->setField('status', 2);
        $couponlist = db('user_coupon')->where('user_id', $this->user->id)->where('id', 'in', array_column($order_coupon, 'user_coupon_id'))->column('type,shop_id,status,money,money_max', 'id');
        foreach ($orderlist as $key => $order) {
            if ($order['user_coupon_id'] > 0) {
                $coupon = $couponlist[$order['user_coupon_id']]['money'];
                if (!array_key_exists($order['user_coupon_id'], $couponlist)) {
                    $this->returnAPI('优惠券查询失败,请重新确认');
                }
                if ($couponlist[$order['user_coupon_id']]['status'] == 1) {
                    $this->returnAPI('优惠券已使用,请重新确认');
                }
                if ($couponlist[$order['user_coupon_id']]['status'] == 2) {
                    $this->returnAPI('优惠券已过期,请重新确认');
                }
                if ($couponlist[$order['user_coupon_id']]['money_max'] > 0 && floatval($couponlist[$order['user_coupon_id']]['money_max']) > $order['total']) {
                    $this->returnAPI('优惠券未满足使用条件,请重新确认');
                }
                if (in_array($order['user_coupon_id'], array_column($usedlist['coupon'], 'id'))) {
                    $this->returnAPI('优惠券重复选用,请重新确认');
                }
                $usedlist['coupon'][] = [
                    'id'        => $order['user_coupon_id'],
                    'status'    => 1,
                    'used_time' => time(),
                ];
            } else {
                $coupon = 0;
            }
            $orderlist[$key]['coupon']   = $coupon;
            $orderlist[$key]['pay_true'] = $order['total'] - $coupon + $order['express_fee'];
            foreach ($order['goods_list'] as $k => $goods) {
                if ($goods['specs'] === '') {
                    $this_specs = '';
                    if (!array_key_exists($goods['goods_id'], $goodslist)) {
                        $this->returnAPI('所选商品信息查询失败,请重新确认');
                    }
                    if ($goodslist[$goods['goods_id']]['on_sale'] == 0 || $goodslist[$goods['goods_id']]['is_del'] == 1) {
                        $this->returnAPI('所选商品' . $goodslist[$goods['goods_id']]['name'] . '已下架,请重新确认');
                    }
                    if ($goodslist[$goods['goods_id']]['kucun'] < $goods['amount']) {
                        $this->returnAPI('所选商品' . $goodslist[$goods['goods_id']]['name'] . '库存不足,请重新确认');
                    }
                    $usedlist['goods_kucun'][] = [
                        'id'    => $goods['goods_id'],
                        'kucun' => $goodslist[$goods['goods_id']]['kucun'] - $goods['amount'],
                    ];
                } else {
                    $this_specs = implode(',', array_map(function ($specs) use ($all_specs) {
                        return $all_specs[$specs];
                    }, explode(',', $goods['specs'])));
                    if (!array_key_exists($goods['specs'], $specslist)) {
                        $this->returnAPI('所选商品规格信息查询失败,请重新确认');
                    }
                    if ($specslist[$goods['specs']]['on_sale'] == 0) {
                        $this->returnAPI('所选商品规格' . $this_specs . '已下架,请重新确认');
                    }
                    if ($specslist[$goods['specs']]['kucun'] < $goods['amount']) {
                        $this->returnAPI('所选商品规格' . $this_specs . '库存不足,请重新确认');
                    }
                    $usedlist['goods_specs_kucun'][] = [
                        'id'    => $specslist[$goods['specs']]['id'],
                        'kucun' => $specslist[$goods['specs']]['kucun'] - $goods['amount'],
                    ];
                }
                $goods_total = round($goods['amount'] * $goods['price'], 2);
                if ($coupon === 0) {
                    $goods_pay_true  = $goods_total;
                    $goods_pay_price = $goods['price'];
                } else {
                    $goods_pay_true  = round($goods_total * (1 - $coupon / $order['total']), 2);
                    $goods_pay_price = round($goods_pay_true / $goods['amount'], 2);
                }
                $orderlist[$key]['goods_list'][$k]['specs_id']  = $goods['specs'];
                $orderlist[$key]['goods_list'][$k]['specs']     = $this_specs;
                $orderlist[$key]['goods_list'][$k]['total']     = $goods_total;
                $orderlist[$key]['goods_list'][$k]['pay_true']  = $goods_pay_true;
                $orderlist[$key]['goods_list'][$k]['pay_price'] = $goods_pay_price;
            }
        }
        return [$orderlist, $usedlist];
    }
}
