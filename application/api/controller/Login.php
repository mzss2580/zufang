<?php
namespace app\api\controller;

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key;
use think\Db;
use Ws\Http\Request;

/**
 * @title 登录注册
 * @description 接口说明
 */
class Login extends Base
{
    private function user_token($user_id, $pwd)
    {
        $builder = new Builder();
        $sha256  = new Sha256();
        $key     = new Key(config('encrypt_salt'));
        $token   = $builder->withClaim('user_id', $user_id)->withClaim('user_pwd', $pwd)->getToken($sha256, $key);
        return simple_encrypt($token->__toString());
    }

    private function log_suc($user, $msg)
    {
        $this->returnAPI($msg, 0, [
            'token'            => $this->user_token($user->id, $user->pwd),
            'head'             => (strpos($user->head, '/') === 0) ? $this->request->domain() . $user->head : $user->head,
            'nick'             => $user->nick,

        ]);
    }
     public function city(){

        $lng=input('lng');
        $lat=input('lat');
        $url = "https://restapi.amap.com/v3/geocode/regeo?location={$lng},{$lat}&key=bbe66d87b7156cb11752d4c3bdd3308d";
        $result = json_decode($this->https_request($url));
//        dump($result->regeocode->addressComponent);
        $city=$result->regeocode->addressComponent;
        $city=$result->regeocode->addressComponent;
        $area=Db::name('citys')->where('merger_name','like','%'.$city->province.'%'."%".$city->district."%")->find();
        $city=Db::name('citys')->where('id',$area['pid'])->find();
        $province=Db::name('citys')->where('id',$city['pid'])->find();
        $this->returnAPI('获取成功',0,[
            'province'=>$province['name'],
            'pid'=>$province['id'],
            'city'=>$city['name'],
            'cid'=>$city['id'],
            'area'=>$area['name'],
            'aid'=>$area['id'],
        ]);
    }
    public function zhucexieyi()
    {
        $this->returnAPI('', 0, db('system_config')->where('config', 'in', ['yonghuxieyi', 'yinsishengming'])->column('value', 'config'));
    }
    /**
     * @title 登录
     * @description 接口说明
     * @author MOON
     * @url /api/login/login
     * @method POST
     *
     * @param name:phone type:string require:1 default:1 other: desc:用户手机号
     * @param name:pwd type:string require:1 default:1 other: desc:密码
     *
     * @return token:用户token
     * @return head:头像地址
     * @return nick:用户昵称
     * @return phone:手机号
     * @return name:用户姓名
     * //@return money:余额
     * //@return integral:积分
     */
//    public function login()
//    {
//        $data = [
//            'phone' => input('post.phone'),
//            'pwd'   => input('post.pwd'),
//        ];
//        $result = $this->validate($data, $this->validate . 'User.login');
//        if ($result === true) {
//            $user = model('user')->get(['phone' => $data['phone']]);
//            if (is_null($user)) {
//                $this->returnAPI('手机号未注册');
//            }
//            if ($user->pwd !== md5($data['pwd'])) {
//                $this->returnAPI('密码有误');
//            }
//            $this->log_suc($user, '登录成功');
//        }
//        $this->returnAPI($result);
//    }
//    public function wx_login()
//    {
//        $code = input('post.code');
//        if (is_null($code)) {
//            $this->returnAPI('微信登录失败');
//        }
//        $http = Request::create();
//        $resp = $http->get('https://api.weixin.qq.com/sns/oauth2/access_token?appid=' . config('wechat.app_id') . '&secret=' . config('wechat.secret') . '&code=' . $code . '&grant_type=authorization_code');
//        $res  = json_decode($resp->raw_body, true);
//        if (isset($res['errcode'])) {
//            trace('微信登录失败' . $resp->raw_body);
//            $this->returnAPI('微信登录失败');
//        }
//        $user = model('user')->get(['wx_openid' => $res['openid']]);
//        if (is_null($user)) {
//            $resp = $http->get('https://api.weixin.qq.com/sns/userinfo?access_token=' . $res['access_token'] . '&openid=' . $res['openid']);
//            $res  = json_decode($resp->raw_body, true);
//            if (isset($res['errcode'])) {
//                trace('微信获取用户个人信息失败' . $resp->raw_body);
//                $this->returnAPI('微信登录失败');
//            }
//            cache('wx_user_info_' . $res['openid'], $resp->raw_body);
//            $this->returnAPI('', 2, ['openid' => $res['openid']]);
//        }
//        $this->log_suc($user, '登录成功');
//    }


    /**
     * @title 微信登录
     * @description 接口说明
     * @author MOON
     * @url /api/index/login
     * @method POST
     *
     * @param name:code type:string require:1 default:1 other: desc:code
     *
     */

    public function login(){
        $code=input('code');
        if (is_null($code)) {
            $this->returnAPI('微信登录失败');
        }
        $http = Request::create();
        $url = "https://api.weixin.qq.com/sns/jscode2session?appid=".config('wechat.app_id')."&secret=" . config('wechat.secret') ."&js_code=$code&grant_type=authorization_code"; //
//        $resp = $http->get('https://api.weixin.qq.com/sns/jscode2session?appid=' . config('wechat.app_id') . '&secret=' . config('wechat.secret') . '&code=' . $code . '&grant_type=authorization_code');

//       dump($url);
        $resp = $http->get($url);
        $res  = json_decode($resp->raw_body, true);



//        die_dump($res);
        if (isset($res['errcode'])) {
            trace('微信登录失败' . $resp->raw_body);
            $this->returnAPI('微信登录失败');
        }
        $user = model('user')->get(['wx_openid' => $res['openid']]);
        if (is_null($user)) {
            $data=[
                'wx_openid' => $res['openid'],
                'nick'=>'微信用户',
                'head'=>'/upload/head.png',
                'create_time'=>time()
            ];
            model('user')->insert($data);
            $user = model('user')->get(['wx_openid' => $res['openid']]);

            $this->log_suc($user, '登录成功');

        }
        $this->log_suc($user, '登录成功');
    }
    public function wx_bind_yzm()
    {
        $data = [
            'phone' => input('post.phone'),
        ];
        $result = $this->validate($data, $this->validate . 'User.yzm');
        if ($result === true) {
            if (db('user')->where('wx_openid', input('post.openid'))->count() > 0) {
                $this->returnAPI('已绑定手机号');
            }
            $user = model('user')->get($data);
            if (!is_null($user) && $user->wx_openid) {
                $this->returnAPI('手机号已绑定');
            }
            $this->sendyzm($data['phone'], 'wx_bind');
        }
        $this->returnAPI($result);
    }
    /**
     * @title 微信绑定
     * @description 接口说明
     * @author MOON
     * @url /api/login/wx_bind
     * @method POST
     *
     * @param name:phone type:string require:1 default:1 other: desc:手机号
     * @param name:code type:string require:1 default:1 other: desc:验证码
     * @param name:tj_code type:string require:0 default:1 other: desc:推荐人推荐码
     */
    public function wx_bind()
    {
        $data = [
            'phone'   => input('post.phone'),
            'code'    => input('post.code'),
            'tj_code' => input('post.tj_code'),
        ];
        $result = $this->validate($data, $this->validate . 'User.code');
        if ($result === true) {
            $openid = input('post.openid');
            if (db('user')->where('wx_openid', $openid)->count() > 0) {
                $this->returnAPI('已绑定手机号');
            }
            $user = model('user')->get(['phone' => $data['phone']]);
            if (!is_null($user) && $user->wx_openid) {
                $this->returnAPI('手机号已绑定');
            }
            $info = json_decode(cache('wx_user_info_' . $openid), true);
            if (is_null($info) || empty($info)) {
                $this->returnAPI('请先进行微信登录');
            }
            $invite_user_id = 0;
            if(!empty($data['tj_code'])){
                $res = $this->check_tjcode($data['tj_code']);
                if(! $res['status']){
                    $this->returnAPI($res['msg']);
                }
                $invite_user_id = $res['msg'];
            }
            $this->checkyzm($data['phone'], $data['code'], 'wx_bind');
            if (is_null($user)) {
                $user = model('user');
                $user->data([
                    'head'             => $info['headimgurl'],
                    'nick'             => $info['nickname'],
                    'phone'            => $data['phone'],
                    'pwd'              => '',
                    'name'             => '',
                    'idcard'           => '',
                    'real_phone'       => '',
                    'personnel_status' => 0,
                    'personnel_auth'   => 0,
                    'invite_user_id'   => $invite_user_id,
                    'is_ban'           => 0,
                ]);
            } else {
                if (!$user->head) {
                    $user->head = $info['headimgurl'];
                }
            }
            $user->save(['wx_unionid' => $info['unionid'], 'wx_openid' => $info['openid']]);
            cache('wx_user_info_' . $openid, null);
            $this->log_suc($user, '手机号绑定成功');
        }
        $this->returnAPI($result);
    }
    public function qq_login()
    {
        $qq_openid    = input('post.openid');
        $access_token = input('post.access_token');
        if (is_null($qq_openid) || is_null($access_token)) {
            $this->returnAPI('QQ登录失败');
        }
        $user = model('user')->get(['qq_openid' => $qq_openid]);
        if (is_null($user)) {
            $http = Request::create();
            $resp = $http->get('https://graph.qq.com/user/get_user_info?access_token=' . $access_token . '&oauth_consumer_key=' . config('qq.APP_ID') . '&openid=' . $qq_openid);
            if ($resp->body->ret == 0) {
                cache('qq_user_info_' . $qq_openid, $resp->raw_body);
                $this->returnAPI('', 2, ['openid' => $qq_openid]);
            }
            trace('QQ获取用户个人信息失败' . $resp->raw_body);
            $this->returnAPI('QQ登录失败');
        }
        $this->log_suc($user, '登录成功');
    }
    public function qq_bind_yzm()
    {
        $data = [
            'phone' => input('post.phone'),
        ];
        $result = $this->validate($data, $this->validate . 'User.yzm');
        if ($result === true) {
            if (db('user')->where('qq_openid', input('post.openid'))->count() > 0) {
                $this->returnAPI('已绑定手机号');
            }
            $user = model('user')->get($data);
            if (!is_null($user) && $user->qq_openid) {
                $this->returnAPI('手机号已绑定');
            }
            $this->sendyzm($data['phone'], 'qq_bind');
        }
        $this->returnAPI($result);
    }
    /**
     * @title QQ绑定
     * @description 接口说明
     * @author MOON
     * @url /api/login/qq_bind
     * @method POST
     *
     * @param name:phone type:string require:1 default:1 other: desc:手机号
     * @param name:code type:string require:1 default:1 other: desc:验证码
     * @param name:tj_code type:string require:0 default:1 other: desc:推荐人推荐码
     */
    public function qq_bind()
    {
        $data = [
            'phone'   => input('post.phone'),
            'code'    => input('post.code'),
            'tj_code' => input('post.tj_code'),
        ];
        $result = $this->validate($data, $this->validate . 'User.code');
        if ($result === true) {
            $openid = input('post.openid');
            if (db('user')->where('qq_openid', $openid)->count() > 0) {
                $this->returnAPI('已绑定手机号');
            }
            $user = model('user')->get(['phone' => $data['phone']]);
            if (!is_null($user) && $user->qq_openid) {
                $this->returnAPI('手机号已绑定');
            }
            $info = json_decode(cache('qq_user_info_' . $openid), true);
            if (is_null($info) || empty($info)) {
                $this->returnAPI('请先进行QQ登录');
            }

            $invite_user_id = 0;
            if(!empty($data['tj_code'])){
                $res = $this->check_tjcode($data['tj_code']);
                if(! $res['status']){
                    $this->returnAPI($res['msg']);
                }
                $invite_user_id = $res['msg'];
            }
            $this->checkyzm($data['phone'], $data['code'], 'qq_bind');
            $head = $info['figureurl_qq_2'] ?: $info['figureurl_qq_1'];
            if (is_null($user)) {
                $user = model('user');
                $user->data([
                    'head'             => $head,
                    'nick'             => $info['nickname'],
                    'phone'            => $data['phone'],
                    'pwd'              => '',
                    'name'             => '',
                    'idcard'           => '',
                    'real_phone'       => '',
                    'personnel_status' => 0,
                    'personnel_auth'   => 0,
                    'invite_user_id'   => $invite_user_id,
                    'is_ban'           => 0,
                ]);
            } else {
                if (!$user->head) {
                    $user->head = $head;
                }
            }
            $user->save(['qq_openid' => $openid]);
            cache('qq_user_info_' . $openid, null);
            $this->log_suc($user, '手机号绑定成功');
        }
        $this->returnAPI($result);
    }
    /**
     * @title 注册发送验证码
     * @description 接口说明
     * @author MOON
     * @url /api/login/register_yzm
     * @method POST
     *
     * @param name:phone type:string require:1 default:1 other: desc:手机号
     *
     */
    public function register_yzm()
    {
        $data = [
            'phone' => input('post.phone'),
        ];
        $result = $this->validate($data, $this->validate . 'User.yzm');
        if ($result === true) {
            $user = model('user')->get($data);
            if (!is_null($user)) {
                $this->returnAPI('手机号已注册', 1, $user);
            }
            $this->sendyzm($data['phone'], 'register');
        }
        $this->returnAPI($result);
    }

    private function check_tjcode($tj_code){
        $code = db('user_invite_code')->where('code', $tj_code)->find();
        if(is_null($code)){
            return returnMsg(0, '当前推荐码不存在。');
        }
        //判断当前uid的用户是否存在
        $r_user = model('user')->get([ 'id'=>$code['uid'] ]);
        if(is_null($r_user)){
            return returnMsg(0, '推荐码用户不存在。');
        }
        return returnMsg(1, $r_user->id);
    }

    /**
     * @title 用户注册
     * @description 接口说明
     * @author MOON
     * @url /api/login/register
     * @method POST
     *
     * @param name:phone type:string require:1 default:1 other: desc:手机号
     * @param name:pwd type:string require:1 default:1 other: desc:密码
     * @param name:code type:string require:1 default:1 other: desc:验证码
     * @param name:tj_code type:string require:0 default:1 other: desc:推荐人推荐码
     */
    public function register()
    {
        $data = [
            'phone'   => input('post.phone'),
            'pwd'     => input('post.pwd'),
            'code'    => input('post.code'),
            'tj_code' => input('post.tj_code'),
        ];
        $result = $this->validate($data, $this->validate . 'User.register');
        if ($result === true) {
            $user = model('user')->get(['phone' => $data['phone']]);
            if (!is_null($user)) {
                $this->returnAPI('手机号已注册');
            }
            $invite_user_id = 0;
            if(!empty($data['tj_code'])){
                $res = $this->check_tjcode($data['tj_code']);
                if(! $res['status']){
                    $this->returnAPI($res['msg']);
                }
                $invite_user_id = $res['msg'];
            }
            $this->checkyzm($data['phone'], $data['code'], 'register');
            $nick = mt_rand(100000,999999);
            $user = model('user');
            $user->save([
                'phone'            => $data['phone'],
                'pwd'              => md5($data['pwd']),
                'head'             => '/head.png',
                'nick'             => $nick,
                'name'             => '',
                'idcard'           => '',
                'real_phone'       => '',
                'personnel_status' => 0,
                'personnel_auth'   => 0,
                'invite_user_id'   => $invite_user_id,
                'is_ban'           => 0,
            ]);
            $this->log_suc($user, '注册成功');
        }
        $this->returnAPI($result);
    }

    /**
     * @title 找回密码--验证码
     * @description 接口说明
     * @author MOON
     * @url /api/login/forget_yzm
     * @method POST
     *
     * @param name:phone type:string require:1 default:1 other: desc:手机号
     */
    public function forget_yzm()
    {
        $data = [
            'phone' => input('post.phone'),
        ];
        $result = $this->validate($data, $this->validate . 'User.yzm');
        if ($result === true) {
            $user = model('user')->get($data);
            if (is_null($user)) {
                $this->returnAPI('手机号未注册');
            }
            $this->sendyzm($data['phone'], 'forget');
        }
        $this->returnAPI($result);
    }

    /**
     * @title 找回密码
     * @description 接口说明
     * @author 开发者
     * @url /api/login/forget
     * @method POST
     *
     * @param name:phone type:string require:1 default:1 other: desc:手机号
     * @param name:pwd type:string require:1 default:1 other: desc:新密码
     * @param name:code type:string require:1 default:1 other: desc:验证码
     */
    public function forget()
    {
        $data = [
            'phone' => input('post.phone'),
            'pwd'   => input('post.pwd'),
            'code'  => input('post.code'),
        ];
        $result = $this->validate($data, $this->validate . 'User.register');
        if ($result === true) {
            $user = model('user')->get(['phone' => $data['phone']]);
            if (is_null($user)) {
                $this->returnAPI('手机号未注册');
            }
            if ($user->pwd === md5($data['pwd'])) {
                $this->returnAPI('与原密码相同,无需修改');
            }
            $this->checkyzm($data['phone'], $data['code'], 'forget');
            $user->save(['pwd' => md5($data['pwd'])]);
            $this->returnAPI('找回密码成功', 0);
        }
        $this->returnAPI($result);
    }
    /**
     * @title 虚假店铺
     * @description 接口说明
     * @author MOON
     * @url /api/login/false_shop
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     *
     */

    public function false_shop(){
        $list=Db::name('false_shop')->select();
        foreach ($list as $key=>$item){
            $list[$key]['create']=date('Y-m-d H:i:s',$item['create_time']);
        }
        $ruzhu=get_field('system_config',['config'=>'count'],'value');
        $fuwu=get_field('system_config',['config'=>'browse'],'value');
        $this->returnAPI('获取成功',0,['list'=>$list,'ruzhu'=>$ruzhu,'fuwu'=>$fuwu]);
    }
    public function banner(){
//        $id = input('post.id');
        $info = db('banner')->select();
        foreach ($info as $k=>$item){
            $info[$k]['create_time']=date('Y-m-d H:i:s',$item['create_time']);
            $info[$k]['imag']= (strpos($item['image'], '/') === 0) ? $this->request->domain() . $item['image'] : $item['image'];
        }
        $this->returnAPI('', 0, $info);
    }
    public function shop_list(){
        $db=Db::name('shops')->where('is_show',1)->where('pay_status',1)->where('status',1);
        $type=input('type');
        $status=input('status');
        if (!empty($type)){
            $db->where('type',$type);
        }
        if (!empty(input('keyword'))){
            $db->where('name','like',"%".input('keyword')."%");
        }
        if (!empty(input('province'))){
            $db->where('province',input('province'));
        }
        if (!empty(input('city'))){
            $db->where('city',input('city'));
        }
        if (!empty(input('area'))){
            $db->where('area',input('area'));
        }

        switch ($status){
            case 1;//入驻
                $db->where('uid',$this->user->id);
                break;
            case 2;//收藏
                $ids=Db::name('shop_collection')->where('uid',$this->user->id)->field('shop_id')->select();
                $arr=[];
                foreach ($ids as $key=>$item){
                    array_push($arr,$item['shop_id']);
                }
                $arr=implode(',',$arr);
                $db->whereIn('id',$arr)->where('status',1);
                break;
            case 3;//浏览
                $ids=Db::name('shop_show')->where('uid',$this->user->id)->field('shop_id')->select();
                $arr=[];
                foreach ($ids as $key=>$item){
                    array_push($arr,$item['shop_id']);
                }
                $arr=implode(',',$arr);
                break;
        }

        if (input('sort')==1){//最新
            $db->order('id','desc');
        }
        if (input('sort')==2){//是否推荐
            $db->where('is_recommend',1);
        }
        $list=$db->field('id,head,create_time,name,phone,address,status,lng,lat,uid')->select();

        foreach ($list as $key=>$item){
//            $juli=floatval($this->drive(input('lng'),input('lat'),$item['lng'],$item['lat']));
//            $list[$key]['juli']=$juli/1000;
//
////            $list[$key]['juli']=$juli;
//            $list[$key]['julis']=round($juli/1000,1) . '公里';
            $list[$key]['create_time']=date('Y-m-d H:i:s',$item['create_time']);
            $list[$key]['create_time']=date('Y-m-d H:i:s',$item['create_time']);
        }
        if (input('sort')==3){//距离
            $arr=$list;
            $len = count($arr); // 数组长度
            for ($i=0; $i < $len-1; $i++) {
                for ($j=0; $j < $len-1-$i; $j++) {
//                    if($arr[$j]['juli'] > $arr[$j+1]['juli']){ // 相邻两个值作比较，选出大的那个值，交换位置，然后继续往后做比较直到数组最后一个值
//                        $temp = $arr[$j];
//                        $arr[$j] = $arr[$j+1];
//                        $arr[$j+1] = $temp;
//                    }
                }
            }
            $list=$arr;
        }
        $this->returnAPI('获取成功',0,$list);
    }
    
     public function shop_type(){
        $list=Db::name('shop_type')->select();
        foreach ($list as $Key=>$item){
            $list[$Key]['create_time']=date('Y-m-d H:i:s',$item['create_time']);
        }
        $this->returnAPI('获取成功',0,$list);
    }

    /**
     * @title 入驻费用
     * @description 接口说明
     * @author MOON
     * @url /api/index/price
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     *
     * @return id:类型id
     * @return duration:时长
     * @return price:价格
     * @return create_time:创建时间
     */

    public function price(){
        $list=Db::name('shop_join')->select();
        foreach ($list as $Key=>$item){
            $list[$Key]['create_time']=date('Y-m-d H:i:s',$item['create_time']);
        }
        $this->returnAPI('获取成功',0,$list);
    }


    public function types()
    {
        $type=Db::name('shop_type')->column('name','id');
        $price=Db::name('shop_join')->column('duration','id');
        $zhuangxiu=Db::name('decoration')->column('name','id');
        $louceng=Db::name('storey')->column('name','id');
        $louti=Db::name('louti')->column('name','id');
        $this->returnAPI('获取成功',0,['type'=>$type,'price'=>$price,'zhuangxiu'=>$zhuangxiu,'louceng'=>$louceng,'louti'=>$louti]);
    }
}
