<?php
namespace app\api\controller;

use app\common\model\User as Users;
use http\Message\Body;
use think\Db;
use think\Image;
use PHPQRCode\QRcode;

/**
 * @title 个人中心
 * @description 接口说明
 */
class User extends Base
{
    protected $beforeActionList = ['check_login'];

    protected function check_login()
    {
        $user = $this->check_token();
        if (is_null($user)) {
            $this->returnAPI('登录信息有误,请重新登录', 10000);
        }else{
            $this->user = $user;

        }
    }

    public function up($id,$after){
        //会员升级
        $user=model('user')->get(['id'=>$id]);
        if ($user->gid == 1 && $after > 2000) {
            $user->save(['gid' => 2]);
        }
        if ($user->gid == 2 && $after > 30000) {
            $user->save(['gid' => 3]);
        }
        //黄金会员及以上升级
        if ($user->gid > 2) {
//            if ($user->invite_user_id != '') {
//                $u=new \app\common\model\User();
                $teamss = implode(',',self::total($user->id));

                $gid = db('user')
                    ->whereIn('id', $teamss)
                    ->max('gid');
                $uid = db('user')
                    ->whereIn('id', $teamss)
                    ->where('gid', $gid)
                    ->value('id');


                $maxCredit = floatval(db('user')->where('id', $uid)->value('credit1'));
                $credit1 = db('user')
                    ->whereIn('id', $teamss)
                    ->sum('credit1');
                $credit1 = $credit1 - $maxCredit;//减去最大等级会员所有的积分
                if ($credit1 >= 100000 && $user->gid == 3) {//白银晋升黄金
                    $user->save(['gid' => 4]);
                }
                if ($credit1 >= 800000 && $user->gid == 4) {//黄金晋升铂金
                    $user->save(['gid' => 5]);
                }
                if ($credit1 >= 5000000 && $user->gid == 5) {//铂金晋升钻石
                    $user->save(['gid' => 6]);
                }
            }
        }
//    }


    //计算数量
    public function total($uid,$arr=[],$num=[]){

        if ($num==[]){
            array_push($num,$uid);
        }
//        dump($num);
        if (count($arr)==0){

            $arr=db('user')->where('invite_user_id',$uid)->where('is_delete',0)->field('id uid')->select();

          foreach ($arr as $item =>$key){
//              dump($arr[$item]['uid']);
              array_push($num,$arr[$item]['uid']);
          }


            if ($arr){
                $num = $this->total('',$arr,$num);
            }


        }else{
            foreach ($arr as $k=>$v){
                $arr[$k]['info']=db('user')->where('invite_user_id',$v['uid'])->where('is_delete',0)->field('id uid')->select();
//                $num=$num+count($arr[$k]['info']);
                foreach ($arr[$k]['info'] as $key=>$item){
                    array_push($num,$arr[$k]['info'][$key]['uid']);
                }
                $num=$this->total($v['uid'],$arr[$k]['info'],$num);
            }
        }

        return $num;
    }




    /**
     * @title 个人中心首页
     * @description 接口说明
     * @author 开发者
     * @url /api/user
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     *
     * @return nick:昵称
     * @return head:头像
     * @return phone:手机号
     *  @return phones:手机号未加秘

     * @return sex:性别
     * @return is_shop:0不是店铺大于0为店铺id
     * @return shop_status:店铺状态0待审核1审核通过2审核未通过，没申请或不是店铺的时候没有意义
     * @return gid:等级
     * @return credit1:总消费积分
     * @return credit2:绿色积分
     * @return t_credit1:今日消费积分，个人中心展示这个
     * @return jf:距离下一级还需积分
     * @return bfb:进度条百分比
     * @return wechat_code:微信收款码
     * @return alipay_code:支付宝收款码
     * @return qr_code:二维码地址
     * @return UserSig:IM标识
     * @return sig_time:时间
     */
    public function index()
    {
        $shop = model('shop')->get(['user_id' => $this->user->id]);

        if (is_null($shop)) {
            $is_shop = 0;
            $shop_status = 0;
        } else {
            $is_shop = $shop->id;
            $shop_status = $shop->status;
        }
        if ($this->user->gid==1){
            $jf=2000-$this->user->credit1;
            if ($jf<0){
                $this->up($this->user->id,$this->user->credit1);
                }
            $bfb=floatval($this->user->credit1/2000)*100;

        }else if ($this->user->gid==2){
            $jf=30000-$this->user->credit1;
            if ($jf<0){
                $this->up($this->user->id,$this->user->credit1);
            }
            $bfb=floatval($this->user->credit1/30000)*100;

        }else if ($this->user->gid>2){
            $team=implode(',',$this->total($this->user->id));
            //            dump($team);
            $gid = db('user')
                ->whereIn('id', $team)
                ->max('gid');//去除一个最高等级
            $uid = db('user')
                ->where('gid', $gid)
                ->whereIn('id', $team)
                ->value('id');
            $maxCredit = db('user')->where('id', $uid)->value('credit1');
            $credit1 = db('user')
                ->whereIn('id', $team)
                ->sum('credit1');

            unset($team);
            $credit1=$credit1-floatval($maxCredit);

            if ($this->user->gid==3){
                $jf=100000-$credit1;
                if ($jf<0){
                    $this->up($this->user->id,$this->user->credit1);
                }
                $bfb=floatval($credit1/100000)*100;
            }
            if ($this->user->gid==4){
                $jf=800000-$credit1;
                if ($jf<0){
                    $this->up($this->user->id,$this->user->credit1);
                }
                $bfb=floatval($credit1/800000)*100;
            }
            if ($this->user->gid==5){
                $jf=5000000-$credit1;

                if ($jf<0){
                    $this->up($this->user->id,$this->user->credit1);
                }
                $bfb=floatval($credit1/5000000)*100;
            }
            if ($this->user->gid==6){
                $jf=0;
                $bfb=100;
            }



        }

        $res = $this->gen_User_Sig($this->user->phone, $this->user->UserSig, $this->user->sig_time); //im信息

        $user_invite_code = db('user_invite_code')->where('uid', $this->user->id)->find();

        $url = $this->request->domain() . '/login/index.html?tj_code='.$user_invite_code['code']; //邀请链接
        $qr_code = $this->user->qr_code;
        if(empty($this->user->qr_code)){ //邀请二维码
            $pathname = env('ROOT_PATH') . 'public' . DIRECTORY_SEPARATOR . 'qrcode';
            if (!is_dir($pathname)) {
                //mkdir(dirname($pathname), 0755, true);
                @mkdir(env('ROOT_PATH') . 'public' . DIRECTORY_SEPARATOR . 'qrcode');
            }

            $qrcode = md5(time() . mt_rand(100000, 999999)) . '.png';
            QRcode::png($url, env('ROOT_PATH') . 'public' . DIRECTORY_SEPARATOR . 'qrcode' . DIRECTORY_SEPARATOR . $qrcode);
            $qr_code = '/qrcode/' . $qrcode; // 生成的二维码,用来扫码支付
            //保存地址
            db('user')->where('id', $this->user->id)->update(['qr_code'=>$qr_code]);
        }
        $qr_code_url = $this->request->domain() . $qr_code;

        $data = [
            'jf'=>$jf,
            'bfb'=>$bfb,
            'wechat_code'=>$this->user->wechat_code,
            'alipay_code'=>$this->user->alipay_code,
            'price'=>db('market_config')->value('price'),
            'nick'  => $this->user->nick,
            'head'  =>  (strpos($this->user->head, '/') === 0) ? $this->request->domain() . $this->user->head : $this->user->head,
            'phone' => substr_replace($this->user->phone, '****', 3, 4),
            'phones' =>$this->user->phone ,

            'sex'   => $this->user->sex ? ($this->user->sex == 1 ? '女' : '男') : '未设置',
            'is_shop'     => $is_shop,
            'shop_status' => $shop_status,
            'gid'         => $this->user->gid,
            'credit1'     => $this->user->credit1,
            'credit2'     => $this->user->credit2,
            't_credit1'   => $this->user->t_credit1,
            'is_invited'  => $this->user->invite_user_id > 0 ? 1 : 0,
            'code'        => $user_invite_code['code'],
            'url'         => $url,
            'UserSig'     => $res['UserSig'],
            'sig_time'    => $res['sig_time'],
            'qr_code'     => $qr_code_url,
            ];
        $this->returnAPI('', 0, $data);
    }
    /**
     * @title 我的收货地址
     * @description 接口说明
     * @author 刘宇
     * @url /api/user/address
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:city_id type:int require:0 default: other: desc:城市id
     * @param name:type type:string require:1 default: other: desc:all:全部收货地址，city:当前城市收货地址
     * @param name:page type:int require:1 default:1 other: desc:当前页码
     * @param name:limit type:int require:1 default:10 other: desc:当页数据量
     *
     * @return address_id:地址id
     * @return person:收货人名称
     * @return phone:收货人手机号
     * @return city:所在城市
     * @return district:所在区
     * @return addr:详细地址
     * @return is_default:是否默认，0不默认1默认
     * @return p_id:省级id
     * @return c_id:市级id
     * @return a_id:区级id
     * @return s_id:街道id
     * @return street:所在街道
     */
    public function address()
    {
        $list = db('address')->field(['id' => 'address_id', 'person', 'phone', 'province', 'city', 'district', 'addr', 'is_default','street','p_id','c_id','a_id','s_id'])
            ->where(['user_id' => $this->user->id])->order('create_time', 'desc')->page(input('post.page', 1))->limit(input('post.limit', 10))->select();
        $this->returnAPI('', 0, ['list' => $list]);
    }

    /**
     * @title 添加收货地址
     * @description 接口说明
     * @author 刘宇
     * @url /api/user/add_address
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:province type:int require:1 default: other: desc:省份
     * @param name:city type:int require:1 default: other: desc:城市名称
     * @param name:district type:int require:1 default: other: desc:县区名称
     * @param name:street type:int require:1 default: other: desc:街道名称
     * @param name:p_id type:int require:1 default: other: desc:省id
     * @param name:c_id type:int require:1 default: other: desc:城市id
     * @param name:a_id type:int require:1 default: other: desc:县区id
     * @param name:s_id type:int require:1 default: other: desc:街道id

     * @param name:person type:string require:1 default:1 other: desc:收货人
     * @param name:phone type:string require:1 default:1 other: desc:收货人手机号
     * @param name:is_default type:int require:1 default:0 other: desc:是否默认，0不默认1默认
     * @param name:addr type:string require:1 default:0 other: desc:详细地址
     *
     *
     */
    public function add_address()
    {
        $data = [
            'user_id'     => $this->user->id,
            'person'      => input('post.person'),
            'phone'       => input('post.phone'),
            'province'    => input('post.province'),
            'city'        => input('post.city'),
            'district'    => input('post.district'),
            'street'     =>input('post.street'),
            'p_id'       =>input('post.p_id'),
            'c_id'=>input('post.c_id'),
            'a_id'=>input('post.a_id'),
            's_id'=>input('post.s_id'),
            'addr'        => input('post.addr'),
            'is_default'  => input('post.is_default', 0),
            'update_time' => time(),
        ];
        $result = $this->validate($data, $this->validate . 'User.address');
        if ($result === true) {
            if ($data['is_default'] == 1) {
                db('address')->where('user_id', $data['user_id'])->where('is_default', 1)->setField('is_default', 0);
            }
            $add_result = model('address')->save($data);
            if ($add_result === false) {
                $this->returnAPI('添加失败', 1);
            }
            $this->returnAPI('添加成功', 0);
        } else {
            $this->returnAPI($result);
        }
    }

    /**
     * @title 添加收货地址
     * @description 接口说明
     * @author 刘宇
     * @url /api/user/address_edit
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:address_id type:int require:1 default: other: desc:address_id
     * @param name:province type:int require:1 default: other: desc:省份
     * @param name:city type:int require:1 default: other: desc:城市名称
     * @param name:district type:int require:1 default: other: desc:县区名称
     * @param name:street type:int require:1 default: other: desc:街道名称
     * @param name:p_id type:int require:1 default: other: desc:省id
     * @param name:c_id type:int require:1 default: other: desc:城市id
     * @param name:a_id type:int require:1 default: other: desc:县区id
     * @param name:s_id type:int require:1 default: other: desc:街道id
     * @param name:person type:string require:1 default:1 other: desc:收货人
     * @param name:phone type:string require:1 default:1 other: desc:收货人手机号
     * @param name:is_default type:int require:1 default:0 other: desc:是否默认，0不默认1默认
     * @param name:addr type:string require:1 default:0 other: desc:详细地址
     *
     */
    public function address_edit()
    {
        $data = [
            'id'         => input('post.address_id'),
            'person'     => input('post.person'),
            'phone'      => input('post.phone'),
            'province'   => input('post.province'),
            'city'       => input('post.city'),
            'district'   => input('post.district'),
            'street'     =>input('post.street'),
            'p_id'       =>input('post.p_id'),
            'c_id'=>input('post.c_id'),
            'a_id'=>input('post.a_id'),
            's_id'=>input('post.s_id'),
            'addr'       => input('post.addr'),
            'is_default' => input('post.is_default', 0),
        ];
        $result = $this->validate($data, $this->validate . 'User.address');
        if ($result === true) {
            $address = model('address')->get(['id' => input('post.address_id')]);
            if (!is_null($address)) {
                if ($data['is_default'] == 1) {
                    db('address')->where('user_id', $this->user->id)->where('id', '<>', $data['id'])->where('is_default', 1)->setField('is_default', 0);
                }
                $edit = $address->allowField(true)->save($data);
                if ($edit === false) {
                    $this->returnAPI('编辑失败', 1);
                }
                $this->returnAPI('编辑成功', 0, [
                    'address_id' => $address->id,
                    'person'     => $address->person,
                    'phone'      => $address->phone,
                    'province'   => $address->province,
                    'city'       => $address->city,
                    'district'   => $address->district,
                    'addr'       => $address->addr,
                    'is_default' => $address->is_default,
                    'street'     =>$address->street,
                    'p_id'       =>$address->p_id,
                    'c_id'=>$address->c_id,
                    'a_id'=>$address->a_id,
                    's_id'=>$address->s_id,
                ]);
            } else {
                $this->returnAPI('收货地址不存在', 1);
            }
        } else {
            $this->returnAPI($result);
        }
    }
    /**
     * @title 收货地址详情
     * @description 接口说明
     * @author 刘宇
     * @url /api/user/address_info
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:address_id type:int require:1 default: other: desc:收货地址id
     *
     * @return address_id:收货地址id
     * @return person:收货人姓名
     * @return phone:收货人手机号
     * @return province:province
     * @return city:城市名称
     * @return district:县区名称
     * @return street:街道名称
     * @return p_id:省级id
     * @return c_id:市级id
     * @return a_id:区级id
     * @return s_id:街道id
     * @return addr:详细地址
     * @return is_default:是否默认
     */
    public function address_info()
    {
        $data = [
            'id' => input('post.address_id'),
        ];
        $address = model('address')->get($data);
        if (!is_null($address)) {
            $this->returnAPI('', 0, [
                'address_id' => $address->id,
                'person'     => $address->person,
                'phone'      => $address->phone,
                'province'   => $address->province,
                'city'       => $address->city,
                'district'   => $address->district,
                'addr'       => $address->addr,
                'is_default' => $address->is_default,
                'street'     =>$address->street,
                'p_id'       =>$address->p_id,
                'c_id'=>$address->c_id,
                'a_id'=>$address->a_id,
                's_id'=>$address->s_id,
            ]);
        } else {
            $this->returnAPI('收货地址不存在', 1);
        }

    }
    /**
     * @title 删除收货地址
     * @description 接口说明
     * @author 刘宇
     * @url /api/user/address_del
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:address_id type:int require:1 default: other: desc:收货地址id
     *
     */
    public function address_del()
    {
        $data = [
            'id'      => input('post.address_id'),
            'user_id' => $this->user->id,
        ];
        $address = model('address')->get($data);
        if (!is_null($address)) {
            $address->delete();
        }
        $this->returnAPI('删除成功', 0);
    }
    /**
     * @title 修改密码
     * @description 接口说明
     * @author 刘宇
     * @url /api/user/edit_pwd
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:pwd type:string require:1 default: other: desc:原密码
     * @param name:npwd type:string require:1 default: other: desc:新密码
     * @param name:z_npwd type:string require:1 default: other: desc:确认密码
     *
     */
    public function edit_pwd()
    {
        $data = [
            'pwd'    => input('post.pwd'),
            'npwd'   => input('post.npwd'),
            'z_npwd' => input('post.z_npwd'),
        ];
        $result = $this->validate($data, $this->validate . 'User.pwd');
        if ($result === true) {
            if ($data['npwd'] !== $data['z_npwd']) {
                $this->returnAPI('新密码不一致', 1);
            }
            if ($this->user->pwd !== md5($data['pwd'])) {
                $this->returnAPI('旧密码错误', 1);
            }
            if ($data['pwd'] === $data['npwd']) {
                $this->returnAPI('新密码与旧密码一致', 1);
            }
            $edit = $this->user->save(['pwd' => md5($data['npwd'])]);
            if ($edit === false) {
                $this->returnAPI('修改失败', 1);
            } else {
                $this->returnAPI('修改成功', 0);
            }
        } else {
            $this->returnAPI($result);
        }
    }
    public function edit_phone_yzm()
    {
        $data = [
            'phone' => input('post.phone'),
        ];
        $result = $this->validate($data, $this->validate . 'User.yzm');
        if ($result === true) {
            if ($this->user->phone === $data['phone']) {
                $this->returnAPI('新手机号与原手机号相同');
            }
            $user = model('user')->get($data);
            if (!is_null($user)) {
                $this->returnAPI('手机号已注册');
            }
            $this->sendyzm($data['phone'], 'edit_phone');
        }
        $this->returnAPI($result);
    }
    public function edit_phone()
    {
        $data = [
            'pwd'   => input('post.pwd'),
            'phone' => input('post.phone'),
            'code'  => input('post.code'),
        ];
        $result = $this->validate($data, $this->validate . 'User.register');
        if ($result === true) {
            if ($this->user->pwd !== md5($data['pwd'])) {
                $this->returnAPI('密码错误');
            }
            if ($this->user->phone === $data['phone']) {
                $this->returnAPI('新手机号与原手机号相同');
            }
            $user = model('user')->get(['phone' => $data['phone']]);
            if (!is_null($user)) {
                $this->returnAPI('手机号已注册');
            }
            $this->checkyzm($data['phone'], $data['code'], 'edit_phone');
            $edit = $this->user->save(['phone' => $data['phone']]);
            if ($edit === false) {
                $this->returnAPI('修改失败', 1);
            } else {
                $this->returnAPI('修改成功', 0);
            }
        } else {
            $this->returnAPI($result);
        }
    }
    /**
     * @title 领取优惠券
     * @description 接口说明
     * @author 刘宇
     * @url /api/user/add_coupon
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:coupon_id type:int require:1 default: other: desc:优惠券id
     *
     */
    public function add_coupon()
    {
        $coupon = model('coupon')->get(['id' => input('post.coupon_id'), 'type' => 0]);
        if (is_null($coupon)) {
            $this->returnAPI('优惠券不存在');
        }
        $data = [
            'coupon_id'  => $coupon->id,
            'shop_id'    => $coupon->shop_id,
            'user_id'    => $this->user->id,
            'money'      => $coupon->money,
            'money_max'  => $coupon->money_max,
            'start_time' => $coupon->start_time,
            'end_time'   => $coupon->end_time,
            'type'       => $coupon->type,
        ];
        $user_coupon = model('user_coupon')->get($data);
        if (!is_null($user_coupon) && $user_coupon->id === 0) {
            $this->returnAPI('您已领取该优惠券');
        }
        $result = model('user_coupon')->save($data);
        if ($result === true) {
            $this->returnAPI('领取成功', 0);
        } else {
            $this->returnAPI('领取失败，请稍后再试');
        }
    }
    public function coupon()
    {
        db('user_coupon')->where('user_id', $this->user->id)->where('status', 0)->where('end_time', '<=', time())->setField('status', 2);
        $list = db('user_coupon')->alias('uc')->leftJoin('shop s', 's.id=uc.shop_id')
            ->field(['uc.shop_id', 's.name', 's.logo'])->where(['uc.user_id' => $this->user->id, 'uc.status' => 0])
            ->group('uc.shop_id')->order('uc.create_time', 'desc')
            ->page(input('post.page', 1))->limit(input('post.limit', 10))->select();
        if (!is_null($list)) {
            $myshop = db('system_config')->column('value', 'config');
            foreach ($list as $k => $v) {
                if ($v['shop_id'] == 0) {
                    $myshop                 = db('system_config')->column('value', 'config');
                    $list[$k]['name']       = $myshop['shop_name'];
                    $list[$k]['logo_thumb'] = $this->image_thumb($myshop['shop_logo']);
                    $list[$k]['logo']       = (strpos($myshop['shop_logo'], '/') === 0) ? $this->request->domain() . $myshop['shop_logo'] : $myshop['shop_logo'];
                } else {
                    $list[$k]['logo_thumb'] = $this->image_thumb($v['logo']);
                    $list[$k]['logo']       = (strpos($v['logo'], '/') === 0) ? $this->request->domain() . $v['logo'] : $v['logo'];
                }
                $coupon = db('user_coupon')
                    ->field(['id' => 'user_coupon_id', 'type', 'coupon_id', 'money', 'money_max', 'start_time', 'end_time', 'status'])
                    ->where(['user_id' => $this->user->id, 'shop_id' => $v['shop_id'], 'status' => 0])->select();
                if (!is_null($coupon)) {
                    foreach ($coupon as $key => $value) {
                        $coupon[$key]['start_time'] = date('Y-m-d H:i:s', $value['start_time']);
                        $coupon[$key]['end_time']   = date('Y-m-d H:i:s', $value['end_time']);
                    }
                    $list[$k]['coupon'] = $coupon;
                } else {
                    $list[$k]['coupon'] = [];
                }
            }
        }
        $this->returnAPI('', 0, ['list' => $list]);
    }
    public function used_coupon()
    {
        db('user_coupon')->where('user_id', $this->user->id)->where('status', 0)->where('end_time', '<=', time())->setField('status', 2);
        $list = db('user_coupon')->alias('uc')->leftJoin('shop s', 's.id=uc.shop_id')
            ->field(['uc.id' => 'user_coupon_id', 'uc.type', 'uc.status', 's.name' => 'shop_name', 'uc.start_time', 'uc.end_time', 'uc.money_max', 'uc.money'])
            ->where(['uc.user_id' => $this->user->id])->where('uc.status', '<>', 0)
            ->order('uc.create_time', 'desc')
            ->page(input('post.page', 1))->limit(input('post.limit', 10))->select();
        if (!is_null($list)) {
            foreach ($list as $key => $value) {
                $list[$key]['start_time'] = date('Y-m-d H:i:s', $value['start_time']);
                $list[$key]['end_time']   = date('Y-m-d H:i:s', $value['end_time']);
            }
        }
        $this->returnAPI('', 0, ['list' => $list]);
    }
    /**
     * @title 商品收藏列表
     * @description 接口说明
     * @author 刘宇
     * @url /api/user/goods_collect
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:page type:int require:1 default:1 other: desc:当前页码
     * @param name:limit type:int require:1 default:10 other: desc:当页数据量
     *
     * @return collect_id:收藏id
     * @return goods_id:商品id
     * @return image:商品图片
     * @return name:商品名称
     * @return shop_id:店铺id
     * @return price:商品价格
     * @return sale:销量
     * @return shop_name:店铺名称
     * @return image_thumb:缩略图
     */
    public function goods_collect()
    {
        $data = [
            'u.user_id' => $this->user->id,
            'u.type'    => 1,
        ];
        $list = db('user_collect')->alias('u')->join('goods g', 'g.id=u.other_id')
            ->field(['u.id' => 'collect_id', 'u.other_id' => 'goods_id', 'g.type', 'g.image', 'g.name', 'g.shop_id', 'g.price', 'g.sale'])
            ->where($data)->order('u.create_time', 'desc')
            ->page(input('post.page', 1))->limit(input('post.limit', 10))->select();
        if (!is_null($list)) {
            $myshop = db('system_config')->column('value', 'config');
            $array  = db('shop')->alias('s')->join('goods g', 's.id = g.shop_id')
                ->where('s.id', 'in', array_unique(explode(',', implode(',', array_column($list, 'shop_id')))))
                ->column('s.name', 's.id');
            foreach ($list as $k => $v) {
                if ($v['shop_id'] == '0') {
                    $list[$k]['shop_name'] = $myshop['shop_name'];
                } else {
                    $list[$k]['shop_name'] = $array[$v['shop_id']];
                }
                $list[$k]['image_thumb'] = $this->image_thumb($v['image']);
                $list[$k]['image']       = (strpos($v['image'], '/') === 0) ? $this->request->domain() . $v['image'] : $v['image'];
            }
        }
        $this->returnAPI('', 0, ['list' => $list]);
    }
    /**
     * @title 取消商品收藏
     * @description 接口说明
     * @author 刘宇
     * @url /api/user/goods_delcollect
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:goods_id type:int require:1 default:1 other: desc:商品id
     *
     */
    public function goods_delcollect()
    {
        $data = [
            'user_id' => $this->user->id,
            'type'    => 1,
        ];
        $db      = db('user_collect')->where($data);
        $goodsid = input('post.goods_id');
        if (!is_null($goodsid) && $goodsid !== '') {
            $db->where('other_id', 'in', $goodsid);
        }
        $result = $db->delete();
        if ($result) {
            $this->returnAPI('取消收藏成功', 0);
        }
        $this->returnAPI('取消收藏失败,请稍后重试');
    }

    /**
     * @title 修改头像
     * @description 修改资料
     * @author 开发者
     * @url /api/user/edit_head
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户token
     * @param name:head type:string require:1 default:1 other: desc:修改头像
     *
     */
    public function edit_head()
    {
        $head = input('head');//$this->upload_thumb('head', true, '头像');
        $this->user->save(['head' => $head]);
        $this->returnAPI('头像更换成功', 0, ['head' => $head]);
    }

    /**
     * @title 修改昵称
     * @description 修改资料
     * @author 开发者
     * @url /api/user/edit_nick
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户token
     * @param name:nick type:string require:1 default:1 other: desc:修改头像
     *
     */
    public function edit_nick()
    {
        $this->user->save(['nick' => input('post.nick')]);
        $this->returnAPI('昵称更换成功', 0);
    }

    /**
     * @title 我的评价列表
     * @description 修改资料
     * @author 开发者
     * @url /api/user/comment_list
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户token
     *
     * @return comment_id:评价id
     * @return goods_id:商品id
     * @return name:商品名
     * @return image:图片
     * @return specs:规格
     * @return price:价格
     * @return amount:数量
     * @return score:评分
     * @return content:评价内容
     * @return images:图片数组@
     * @images images_thumb:缩略图 image:原图
     * @return create_time:评价时间
     * @return comment_reply:评价回复
     */
    public function comment_list()
    {
        $list = db('goods_comment')->field(['id' => 'comment_id', 'goods_id', 'type', 'name', 'image', 'specs', 'price', 'amount', 'score', 'content', 'images', 'create_time', 'comment_reply'])
            ->where('user_id', $this->user->id)->where('comment_del', 0)
            ->order('create_time', 'desc')->page(input('post.page', 1))->limit(input('post.limit', 10))->select();
        foreach ($list as $k => $v) {
            if (is_null($v['images']) || $v['images'] == '') {
                $score_images = [];
            } else {
                $score_images = json_decode($v['images'], true);
            }
            if (!empty($score_images)) {
                foreach ($score_images as $key => $value) {
                    $score_images[$key] = [
                        'images_thumb' => $this->image_thumb($value),
                        'image'        => (strpos($value, '/') === 0) ? $this->request->domain() . $value : $value,
                    ];
                }
            }
            $list[$k]['images_thumb'] = $this->image_thumb($v['image']);
            $list[$k]['image']        = (strpos($v['image'], '/') === 0) ? $this->request->domain() . $v['image'] : $v['image'];
            $list[$k]['images']       = $score_images;
            $list[$k]['create_time']  = date('Y-m-d H:i:s', $v['create_time']);
        }
        $this->returnAPI('', 0, ['list' => $list]);
    }

    /**
     * @title 删除评论
     * @description 修改资料
     * @author 开发者
     * @url /api/user/comment_del
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户token
     * @param name:comment_id type:string require:1 default:1 other: desc:评价id
     */
    public function comment_del()
    {
        $model = model('goods_comment')->get(['id' => input('post.comment_id'), 'user_id' => $this->user->id, 'comment_del' => 0]);
        if (!is_null($model)) {
            $model->save(['comment_del' => 1, 'del_time' => time()]);
        }
        $this->returnAPI('评价删除成功', 0);
    }

    public function score_list()
    {
        $type = get_status('user_score_type');
        $list = db('user_score')->field(['type', 'score', 'create_time'])->where('user_id', $this->user->id)->order('create_time', 'desc')->page(input('post.page', 1))->limit(input('post.limit', 10))->select();
        foreach ($list as $k => $v) {
            $list[$k]['type_desc']   = $type[$v['type']];
            $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
        }
        $this->returnAPI('', 0, [
            'list'       => $list,
            'score'      => $this->user->score,
            'score_rule' => get_config('user_score_rule'),
        ]);
    }

    /**
     * @title 修改资料
     * @description 修改资料
     * @author 开发者
     * @url /api/user/edit_info
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户token
     * @param name:nick type:string require:1 default:1 other: desc:姓名
     * @param name:sex type:int require:1 default:1 other: desc:1为女，2为男
     * @param name:head type:string require:1 default:1 other: desc:修改头像
     *
     */
    public function edit_info()
    {
        $nick = input('post.nick');
        $sex  = input('post.sex');
        $head = input('post.head'); //$this->upload_thumb('head', true, '头像');
        $this->user->save(['nick'=>$nick, 'sex' => $sex,'head' => $head]);
        $this->returnAPI('资料修改成功', 0);
    }

    /**
     * @title 获取个人余额和可提现金额
     * @description 接口说明
     * @author 开发者
     * @url /api/user/balance_info
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     *
     * @return balance:余额
     * @return all_balance:历史总商城余额
     * @return withdraw_money:可提现收益
     * @return history_money:历史总 可提现收益
     */
    public function balance_info(){
        $this->returnAPI('', 0, [
            'balance'        => $this->user->balance,
            'all_balance'    => $this->user->all_balance,
            'withdraw_money' => $this->user->withdraw_money,
            'history_money'  => $this->user->history_money,
        ]);
    }

    /**
     * @title 获取个人余额和可提现金额列表
     * @description 接口说明
     * @author 开发者
     * @url /api/user/balance_records
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:type type:int require:0 default:1 other: desc:余额类型（1.余额2.可提现余额）
     *
     * @return list:列表@
     * @list number:金额 now_number:加减后当前用户余额 order_amount:订单金额 type:产生方式 text:描述
     */
    public function balance_records(){
        $where = ['uid' => $this->user->id];

        $type = input('post.type');
        if(!empty($type)){
            $where['account_type'] = $type;
        }

        $list = db('user_account_records')->field('id,uid,number,now_number,order_amount,from_type,text')->where($where)->order('create_time','desc')->select();
        $arr = ['商城订单','订单退还','提现','提现退还','余额充值'];
        foreach($list as $k=>$v){
            $list[$k]['type'] = $arr[$v['from_type']];
        }
        $this->returnAPI('',0,[
            'list' => $list
        ]);
    }

    /**
     * @title 个人余额充值
     * @description 接口说明
     * @author 开发者
     * @url /api/user/money_recharge
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:money type:int require:0 default:1 other: desc:充值金额
     * @param name:pay_type type:int require:0 default:1 other: desc:1支付宝2微信3可提现余额充值
     *
     * @return data:返回的对应的微信或者支付宝的参数微信的是object，支付宝的是string
     */
    public function money_recharge()
    {
        $money      = $this->StrictVerify('money','充值金额');
        $pay_type   = $this->StrictVerify('pay_type','支付方式');

        if($money<0){
            $this->returnAPI('充值金额必须大于0',1,'');
        }

        if(!is_numeric($money)){
            $this->returnAPI('充值金额必须是数字',1,'');
        }

        //获取返回数据
        $Users = new Users();
        $res = $Users->money_recharge($this->user->id,$money,$pay_type);

        if($res['status'] == 1){
            $this->returnAPI('获取成功', 0, $res['data']);
        }else{
            $this->returnAPI($res['msg'],1,'');
        }

    }

    /**
     * @title 用户提现公告
     * @description 接口说明
     * @author 开发者
     * @url /api/user/withdraw_first
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     *
     * @return withdraw_money:当前可提现余额
     * @return notice:提现公告
     * @return day_times:下方提现次数公告
     * @return withdrawable:是否可提现，0不可提现，1可提现
     * @return min_money:最低提现金额，0为不限
     */
    public function withdraw_first()
    {
        //查询提现公告
        $notice = '提现时间为';
        //可以提现的类型
        $type = get_config('tixian_type');
        if($type == 0){ //每天都可以提现
            $notice .= '每天';
        }else if($type == 1){ //工作日可以提现
            $notice .= '周一到周五';
        }else if($type == 2){ // 选择特定日期才可以提现
            $week = ["周一", "周二", "周三", "周四", "周五", "周六", "周日"];

            $data = get_config('tixian_data');
            $data_arr = explode(',',$data);
            foreach($data_arr as $d){
                $notice .= $week[$d-1] .'、';
            }
            $notice = rtrim($notice, '、');
        }

        $start_time  = get_config('tixian_time_start');
        $end_time    = get_config('tixian_time_end');
        $notice .= $start_time.'-'.$end_time.'，';

        $tiaixn_time = get_config('tixian_time');
        $notice .= $tiaixn_time.'小时内到账';

        $limit_times = get_config('tixian_day_times');
        if($limit_times > 0)
            $day_times = '一天可提现'.$limit_times.'次';
        else
            $day_times = '每日提现次数不限';

        //最低提现金额
        $min_money = get_config('tixian_min_money');

        $check = $this->check_tixian();
        $this->returnAPI($check['msg'], 0, [
            'user_id' => $this->user->id,
            'withdraw_money' => $this->user->withdraw_money, //可提现余额
            'notice'         => $notice, //提现公告
            'day_times'      => $day_times,
            'withdrawable'   => $check['status'],
            'min_money'      => $min_money,
        ]);
    }

    //判断我现在是否可以提现
    public function check_tixian(){
        $w =date("w",time( ));
        //判断提现类型
        $type = get_config('tixian_type');
        if($type == 1){
            //判断今天是周几
            if($w > 5){
                return returnMsg(0,'周六周天不能提现哦~');
            }
        }else if($type == 2){
            $data = get_config('tixian_data');
            $data_arr = explode(',',$data);
            if(!in_array($w,$data_arr)){
                return returnMsg(0,'今天不可提现');
            }
        }
        //判断当前时间是否在区间之内
        $start_time  = get_config('tixian_time_start');
        $end_time    = get_config('tixian_time_end');

        $time = time();
        $timeBetween=[$start_time,$end_time];
        $is_between = $this->getTime($time,$timeBetween);
        if(!$is_between){
            return returnMsg(0,'提现时间为：'.$start_time.'-'.$end_time);
        }

        $limit_times = get_config('tixian_day_times');
        if($limit_times > 0){
            $s_time = strtotime(date('Y-m-d 00:00:00'));
            $e_time = strtotime(date('Y-m-d 23:59:59'));
            //判断用户今天申请了几次提现
            $withdraw = db('user_withdraw')
                ->where('uid', $this->user->id)
                ->whereBetween('create_time',$s_time.','.$e_time)
                ->count();
            //echo db()->getLastSql();die;
            if($withdraw >= $limit_times){
                return returnMsg(0,'今日提现次数已用完。');
            }
        }

        return returnMsg(1);
    }

    function getTime($time,$timeBetween){

        $checkDayStr = date('Y-m-d ',time());
        $timeBegin1 = strtotime($checkDayStr.$timeBetween[0]);
        $timeEnd1 = strtotime($checkDayStr.$timeBetween[1]);

        if($time > $timeBegin1 && $time < $timeEnd1){
            //echo $time."在区间【".$timeBetween[0]."~".$timeBetween[1]."】内";
            return 1;
        }else{
            //echo "不在区间内";
            return 0;
        }
    }

    /**
     * @title 用户提现提交申请
     * @description 接口说明
     * @author 开发者
     * @url /api/user/withdraw_apply
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:money type:float require:1 default:1 other: desc:金额
     * @param name:alipay_acc type:float require:1 default:1 other: desc:支付宝账号
     * @param name:alipay_name type:string require:1 default:1 other: desc:支付宝姓名
     *
     */
    public function withdraw_apply()
    {
        $check = $this->check_tixian();
        if(!$check['status']){
            $this->returnAPI($check['msg']);
        }
        $data = [
            'money'       => input('post.money'),
            'alipay_acc'  => input('post.alipay_acc'),
            'alipay_name' => input('post.alipay_name'),
        ];
        $result = $this->validate($data, $this->validate . 'ShopWithdraw.apply');
        if ($result !== true) {
            $this->returnAPI($result);
        }
        $money = round($data['money'], 2);
        if ($money > $this->user->withdraw_money) {
            $this->returnAPI('超出可提现金额');
        }
        $min_money = get_config('tixian_min_money');
        if($min_money > 0 && $money < $min_money){
            $this->returnAPI('提现金额不能小于最小提现金额');
        }
        $this->user->save([
            'alipay_acc'     => $data['alipay_acc'],
            'alipay_name'    => $data['alipay_name'],
        ]);
        Db::startTrans();
        try {
            $insert_id = db('user_withdraw')->insert($data + [
                    'uid'         => $this->user->id,
                    'balance'     => $this->user->withdraw_money,
                    'create_time' => time(),
                ]);
            if(!$insert_id){
                throw new \Exception("提现申请失败，请重新提交申请");
            }

            $users = new Users();
            $sub = $users->add_user_account_records($this->user->id, 2, 0, bcmul($data['money'], -1,2), $data['money'], 3, $insert_id, 'user_withdraw', '申请提现扣除可提现余额');
            if(! $sub['status']){
                throw new \Exception($sub['msg']);
            }
            Db::commit();
        }catch(\Exception $e){
            Db::rollback();
            $this->returnAPI($e->getMessage());
        }
        $this->returnAPI('提现申请成功', 0);

    }

    /**
     * @title 用户提现记录
     * @description 接口说明
     * @author 开发者
     * @url /api/user/withdraw_list
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:page type:int require:0 default:1 other: desc:页数
     * @param name:limit type:float require:1 default:10 other: desc:一页查询数量
     *
     * @return money:申请提现金额
     * @return create_time:申请时间
     * @return title:描述
     * @return status_desc:审核状态
     */
    public function withdraw_list()
    {
        $status = get_status('shop_withdraw_status');
        $list   = db('user_withdraw')->field(['alipay_acc', 'money', 'status', 'create_time'])->where('uid', $this->user->id)->order('create_time', 'desc')->page(input('post.page', 1))->limit(input('post.limit', 10))->select();
        foreach ($list as $k => $v) {
            $list[$k]['title']       = '提现到支付宝(' . $this->alipay_acc($v['alipay_acc']) . ')';
            $list[$k]['status_desc'] = $status[$v['status']];
            $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
            unset($list[$k]['alipay_acc']);
        }
        $this->returnAPI('', 0, ['list' => $list]);
    }
    private function alipay_acc($alipay_acc)
    {
        $index = strpos($alipay_acc, '@');
        if ($index > 0) {
            $length = strlen(substr($alipay_acc, 0, $index));
        } else {
            $length = strlen($alipay_acc);
        }
        $three = floor($length / 3);
        $two   = floor($length / 2);
        return substr_replace($alipay_acc, str_pad('*', $two, '*'), $three, $two);
    }

    /**
     * @title 推广页面
     * @description 接口说明
     * @author 开发者
     * @url /api/user/recommend_info
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     *
     * @return code:我的推荐码
     * @return invite_user_id:推荐人id，大于0为有推荐人
     * @return recommend_user:我推荐人的信息@
     * @recommend_user nick:推荐人昵称 head:推荐人头像 code:推荐人推荐码
     * @return logo:商城logo
     * @return my_link:我的推广链接
     */
    public function recommend_info(){
        $data = [];

        $data['code'] = $this->user->invite_code;
        //查询推荐人信息
        //$data['recommend_user'] = [];
        if($this->user->invite_user_id > 0){
            $recommend_user = model('user')->field('id,nick,head')->get(['id'=>$this->user->invite_user_id]);
            $recommend_code = db('user_invite_code')->where('uid', $recommend_user->id)->find();

            $data['recommend_user'] = ['nick'=>$recommend_user->nick, 'code'=>$recommend_code['code'], 'head'=>(strpos($recommend_user->head, '/') === 0) ? $this->request->domain() . $recommend_user->head : $recommend_user->head];
        }
        $data['invite_user_id'] = $this->user->invite_user_id;

        $logo = get_config('shop_logo');

        $data['logo'] = (strpos($logo, '/') === 0) ? $this->request->domain() . $logo : $logo;

        $data['my_link'] = $this->user->my_link;

        $this->returnAPI('', 0, $data);
    }

    /**
     * @title 无推荐人绑定推荐人
     * @description 接口说明
     * @author 开发者
     * @url /api/user/recommend_bind
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:code type:int require:0 default:1 other: desc:推荐人的推荐码
     *
     * @return tj_code:推荐人code
     */
    public function recommend_bind(){
        //判断我现在是否有推荐人
        if($this->user->invite_user_id){
            $this->returnAPI('您当前已经有推荐人了哦~');
        }
        $data = [
            'code'       => input('post.code'),
        ];
        //判断code是否存在
        $code = db('user_invite_code')->where($data)->find();
        if(is_null($code)){
            $this->returnAPI('当前推荐码不存在。');
        }
        //判断当前验证码是否是自己
        if($this->user->id == $code['uid']){
            $this->returnAPI('推荐人不能是自己哦~');
        }
        //判断当前uid的用户是否存在
        $r_user = model('user')->get([ 'id'=>$code['uid'] ]);
        if(is_null($r_user)){
            $this->returnAPI('推荐码用户不存在。');
        }
        //绑定
        $edit = $this->user->save(['invite_user_id' => $r_user->id]);
        if ($edit === false) {
            $this->returnAPI('绑定失败', 1);
        } else {
            $this->returnAPI('绑定成功', 0, ['tj_code'=>$data['code']]);
        }
    }

    public function test(){
        print_r($this->user);
    }

    /**
     * @title 上传图片
     * @description 接口说明
     * @author 刘宇
     * @url /api/user/upload
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:file type:file require:1 default:1 other: desc:文件格式图片
     *
     */
    public function upload()
    {
        $path = $this->request->post('path', 'api_images');
        $file = $this->request->file($this->request->post('name', 'file'));
        $file_info = $file->getInfo();
        if ($file) {
            $return = $this->upload_oss($file_info['tmp_name'], $file_info['name']);
            $this->returnAPI('上传成功', 0, $return);
            /*$move = env('ROOT_PATH') . 'public' . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR . $path;
            $info = $file->rule('md5')->move($move);
            if ($info) {
                $src = $move . DIRECTORY_SEPARATOR . $info->getSaveName();
                $img = getimagesize($src);
                if ($img !== false && $img['mime'] != 'image/gif') {
                    $thumb = str_replace('.', '_thumb.', $src);
                    $image = Image::open($src);
                    $image->thumb(150, 150)->save($thumb);
                }
                if ($this->request->post('domain', 1)) {
                    $domain = $this->request->domain();
                } else {
                    $domain = '';
                }
                $return = ['src' => str_replace('\\', '/', str_replace(env('ROOT_PATH') . 'public', $domain, $src))];
                if (isset($thumb)) {
                    $return['thumb'] = str_replace('\\', '/', str_replace(env('ROOT_PATH') . 'public', $domain, $thumb));
                }
                $this->returnAPI('上传成功', 0, $return);
            }
            $this->returnAPI($file->getError());*/
        }
        $this->returnAPI('上传失败,请稍后重试');
    }

    /**
     * @title 百问百答列表
     * @description 接口说明
     * @author 刘宇
     * @url /api/user/hundred_list
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:page type:file require:1 default:1 other: desc:页码
     * @param name:limit type:file require:1 default:1 other: desc:数量
     *
     * @return nick:昵称
     * @return head:头像
     * @return content:体温内容
     * @return image:图片
     * @return image_arr:图片数组
     * @return video:视频
     * @return reply:回复内容
     */
    public function hundred_list(){
        $list = db('hundred')->alias('h')
            ->join('user u', 'h.user_id = u.id')
            ->field('h.*, u.nick, u.head')
            ->page(input('page', 1))
            ->limit(input('limit', 10))
            ->order('id desc')
            ->select();
        foreach ($list as $k => $v){
            $list[$k]['image_arr'] = explode(',', $v['image']);
        }
        $this->returnAPI('', 0, $list);
    }

    /**
     * @title 提交百问百答问题
     * @description 接口说明
     * @author 刘宇
     * @url /api/user/add_hundred
     * @method POST
     *
     * @param name:token type:string require:1 default:1 other: desc:用户登录token
     * @param name:content type:string require:1 default:1 other: desc:内容
     * @param name:image type:string require:1 default:1 other: desc:图片
     * @param name:video type:string require:1 default:1 other: desc:视频
     *
     */
    public function add_hundred(){
        $param = input('param.');
        $param['create_time'] = date('Y-m-d H:i:s', time());
        $param['user_id'] = $this->user->id;
        unset($param['token']);
        $res = db('hundred')->insert($param);
        if(!$res){
            $this->returnAPI('提交失败');
        }
        $this->returnAPI('提交成功', 0);
    }
}
