<?php
namespace app\tasks\controller;

use app\api\controller\Base;
use app\api\controller\Market;
use app\common\model\CapitalRecord;
use app\common\model\User;
use think\Db;

/**
 * Created by 定时任务大厅.
 * User: 李磊
 * Date: 2022/9/6
 * Time: 14:50
 */
class Grant extends Base
{


//      public function romve(){
//         $db=db('capital_record_copy1')->where('create_time','>',1669910400)->select();
// //
//         foreach ($db as $item =>$a){
// //            dump();
// //            if ()
// //            if ($db[$item]['type']=)
//             if (floatval($db[$item]['money'])>0){
//                 $db[$item]['money']=0-floatval($db[$item]['money']);
//             }else{
//                 $db[$item]['money']=abs(floatval($db[$item]['money']));
//             }

//         }
//         foreach ($db as $item=>$a){
//             if ($db[$item]['type']==3){
//                 User::capitalRecord($db[$item]['uid'],$db[$item]['money'],1,'系统回退');
//             }
//             User::capitalRecord($db[$item]['uid'],$db[$item]['money'],$db[$item]['type'],'系统回退');
//         }
//         echo('回退完成');
// //        dump($db);
//     }
    /**
     *
     * 每天定时发放易贝
     *
     */

    public function everyYb(){
        $mode=db('pond')->where('id',1)->value('flag');
//        dump($mode);
//        die();
        if ($mode==1){
            echo ('今日已完成释放无需重复释放易贝');
            die();
        }else{
            $system=model('distribute')->get(['id'=>1]);
            $market=model('marketConfig')->get(['id'=>1]);


            $user=model('user')->where('t_credit1','>',0)->field('id,credit1,credit2,t_credit1')->select();
            $sum=db('user')->sum('t_credit1');
            foreach ($user as $k=>$v){
//            dump($user[$k]['t_credit1']/$sum);
                /*$money=number_format($user[$k]['t_credit1']/$system['green_points']*$system['every'],11);
                User::capitalRecord($user[$k]['id'],$money,2,'每天释放易贝','每天释放易贝');
                User::capitalRecord($user[$k]['id'],-$user[$k]['t_credit1'],3,'','易贝释放');*/
                $money=($user[$k]['t_credit1']/$sum)*$system['every'];//获得的易贝\
//            dump($money);
//            dump($money*$market->price);
                $flag=User::capitalRecord($user[$k]['id'],-($money*floatval($market->price)),3,'易贝释放','易贝释放');
                if ($flag['status']==1){
                    User::capitalRecord($user[$k]['id'],$money,2,'每天释放易贝','每天释放易贝');
                    $d=model('distribute')->get(['id'=>1]);
                    $d->save(['surplus'=>floatval($d->surplus)-floatval($money)]);
                    $pond=model('pond')->get(['id'=>1]);
                    $pond->save([
                        't_yb'=>floatval($pond->t_yb)+floatval($money),
                        'sum_yb'=>floatval($pond->sum_yb)+floatval($money)
                    ]);

                    echo ($user[$k]['id'].'---释放易贝成功-获得易贝'.$money);
                    echo ('-');
                }else{
                    echo($flag['msg']);
                    echo ('-');
                }

            }
            Db::name('pond')->where('id',1)->update(['flag'=>1]);
        }

    }
    /**
     *
     * 易贝开关
     *
     */
    public function flag(){
        Db::name('pond')->where('id',1)->update(['flag'=>0]);
        echo ('开关已重置');

    }
    /**
     *
     * 挂卖易贝
     *
     */

    public function listingYb(){
        $uid=[];
        $today=strtotime(date('Y-m-d',strtotime('-1 day')));
        $day=strtotime(date('Y-m-d 23:59:59',strtotime('-1 day')));
        //查找符合条件的会员
        $False_market=Db::name('market')->where('create_time','between',[$today,$day])->whereIn('status','1,2');
        $After_market=Db::name('market')->where('create_time','between',[$today,$day])->whereIn('status','1,2');
        $user=Db::name('user')->where('gid','>=',2)->field('id')->select();//过滤会员等级
        if ($user){
            foreach ($user as $key=>$item){
                array_push($uid,$item['id']);
            }
            $uid=implode(',',$uid);
            $f_market=$False_market->whereNotIn('uid',$uid)->select();
            if ($f_market){//青铜会员把未卖完的易贝返还
                foreach ($f_market as $f=>$f_item){
                    $this->marketCancel($f_item['id']);
                }
            }
            $a_market=$After_market->whereIn('uid',$uid)->select();//白银及以上的会员昨日未卖完的易贝进行回购
            if ($a_market){
                foreach ($a_market as $a=>$a_item){//未卖完的易贝按照当前日期重新挂卖
                    $market=model('market')->get(['id'=>$a_item['id']]);
                    $market->save(['create_time'=>time()]);
                }
            }
        }
    }
    //取消订单
    public function marketCancel($id){
        $mid=input('post.mid')?input('post.mid'):$id;
        $market=model('market')->get(['id'=>$mid,'status'=>1]);
        if (!$market){
            echo('挂售订单不存在');
        }
        $user=new \app\common\model\User();
        $flag=$user->capitalRecord($market->uid,$market->surplus,2,'昨日未卖出易贝返还');
        if ($flag['status']==1){
            $request=$market->save(['status'=>4,'false_time'=>time()]);
            if ($request===true){
                echo('返还成功');
            }else{
                $user->capitalRecord($market->uid,'-'.$market->surplus,2,'昨日未卖出易贝失败返还');
            }
        }else{
            echo($flag['msg']);
        }
    }


    /***
     * 强制挂售
     */
    public function forceListing(){
        $config=model('market_config')->get(['id'=>1]);
        if ($config->flag==0){
            $this->returnAPI('强制挂售系统已关闭');
        }else{
            $market=Db::name('market')->where('status','in','1,2')->group('uid')->field('uid')->select();

            $m_uid=[];
            foreach ($market as $key=>$v){
                array_push($m_uid,$v['uid']);
            }
            $m_uid=implode(',',$m_uid);
            $m_price=get_field('market_config',['id'=>1],'price');
            $user=db('user')
//                ->where('gid','>',3)//以去掉等级限制
                ->where('credit2','>',$config->market_num)
                ->where('id','not in',$m_uid)
                ->where('wechat_code','<>','')
                ->field('id,credit2,phone')
                ->select();
            foreach ($user as $k=>$v ){
                $data=[
                    'uid'=>$v['id'],
                    'num'=>intval($v['credit2']*floatval($config->market_service)),
                    'price'=>floatval($m_price),
                    'phone'=>$v['phone']
                ];
                $result = $this->validate($data, $this->validate . 'Market.edit');
                if ($result){
                    $flag= User::capitalRecord($data['uid'],'-'.$data['num'],2,'易贝强制挂售');
                    if ($flag['status']==1){
                        $market=new \app\common\model\Market();
                        $data['create_time']=time();
                        $data['surplus']=$data['num'];
                        $market->save($data);
                        echo ($data['phone'].'-----易贝强制挂售成功');
                        echo ('-');
                    }
                }

            }

        }

    }
    /***
     * 易贝池清空
     */
    public function pond(){
        $pond=model('pond')->get(['id'=>1]);
        $pond->save([
            't_credi2'=>0,
            't_yb'=>0
        ]);
    }
}