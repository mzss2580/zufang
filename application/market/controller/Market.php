<?php
namespace app\market\controller;

class Market extends Base
{
    private $tn = 'market';

    public function index()
    {
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $where = [];
            if(request()->get('seach_name') != ''){
                if(request()->get('seach_type') == 'name'){
                    $where['u.nick'] = request()->get('seach_name');
                }else if(request()->get('seach_type') == 'tel'){
                    $where['m.phone'] = request()->get('seach_name');
                }
            }
            if (request()->get('status')!=''){
                if (request()->get('status')!='-2'){
                    $where['m.status']=request()->get('status');
                }else{
                    $market_order=db('market_order')->where('status',1)->group('mid')->field('mid')->select();
                    $mid=[];
//                    dump($market_order);
                    foreach ($market_order as $item){
                        array_push($mid,$item['mid']);
                    }
                    $mid=implode(',',$mid);
                }


            }
            $db    = db($this->tn)->alias('m')->join('user u','u.id=m.uid');

            if (request()->get('status')=='-2'){
                $count = $db->where($where)->whereIn('m.id',$mid)->count();
            }else{
                $count = $db->where($where)->count();
            }
            if (request()->get('status')=='-2'){
                $list  = $db->field('m.*,u.nick,u.head')


                    ->page($this->get['page'])->limit($this->get['limit'])
                    ->order('m.create_time', 'desc')
                    ->select();
            }else{
                $list  = $db->field('m.*,u.nick,u.head')
                    ->where($where)
                    ->page($this->get['page'])->limit($this->get['limit'])
                    ->order('m.create_time', 'desc')
                    ->select();
            }

//            dump($list);
            foreach ($list as $k => $v) {
                if ($list[$k]['status']==1){
                    $list[$k]['statuss']="<a class='layui-btn layui-btn-xs layui-bg-green'>挂售中 </a><a class='layui-btn layui-btn-xs layui-bg-red' lay-event='cancel'>取消挂售 </a>";
                }
                if ($list[$k]['status']==2){
                    $list[$k]['statuss']="<a class='layui-btn layui-btn-xs' lay-event='log'>已出售 </a>";
                }
                if ($list[$k]['status']==3){
                    $list[$k]['statuss']="<a class='layui-btn layui-btn-xs layui-bg-orange' lay-event='log'>已完成 </a>";
                }
                if ($list[$k]['status']==4){
                    $list[$k]['statuss']="<a class='layui-btn layui-btn-xs layui-bg-red' >已取消 </a>";
                }
                $list[$k]['head'] = '<img class="upload-image show-image" src="' . $v['head'] . '"/>';
                $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
                $list[$k]['true_time'] = $v['true_time']?date('Y-m-d H:i:s', $v['true_time']):'';
                $list[$k]['false_time'] = $v['false_time']?date('Y-m-d H:i:s', $v['false_time']):'';
            }
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit']]);
        }
        return $this->fetch();
    }

    public function edit($id = 0)
    {
        if ($this->post) {
            if ($id === 0) {
                $model = model($this->tn);
            } else {
                $model = model($this->tn)->get($id);
                if (is_null($model)) {
                    $this->returnAPI('信息有误，请稍后重试');
                }
            }
            $validate = $this->validate(['id' => $id] + $this->post, $this->tn);
            if ($validate === true) {
                $result = $model->allowField(true)->save($this->post);
                if ($result === false) {
                    $this->returnAPI($model->getError());
                }
                $this->returnAPI('操作成功', 0);
            }
            $this->returnAPI($validate);
        }
        $model = model($this->tn)->get($id);
        if (is_null($model)) {
            $data = $this->table_fields($this->tn);
        } else {
            $data = $model->getData();
        }
        return $this->fetch('', ['data' => $data]);
    }
    public function log(){
        $uid = $this->get['id'];

        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $db    = db('market_order')->alias('mo')->join('user u','u.id=mo.pay_uid');
            $count = $db->where('mid',$uid)->count();
            $list  = $db->where('mid',$uid)
                ->field('mo.*,u.nick,u.head')
//                ->page($this->get['page'])->limit($this->get['limit'])
                ->order('mo.pay_time', 'desc')
                ->select();

            foreach ($list as $k => $v) {
                if ($list[$k]['status']==1){
                    $list[$k]['statuss']="<a class='layui-btn layui-btn-xs layui-bg-green'>等待商家确认 </a><a class='layui-btn layui-btn-xs layui-bg-red' lay-event='queren'>确认放行 </a>";
                }
                if ($list[$k]['status']==2){
                    $list[$k]['statuss']="<a class='layui-btn layui-btn-xs layui-bg-blue'>已完成 </a>";
                }
                $list[$k]['head'] = '<img class="upload-image show-image" src="' . $v['head'] . '"/>';
                $list[$k]['pay_time'] =$list[$k]['pay_time']? date('Y-m-d H:i:s', $v['pay_time']):'';
                $list[$k]['true_time']=$list[$k]['true_time']? date('Y-m-d H:i:s', $v['true_time']):'';
            }
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit']]);
        }
        return $this->fetch('',['uid'=>$uid]);
    }
    public function cancel($id){
        $market=model('market')->get(['id'=>$id]);
        if (!$market){
            $this->returnAPI('挂售订单不存在');
        }
        $user=new \app\common\model\User();
        $sxf=floatval(get_field('market_config',['id'=>1],'service'));
        $flag=$user->capitalRecord($market->uid,$market->surplus,2,'取消挂售易贝返还');
        if ($flag['status']==1){
            $request=$market->save(['status'=>4,'false_time'=>time()]);
            if ($request===true){
                $this->returnAPI('挂售订单取消成功',0);
            }else{
                $user->capitalRecord($market->uid,'-'.$market->surplus,2,'取消挂售易贝失败返还');
                $this->returnAPI('挂售订单取消失败');
            }
        }else{
            $this->returnAPI($flag['msg']);
        }
    }
    public function queren($id){
        $marketOrder=model('MarketOrder')->get(['id'=>$id,'status'=>1]);
        $market=model('market')->get(['id'=>$marketOrder->mid]);
        $muser=get_field('user',['id'=>$market->uid],'phone');
        if (!$marketOrder){
            $this->returnAPI('订单不存在');
        }
        $user=new \app\common\model\User();
        $flag=$user->capitalRecord($marketOrder->pay_uid,$marketOrder->pay_num,2,'市场购买'.$muser.'的易贝');
        if ($flag['status']==1){
            $flagg=$marketOrder->save(['status'=>2,'true_time'=>time()]);
            if ($flagg===true){
                $this->returnAPI('确认成功',0);
            }else{
                $this->returnAPI('确认失败');
            }
        }else{
            $this->returnAPI($flag['msg']);
        }
    }
}
