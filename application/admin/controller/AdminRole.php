<?php
namespace app\admin\controller;

class AdminRole extends Base
{
    private $tn = 'admin_role';

    public function index()
    {
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $db    = db($this->tn);
            $count = $db->count();
            $list  = $db->field(true)->page($this->get['page'])->limit($this->get['limit'])->order('id')->select();
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit']]);
        }
        return $this->fetch();
    }

    public function edit($id = 0)
    {
        if ($this->post) {
            if ($id === 0) {
                $model = model($this->tn);
            } else {
                $model = model($this->tn)->get($id);
                if (is_null($model)) {
                    $this->returnAPI('信息有误，请稍后重试');
                }
            }
            $validate = $this->validate(['id' => $id] + $this->post, $this->tn);
            if ($validate === true) {
                $result = $model->allowField(true)->save($this->post);
                if ($result === false) {
                    $this->returnAPI($model->getError());
                }
                $this->returnAPI('操作成功', 0);
            }
            $this->returnAPI($validate);
        }
        $model = model($this->tn)->get($id);
        if (is_null($model)) {
            $data = $this->table_fields($this->tn);
        } else {
            $data = $model->getData();
        }
        return $this->fetch('', ['data' => $data]);
    }

    public function auth($id)
    {
        $auth  = [];
        $model = model('admin_role')->get($id);
        if (!is_null($model) && $model->auth) {
            $auth = explode(',', $model->auth);
        }
        $menu_list = db('admin_menu')->field(['id', 'name', 'route'])->where('parent_id', 0)->where('disabled', 0)->where('id', '>=', 8)->order('sort', 'desc')->select();
        foreach ($menu_list as $k => $v) {
            if (!$v['route']) {
                $sub_list = db('admin_menu')->field(['id', 'name', 'route'])->where('parent_id', $v['id'])->where('disabled', 0)->where('id', '>=', 8)->order('sort', 'desc')->select();
                if (empty($sub_list)) {
                    unset($menu_list[$k]);
                } else {
                    $menu_list[$k]['sub'] = $sub_list;
                }
            }
        }
        return $this->fetch('', ['id' => $id, 'auth' => $auth, 'menulist' => $menu_list]);
    }
}
