<?php
namespace app\admin\controller;

class Config extends Base
{
    private $tn = 'system_config';

    public function _empty($action)
    {
        if ($this->post) {
            foreach ($this->post as $config => $value) {
                if($config == 'tixian_data'){
                    if(config($value) > 0){
                        $value = implode(',',$value);
                    }
                }
                db($this->tn)->where('config', $config)->setField('value', $value);
            }
            $this->returnAPI('保存成功', 0);
        }
        $data = ['configs' => db($this->tn)->column('value', 'config')];
        if($action == 'tixian'){
            $tixian_data = explode(',',$data['configs']['tixian_data']);
            $data['tixian_data'] = $tixian_data;
        }
        return $this->fetch($action, $data);
    }

    public function pwd()
    {
        if ($this->post) {
            $admin = model('admin')->get(session('admin_id'));
            if (is_null($admin)) {
                $this->returnAPI('登录信息有误,请重新登录', -10000);
            }
            if ($admin->password !== md5($this->post['oldpwd'])) {
                $this->returnAPI('原密码有误');
            }
            if ($admin->password === md5($this->post['newpwd'])) {
                $this->returnAPI('与原密码相同');
            }
            $result = $admin->save(['password' => md5($this->post['newpwd'])]);
            if ($result === false) {
                $this->returnAPI($admin->getError());
            }
            $this->returnAPI('密码修改成功', 0);
        }
        return $this->fetch();
    }
    public function pwds()
    {
        if ($this->post) {
            $admin = db($this->tn)->where('id',39)->find();
//            dump($admin);

            if ($admin['value']!== md5($this->post['oldpwd'])) {
                $this->returnAPI('原密码有误');
            }
            if ($admin['value'] === md5($this->post['newpwd'])) {
                $this->returnAPI('与原密码相同');
            }
            $result = db($this->tn)->where('id',39)->update(['value'=>md5($this->post['newpwd'])]);
            if (!$result) {
                $this->returnAPI('修改失败');
            }
            $this->returnAPI('密码修改成功', 0);
        }
        return $this->fetch();
    }
}
