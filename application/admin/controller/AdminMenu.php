<?php
namespace app\admin\controller;

use think\helper\Str;

class AdminMenu extends Base
{
    private $tn = 'admin_menu';

    public function index()
    {
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $db = db($this->tn);
            if (isset($this->get['parent_id'])) {
                $db->where('parent_id', $this->get['parent_id']);
            }
            if (isset($this->get['module'])) {
                $db->where('module', $this->get['module']);
            }
            if (isset($this->get['name'])) {
                $db->where('name', 'like', '%' . $this->get['name'] . '%');
            }
            $count = $db->count();
            $list  = $db->field(true)->page($this->get['page'])->limit($this->get['limit'])->order('id')->select();
            foreach ($list as $k => $v) {
                $list[$k]['parent_id'] = get_field('admin_menu', $v['parent_id'], 'name', '顶级菜单');
                $list[$k]['module']    = get_field('admin_module', $v['module'], 'route', 'admin');
                $list[$k]['icon']      = '<i class="fa ' . $v['icon'] . '"></i>';
            }
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit']]);
        }
        return $this->fetch('', [
            'parent_id' => ['' => '父级菜单', '0' => '顶级菜单'] + db($this->tn)->where('parent_id', 0)->order('sort', 'desc')->column('name', 'id'),
            'module'    => ['' => '所属模块'] + db('admin_module')->where('type', 0)->order('route')->column('route', 'id'),
        ]);
    }

    public function edit($id = 0)
    {
        if ($this->post) {
            if ($id === 0) {
                $model = model($this->tn);
            } else {
                $model = model($this->tn)->get($id);
                if (is_null($model)) {
                    $this->returnAPI('信息有误，请稍后重试');
                }
            }
            $validate = $this->validate(['id' => $id] + $this->post, $this->tn);
            if ($validate === true) {
                $result = $model->allowField(true)->save($this->post);
                if ($result === false) {
                    $this->returnAPI($model->getError());
                }
                $this->returnAPI('操作成功', 0);
            }
            $this->returnAPI($validate);
        }
        $model = model($this->tn)->get($id);
        if (is_null($model)) {
            $data = $this->table_fields($this->tn);
        } else {
            $data = $model->getData();
        }
        return $this->fetch('', [
            'data'      => $data,
            'parent_id' => ['0' => '顶级菜单'] + db($this->tn)->where('parent_id', 0)->where('route', '')->where('id', '<>', $id)->order('sort', 'desc')->column('name', 'id'),
            'route'     => $this->routes(),
            'module'    => db('admin_module')->where('type', 0)->order('route')->column('route', 'id'),
        ]);
    }

    private function routes()
    {
        $routes  = [];
        $modules = db('admin_module')->where('type', 0)->order('route')->column('route', 'id');
        foreach ($modules as $module) {
            $controllers = $this->controllers($module);
            foreach ($controllers as $controller) {
                $actions = $this->actions($module, $controller);
                foreach ($actions as $action) {
                    $routes[] = '/' . $module . '/' . Str::snake($controller) . '/' . $action;
                }
            }
        }
        return $routes;
    }

    private function controllers($module)
    {
        $controllers = glob(env('ROOT_PATH') . 'application' . DIRECTORY_SEPARATOR . $module . DIRECTORY_SEPARATOR . 'controller' . DIRECTORY_SEPARATOR . '*.php');
        foreach ($controllers as $k => $v) {
            $controller = basename($v, '.php');
            if ($controller === 'Base') {
                unset($controllers[$k]);
            } else {
                $controllers[$k] = $controller;
            }
        }
        return array_values($controllers);
    }

    private function actions($module, $controller)
    {
        $content = file_get_contents(env('ROOT_PATH') . 'application' . DIRECTORY_SEPARATOR . $module . DIRECTORY_SEPARATOR . 'controller' . DIRECTORY_SEPARATOR . $controller . '.php');
        $result  = [];
        preg_match_all('/.*?public.*?function(.*?)\(.*?\)/i', $content, $result);
        $actions = $result[1];
        foreach ($actions as $k => $v) {
            $action = trim($v);
            if ($action === '') {
                unset($actions[$k]);
            } else {
                $actions[$k] = $action;
            }
        }
        return array_values($actions);
    }
}
