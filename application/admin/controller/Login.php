<?php
namespace app\admin\controller;

class Login extends Base
{
    public function login()
    {
        if (!is_null($this->check_login())) {
            $this->redirect('index/index');
        }
        $login_captcha = get_config('login_captcha');
        if ($this->post) {
            if ($login_captcha && !captcha_check($this->post['captcha'])) {
                $this->returnAPI('验证码有误');
            }
            $admin = model('admin')->get(['username' => $this->post['username']]);
            if (is_null($admin)) {
                $this->returnAPI('账号不存在');
            }
            if ($admin->password !== $this->post['password']) {
                $this->returnAPI('密码不正确');
            }
            if ($admin->disabled) {
                $this->returnAPI('账号被禁用');
            }
            if ($admin->role > 0) {
                $role = model('admin_role')->get($admin->role);
                if (is_null($role) || $role->disabled) {
                    $this->returnAPI('账号无权限');
                }
            }
            session('admin_id', $admin->id);
            if (isset($this->post['remember']) && $this->post['remember']) {
                cookie('admin_login', simple_encrypt($admin->id . '@@@' . $admin->password), 30 * 86400);
            }
            $this->returnAPI('登录成功', 0);
        }
        return $this->fetch('', ['login_captcha' => $login_captcha]);
    }

    public function logout()
    {
        session('admin_id', null);
        cookie('admin_login', null);
        $this->returnAPI('退出登录成功', 0);
    }
}
