<?php
namespace app\admin\controller;

class AdminModule extends Base
{
    private $tn = 'admin_module';

    private $type = ['0' => '后台模块', '1' => '基础模块'];

    public function index()
    {
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $db    = db($this->tn)->where('id', '>', 1);
            $count = $db->count();
            $list  = $db->field(true)->page($this->get['page'])->limit($this->get['limit'])->order('id')->select();
            foreach ($list as $k => $v) {
                $list[$k]['type'] = $this->type[$v['type']];
            }
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit']]);
        }
        return $this->fetch();
    }

    public function edit($id = 0)
    {
        if ($this->post) {
            if ($id === 0) {
                $module_path = env('APP_PATH') . $this->post['route'];
                if (is_dir($module_path)) {
                    $this->returnAPI('模块已存在');
                }
                $model = model($this->tn);
            } else {
                $model = model($this->tn)->get($id);
                if (is_null($model)) {
                    $this->returnAPI('信息有误，请稍后重试');
                }
            }
            $validate = $this->validate(['id' => $id] + $this->post, $this->tn);
            if ($validate === true) {
                $result = $model->allowField(true)->save($this->post);
                if ($result === false) {
                    $this->returnAPI($model->getError());
                }
                if ($id === 0) {
                    @mkdir($module_path);
                    @mkdir($module_path . DIRECTORY_SEPARATOR . 'controller');
                    @mkdir($module_path . DIRECTORY_SEPARATOR . 'view');
                    $template_module = env('ROOT_PATH') . 'extend' . DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR . 'module' . DIRECTORY_SEPARATOR;
                    if ($this->post['type']) {
                        @file_put_contents($module_path . DIRECTORY_SEPARATOR . 'controller' . DIRECTORY_SEPARATOR . 'Base.php', str_replace(['{{$module}}'], [$this->post['route']], file_get_contents($template_module . 'base.php.tpl')));
                        @file_put_contents($module_path . DIRECTORY_SEPARATOR . 'view' . DIRECTORY_SEPARATOR . 'layout.html', '{__CONTENT__}');
                    } else {
                        @file_put_contents($module_path . DIRECTORY_SEPARATOR . 'controller' . DIRECTORY_SEPARATOR . 'Base.php', str_replace(['{{$module}}'], [$this->post['route']], file_get_contents($template_module . 'base.auth.php.tpl')));
                        @file_put_contents($module_path . DIRECTORY_SEPARATOR . 'view' . DIRECTORY_SEPARATOR . 'layout.html', file_get_contents($template_module . 'layout.html.tpl'));
                        @mkdir(env('ROOT_PATH') . 'public' . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . 'module' . DIRECTORY_SEPARATOR . $this->post['route']);
                    }
                }
                $this->returnAPI('操作成功', 0);
            }
            $this->returnAPI($validate);
        }
        $model = model($this->tn)->get($id);
        if (is_null($model)) {
            $data = $this->table_fields($this->tn);
        } else {
            $data = $model->getData();
        }
        return $this->fetch('', ['data' => $data, 'type' => $this->type]);
    }

    public function remove()
    {
        $model = model($this->tn)->get($this->post['id']);
        if (is_null($model)) {
            $result = true;
        } else {
            $result = $model->delete();
            if ($result) {
                delete_dir(env('APP_PATH') . $model->route);
                if (!$model->type) {
                    delete_dir(env('ROOT_PATH') . 'public' . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . 'module' . DIRECTORY_SEPARATOR . $model->route);
                }
            }
        }
        if ($result) {
            $this->returnAPI('操作成功', 0);
        }
        $this->returnAPI('操作失败,请稍后重试');
    }
}
