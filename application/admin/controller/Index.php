<?php
namespace app\admin\controller;

class Index extends Base
{
    public function index()
    {
        list($modules, $menulist) = $this->menu_list(db('admin_module')->where('type', 0)->order('route')->column('name', 'id'), $this->admin);
        return $this->fetch('', [
            'modules'  => $modules,
            'menulist' => $menulist,
            'username' => $this->admin->username,
        ]);
    }

    private function menu_list($modules, $admin)
    {
        $auth = '';
        if ($admin->role > 0) {
            $auth = get_field('admin_role', $admin->role, 'auth');
        }
        $menulist = [];
        foreach ($modules as $module => $name) {
            $menu = db('admin_menu')->field(['id', 'name', 'route', 'icon'])->where('parent_id', 0)->where('module', $module)->where('disabled', 0);
            if ($admin->id > 1) {
                $menu->where('id', '>=', 8);
            } else if (config('app_debug') === false) {
                $menu->where('id', '>=', 5);
            }
            if ($auth !== '') {
                $menu->whereIn('id', $auth);
            }
            $menu_list = $menu->order('sort', 'desc')->select();
            foreach ($menu_list as $k => $v) {
                if (!$v['route']) {
                    $sub = db('admin_menu')->field(['id', 'name', 'route', 'icon'])->where('parent_id', $v['id'])->where('module', $module)->where('disabled', 0);
                    if ($admin->id > 1) {
                        $sub->where('id', '>=', 8);
                    } else if (config('app_debug') === false) {
                        $sub->where('id', '>=', 5);
                    }
                    if ($auth !== '') {
                        $sub->whereIn('id', $auth);
                    }
                    $sub_list = $sub->order('sort', 'desc')->select();
                    if (empty($sub_list)) {
                        unset($menu_list[$k]);
                    } else {
                        $menu_list[$k]['sub'] = $sub_list;
                    }
                }
            }
            if (empty($menu_list)) {
                unset($modules[$module]);
            } else {
                $menulist[] = $menu_list;
            }
        }
        return [$modules, $menulist];
    }
}
