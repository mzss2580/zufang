<?php
namespace app\admin\controller;

use think\Db;
use think\helper\Str;

class Gen extends Base
{
    private $table;
    private $tn;
    private $tn_cn;
    private $tn_hump;
    private $ext_php;
    private $module;
    private $column_data;
    private $module_path;
    private $template_path;

    public function index()
    {
        return $this->fetch('', [
            'tables'  => array_column(Db::query('SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_SCHEMA = "' . config('database.database') . '" AND TABLE_NAME NOT IN ("bz_admin","bz_admin_menu","bz_admin_module","bz_admin_role","bz_system_config","bz_system_region","bz_system_status")'), 'TABLE_NAME'),
            'modules' => db('admin_module')->where('type', 0)->order('route')->column('route'),
        ]);
    }

    public function preview()
    {
        // 完整表名
        $this->table = $this->post['tn'];
        // 表名
        $this->tn = str_replace(config('database.prefix'), '', $this->table);
        // 模块路径
        $this->module = $this->post['module'];
        // 模块本地路径
        $this->module_path = env('ROOT_PATH') . 'application' . DIRECTORY_SEPARATOR . $this->module . DIRECTORY_SEPARATOR;
        // 表名注释
        $tn_cn_result = Db::query('SELECT TABLE_COMMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA = "' . config('database.database') . '" AND TABLE_NAME = "' . $this->table . '"');
        if (!empty($tn_cn_result)) {
            $this->tn_cn = $tn_cn_result[0]['TABLE_COMMENT'];
        } else {
            $this->tn_cn = '';
        }
        // 表字段数据
        $this->column_data = Db::query('SELECT COLUMN_NAME,IS_NULLABLE,DATA_TYPE,IF(COLUMN_COMMENT = "",COLUMN_NAME,COLUMN_COMMENT) COLUMN_COMMENT,COLUMN_TYPE FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = "' . config('database.database') . '" AND TABLE_NAME = "' . $this->table . '" AND COLUMN_NAME <> "id"');
        // 表名转驼峰
        $this->tn_hump = Str::studly($this->tn);
        // 控制器，模型，验证器文件名称
        $this->ext_php = DIRECTORY_SEPARATOR . $this->tn_hump . '.php';
        // 模板文件路径
        $this->template_path = env('ROOT_PATH') . 'extend' . DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR . 'table' . DIRECTORY_SEPARATOR;
        if (isset($this->post['preview']) && $this->post['preview'] === 'preview') {
            $preview = false;
        } else {
            $preview = true;
        }
        $data = [$this->getTplController($preview), $this->getTplModel($preview), $this->getTplIndexJs($preview), $this->getTplValidate($preview), $this->getTplEditHtml($preview), $this->getTplIndexHtml($preview)];
        if ($preview) {
            return $this->fetch('', ['data' => $data, 'module' => $this->module, 'tn' => $this->table]);
        }
        foreach ($data as $v) {
            @mkdir(dirname($v[0]));
            if (!is_file($v[0])) {
                @file_put_contents($v[0], $v[1]);
            }
        }
        $this->returnAPI('操作成功', 0);
    }
    private function getTplController($preview)
    {
        $file = $this->module_path . 'controller' . $this->ext_php;
        if ($preview) {
            $file = str_replace(env('ROOT_PATH'), '', $file);
        }
        $list = '
            foreach ($list as $k => $v) {';
        foreach ($this->column_data as $column) {
            if (in_array($column['COLUMN_NAME'], ['create_time', 'update_time'])) {
                $list .= '
                $list[$k][\'' . $column['COLUMN_NAME'] . '\'] = date(\'Y-m-d H:i:s\', $v[\'' . $column['COLUMN_NAME'] . '\']);';
            } else if ($column['COLUMN_TYPE'] === 'varchar(233)') {
                $list .= '
                $list[$k][\'' . $column['COLUMN_NAME'] . '\'] = \'<img class="upload-image show-image" src="\' . $v[\'' . $column['COLUMN_NAME'] . '\'] . \'"/>\';';
            }
        }
        $list .= '
            }';
        $content = str_replace(['{{$module}}', '{{$tn_hump}}', '{{$tn}}', '{{$list}}'], [$this->module, $this->tn_hump, $this->tn, $list], file_get_contents($this->template_path . 'controller.php.tpl'));
        return [$file, $content];
    }
    private function getTplModel($preview)
    {
        $file = env('APP_PATH') . 'common' . DIRECTORY_SEPARATOR . 'model' . $this->ext_php;
        if (is_file($file) && $preview) {
            return [];
        }
        $create_time = '';
        if (deep_in_array('create_time', $this->column_data)) {
            $create_time = '
    protected $insert = [\'create_time\'];

    protected function setCreateTimeAttr()
    {
        return time();
    }';
        }
        $update_time = '';
        if (deep_in_array('update_time', $this->column_data)) {
            $update_time = '
    protected $update = [\'update_time\'];

    protected function setUpdateTimeAttr()
    {
        return time();
    }';
        }
        if ($preview) {
            $file = str_replace(env('ROOT_PATH'), '', $file);
        }
        $content = str_replace(['{{$tn_hump}}', '{{create_time}}', '{{update_time}}'], [$this->tn_hump, $create_time, $update_time], file_get_contents($this->template_path . 'model.php.tpl'));
        return [$file, $content];
    }
    private function getTplIndexJs($preview)
    {
        $file = env('ROOT_PATH') . 'public' . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . 'module' . DIRECTORY_SEPARATOR . $this->module . DIRECTORY_SEPARATOR . $this->tn . DIRECTORY_SEPARATOR . 'index.js';
        if ($preview) {
            $file = str_replace(env('ROOT_PATH'), '', $file);
        }
        $columns = '';
        foreach ($this->column_data as $column) {
            $columns .= '{
                field: \'' . $column['COLUMN_NAME'] . '\',
                title: \'' . $column['COLUMN_COMMENT'] . '\',
                unresize: true,
                align: \'center\'
            }, ';
        }
        $content = str_replace(['{{$tn_cn}}', '{{$columns}}', '{{$tn}}', '{{$module}}'], [$this->tn_cn, $columns, $this->tn, $this->module], file_get_contents($this->template_path . 'module.index.js.tpl'));
        return [$file, $content];
    }
    private function getTplValidate($preview)
    {
        $file = env('APP_PATH') . 'common' . DIRECTORY_SEPARATOR . 'validate' . $this->ext_php;
        if (is_file($file) && $preview) {
            return [];
        }
        $rule    = '';
        $message = '';
        $scene   = '';
        foreach ($this->column_data as $column) {
            if (!in_array($column['COLUMN_NAME'], ['create_time', 'update_time'])) {
                if ($column['IS_NULLABLE'] === 'NO') {
                    $rule .= '
        \'' . $column['COLUMN_NAME'] . '\' => \'require';
                    $message .= '
        \'' . $column['COLUMN_NAME'] . '.require\' => \'' . $column['COLUMN_COMMENT'] . '为必填项\',';
                    if (in_array($column['DATA_TYPE'], ['int', 'decimal', 'float', 'double'])) {
                        $rule .= '|number';
                        $message .= '
        \'' . $column['COLUMN_NAME'] . '.number\' => \'' . $column['COLUMN_COMMENT'] . '只能填写数字\',';
                    }
                    $rule .= '\',';
                    $scene .= '
            \'' . $column['COLUMN_NAME'] . '\',';
                }
            }
        }
        if ($preview) {
            $file = str_replace(env('ROOT_PATH'), '', $file);
        }
        $content = str_replace(['{{$tn}}', '{{$tn_hump}}', '{{$rule}}', '{{$message}}', '{{$scene}}'], [$this->tn, $this->tn_hump, $rule, $message, $scene], file_get_contents($this->template_path . 'validate.php.tpl'));
        return [$file, $content];
    }
    private function getTplEditHtml($preview)
    {
        $file = $this->module_path . 'view' . DIRECTORY_SEPARATOR . $this->tn . DIRECTORY_SEPARATOR . 'edit.html';
        if ($preview) {
            $file = str_replace(env('ROOT_PATH'), '', $file);
        }
        $columns = '';
        foreach ($this->column_data as $column) {
            if (!in_array($column['COLUMN_NAME'], ['create_time', 'update_time'])) {
                $columns .= '
    <div class="layui-form-item">
        <label class="layui-form-label">
            ' . $column['COLUMN_COMMENT'] . '
        </label>
        <div class="layui-input-block">
            ';
                $lay_verify = '';
                if ($column['COLUMN_TYPE'] === 'varchar(233)') {
                    if ($column['IS_NULLABLE'] === 'NO') {
                        $lay_verify = ' lay-verify="uploadimg"';
                    }
                    $columns .= '<button class="layui-btn layui-btn-sm upload-image" type="button">
                <i class="fa fa-image">
                </i>
                ' . $column['COLUMN_COMMENT'] . '
            </button>
            <input' . $lay_verify . ' name="' . $column['COLUMN_NAME'] . '" type="hidden" value="{$data[\'' . $column['COLUMN_NAME'] . '\']}"/>
            <div class="upload-image">
                <span>
                </span>
                <img class="upload-image" src="{$data[\'' . $column['COLUMN_NAME'] . '\']}"/>
            </div>';
                } else {
                    if ($column['IS_NULLABLE'] === 'NO') {
                        $lay_verify = ' lay-verify="required';
                        if (in_array($column['DATA_TYPE'], ['int', 'decimal', 'float', 'double'])) {
                            $lay_verify .= '|number';
                        }
                        $lay_verify .= '"';
                    }
                    $columns .= '<input autocomplete="off" class="layui-input"' . $lay_verify . ' name="' . $column['COLUMN_NAME'] . '" type="text" value="{$data[\'' . $column['COLUMN_NAME'] . '\']}"/>';
                }
                $columns .= '
        </div>
    </div>';
            }
        }
        $content = str_replace(['{{$columns}}'], [$columns], file_get_contents($this->template_path . 'view.edit.html.tpl'));
        return [$file, $content];
    }
    private function getTplIndexHtml($preview)
    {
        $file = $this->module_path . 'view' . DIRECTORY_SEPARATOR . $this->tn . DIRECTORY_SEPARATOR . 'index.html';
        if ($preview) {
            $file = str_replace(env('ROOT_PATH'), '', $file);
        }
        $content = str_replace(['{{$tn_cn}}', '{{$tn}}', '{{$module}}'], [$this->tn_cn, $this->tn, $this->module], file_get_contents($this->template_path . 'view.index.html.tpl'));
        return [$file, $content];
    }
}
