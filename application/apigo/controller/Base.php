<?php
namespace app\apigo\controller;

use alisms\Alisms;
use app\common\controller\Ctrl;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use think\Db;
use Ws\Http\Request;

class Base extends Ctrl
{
    /**
     * 初始化参数
     */
    protected function initialize()
    {
        $this->validate = 'app\common\validate\\';
    }
    protected function check_token()
    {
        $user  = null;
        $token = input('param.token');
        if (!is_null($token)) {
            try {
                $token  = simple_decrypt($token);
                $parser = new Parser();
                $token  = $parser->parse($token);
                $sha256 = new Sha256();
                if ($token->verify($sha256, config('encrypt_salt'))) {
                    $user = model('user')->get($token->getClaim('user_id'));
                    if (!is_null($user) && $user->pwd !== $token->getClaim('user_pwd')) {
                        $user = null;
                    }
                }
            } catch (\Exception $e) {
            }
        }
        return $user;
    }
    protected function sendyzm($phone, $prefix)
    {
        $time  = time();
        $again = config('alisms.yzm_again');
        if (cache($phone . '_' . $prefix . '_yzm') >= $time - $again) {
            $this->returnAPI('请' . $again . '秒后再次发送');
        }
        $code = mt_rand(100000, 999999);
        $resp = Alisms::send(config('alisms.accessKeyId'), config('alisms.accessKeySecret'), $phone, config('alisms.SignName'), config('alisms.TemplateCode'), ['code' => $code]);
        // $code = 123456;
        // $resp = true;
        if ($resp === true) {
            cache($phone . '_' . $prefix, $code, config('alisms.yzm_indate') * 60);
            cache($phone . '_' . $prefix . '_yzm', $time, $again);
            $this->returnAPI('验证码发送成功', 0);
        }
        $this->returnAPI('验证码发送失败');
    }
    protected function checkyzm($phone, $code, $prefix)
    {
        if (!cache($phone . '_' . $prefix . '_yzm')) {
            $this->returnAPI('请先发送验证码');
        }
        if (intval(cache($phone . '_' . $prefix . '_yzm')) <= (time() - config('alisms.yzm_indate') * 60)) {
            $this->returnAPI('验证码已失效');
        }
        if ($code !== cache($phone . '_' . $prefix)) {
            $this->returnAPI('验证码有误');
        }
        cache($phone . '_' . $prefix, null);
        cache($phone . '_' . $prefix . '_yzm', null);
    }
    /**
     * 设置定时器
     * @param  integer $time 执行时间
     * @param  string  $url  执行方法
     */
    protected function timer($time, $url)
    {
        $http = Request::create();
        $http->get('http://0.0.0.0:2345', [], [
            'type' => 'create',
            'time' => $time,
            'url'  => $this->request->domain() . $url,
        ]);
    }
    protected function cancel_order($order, $time)
    {
        Db::startTrans();
        try {
            $order->save(['status' => 9, 'cancel_time' => $time]);
            if ($order->user_coupon_id > 0) {
                $user_coupon = model('user_coupon')->get($order->user_coupon_id);
                if (!is_null($user_coupon) && $user_coupon->status == 1) {
                    if ($user_coupon->end_time <= time()) {
                        $user_coupon->status = 2;
                    } else {
                        $user_coupon->status = 0;
                    }
                    $user_coupon->save(['used_time' => null]);
                }
            }
            $order_goods = db('order_goods')->field(['goods_id', 'specs_id', 'amount'])->where('order_id', $order->id)->select();
            foreach ($order_goods as $v) {
                if ($v['specs_id'] === '') {
                    db('goods')->where('id', $v['goods_id'])->setInc('kucun', $v['amount']);
                } else {
                    db('goods_specs')->where('goods_id', $v['goods_id'])->where('specs', $v['specs_id'])->setInc('kucun', $v['amount']);
                }
            }
            // 提交事务
            Db::commit();
            $result = true;
        } catch (\Exception $e) {
            trace('取消订单失败:' . $e->getMessage());
            // 回滚事务
            Db::rollback();
            $result = false;
        }
        return $result;
    }
    protected function refund_num_back($refund)
    {
        $refund_cont = json_decode($refund->refund_cont, true);
        foreach ($refund_cont as $v) {
            db('order_goods')->where('id', $v['id'])->setDec('refund_num', $v['refund_num']);
        }
    }
    public function user_score($user_id, $score, $type)
    {
        $user = model('user')->get($user_id);
        if (!is_null($user)) {
            if (in_array($type, [1])) {
                $user->score -= $score;
            } else {
                $user->score += $score;
                $user->allscore += $score;
            }
            $user->save();
            model('user_score')->save([
                'user_id' => $user->id,
                'type'    => $type,
                'score'   => $score,
                'balance' => $user->score,
            ]);
        }
    }
    //根据经纬度计算距离
    protected function getdistance($lng1, $lat1, $lng2, $lat2)
    {
        //将角度转为狐度
        $radLat1 = deg2rad($lat1);
        $radLat2 = deg2rad($lat2);
        $radLng1 = deg2rad($lng1);
        $radLng2 = deg2rad($lng2);
        $a       = $radLat1 - $radLat2; //两纬度之差,纬度<90
        $b       = $radLng1 - $radLng2; //两经度之差纬度<180
        $s       = 2 * asin(sqrt(pow(sin($a / 2), 2) + cos($radLat1) * cos($radLat2) * pow(sin($b / 2), 2))) * 6371 * 1000;
        return $s;
    }
}
