<?php
namespace app\apigo\controller;

use GuzzleHttp\Psr7\Request;
use think\Db;

class Shop extends Base
{
    /**
     * 获取商品列表
     * @return json
     */
    public function goods()
    {
        $goods = model('goods')->alias('a')->join('goods_category b','a.category=b.id')
        ->order('sale desc')->column('a.id,a.name,a.image,a.price,a.sale,b.name as shopname');
        dump($goods);
    }
    /**
     * 查询某一类商品
     * @param int $category  所属商品分类id
     * @return void  返回一类商品 
     */
    public function Onetype($category = 1)
    {
        $goods = model('goods')->alias('a')->join('goods_category b','a.category=b.id')
        ->where('a.category='.$category)
        ->order('sale desc')->column('a.id,a.name,a.image,a.price,a.sale,b.name as shopname');
        dump($goods);
    }

    /**
     * 查询销量靠前的商品
     * @param int $page  取前多少个
     * @return arr  
     */
    public function ranking($page = 3)
    {
        $goods = model('goods')->alias('a')->join('goods_category b','a.category=b.id')
        ->order('sale desc')->limit($page)->column('a.id,a.name,a.image,a.price,a.sale,b.name as shopname');
        dump($goods);
    }

    /**
     * 商铺推荐
     * @param int $page  取前多少个
     * @return arr  所有商铺信息
     */
    public function shopRecommend($page = 3)
    {
        $groupShop = model('order_goods')->field('shop_id,count(*) as xx')->where('shop_id!=0')->order('xx desc')->group('shop_id')->limit($page)->select();
        if(!$groupShop){
            dump('没有推荐商铺');exit;
        }
        //查询出所有的商铺
        $shop = [];
        foreach($groupShop as $shopId){
            $shopTmp = [];
            $shopTmp = model('shop')->where('id='.$shopId['shop_id'])->find()->toArray();
            $shop[] = $shopTmp;
        }
        dump($shop);
    }

    /**
     * 查询首页banner
     * @param int $page  取前多少个
     * @return arr 返回图片名称  图片地址
     */
    public function banner($page = 4)
    {
        $banner = model('banner')->order('')->limit($page)->select();
        dump($banner);
    }

    /**
     * 关键词搜索
     * @param string $keyWord  关键词
     * @return arr  返回关键词的一些数据
     */
    public function serch($keyWord = '')
    {
        $keyWord = input('param.keyWord');
        // $where['name|content'] = ['like', '%'.$keyWord.'%'];
                                        //商品名称|商品内容
        $serchData = model('goods')->where('name|content','like','%'.$keyWord.'%')->select();
        if(empty($serchData)){return $this->returnAPI('抱歉没有查到',0);}

        //记录一下关键词
        // $res = model('search_key')->get(['search_key',$keyWord]);
        // if($res){
        //     model('search_key')->where('id',$res['id'])->save(['amount',1]);
        // }else{
        //     model('search_key')->save(['user_id'=>1778,'search_key'=>$keyWord,'amount'=>1,'update_time'=>time()]);
        // }

        return $this->returnAPI($serchData,1);
        
    }
    
    /**
     * 默认关键词
     * @return string  
     */
    public function defaultKey()
    {
        dump('默认关键词');
    }

//---------------------------------------------------------------//
        /**
     * 获取商品详情
     * @param int $id 商品的ID
     * @param int $page 评价内容条数
     * @return void 返回商品的信息
     */
    public function productDetail($id = 5,$page = 3)
    {
        $product = model('goods')->where('id='.$id)->find();
        dump($product);
        //根据id查询出所有的评价内容 ；
        $count = model('goods_comment')->where('goods_id',$id)->count();
        $comment = model('goods_comment')->where('goods_id',$id)->limit($page)->select();
        


        dump($count);    
        dump($comment);
    }

    /**
     * 添加购物车
     * @param string $goods_id 商品id
     * @param string $shop_id  店铺id
     * @param string $user_id  用户id
     * @param string $specs  选择规格
     * @param string $amount  选择数量
     * @return void
     */
    public function addShopCart($goods_id='',$shop_id='',$user_id='')
    {
        //需要接收 参数  用户id,商品id，店铺id，选择规格，选择数量
        $data = [
            'goods_id' => 5,
            'shop_id' => 1,
            'user_id' => 6,
            'specs' => '12*12',
            'amount' => 10,
        ];
        $res = model('cart')->save($data);
        dump($res);
    }

    /**
     * 查询出所有分类
     * @param string $page  展示分类的数量
     * @return arr
     */
    public function category($page = 4)
    {
        $category = model('goods_category')->order('sort asc')->limit($page)->select()->toArray();
        if(empty($category)){
            return $this->returnAPI('暂时没有分类',0);
        }
        return $this->returnAPI($category,1);
    }

    /**
     * 查看分类下的所有商品  -- 按照综合排序
     * @param int $shop_id  店铺id
     * @param int $category_id  分类id
     * @return void
     */
    public function categoryGoodsZH($shop_id = 0,$category_id = 7)
    {
        $where = ['shop_id' => $shop_id,'category'=> $category_id,'on_sale'=>1];
        $dataZH = model('goods')->where($where)->order('score desc')->select()->toArray();
        if(empty($dataZH)){return $this->returnAPI('商品为空',0);}
        return $this->returnAPI($dataZH,1);
    }
    /**
     * 查看分类下的所有商品  --按照最新排序
     * @param int $shop_id  店铺id
     * @param int $category_id  分类id
     * @return void
     */
    public function categoryGoodsZX($shop_id = 0,$category_id = 7)
    {
        $where = ['shop_id' => $shop_id,'category'=> $category_id,'on_sale'=>1];
        $dataZH = model('goods')->where($where)->order('create_time desc')->select()->toArray();
        if(empty($dataZH)){return $this->returnAPI('商品为空',0);}
        return $this->returnAPI($dataZH,1);
    }
    /**
     * 查看分类下的所有商品  -- 按照销量排序
     * @param int $shop_id  店铺id
     * @param int $category_id  分类id
     * @return arr
     */
    public function categoryGoodsXL($shop_id = 0,$category_id = 7)
    {
        $where = ['shop_id' => $shop_id,'category'=> $category_id,'on_sale'=>1];
        $dataZH = model('goods')->where($where)->order('sale desc')->select()->toArray();
        if(empty($dataZH)){return $this->returnAPI('商品为空',0);}
        return $this->returnAPI($dataZH,1);
    }

    /**
     * 查看商品  （全部/好评/中评/差评）评价数量
     * @param int $shop_id  商铺id
     * @param int $goods_id  商品id
     * @return arr
     */
    public function countPJ($shop_id = 1 ,$goods_id = 5)
    {
        $where = [ 'gs.shop_id'  =>$shop_id, 'gs.goods_id' =>$goods_id, ];
        //查看全部评价数量
        $allNum = model('goods_comment')->alias('gs')
            ->join('goods g','gs.goods_id=g.id')
            ->join('shop s','gs.shop_id=s.id')
            ->join('user u','gs.user_id=u.id','left')
            ->join('specs sp','gs.specs_id=sp.id','left')
            ->where($where)
            ->count();
        //查看好评数量
        $where['gs.score'] = [1,2];
        $goodNum = model('goods_comment')->alias('gs')
            ->join('goods g','gs.goods_id=g.id')
            ->join('shop s','gs.shop_id=s.id')
            ->join('user u','gs.user_id=u.id','left')
            ->join('specs sp','gs.specs_id=sp.id','left')
            ->where($where)
            ->count();
        //查看中评数量
        $where['gs.score'] = 3;
        $medNum = model('goods_comment')->alias('gs')
            ->join('goods g','gs.goods_id=g.id')
            ->join('shop s','gs.shop_id=s.id')
            ->join('user u','gs.user_id=u.id','left')
            ->join('specs sp','gs.specs_id=sp.id','left')
            ->where($where)
            ->count();
        //查看差评数量
        $where['gs.score'] = [4,5];
        $botNum = model('goods_comment')->alias('gs')
            ->join('goods g','gs.goods_id=g.id')
            ->join('shop s','gs.shop_id=s.id')
            ->join('user u','gs.user_id=u.id','left')
            ->join('specs sp','gs.specs_id=sp.id','left')
            ->where($where)
            ->count();
        $num = [
            'all' => $allNum,
            'good' => $goodNum,
            'med' => $medNum,
            'bot' => $botNum
        ];
        return $this->returnAPI($num,1);
    }

    /**
     * 查看商铺的 商品的所有评价
     * @param [type] $shop_id  商铺的id
     * @param [type] $goods_id  商品的id
     * @param [type] $level  查询级别 （0:全部;1:好评;2:中评;3:差评;）
     * @return void
     */
    public function comment($shop_id = 1 ,$goods_id = 5 , $level = 0)
    {
        $where = [ 'gs.shop_id'  =>$shop_id, 'gs.goods_id' =>$goods_id];
        switch ($level){
            case 1:
                $where['gs.score'] = [4,5];
            break;  
            case 2:
                $where['gs.score'] = [3];
                break;
            case 3:
                $where['gs.score'] = [1,2];
                break;
        }
        $comment = model('goods_comment')->alias('gs')
                ->join('goods g','gs.goods_id=g.id')
                ->join('shop s','gs.shop_id=s.id')
                ->join('user u','gs.user_id=u.id','left')
                ->join('specs sp','gs.specs_id=sp.id','left')
                ->where($where)
                ->select()->toArray();
        if(empty($comment)){return $this->returnAPI('暂无评价',0);}
        return $this->returnAPI($comment,1);
        
    } 
    
}
