<?php
namespace app\ajax\controller;

use app\common\controller\Ctrl;
use think\Db;
use think\Image;

/**
 * @title 通用方法
 * @description 接口说明
 */
class Index extends Ctrl
{
    protected $beforeActionList = ['check_auth'];
    protected function check_auth()
    {
        if (
            is_null(session('admin_id'))
            &&
            is_null(session('shop_id'))
        ) {
            die();
        }
    }
    /**
     * 通用删除
     */
    public function remove()
    {
        $time=date('Y-m-d H:i:s',time());
        $where = $this->request->post('where');
        $table = $this->request->post('table');

        if (!is_null($where) && !is_null($table)) {
            if (!is_array($where)) {
                $where = ['id' => $where];
            }
//            if ($table=='user'){
//                $user=model('user')->get($where);
//                Db::name('typelog')->insert([
//                    'text'=>'后台与'.$time.'时间删除了会员id为:'.$user->id.',手机号为:'.$user->phone.',昵称为:'.$user->nick.'的用户',
//                    'time'=>$time,
//                    'type'=>'会员删除'
//                ]);
//            }
            $result = db($table)->where($where)->delete();
            if ($result) {
                $this->returnAPI('删除成功', 0);
            }
        }
        $this->returnAPI('删除失败,请稍后重试');
    }
    /**
     * 通用更新
     */
    public function update()
    {
        $where  = $this->request->post('where');
        $table  = $this->request->post('table');
        $update = $this->request->post('update');
        if (!is_null($where) && !is_null($table) && !is_null($update)) {
            if (!is_array($where)) {
                $where = ['id' => $where];
            }
            $result = db($table)->where($where)->update($update);
            if ($result) {
                $this->returnAPI('操作成功', 0);
            }
        }
        $this->returnAPI('操作失败,请稍后重试');
    }
    /**
     * 通用上传
     */
    public function upload()
    {
        $path = $this->request->post('path', 'images');
        $file = $this->request->file($this->request->post('name', 'file'));
        $file_info = $file->getInfo();
        //dump($file->getInfo());die;
        if ($file) {
//            $return = $this->upload_oss($file_info['tmp_name'], $file_info['name']);
//            //$image = $return['src'];
//            $this->returnAPI('上传成功', 0, $return);


            $move = env('ROOT_PATH') . 'public' . DIRECTORY_SEPARATOR . 'upload' . DIRECTORY_SEPARATOR . $path;
            $info = $file->rule('md5')->move($move);
            if ($info) {
                $src = $move . DIRECTORY_SEPARATOR . $info->getSaveName();
                $img = getimagesize($src);
                if ($img !== false && $img['mime'] != 'image/gif') {
                    $thumb = str_replace('.', '_thumb.', $src);
                    $image = Image::open($src);
                    $image->thumb(150, 150)->save($thumb);
                }
                if ($this->request->post('domain', 1)) {
                    $domain = $this->request->domain();
                } else {
                    $domain = '';
                }
                $return = ['src' => str_replace('\\', '/', str_replace(env('ROOT_PATH') . 'public', $domain, $src))];
                if (isset($thumb)) {
                    $return['thumb'] = str_replace('\\', '/', str_replace(env('ROOT_PATH') . 'public', $domain, $thumb));
                }
                $this->returnAPI('上传成功', 0, $return);
            }
            //$this->returnAPI($file->getError());
        }
        $this->returnAPI('上传失败,请稍后重试');
    }
}
