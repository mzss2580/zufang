<?php

/**
 * Created by 区域代理.
 * User: 李磊
 * Date: 2022/12/5
 * Time: 14:23
 */
namespace app\system\controller;
use think\Db;

class Agent extends Base
{
    private $tn = 'agent';
    public function index()
    {
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $db    = db($this->tn);
            $count = $db->count();
            $list  = $db->field(true)->page($this->get['page'])->limit($this->get['limit'])->order('id', 'desc')->select();
            foreach ($list as $k => $v) {
                $user=model('user')->get($v['uid']);
                $list[$k]['nick']=$user->nick;
                $list[$k]['head']='<img class="upload-image show-image" src="' . $user->head . '"/>';
                $list[$k]['phone']=$user->phone;
                $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
//               $list[$k]['grade']
                switch ($v['region']){
                    case 1;
                        $list[$k]['address']=get_field('city',$v['province'],'name').'---'.get_field('city',$v['city'],'name').'---'.get_field('city',$v['area'],'name').'---'.get_field('city',$v['street'],'name');
                        $list[$k]['grade']='街道代理';
                    break;
                    case 2;
                        $list[$k]['address']=get_field('city',$v['province'],'name').'---'.get_field('city',$v['city'],'name').'---'.get_field('city',$v['area'],'name');
                        $list[$k]['grade']='区级代理';
                    break;
                    case 3;
                        $list[$k]['address']=get_field('city',$v['province'],'name').'---'.get_field('city',$v['city'],'name');
                        $list[$k]['grade']='市级代理';
                    break;
                    case 4;
                        $list[$k]['address']=get_field('city',$v['province'],'name');

                        $list[$k]['grade']='省级代理';
                        break;
                }
            }
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit']]);
        }
        return $this->fetch();
    }
    public function edit($id = 0)
    {
        $agent_grade=Db::name('agent_grade')->select();
        $province=Db::name('city')->where('type',1)->field('id,pid,type,name')->select();
        $user=Db::name('user')->field('id,phone,nick')->select();
        if ($this->post) {
            if ($id === 0) {
                $model = model($this->tn);
                $this->post['proportion']=floatval(get_field('agent_grade',$this->post['region'],'proportion'));
            } else {
                $model = model($this->tn)->get($id);
                if (is_null($model)) {
                    $this->returnAPI('信息有误，请稍后重试');
                }
            }
            $validate = $this->validate(['id' => $id] + $this->post, $this->tn);
            if ($validate === true) {
                $this->post['create_time']=time();
                //限制区域只能有一个代理


                if ($id==0){
                    switch ($this->post['region']){
                        case 4;
                            $aid=Db::name('agent')->where(['province'=>$this->post['province'],'region'=>4])->select();
                            break;
                        case 3;
                            $aid=Db::name('agent')->where(['area'=>$this->post['area'],'region'=>3])->select();
                            break;
                        case 2;
                            $aid=Db::name('agent')->where(['city'=>$this->post['city'],'region'=>2])->select();
                            break;
                        case 1;
                            $aid=Db::name('agent')->where(['street'=>$this->post['street'],'region'=>1])->select();
                            break;
                    }
                    if($aid){
                        $this->returnAPI('该地区已存在代理不可重复添加');
                    }
                }else{
//                    if ()
                    $agent=model('agent')->get(['id'=>$id]);
                    switch ($this->post['region']){
                        case 4;
                            if ($agent->region!=$this->post['region']){
                                $aid=Db::name('agent')->where(['province'=>$this->post['province'],'region'=>4])->select();
                            }
                            break;
                        case 3;
                            if ($agent->region!=$this->post['region']){
                                $aid=Db::name('agent')->where(['city'=>$this->post['city'],'region'=>3])->select();
                            }
                            break;
                        case 2;
                            if ($agent->region!=$this->post['region']){
                                $aid=Db::name('agent')->where(['area'=>$this->post['area'],'region'=>2])->select();
                            }
                            break;
                        case 1;
                            if ($agent->region!=$this->post['region']){
                                $aid=Db::name('agent')->where(['street'=>$this->post['street'],'region'=>1])->select();
                            }
                            break;
                    }

                    if(!empty($aid)){
                        $this->returnAPI('该地区已存在代理不可重复添加');
                    }
                }
//                    $uid=Db::name('agent')->where('uid',$this->post['uid'])->select();
//                    if($uid){
//                        $this->returnAPI('该用户已存在代理不可重复添加');
//                    }

                //限制一个用户能只能有一个代理区域


                if ($id==0){
                    $uid=Db::name('agent')->where('uid',$this->post['uid'])->select();
                    if($uid){
                        $this->returnAPI('该用户已存在代理不可重复添加');
                    }
                }else{
                    $agent=model('agent')->get(['id'=>$id]);
                    if ($this->post['uid']!=$agent->uid){
                        $uid=Db::name('agent')->where('uid',$this->post['uid'])->select();
                        if($uid){
                            $this->returnAPI('该用户已存在代理不可重复添加');
                        }
                    }
                }
                switch ($this->post['region']){
                    case 1;
                      $address=get_field('city',$this->post['province'],'name').get_field('city',$this->post['city'],'name').get_field('city',$this->post['area'],'name').get_field('city',$this->post['street'],'name');

                        break;
                    case 2;
                        $address=get_field('city',$this->post['province'],'name').get_field('city',$this->post['city'],'name').get_field('city',$this->post['area'],'name');
                        break;
                    case 3;
                        $address=get_field('city',$this->post['province'],'name').get_field('city',$this->post['city'],'name');
                        break;
                    case 4;
                        $address=get_field('city',$this->post['province'],'name');
                        break;
                }
//                dump($address);
                $lnglat=$this->lnglat($address);
                $this->post['lng']=$lnglat[0];
                $this->post['lat']=$lnglat[1];
                $result = $model->allowField(true)->save($this->post);
                if ($result === false) {
                    $this->returnAPI($model->getError());
                }
                $this->returnAPI('操作成功', 0);
            }
            $this->returnAPI($validate);
        }
        $model = model($this->tn)->get($id);
        if (is_null($model)) {
            $data = $this->table_fields($this->tn);
            $city=[];
            $area=[];
            $street=[];
        } else {
            $data = $model->getData();
            $data['cityname']=get_field('city',['id'=>$data['city']]);
            $city=Db::name('city')->where('pid',$data['province'])->select();
            $area=Db::name('city')->where('pid',$data['city'])->select();
            $street=Db::name('city')->where('pid',$data['area'])->select();

            $data['areaname']=get_field('city',['id'=>$data['area']]);
//            $data['streetname']=get_field('city',['id'=>$data['street']]);
        }
        return $this->fetch('', ['data' => $data,'user'=>$user,'province'=>$province,'city'=>$city,'area'=>$area,'street'=>$street,'agent_grade'=>$agent_grade]);
    }

    public function city($pid){
        $city=Db::name('city')->where(['type'=>2,'pid'=>$pid])->select();
        return $city;
    }

    //区
    public function area($cid){
        $area=Db::name('city')->where(['type'=>3,'pid'=>$cid])->select();
        return $area;
    }
    //街道
    public function street($aid){
        $street=Db::name('city')->where(['type'=>4,'pid'=>$aid])->select();
        return $street;
    }

    //根据位置获取经纬度
    public function lnglat($address){
        $key="eccdd9df4d9d2ff30949c2607ef59aae";
        $regeo_url="https://restapi.amap.com/v3/geocode/geo?address=$address";
        $address_location=$regeo_url."&output=JSON&key=$key";
        $data_location=file_get_contents($address_location);

        $result_local=json_decode($data_location,true);
        $arr=explode(',',$result_local['geocodes'][0]['location']);
        return $arr;
    }
}