<?php
namespace app\system\controller;

use app\common\controller\Ctrl;
use think\Db;

class Base extends Ctrl
{
    protected $get;
    protected $post;
    /**
     * 初始化参数
     */
    protected function initialize()
    {

        $this->get  = $this->request->get();
        $this->post = $this->request->post();
    }
    protected $beforeActionList = [
        'check_auth' => [
            'except' => 'login,logout',
        ],
    ];
    protected function check_login()
    {
        $admin_login = cookie('admin_login');
        if (!is_null($admin_login)) {
            $admin_login = explode('@@@', simple_decrypt($admin_login));
        }
        $admin_id = session('admin_id');
        if (is_null($admin_id) && !is_null($admin_login)) {
            $admin_id = $admin_login[0];
            session('admin_id', $admin_id);
        }
        $admin = null;
        if (!is_null($admin_id)) {
            $admin = model('admin')->get($admin_id);
            if (is_null($admin) || $admin->disabled !== 0 || (!is_null($admin_login) && $admin->password !== $admin_login[1])) {
                $admin = null;
            }
        }
        return $admin;
    }
    protected function check_auth()
    {
        $admin = $this->check_login();
        if (!is_null($admin)) {
            if ($admin->role > 0) {
                $role = model('admin_role')->get($admin->role);
                if (is_null($role) || $role->disabled !== 0) {
                    $admin = null;
                } else if ($role->auth) {
                    $menu = model('admin_menu')->get(['route' => $this->request->url()]);
                    if (!is_null($menu) && strpos($role->auth, $menu->id) !== false && $menu->disabled !== 0) {
                        $admin = null;
                    }
                }
            }
        }
        if (is_null($admin)) {
            session('admin_id', null);
            cookie('admin_login', null);
            if (!empty($this->post) || (isset($this->get['page']) && isset($this->get['limit']))) {
                $this->returnAPI('登录信息有误,请重新登录', -10000);
            } else {
                $this->redirect('/admin/login/login');
            }
        }
        $this->admin = $admin;
    }

}
