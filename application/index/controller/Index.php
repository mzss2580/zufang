<?php
namespace app\index\controller;

use app\common\controller\Ctrl;

/**
 * @title 登录注册
 * @description 接口说明
 */
class Index extends Ctrl
{
    public function index(){
        $code = input('get.code');
        if(!empty($code)){
            $tj_code = db('user_invite_code')->where('code', $code)->find();
            if(empty($tj_code)){
                $code = '';
            }
        }
        return $this->fetch('',['tj_code'=>$code]);
    }
}
