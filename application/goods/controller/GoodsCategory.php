<?php
namespace app\goods\controller;

class GoodsCategory extends Base
{
    private $tn = 'goods_category';

    public function index()
    {
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $category = input('category', 0);
            $where = ['parent_id'=>$category];
            $db    = db($this->tn)->where($where);
            $count = $db->count();
            $list  = $db->field(true)->page($this->get['page'])->limit($this->get['limit'])->order('id', 'desc')->select();
            foreach ($list as $k => $v) {
                $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
            }
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit']]);
        }
        return $this->fetch();
    }

    public function edit($id = 0)
    {
        if ($this->post) {
            if ($id === 0) {
                $model = model($this->tn);
            } else {
                $model = model($this->tn)->get($id);
                if (is_null($model)) {
                    $this->returnAPI('信息有误，请稍后重试');
                }
            }
            $validate = $this->validate(['id' => $id] + $this->post, $this->tn);
            if ($validate === true) {
                $result = $model->allowField(true)->save($this->post);
                if ($result === false) {
                    $this->returnAPI($model->getError());
                }
                $this->returnAPI('操作成功', 0);
            }
            $this->returnAPI($validate);
        }
        $model = model($this->tn)->get($id);
        if (is_null($model)) {
            $data = $this->table_fields($this->tn);
        } else {
            $data = $model->getData();
        }
        $category = [0=>'顶级分类'];
        $list = db($this->tn)->where('parent_id', 0)->column('name', 'id');
        foreach($list as $k => $v){
            $category[$k] = '--'.$v;
            //查询二级分类
            $two = db($this->tn)->where('parent_id', $k)->column('name', 'id');
            foreach ($two as $tk => $tv){
                $category[$tk] = '------'.$tv;
            }
        }
        return $this->fetch('', ['data' => $data, 'category'=>$category]);
    }

    public function remove()
    {
        $model = model($this->tn)->get(['id' => $this->post['id']]);
        if (!is_null($model)) {
            if (db('goods')->where('category', $model->id)->count() > 0) {
                $this->returnAPI('该商品分类下有商品,无法删除');
            }
            $model->delete();
        }
        $this->returnAPI('删除成功', 0);
    }

    /**
     * 编辑排序
     * @return void
     */
    public function editSort()
    {
        if(!$this->post['id'] || !is_numeric($this->post['sort'])){ return $this->returnAPI('更新失败',0);}
        $id = $this->post['id'];
        $sort = $this->post['sort'];
        $res = model('goods_category')->allowField('sort')->save(['sort'=>$sort],['id'=>$id]);
        if(!$res){return $this->returnAPI('更新失败',0);}else{return $this->returnAPI('更新成功',1);
        }
    }
}
