<?php
namespace app\goods\controller;

use think\Db;

class Goods extends Base
{
    private $tn = 'goods';

    public function index()
    {
        $category = db('goods_category')->order(['sort' => 'desc', 'id' => 'desc'])->column('name', 'id');
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $where = [];
            $name = input('name');

            $cate = input('cate');
            if(!empty($cate)){
                $where['g.category'] = $cate;
            }
            $db = db($this->tn)->alias('g')->join('shop s', 'g.shop_id = s.id', 'LEFT')->where(function ($query) {
                $query->whereOr('g.shop_id', 0)->whereOr('s.id', 'not null');
            })->where('g.type', 0)->where($where)->where('g.is_del', 0)->where('shop_id', 0);
            if(!empty($name)){
                $db->where('g.name', 'like', '%'.$name.'%');
            }

            $count = $db->count();
            $list  = $db->field(['g.id', 'IFNULL(s.name,"--")' => 'shop_name', 'g.category', 'g.name', 'g.image', 'g.price', 'g.del_price', 'g.sort', 'g.on_sale', 'g.create_time'])->page($this->get['page'])->limit($this->get['limit'])->order('g.id', 'desc')->select();
            foreach ($list as $k => $v) {
                $list[$k]['specs']       = $this->parse_specs(get_field('goods_specs', ['goods_id' => $v['id']], 'specs'), true);
                $list[$k]['category']    = $category[$v['category']];
                $list[$k]['image']       = '<img class="upload-image show-image" src="' . $v['image'] . '"/>';
                $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
            }
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit']]);
        }
        return $this->fetch('', ['category' => ['' => '商品分类'] + $category]);
    }

    public function shop_goods()
    {
        $category = db('goods_category')->order(['sort' => 'desc', 'id' => 'desc'])->column('name', 'id');
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $where = [];
            $name = input('name');

            $cate = input('cate');
            if(!empty($cate)){
                $where['g.category'] = $cate;
            }
            $db = db($this->tn)->alias('g')->join('shop s', 'g.shop_id = s.id', 'LEFT')->where(function ($query) {
                $query->whereOr('g.shop_id', 0)->whereOr('s.id', 'not null');
            })->where('g.type', 0)->where($where)->where('g.is_del', 0)->where('shop_id', '>', 0);

            if(!empty($name)){
                $db->where('g.name', 'like', '%'.$name.'%');
            }

            $count = $db->count();
            $list  = $db->field(['g.id', 'IFNULL(s.name,"--")' => 'shop_name', 'g.category', 'g.name', 'g.image', 'g.price', 'g.del_price', 'g.sort', 'g.on_sale', 'g.create_time'])->page($this->get['page'])->limit($this->get['limit'])->order('g.id', 'desc')->select();
            foreach ($list as $k => $v) {
                $list[$k]['specs']       = $this->parse_specs(get_field('goods_specs', ['goods_id' => $v['id']], 'specs'), true);
                $list[$k]['category']    = $category[$v['category']];
                $list[$k]['image']       = '<img class="upload-image show-image" src="' . $v['image'] . '"/>';
                $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
            }
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit']]);
        }
        return $this->fetch('shop_goods', ['category' => ['' => '商品分类'] + $category]);
    }

    public function edit($id = 0)
    {
        if ($this->post) {
            if ($id === 0) {
                $model = model($this->tn);
                $model->data(['type' => 0]);
            } else {
                $model = model($this->tn)->get(['id' => $id, 'type' => 0]);
                if (is_null($model)) {
                    $this->returnAPI('信息有误，请稍后重试');
                }
            }
            $validate = $this->validate(['id' => $id] + $this->post, $this->tn);
            if ($validate === true) {
                $result = $model->allowField(true)->save($this->post);
                if ($result === false) {
                    $this->returnAPI($model->getError());
                }
                $this->returnAPI('操作成功', 0);
            }
            $this->returnAPI($validate);
        }
        $model = model($this->tn)->get($id);
        if (is_null($model)) {
            $data = $this->table_fields($this->tn);
        } else {
            $data = $model->getData();
        }
        return $this->fetch('', ['data' => $data, 'category' => db('goods_category')->order(['sort' => 'desc', 'id' => 'desc'])->column('name', 'id')]);
    }

    public function comment($goods_id)
    {
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $db = db('goods_comment')->alias('c')->join('user u', 'c.user_id = u.id')
                ->field(['u.head', 'u.nick', 'c.id', 'c.score', 'c.content', 'c.images', 'c.create_time'])
                ->where('c.score', '>', 0)->where('c.goods_id', $goods_id);
            if (isset($this->get['score']) && $this->get['score'] > 0) {
                $score = $this->get['score'];
                if ($score == 1) {
                    $db->where('c.score', 'in', [4, 5]);
                } else if ($score == 2) {
                    $db->where('c.score', 'in', [2, 3]);
                } else if ($score == 3) {
                    $db->where('c.score', 1);
                }
            }
            $count = $db->count();
            $list  = $db->order('c.create_time', 'desc')->page($this->get['page'])->limit($this->get['limit'])->select();
            foreach ($list as $k => $v) {
                $images = json_decode($v['images'], true);
                foreach ($images as $key => $value) {
                    $images[$key] = '<img class="upload-image show-image" src="' . $value . '"/>';
                }
                $list[$k]['images']      = $images;
                $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
            }
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit']]);
        }
        return $this->fetch();
    }
    public function kanjia()
    {
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $db    = db($this->tn)->where('type', 1);
            $count = $db->count();
            $list  = $db->field(true)->page($this->get['page'])->limit($this->get['limit'])->order('id', 'desc')->select();
            foreach ($list as $k => $v) {
                $list[$k]['image']       = '<img class="upload-image show-image" src="' . $v['image'] . '"/>';
                $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
            }
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit']]);
        }
        return $this->fetch();
    }

    public function edit_kj($id = 0)
    {
        if ($this->post) {
            if ($id === 0) {
                $model = model($this->tn);
                $model->data(['type' => 1]);
            } else {
                $model = model($this->tn)->get(['id' => $id, 'type' => 1]);
                if (is_null($model)) {
                    $this->returnAPI('信息有误，请稍后重试');
                }
            }
            $this->post['kj_time'] = intval($this->post['kj_time_day']) * 86400 + intval($this->post['kj_time_hour']) * 3600 + intval($this->post['kj_time_minute']) * 60 + intval($this->post['kj_time_second']);
            if ($this->post['kj_time'] == 0) {
                $this->returnAPI('请填写砍价时长');
            }
            if (
                $this->post['kj_min_80'] > $this->post['kj_max_80']
                ||
                $this->post['kj_min_60'] > $this->post['kj_max_60']
                ||
                $this->post['kj_min_40'] > $this->post['kj_max_40']
                ||
                $this->post['kj_min_20'] > $this->post['kj_max_20']
                ||
                $this->post['kj_min_0'] > $this->post['kj_max_0']
            ) {
                $this->returnAPI('请正确填写砍价范围');
            }
            $validate = $this->validate(['id' => $id] + $this->post, $this->tn);
            if ($validate === true) {
                $result = $model->allowField(true)->save($this->post);
                if ($result === false) {
                    $this->returnAPI($model->getError());
                }
                $this->returnAPI('操作成功', 0);
            }
            $this->returnAPI($validate);
        }
        $model = model($this->tn)->get($id);
        if (is_null($model)) {
            $data = $this->table_fields($this->tn);
        } else {
            $data = $model->getData();
        }
        $data['kj_time_day']    = floor($data['kj_time'] / 86400);
        $data['kj_time_hour']   = floor($data['kj_time'] % 86400 / 3600);
        $data['kj_time_minute'] = floor($data['kj_time'] % 3600 / 60);
        $data['kj_time_second'] = floor($data['kj_time'] % 60);
        return $this->fetch('', ['data' => $data]);
    }

    public function miaosha()
    {
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $db    = db($this->tn)->where('type', 2);
            $count = $db->count();
            $list  = $db->field(true)->page($this->get['page'])->limit($this->get['limit'])->order('id', 'desc')->select();
            foreach ($list as $k => $v) {
                $list[$k]['image']       = '<img class="upload-image show-image" src="' . $v['image'] . '"/>';
                $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
            }
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit']]);
        }
        return $this->fetch();
    }

    public function edit_ms($id = 0)
    {
        if ($this->post) {
            if ($id === 0) {
                $model = model($this->tn);
                $model->data(['type' => 2]);
            } else {
                $model = model($this->tn)->get(['id' => $id, 'type' => 2]);
                if (is_null($model)) {
                    $this->returnAPI('信息有误，请稍后重试');
                }
            }
            $this->post['ms_start_time'] = strtotime($this->post['ms_start_time'] . ' ' . $this->post['ms_start_times']);
            $this->post['ms_end_time']   = strtotime($this->post['ms_end_time'] . ' ' . $this->post['ms_end_times']);

            $validate = $this->validate(['id' => $id] + $this->post, $this->tn);
            if ($validate === true) {
                $result = $model->allowField(true)->save($this->post);
                if ($result === false) {
                    $this->returnAPI($model->getError());
                }
                $this->returnAPI('操作成功', 0);
            }
            $this->returnAPI($validate);
        }
        $model = model($this->tn)->get($id);
        if (is_null($model)) {
            $data = $this->table_fields($this->tn);

            $data['ms_start_times'] = '';
            $data['ms_end_times']   = '';
        } else {
            $data = $model->getData();

            $data['ms_start_times'] = date('H:00:00', $data['ms_start_time']);
            $data['ms_end_times']   = date('H:00:00', $data['ms_end_time']);
            $data['ms_start_time']  = date('Y-m-d', $data['ms_start_time']);
            $data['ms_end_time']    = date('Y-m-d', $data['ms_end_time']);
        }
        return $this->fetch('', [
            'data'  => $data,
            'hours' => [
                '00:00:00',
                '01:00:00',
                '02:00:00',
                '03:00:00',
                '04:00:00',
                '05:00:00',
                '06:00:00',
                '07:00:00',
                '08:00:00',
                '09:00:00',
                '10:00:00',
                '11:00:00',
                '12:00:00',
                '13:00:00',
                '14:00:00',
                '15:00:00',
                '16:00:00',
                '17:00:00',
                '18:00:00',
                '19:00:00',
                '20:00:00',
                '21:00:00',
                '22:00:00',
                '23:00:00',
            ],
        ]);
    }
}
