<?php
namespace app\goods\controller;

use alipay\Alipay;
use app\common\model\Order as UserOrder;
use app\common\model\User;
//use app\common\model\User;
use app\common\model\User as Users;
use EasyWeChat\Foundation\Application;
use think\Db;

class Order extends Base
{
    private $tn = 'order';

    public function index()
    {
        $orders = UserOrder::all(function ($query) {
            $query->where('shop_id', 0)->where('status', 0)->where('create_time', '<=', time() - 600); //config('api.order_timeout')
        });
        if (!empty($orders)) {
            foreach ($orders as $order) {
                $this->cancel_order($order, $order->create_time + 600); //config('api.order_timeout')
            }
        }
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $db = db('order')->alias('o')->join('shop s', 'o.shop_id = s.id', 'left')->join('user u', 'u.id = o.user_id', 'left')
                ->field(['o.id', 'o.orderno', 'o.union_orderno', 'o.status', 'o.express_type', 'o.amount', 'o.person', 'o.phone', 'o.total', 'o.create_time', 'o.pay_true', 'u.nick','o.pay_type'])
                ->where('o.shop_id', 0);
            if (isset($this->get['status'])) {
                $db->where('o.status', $this->get['status']);
            }
             if (isset($this->get['pay_type'])) {
                $db->where('o.pay_type', $this->get['pay_type']);
            }
            if (isset($this->get['orderno'])) {
                $db->where('o.orderno', 'like', '%' . $this->get['orderno'] . '%');
            }
            if (isset($this->get['union_orderno'])) {
                $db->where('o.union_orderno', 'like', '%' . $this->get['union_orderno'] . '%');
            }
            if (isset($this->get['person'])) {
                $db->where('o.person', 'like', '%' . $this->get['person'] . '%');
            }
            if (isset($this->get['phone'])) {
                $db->where('o.phone', 'like', '%' . $this->get['phone'] . '%');
            }
            if (isset($this->get['start'])) {
                if (isset($this->get['end'])) {
                    $db->where('o.create_time', 'between', [strtotime($this->get['start'] . ' 00:00:00'), strtotime($this->get['end'] . ' 23:59:59')]);
                } else {
                    $db->where('o.create_time', '>=', strtotime($this->get['start'] . ' 00:00:00'));
                }
            } else if (isset($this->get['end'])) {
                $db->where('o.create_time', '<=', strtotime($this->get['end'] . ' 23:59:59'));
            }
            $order_status = get_status('order_status');
            $count        = $db->count();
            $list         = $db->order('o.id', 'desc')->page($this->get['page'])->limit($this->get['limit'])->select();
            foreach ($list as $k => $v) {
                $list[$k]['shop_name'] = '自营';
                $list[$k]['pay_type']= $v['pay_type']==1?'支付宝':'微信';

                $list[$k]['status_desc'] = $order_status[$v['status']];
                $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
            }
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit']]);
        }
        return $this->fetch('');
    }

    public function shop_order()
    {
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $db = db('order')->alias('o')->join('shop s', 'o.shop_id = s.id', 'left')->join('user u', 'u.id = o.user_id', 'left')
                ->field(['o.id', 'o.orderno', 'o.union_orderno', 'o.status', 'o.express_type', 'o.amount', 'o.person', 'o.phone', 'o.total', 'o.create_time', 'o.pay_true', 'u.nick','o.pay_type','s.name'=>'shop_name'])
                ->where('o.shop_id','>', 0);
            if (isset($this->get['status'])) {
                $db->where('o.status', $this->get['status']);
            }
            if (isset($this->get['pay_type'])) {
                $db->where('o.pay_type', $this->get['pay_type']);
            }
            if (isset($this->get['orderno'])) {
                $db->where('o.orderno', 'like', '%' . $this->get['orderno'] . '%');
            }
            if (isset($this->get['union_orderno'])) {
                $db->where('o.union_orderno', 'like', '%' . $this->get['union_orderno'] . '%');
            }
            if (isset($this->get['person'])) {
                $db->where('o.person', 'like', '%' . $this->get['person'] . '%');
            }
            if (isset($this->get['phone'])) {
                $db->where('o.phone', 'like', '%' . $this->get['phone'] . '%');
            }
            if (isset($this->get['start'])) {
                if (isset($this->get['end'])) {
                    $db->where('o.create_time', 'between', [strtotime($this->get['start'] . ' 00:00:00'), strtotime($this->get['end'] . ' 23:59:59')]);
                } else {
                    $db->where('o.create_time', '>=', strtotime($this->get['start'] . ' 00:00:00'));
                }
            } else if (isset($this->get['end'])) {
                $db->where('o.create_time', '<=', strtotime($this->get['end'] . ' 23:59:59'));
            }
            $order_status = get_status('order_status');
            $count        = $db->count();
            $list         = $db->order('o.id', 'desc')->page($this->get['page'])->limit($this->get['limit'])->select();
            foreach ($list as $k => $v) {
                $list[$k]['pay_type']= $v['pay_type']==1?'支付宝':'微信';

                $list[$k]['status_desc'] = $order_status[$v['status']];
                $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
            }
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit']]);
        }
        return $this->fetch('');
    }

    public function info($id = 0)
    {
        $order = model('order')->get(['id' => $id]); //, 'shop_id' => 0
        if (is_null($order)) {
            $this->returnAPI('订单信息查询失败,请稍后重试');
        }
        $order->status_desc = get_status('order_status', $order->status);
        $order->create      = date('Y-m-d H:i:s', $order->create_time);
        $order->pay_time    = date('Y-m-d H:i:s', $order->pay_time);
        $order->send_time   = date('Y-m-d H:i:s', $order->send_time);
        $goods_list         = db('order_goods')->field(['goods_id', 'type', 'name', 'specs', 'image', 'price', 'amount', 'pay_price'])->where('order_id', $order->id)->select();
        return $this->fetch('', ['order' => $order, 'goods_list' => $goods_list]);
    }
    public function refund()
    {
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $db = db('order_refund')->field(['id', 'order_id', 'refund_money', 'orderno', 'refund_type', 'goods_state', 'status', 'refund_cont', 'create_time'])
                ->where('shop_id', 0);
            if (isset($this->get['status'])) {
                $db->where('status', $this->get['status']);
            }
            if (isset($this->get['orderno'])) {
                $db->where('orderno', 'like', '%' . $this->get['orderno'] . '%');
            }
            if (isset($this->get['refund_type'])) {
                $db->where('refund_type', $this->get['refund_type']);
            }
            if (isset($this->get['goods_state'])) {
                $db->where('goods_state', $this->get['goods_state']);
            }
            if (isset($this->get['start'])) {
                if (isset($this->get['end'])) {
                    $db->where('create_time', 'between', [strtotime($this->get['start'] . ' 00:00:00'), strtotime($this->get['end'] . ' 23:59:59')]);
                } else {
                    $db->where('create_time', '>=', strtotime($this->get['start'] . ' 00:00:00'));
                }
            } else if (isset($this->get['end'])) {
                $db->where('create_time', '<=', strtotime($this->get['end'] . ' 23:59:59'));
            }
            $order_status = get_status('order_refund_status');
            $refund_type  = get_status('order_refund_type');
            $goods_state  = get_status('order_refund_goods_state');
            $count        = $db->count();
            $list         = $db->order('id','desc')->page($this->get['page'])->limit($this->get['limit'])->select();
            foreach ($list as $k => $v) {
                $list[$k]['status_desc']      = $order_status[$v['status']];
                $list[$k]['refund_type_desc'] = $refund_type[$v['refund_type']];
                $list[$k]['goods_state_desc'] = $goods_state[$v['goods_state']];
                $list[$k]['create_time']      = date('Y-m-d H:i:s', $v['create_time']);
            }
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit']]);
        }
        return $this->fetch('');
    }
    public function shop_refund()
    {
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $db = db('order_refund')->field(['id', 'order_id', 'refund_money', 'orderno', 'refund_type', 'goods_state', 'status', 'refund_cont', 'create_time'])
                ->where('shop_id', '>', 0);
            if (isset($this->get['status'])) {
                $db->where('status', $this->get['status']);
            }
            if (isset($this->get['orderno'])) {
                $db->where('orderno', 'like', '%' . $this->get['orderno'] . '%');
            }
            if (isset($this->get['refund_type'])) {
                $db->where('refund_type', $this->get['refund_type']);
            }
            if (isset($this->get['goods_state'])) {
                $db->where('goods_state', $this->get['goods_state']);
            }
            if (isset($this->get['start'])) {
                if (isset($this->get['end'])) {
                    $db->where('create_time', 'between', [strtotime($this->get['start'] . ' 00:00:00'), strtotime($this->get['end'] . ' 23:59:59')]);
                } else {
                    $db->where('create_time', '>=', strtotime($this->get['start'] . ' 00:00:00'));
                }
            } else if (isset($this->get['end'])) {
                $db->where('create_time', '<=', strtotime($this->get['end'] . ' 23:59:59'));
            }
            $order_status = get_status('order_refund_status');
            $refund_type  = get_status('order_refund_type');
            $goods_state  = get_status('order_refund_goods_state');
            $count        = $db->count();
            $list         = $db->order('id','desc')->page($this->get['page'])->limit($this->get['limit'])->select();
            foreach ($list as $k => $v) {
                $list[$k]['status_desc']      = $order_status[$v['status']];
                $list[$k]['refund_type_desc'] = $refund_type[$v['refund_type']];
                $list[$k]['goods_state_desc'] = $goods_state[$v['goods_state']];
                $list[$k]['create_time']      = date('Y-m-d H:i:s', $v['create_time']);
            }
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit']]);
        }
        return $this->fetch('');
    }
    public function refund_info($id = 0)
    {
        $order = model('order_refund')->get(['id' => $id, 'shop_id' => 0]);
        if (is_null($order)) {
            $this->returnAPI('订单信息查询失败,请稍后重试');
        }
        $order['status_desc']      = get_status('order_refund_status', $order['status']);
        $order['refund_type_desc'] = get_status('order_refund_type', $order['refund_type']);
        $order['goods_state_desc'] = get_status('order_refund_goods_state', $order['goods_state']);
        $refund_cont               = json_decode($order['refund_cont'], true);
        $order_goods_id            = array_column($refund_cont, 'id');
        $order_goods               = array_combine($order_goods_id, array_column($refund_cont, 'refund_num'));
        $goods_list                = db('order_goods')->field(['id', 'goods_id', 'price', 'type', 'name', 'specs', 'image', 'pay_price'])->where('id', 'in', $order_goods_id)->where('order_id', $order['order_id'])->select();
        foreach ($goods_list as $key => $goods) {
            $goods_list[$key]['image']       = $goods['image'] ? $this->request->domain() . $goods['image'] : '';
            $goods_list[$key]['image_thumb'] = $this->image_thumb($goods['image']);
            $goods_list[$key]['refund_num']  = $order_goods[$goods['id']];
        }
        return $this->fetch('', ['order' => $order, 'goods_list' => $goods_list]);
    }
    /*
    同意退款
     */
    public function agree_refund()
    {
        $order = model('order_refund')->get(['id' => $this->post['id'], 'shop_id' => 0]);
        if (is_null($order)) {
            $this->returnAPI('订单信息查询失败,请稍后重试');
        }
        if ($order->status !== 0) {
            $this->returnAPI('操作失败,该订单不支持退款');
        }
        //查询主订单
        $order_subject = model('order')->get(['id'=>$order->order_id]);
        /*$result = $order->save(['status' => 1]);
        if ($result === true) {*/
            $data = [
                'shop_id'         => 0,
                'order_refund_id' => $order->id,
                'content'         => '商家同意退款',
                'create_time'     => time(),
            ];
            db('order_refund_history')->insert($data);
            if ($order->refund_type == 0 ) {
                if ($order->pay_type === 2) {
                    //退款
                    $app = new Application(config('wechat.'));
                    $res = $app->payment->refund($order->refund_orderno, $order->refundno, $order->order_money * 100, $order->refund_money * 100);
                    if ($res->return_code === 'SUCCESS') {
                        if ($res->result_code === 'SUCCESS') {
                            $order->save(['status' => 6]);
                            $history = [
                                'shop_id'         => $order->shop_id,
                                'order_refund_id' => $order->id,
                                'content'         => '商家退款给买家' . $order->refund_money . '元',
                                'create_time'     => time(),
                            ];
                            db('order_refund_history')->insert($history);
                            //商铺余额操作
                            $this->shop_wallet($order, $order->refund_money);
                            $result = $order->save(['status' => 1]);
                            $order_subject->save(['refund_power'=>1]);
                            $this->returnAPI('确认退款成功', 0);
                        } else {
                            trace('微信退款失败:' . $res->err_code . ' ' . $res->err_code_des);
                        }
                    } else {
                        trace('微信退款配置有误:' . $res->return_msg);
                    }
                    $this->returnAPI('确认退款失败,请稍后重试');
                }
                if ($order->pay_type === 1) {
                    //支付宝退款
                    $res = Alipay::app_refund(config('alipay.'), $order->refund_orderno, $order->refund_money, $order->refundno);
                    if($res === false){
                        $this->returnAPI('确认退款失败,请稍后重试');
                    }
                    $order->save(['status' => 6]);
                    $history = [
                        'shop_id'         => $order->shop_id,
                        'order_refund_id' => $order->id,
                        'content'         => '商家退款给买家' . $order->refund_money . '元',
                        'create_time'     => time(),
                    ];
                    db('order_refund_history')->insert($history);
                    //商铺余额操作
                    $this->shop_wallet($order, $order->refund_money);
                    $result = $order->save(['status' => 1]);
                    $order_subject->save(['refund_power'=>1]);
                    $this->returnAPI('确认退款成功', 0);
                }
                if ($order->pay_type === 3) {
                    //返回商城余额
                    $users = new Users();
                    $res = $users->add_user_account_records($order->user_id, 1, 1, $order->refund_money, $order->order_money, 2, $order->id, 'order_refund', '商家同意退款');
                    if(!$res['status']){
                        $this->returnAPI('确认退款失败,请稍后重试');
                    }else{
                        $order->save(['status' => 6]);
                        $history = [
                            'shop_id'         => $order->shop_id,
                            'order_refund_id' => $order->id,
                            'content'         => '商家退款给买家' . $order->refund_money . '元',
                            'create_time'     => time(),
                        ];
                        db('order_refund_history')->insert($history);
                        $this->shop_wallet($order, $order->refund_money);
                        $this->returnAPI('确认退款成功', 0);
                    }
                }
            } else {
                
                if($order->refund_type == 1){
                    $order->save(['status'=>3]);
                }
                $this->returnAPI('同意申请成功', 0);
            }
        /*} else {
            $this->returnAPI('同意申请失败');
        }*/
    }
    /*
    商家确认收货
     */
    public function returnd_over()
    {
        $order = model('order_refund')->get(['id' => $this->post['id'], 'shop_id' => 0]);
    
        if (is_null($order)) {
            $this->returnAPI('订单信息查询失败,请稍后重试');
        }
        if ($order->status !== 4 || $order->refund_type !== 1) {
            $this->returnAPI('确认收货失败');
        }
        //$result = $order->save(['status' => 5]);
        //if ($result === true) {
            $data = [
                'shop_id'         => 0,
                'order_refund_id' => $order->id,
                'content'         => '商家确认收货',
                'create_time'     => time(),
            ];
            db('order_refund_history')->insert($data);
            if ($order->pay_type === 2) {
                //退款
                $app = new Application(config('wechat.'));
                $res = $app->payment->refund($order->refund_orderno, $order->refundno, $order->order_money * 100, $order->refund_money * 100);
                if ($res->return_code === 'SUCCESS') {
                    if ($res->result_code === 'SUCCESS') {
                        $order->save(['status' => 6]);
                        $history = [
                            'shop_id'         => $order->shop_id,
                            'order_refund_id' => $order->id,
                            'content'         => '商家退款给买家' . $order->refund_money . '元',
                            'create_time'     => time(),
                        ];
                        db('order_refund_history')->insert($history);
                        //商铺余额操作
                        $this->shop_wallet($order, $order->refund_money);
                        $this->returnAPI('退款成功', 0);
                    } else {
                        trace('微信退款失败:' . $res->err_code . ' ' . $res->err_code_des);
                    }
                } else {
                    trace('微信退款配置有误:' . $res->return_msg);
                }
                $this->returnAPI('退款失败,请稍后重试');
            }
            if ($order->pay_type === 1) {
                //支付宝退款
                $res = Alipay::app_refund(config('alipay.'), $order->refund_orderno, $order->refund_money, $order->refundno);
                    // dump($res);
                if($res === false){
                    $this->returnAPI('确认退款失败,请稍后重试');
                }
                $order->save(['status' => 6]);
                $history = [
                    'shop_id'         => $order->shop_id,
                    'order_refund_id' => $order->id,
                    'content'         => '商家退款给买家' . $order->refund_money . '元',
                    'create_time'     => time(),
                ];
                db('order_refund_history')->insert($history);
                //商铺余额操作
                $this->shop_wallet($order, $order->refund_money);
                $this->returnAPI('确认退款成功', 0);
            }

            if ($order->pay_type === 3) {
                //返回商城余额
                $users = new Users();
                $res = $users->add_user_account_records($order->user_id, 1, 1, $order->refund_money, $order->order_money, 2, $order->id, 'order_refund', '商家同意退款');
                if(!$res['status']){
                    $this->returnAPI('确认退款失败,请稍后重试');
                }else{
                    $order->save(['status' => 6]);
                    $history = [
                        'shop_id'         => $order->shop_id,
                        'order_refund_id' => $order->id,
                        'content'         => '商家退款给买家' . $order->refund_money . '元',
                        'create_time'     => time(),
                    ];
                    db('order_refund_history')->insert($history);
                    $this->shop_wallet($order, $order->refund_money);
                    $this->returnAPI('退款成功', 0);
                }
            }
        //} else {
        //    $this->returnAPI('确认收货失败');
        //}
    }
    public function refuse_reason($id)
    {
        return $this->fetch('', ['id' => $id]);
    }
    /*
    拒绝退款
     */
    public function refuse_refund()
    {
        $order = model('order_refund')->get(['id' => $this->post['id'], 'shop_id' => 0]);
        if (is_null($order)) {
            $this->returnAPI('订单信息查询失败,请稍后重试');
        }
        if ($order->status !== 0) {
            $this->returnAPI('操作失败');
        }
        $reason = $this->post['reason'];
        $images = json_encode(input('post.img'));
        if (is_null($reason) || $reason == '') {
            $this->returnAPI('请填写拒绝理由');
        }
        $refund_cont    = json_decode($order['refund_cont'], true);
        $order_goods_id = array_column($refund_cont, 'id');
        $order_goods    = array_combine($order_goods_id, array_column($refund_cont, 'refund_num'));
        $goods_list     = db('order_goods')->where('id', 'in', $order_goods_id)->where('order_id', $order['order_id'])->select();
        foreach ($goods_list as $key => $value) {
            db('order_goods')->where('id', $value['id'])->setDec('refund_num', $order_goods[$value['id']]);
        }
        $result = $order->save(['status' => 2, 'refuse_reason' => $reason, 'refuse_images' => $images]);
        if ($result === true) {
            $data = [
                'shop_id'         => 0,
                'order_refund_id' => $order->id,
                'content'         => '商家拒绝退款 拒绝原因：' . $reason,
                'images'          => $images,
            ];
            model('order_refund_history')->save($data);
            //$wallet = model('shop_wallet')->get(['shop_id' => 0, 'type' => 1, 'order_id' => $order->order_id]);
            $count  = db('order_refund')->where(['shop_id' => $order->shop_id, 'order_id' => $order->order_id])->where('status', 'not in', [2, 6, 9])->count();
            /*if (!is_null($wallet)) {
                if ($wallet->freeze == 1 && $count == 0) {
                    $wallet->save(['freeze' => 0]);
                }
            }*/
            if ($count === 0) {
                db('order')->where('id', $order->order_id)->update(['user_refund' => 0]);
            }
            $this->returnAPI('拒绝申请成功', 0);
        } else {
            $this->returnAPI('拒绝申请失败');
        }
    }
    /*
    商家更换退库货地址
     */
    public function address($id)
    {
        if ($this->post) {
            $order = model('order_refund')->get(['id' => $id, 'shop_id' => 0]);
            if (is_null($order)) {
                $this->returnAPI('订单信息查询失败,请稍后重试');
            }
            if ($order->refund_type === 0) {
                $this->returnAPI('订单不需要退货');
            }
            $address = model('address')->get(['id' => $this->post['address_id'], 'user_id' => 0]);
            if (is_null($address)) {
                $this->returnAPI('地址查询失败，请稍后重试');
            }
            if ($order->status == 1) {
                $content = '商家确认收货地址：姓名，' . $address->person . '，' . $address->province . '省' . $address->city . '市' . $address->district . ' 区  ' . $address->addr . '，电话号码：' . $address->phone;
            } elseif ($order->status == 3) {
                $content = '商家修改收货地址：姓名，' . $address->person . '，' . $address->province . '省' . $address->city . '市' . $address->district . ' 区  ' . $address->addr . '，电话号码：' . $address->phone;
            } else {
                $this->returnAPI('该订单不可更换收货地址');
            }
            $data = [
                'address_id' => $this->post['address_id'],
                'person'     => $address->person,
                'phone'      => $address->phone,
                'province'   => $address->province,
                'city'       => $address->city,
                'district'   => $address->district,
                'addr'       => $address->addr,
                'status'     => 3,
            ];
            $result = $order->allowField(true)->save($data);
            if ($result === true) {
                model('order_refund_history')->save([
                    'order_refund_id' => $id,
                    'shop_id'         => 0,
                    'content'         => $content,
                ]);
                $this->returnAPI('更换地址成功', 0);
            } else {
                $this->returnAPI('更换地址失败');
            }
        }
        $address = db('address')->where('user_id', 0)->select();
        return $this->fetch('', ['list' => $address]);
    }
    /*
    协商历史
     */
    public function refund_history($id)
    {
        $list = db('order_refund_history')->alias('o')->join('user u', 'o.user_id = u.id', 'left')->join('shop s', 'o.shop_id = s.id', 'left')
            ->field(['o.content', 'o.images', 'o.create_time', 'o.user_id', 'u.head', 'u.nick', 'o.shop_id', 's.name' => 'shop_name', 's.logo' => 'shop_logo'])
            ->where('o.order_refund_id', $id)->order('o.create_time', 'desc')->select();
        foreach ($list as $k => $v) {
            if (is_null($v['images']) || $v['images'] == '') {
                $images = [];
            } else {
                $images = json_decode($v['images'], true);
            }
            if (!empty($images)) {
                foreach ($images as $key => $value) {
                    $images[$key] = [
                        'images_thumb' => $this->image_thumb($value),
                        'image'        => $value ? $this->request->domain() . $value : '',
                    ];
                }
            }
            $list[$k]['images']      = $images==''?[]:$images;
            $list[$k]['logo_thumb']  = $this->image_thumb($v['shop_logo']);
            $list[$k]['shop_logo']   = $v['shop_logo'] ? $this->request->domain() . $v['shop_logo'] : '';
            $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
        }
        return $this->fetch('', ['list' => $list]);
    }
    public function refund_credit($orderno){
        $team=Db::name('OrderCredit1')->where(['orderno'=>$orderno,'flag'=>1])->select();
        foreach ($team as $item){
            User::capitalRecord($item['uid'],'-'.$item['money'],3,'用户退款,回收积分:'.$item['text']);
            $OrderCredit1=model('order_credit1')->get(['id'=>$item['id']]);
            $OrderCredit1->save(['flag'=>0]);
        }
    }
    public function shop_wallet($order, $refund_money)
    {
        /*$shop->save(['freeze_money' => $shop->freeze_money - $order->refund_money]);
        $data = [
            'shop_id'         => $order->shop_id,
            'type'            => 0,
            'money'           => $order->refund_money,
            'balance'         => $shop->freeze_money,
            'order_id'        => $order->order_id,
            'order_refund_id' => $order->id,
            'refund_cont'     => $order->refund_cont,
        ];
        $model = model('shop_wallet');
        $model->save($data);
        $wallet = model('shop_wallet')->get(['shop_id' => 0, 'type' => 1, 'order_id' => $order->order_id]);*/
        $count  = db('order_refund')->where(['shop_id' => $order->shop_id, 'order_id' => $order->order_id])->where('status', 'not in', [2, 6, 9])->count();
        /*if (!is_null($wallet)) {
            if ($wallet->freeze == 1 && $count == 0) {
                $wallet->freeze = 0;
            }
            $wallet->save(['freeze_money' => $wallet->freeze_money - $order->refund_money]);
        }*/
        if ($count === 0) {
            db('order')->where('id', $order->order_id)->update(['user_refund' => 0]);
        }
        //增加退款金额
        db('order')->where('id', $order->order_id)->update(['refund_amount' => Db::raw('refund_amount+' . $refund_money)]);
        //订单全部退款的，关闭订单
        $refund_cont    = json_decode($order->refund_cont, true);
        $order_goods_id = array_column($refund_cont, 'id');
        $order_goods    = array_combine($order_goods_id, array_column($refund_cont, 'refund_num'));
        $goods_list     = db('order_goods')->where('id', 'in', $order_goods_id)->where('order_id', $order->order_id)->select();
        foreach ($goods_list as $key => $value) {
            db('order_goods')->where('id', $value['id'])->setInc('real_refund_num', $order_goods[$value['id']]);
        }
        //判断当前订单是否全部退款，全部退款关闭订单
        $order_goods_list = db('order_goods')->where('order_id', $order->order_id)->select();
        $close = 1;
        foreach($order_goods_list as $goods){
            if($goods['amount'] > $goods['real_refund_num']){
                $close = 0;
            }
        }
        if($close == 1){
            //关闭当前订单
            db('order')->where('id', $order->order_id)->update(['status' => 8]);
        }
        //退订单的积分
        //查询主订单
        $order_subject = model('order')->get(['id'=>$order->order_id]);
        if($order_subject->integral > 0){
            $kou = bcmul(-1, $order_subject->integral, 2);
            $user_model = new User();//1
            $user_model->capitalRecord($order_subject->user_id, $kou,1,'订单：'.$order_subject->orderno.'退款扣除');
            $user_model->capitalRecord($order_subject->user_id, $kou,3,'订单：'.$order_subject->orderno.'退款扣除');
        }
        $this->refund_credit($order_subject->orderno);
    }
}
