<?php
namespace app\goods\controller;

use think\Db;

class Shops extends Base
{
    private $tn = 'shops';

    public function index()
    {
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $db    = db($this->tn)->where('is_show',1);
            $count = $db->count();
            $list  = $db->field(true)->page($this->get['page'])->limit($this->get['limit'])->order('id', 'desc')->select();
            foreach ($list as $k => $v) {
                $list[$k]['head'] = '<img class="upload-image show-image" src="' . $v['head'] . '"/>';
                $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
                $list[$k]['type_name'] = get_field('shop_type',$v['type'],'name');
                $list[$k]['qualifications'] = '<img class="upload-image show-image" src="' . $v['qualifications'] . '"/>';
            }
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit']]);
        }
        return $this->fetch();
    }

    public function edit($id = 0)
    {
        $user_list = db('user')->where('id', 'not in', $id)->select();
        $type=Db::name('shop_type')->column('name','id');
        $price=Db::name('shop_join')->column('duration','id');
        $zhuangxiu=Db::name('decoration')->column('name','id');
        $louceng=Db::name('storey')->column('name','id');
        $louti=Db::name('louti')->column('name','id');
//        $huxing=Db::name('house_type')->column('name','id');
        if ($this->post) {
            $citys=$this->citys($this->post['lng'],$this->post['lat']);
            $this->post['province']=$citys['province'];
            $this->post['city']=$citys['city'];
            $this->post['area']=$citys['area'];
            $time=$this->post['time'];
            $time=get_field('shop_join',$time,'duration');
            if ($id == 0) {
                $model = model($this->tn);
                if ($time=='永久'){
                    $this->post['end_time']='永久';
                }else{
                    $this->post['end_time']=date("Y-m-d H:i:s",time()+86400*30*$this->post['time']);
                }


                $this->post['pay_status']=1;
                $this->post['is_show']=1;

            } else {
                $model = model($this->tn)->get($id);
                if (is_null($model)) {
                    $this->returnAPI('信息有误，请稍后重试');
                }
            }
            $validate = $this->validate(['id' => $id] + $this->post, $this->tn);
            if ($validate === true) {


                $this->post['create_time']=time();
////            $data['pay_time']=time();
                $this->post['time']=input('time');




                $result = $model->allowField(true)->save($this->post);
                if ($result === false) {
                    $this->returnAPI($model->getError());
                }
                $this->returnAPI('操作成功', 0);
            }
            $this->returnAPI($validate);
        }
        $model = model($this->tn)->get($id);
        if (is_null($model)) {
            $data = $this->table_fields($this->tn);
        } else {
            $data = $model->getData();
        }
        return $this->fetch('', ['data' => $data,'type'=>$type,'price'=>$price,'user_list'=>$user_list,'zhuangxiu'=>$zhuangxiu,'louceng'=>$louceng,'louti'=>$louti]);
    }
    
  


}
