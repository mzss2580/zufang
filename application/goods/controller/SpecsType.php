<?php
namespace app\goods\controller;

class SpecsType extends Base
{
    private $tn = 'specs_type';

    public function index()
    {
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $db = db($this->tn)->alias('st')->join('shop s', 'st.shop_id = s.id', 'LEFT')->where(function ($query) {
                $query->whereOr('st.shop_id', 0)->whereOr('s.id', 'not null');
            });
            $count = $db->count();
            $list  = $db->field(['st.id', 'st.name', 'st.sort', 'st.create_time', 's.id' => 'shop_id', 'IFNULL(s.name,"--")' => 'shop_name'])->page($this->get['page'])->limit($this->get['limit'])->order('st.id', 'desc')->select();
            foreach ($list as $k => $v) {
                $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
            }
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit']]);
        }
        return $this->fetch();
    }

    public function edit($id = 0)
    {
        if ($this->post) {
            if ($id === 0) {
                $model = model($this->tn);
            } else {
                $model = model($this->tn)->get(['id' => $id]);
                if (is_null($model)) {
                    $this->returnAPI('信息有误，请稍后重试');
                }
            }
            $validate = $this->validate(['id' => $id] + $this->post, $this->tn);
            if ($validate === true) {
                $result = $model->allowField(true)->save($this->post);
                if ($result === false) {
                    $this->returnAPI($model->getError());
                }
                $this->returnAPI('操作成功', 0);
            }
            $this->returnAPI($validate);
        }
        $model = model($this->tn)->get(['id' => $id]);
        if (is_null($model)) {
            $data = $this->table_fields($this->tn);
        } else {
            $data = $model->getData();
        }
        return $this->fetch('', ['data' => $data]);
    }

    public function remove()
    {
        $model = model($this->tn)->get(['id' => $this->post['id']]);
        if (!is_null($model)) {
            if (db('specs')->where('type', $model->id)->count() > 0) {
                $this->returnAPI('该规格类型下有规格明细,无法删除');
            }
            $model->delete();
        }
        $this->returnAPI('删除成功', 0);
    }
}
