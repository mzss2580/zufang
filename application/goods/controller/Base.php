<?php
namespace app\goods\controller;

use app\admin\controller\Base as AdminBase;
use think\Db;

class Base extends AdminBase
{
    protected function cancel_order($order, $time)
    {
        Db::startTrans();
        try {
            $order->save(['status' => 9, 'cancel_time' => $time]);
            if ($order->user_coupon_id > 0) {
                $user_coupon = model('user_coupon')->get($order->user_coupon_id);
                if (!is_null($user_coupon) && $user_coupon->status == 1) {
                    if ($user_coupon->end_time <= time()) {
                        $user_coupon->status = 2;
                    } else {
                        $user_coupon->status = 0;
                    }
                    $user_coupon->save(['used_time' => null]);
                }
            }
            $order_goods = db('order_goods')->field(['goods_id', 'specs_id', 'amount'])->where('order_id', $order->id)->select();
            foreach ($order_goods as $v) {
                if ($v['specs_id'] === '') {
                    db('goods')->where('id', $v['goods_id'])->setInc('kucun', $v['amount']);
                } else {
                    db('goods_specs')->where('goods_id', $v['goods_id'])->where('specs', $v['specs_id'])->setInc('kucun', $v['amount']);
                }
            }
            // 提交事务
            Db::commit();
            $result = true;
        } catch (\Exception $e) {
            trace('取消订单失败:' . $e->getMessage());
            // 回滚事务
            Db::rollback();
            $result = false;
        }
        return $result;
    }
    /**
     * 模拟post进行url请求 * @param string $url * @param array $postData */
    function https_request($url, $data = null)
    {
        $curl = curl_init();//初始化
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);//允许 cURL 函数执行的最长秒数。
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/json;charset=UTF-8'));
        //设置请求目标url头部信息
        if (!empty($data)) {
            //$data不为空，发送post请求
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data); //$data：数组
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);//执行命令
        $error = curl_error($curl);//错误信息
        if ($error || $output == FALSE) {        //报错信息
            return 'ERROR ' . curl_error($curl);
        }
        curl_close($curl);
        return $output;
    }
    public function citys($lng,$lat){


        $url = "https://restapi.amap.com/v3/geocode/regeo?location={$lng},{$lat}&key=bbe66d87b7156cb11752d4c3bdd3308d";
        $result = json_decode($this->https_request($url));
//        dump($result->regeocode->addressComponent);
        $city=$result->regeocode->addressComponent;
        $area=Db::name('citys')->where('merger_name','like','%'.$city->province.'%'."%".$city->district."%")->find();
        $city=Db::name('citys')->where('id',$area['pid'])->find();
        $province=Db::name('citys')->where('id',$city['pid'])->find();

        return [
            'province'=>$province['id'],
            'city'=>$city['id'],
            'area'=>$area['id'],
        ];
    }
}
