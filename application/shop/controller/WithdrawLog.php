<?php
namespace app\shop\controller;

use think\Db;

//提现记录

class WithdrawLog extends Base
{
    public function index()
    {
        $shop_id      = session('shop_id');
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $list = db('shop_withdraw')
                ->where('shop_id',$shop_id);
            if (isset($this->get['status'])) {
                $list->where('status', $this->get['status']);
            }
            $count = $list->count();
            $list  = $list
                ->order('id', 'desc')
                ->page($this->get['page'])
                ->limit($this->get['limit'])
                ->select();
            foreach ($list as &$v) {
                /*$find_bank = Db::name('bank')
                        ->where('id',$v['bank_id'])
                        ->field('name')
                        ->find();
                if($find_bank){
                    $v['bank_name'] = $find_bank['name'];
                }else{
                    $v['bank_name'] = '无';
                }*/
                if($v['status'] == -1){
                    $v['status_name'] = '驳回提现';
                }else if($v['status'] == 0){
                    $v['status_name'] = '等待审核';
                }else if($v['status'] == 1){
                    $v['status_name'] = '批准提现';
                }
                $v['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
            }
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit']]);
        }
        return $this->fetch('');
    }
}
