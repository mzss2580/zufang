<?php
namespace app\shop\controller;

use think\Db;
use think\Request;

//提现记录

class Withdraw extends Base
{
    public function index()
    {
        $shop_id      = session('shop_id');
        //查询你的余额
        $find_money = Db::name('shop')
                ->where('id',$shop_id)
                //->where('is_del',0)
                ->field('withdraw_money')
                ->find();
        if (request()->isPost()) {

            $data = array();
            //$data['order_sn']= "SW".date('Ymdhis').rand(10000,99999);
            $data['shop_id']    = $shop_id;
            $data['alipay_name'] = trim(input('alipay_name'));
            $data['alipay_acc']  = trim(input('alipay_acc'));
            /*$data['name']       = trim(input('name'));
            $data['bank_id']    = trim(input('bank_id'));
            $data['bank_card']  = trim(input('bank_card'));
            $data['bank_address'] = trim(input('bank_address'));*/

            $data['money']      = round((float)trim(input('money')),2);

            if($data['money']<0){
                return array('status'=>0,'msg'=>'金额必须大于0');
            }

            if(!is_numeric($data['money'])){
                return array('status'=>0,'msg'=>'金额必须为数字');
            }

            $cha  = $find_money['withdraw_money'] - $data['money'];
            if($cha<0){
                return array('status'=>0,'msg'=>'余额不足');
            }
            $data['balance']      = $find_money['withdraw_money'];
            $data['create_time']  = time();
            $data['status_time']  = time();

            //减去余额
            $dec = Db::name('shop')
                ->where('id',$shop_id)
                //->where('is_del',0)
                ->setDec('withdraw_money',$data['money']);

            if($dec){
                //添加申请记录
                $withdraw_log = Db::name('shop_withdraw')->insert($data);
                return array('status'=>1,'msg'=>'申请提现成功，请等待审核');
            }else{
                return array('status'=>0,'msg'=>'申请失败');
            }
        }else{
            $this->assign('withdraw_money',$find_money['withdraw_money']);
            //银行信息
            /*$find_bank = Db::name('bank')
                    ->select();
            $this->assign('bank',$find_bank);*/
            return $this->fetch();
        }
    }
}
