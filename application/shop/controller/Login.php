<?php
namespace app\shop\controller;

class Login extends Base
{
    public function login()
    {
        if (!is_null($this->check_login())) {
            $this->redirect('index/index');
        }
        if ($this->post) {
            $user = model('user')->get(['phone' => $this->post['phone']]);
            if (is_null($user)) {
                $this->returnAPI('手机号未注册');
            }
            if ($user->pwd !== $this->post['pwd']) {
                $this->returnAPI('密码不正确');
            }
            $shop = model('shop')->get(['user_id' => $user->id]);
            if (is_null($shop)) {
                $this->returnAPI('未加盟商家');
            }
            if ($shop->status === 0) {
                $this->returnAPI('商家加盟审核中');
            }
            if ($shop->status === 2) {
                $this->returnAPI('商家加盟审核失败,请重新提交申请');
            }
            session('shop_id', $shop->id);
            if (isset($this->post['remember']) && $this->post['remember']) {
                cookie('shop_login', simple_encrypt($shop->id . '@@@' . $user->pwd), 30 * 86400);
            }
            $this->returnAPI('登录成功', 0);
        }
        return $this->fetch();
    }

    public function logout()
    {
        session('shop_id', null);
        cookie('shop_login', null);
        $this->returnAPI('退出登录成功', 0);
    }
}
