<?php
namespace app\shop\controller;

use think\Db;

class OrderGoods extends Base
{
    private $tn = 'order_goods';

    public function index($order_id)
    {
        $list = db($this->tn)->field(['id', 'specs', 'price', 'image', 'name', 'amount'])->where('order_id', $order_id)->select();
        return $this->fetch('', ['list' => $list]);
    }
    public function send($order_id)
    {
        $order = model('order')->get(['id' => $order_id, 'shop_id' => $this->shop->id]);
        if (is_null($order)) {
            $this->returnAPI('订单信息查询失败,请稍后重试');
        }
        if ($this->post) {
            $data = [
                'express_company' => $this->post['express_company'],
                'expressno'       => $this->post['expressno'],
                'send_time'       => time(),
                'status'          => 2,
            ];
            if (is_null($data['express_company']) || $data['express_company'] == '') {
                $this->returnAPI('请填写快递公司');
            }
            if (is_null($data['expressno']) || $data['expressno'] == '') {
                $this->returnAPI('请填写快递单号');
            }
            $result = $order->allowField(true)->save($data);
            if ($result === true) {
                $this->returnAPI('发货成功', 0);
            } else {
                $this->returnAPI('发货失败');
            }
        }
        return $this->fetch('');
    }
}
