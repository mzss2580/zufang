<?php
namespace app\shop\controller;

class GoodsImages extends Base
{
    private $tn = 'goods_images';

    public function index($goods_id)
    {
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $db    = db($this->tn)->where('goods_id', $goods_id)->where('shop_id', $this->shop->id);
            $count = $db->count();
            $list  = $db->field(true)->page($this->get['page'])->limit($this->get['limit'])->order('id', 'desc')->select();
            foreach ($list as $k => $v) {
                $list[$k]['image']       = '<img class="upload-image show-image" src="' . $v['image'] . '"/>';
                $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
            }
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit']]);
        }
        return $this->fetch();
    }

    public function edit($goods_id, $id = 0)
    {
        if ($this->post) {
            if ($id === 0) {
                $model = model($this->tn);
                $model->data(['goods_id' => $goods_id, 'shop_id' => $this->shop->id]);
            } else {
                $model = model($this->tn)->get(['id' => $id, 'shop_id' => $this->shop->id]);
                if (is_null($model)) {
                    $this->returnAPI('信息有误，请稍后重试');
                }
            }
            $validate = $this->validate(['id' => $id] + $this->post, $this->tn);
            if ($validate === true) {
                $result = $model->allowField(true)->save($this->post);
                if ($result === false) {
                    $this->returnAPI($model->getError());
                }
                $this->returnAPI('操作成功', 0);
            }
            $this->returnAPI($validate);
        }
        $model = model($this->tn)->get($id);
        if (is_null($model)) {
            $data = $this->table_fields($this->tn);
        } else {
            $data = $model->getData();
        }
        return $this->fetch('', ['data' => $data]);
    }
}
