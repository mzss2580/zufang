<?php
namespace app\shop\controller;

class Index extends Base
{
    public function index()
    {
        return $this->fetch('', [
            'name'     => $this->shop->name,
            'menulist' => [
                [
                    'id'    => 1,
                    'name'  => '商品管理',
                    'route' => '',
                    'icon'  => 'fa-server',
                    'sub'   => [
                        [
                            'id'    => 11,
                            'name'  => '规格设置',
                            'route' => '/shop/specs_type/index',
                            'icon'  => 'fa-cubes',
                        ],
                        [
                            'id'    => 12,
                            'name'  => '商品管理',
                            'route' => '/shop/goods/index',
                            'icon'  => 'fa-server',
                        ],
                    ],
                ],
                /*[
                    'id'    => 2,
                    'name'  => '优惠券管理',
                    'route' => '',
                    'icon'  => 'fa-server',
                    'sub'   => [
                        [
                            'id'    => 21,
                            'name'  => '优惠券',
                            'route' => '/shop/shop/index',
                            'icon'  => 'fa-cubes',
                        ],
                    ],
                ],*/
                [
                    'id'    => 3,
                    'name'  => '商户设置',
                    'route' => '',
                    'icon'  => 'fa-server',
                    'sub'   => [
                        [
                            'id'    => 31,
                            'name'  => '商户资料',
                            'route' => '/shop/shop/profile',
                            'icon'  => 'fa-cubes',
                        ],
                    ],
                ],
                [
                    'id'    => 4,
                    'name'  => '订单管理',
                    'route' => '',
                    'icon'  => 'fa-server',
                    'sub'   => [
                        [
                            'id'    => 41,
                            'name'  => '订单列表',
                            'route' => '/shop/order/index',
                            'icon'  => 'fa-cubes',
                        ],
                        [
                            'id'    => 42,
                            'name'  => '退货订单',
                            'route' => '/shop/order/refund',
                            'icon'  => 'fa-cubes',
                        ],
                    ],
                ],
                [
                    'id'    => 5,
                    'name'  => '余额管理',
                    'route' => '',
                    'icon'  => 'fa-server',
                    'sub'   => [
                        [
                            'id'    => 51,
                            'name'  => '余额提现',
                            'route' => '/shop/withdraw/index',
                            'icon'  => 'fa-cubes',
                        ],
                        [
                            'id'    => 52,
                            'name'  => '余额提现列表',
                            'route' => '/shop/withdraw_log/index',
                            'icon'  => 'fa-cubes',
                        ],
                        [
                            'id'    => 53,
                            'name'  => '钱包记录列表',
                            'route' => '/shop/money_log/index',
                            'icon'  => 'fa-cubes',
                        ],
                    ],
                ],
            ],
        ]);
    }
}
