<?php
namespace app\shop\controller;

use think\Db;

class GoodsSpecs extends Base
{
    private $tn = 'goods_specs';

    public function index($goods_id)
    {
        $list  = db($this->tn)->field(['id', 'specs', 'price', 'image', 'kucun', 'on_sale'])->where('goods_id', $goods_id)->select();
        $array = db('specs')->alias('s')->join('specs_type t', 's.type = t.id')->where('s.id', 'in', array_unique(explode(',', implode(',', array_column($list, 'specs')))))->column('s.name,t.name as type_name', 's.id');
        $first = [];
        foreach ($list as $k => $v) {
            if ($v['specs']) {
                $specs = explode(',', $v['specs']);
                foreach ($specs as $key => $value) {
                    if ($k === 0) {
                        array_push($first, $array[$value]['type_name']);
                    }
                    $specs[$key] = $array[$value]['name'];
                }
                $list[$k]['specs'] = $specs;
            }
            $list[$k]['price'] = $v['price'] == 0 ? '-' : $v['price'];
            $list[$k]['kucun'] = $v['kucun'] == 0 ? '-' : $v['kucun'];
        }
        return $this->fetch('', ['first' => $first, 'list' => $list]);
    }
    public function edit($id)
    {
        if ($this->post) {
            $model = model($this->tn)->get(['id' => $id, 'shop_id' => $this->shop->id]);
            if (is_null($model)) {
                $this->returnAPI('信息有误，请稍后重试');
            }
            $validate = $this->validate(['id' => $id] + $this->post, $this->tn);
            if ($validate === true) {
                $result = $model->allowField(true)->save($this->post);
                if ($result === false) {
                    $this->returnAPI($model->getError());
                }
                $this->returnAPI('操作成功', 0);
            }
            $this->returnAPI($validate);
        }
        $model = model($this->tn)->get($id);
        if (is_null($model)) {
            $data = $this->table_fields($this->tn);
        } else {
            $data = $model->getData();
        }
        return $this->fetch('', ['data' => $data]);
    }
    public function specs($goods_id)
    {
        if ($this->post) {
            foreach ($this->post['goods_specs'] as $k => $v) {
                $this->post['goods_specs'][$k]['shop_id'] = $this->shop->id;
            }
            db($this->tn)->where('goods_id', $goods_id)->delete();
            db($this->tn)->insertAll($this->post['goods_specs']);
            $this->returnAPI('', 0);
        }
        $specs_array = db('specs_type')->field(['id' => 'type', 'name'])->order(['sort' => 'desc', 'id' => 'desc'])->select();
        foreach ($specs_array as $k => $v) {
            $specs_array[$k]['specs'] = db('specs')->field(['id' => 'specs', 'name'])->where('type', $v['type'])->order(['sort' => 'desc', 'id' => 'desc'])->select();
        }
        return $this->fetch('', ['specs_array' => $specs_array]);
    }
}
