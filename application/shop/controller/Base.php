<?php
namespace app\shop\controller;

use app\common\controller\Ctrl;
use think\Db;

class Base extends Ctrl
{
    protected $get;
    protected $post;
    /**
     * 初始化参数
     */
    protected function initialize()
    {
        $this->get  = $this->request->get();
        $this->post = $this->request->post();
    }
    protected $beforeActionList = [
        'check_auth' => [
            'except' => 'login,logout',
        ],
    ];
    protected function check_login()
    {
        $shop_login = cookie('shop_login');
        if (!is_null($shop_login)) {
            $shop_login = explode('@@@', simple_decrypt($shop_login));
        }
        $shop_id = session('shop_id');
        if (is_null($shop_id) && !is_null($shop_login)) {
            $shop_id = $shop_login[0];
            session('shop_id', $shop_id);
        }
        $shop = null;
        if (!is_null($shop_id)) {
            $shop = model('shop')->get($shop_id);
            if (!is_null($shop) && $shop->user_id && $shop->status === 1) {
                $user = model('user')->get($shop->user_id);
                if (is_null($user) || (!is_null($shop_login) && $user->pwd !== $shop_login[1])) {
                    $shop = null;
                }
            }
        }
        return $shop;
    }
    protected function check_auth()
    {
        $shop = $this->check_login();
        if (is_null($shop)) {
            session('shop_id', null);
            cookie('shop_login', null);
            if (!empty($this->post) || (isset($this->get['page']) && isset($this->get['limit']))) {
                $this->returnAPI('登录信息有误,请重新登录', -10000);
            } else {
                $this->redirect('/shop/login/login');
            }
        }
        $this->shop = $shop;
    }
    protected function cancel_order($order, $time)
    {
        Db::startTrans();
        try {
            $order->save(['status' => 9, 'cancel_time' => $time]);
            if ($order->user_coupon_id > 0) {
                $user_coupon = model('user_coupon')->get($order->user_coupon_id);
                if (!is_null($user_coupon) && $user_coupon->status == 1) {
                    if ($user_coupon->end_time <= time()) {
                        $user_coupon->status = 2;
                    } else {
                        $user_coupon->status = 0;
                    }
                    $user_coupon->save(['used_time' => null]);
                }
            }
            $order_goods = db('order_goods')->field(['goods_id', 'specs_id', 'amount'])->where('order_id', $order->id)->select();
            foreach ($order_goods as $v) {
                if ($v['specs_id'] === '') {
                    db('goods')->where('id', $v['goods_id'])->setInc('kucun', $v['amount']);
                } else {
                    db('goods_specs')->where('goods_id', $v['goods_id'])->where('specs', $v['specs_id'])->setInc('kucun', $v['amount']);
                }
            }
            // 提交事务
            Db::commit();
            $result = true;
        } catch (\Exception $e) {
            trace('取消订单失败:' . $e->getMessage());
            // 回滚事务
            Db::rollback();
            $result = false;
        }
        return $result;
    }
}
