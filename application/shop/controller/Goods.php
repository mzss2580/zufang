<?php
namespace app\shop\controller;

use think\Db;

class Goods extends Base
{
    private $tn = 'goods';

    public function index()
    {
        $category = db('goods_category')->order(['sort' => 'desc', 'id' => 'desc'])->column('name', 'id');
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $db    = db($this->tn)->where('shop_id', $this->shop->id);
            $count = $db->count();
            $list  = $db->field(true)->page($this->get['page'])->limit($this->get['limit'])->order('id', 'desc')->select();
            foreach ($list as $k => $v) {
                $list[$k]['specs']       = $this->parse_specs(get_field('goods_specs', ['goods_id' => $v['id']], 'specs'), true);
                $list[$k]['category']    = $category[$v['category']];
                $list[$k]['image']       = '<img class="upload-image show-image" src="' . $v['image'] . '"/>';
                $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
            }
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit']]);
        }
        return $this->fetch('', ['category' => ['' => '商品分类'] + $category]);
    }

    public function edit($id = 0)
    {
        if ($this->post) {
            if ($id === 0) {
                $model = model($this->tn);
                $model->data(['shop_id' => $this->shop->id,'on_sale'=>0]);
            } else {
                $model = model($this->tn)->get(['id' => $id, 'shop_id' => $this->shop->id]);
                if (is_null($model)) {
                    $this->returnAPI('信息有误，请稍后重试');
                }
            }
            $validate = $this->validate(['id' => $id] + $this->post, $this->tn);
            if ($validate === true) {
                $result = $model->allowField(true)->save($this->post);
                if ($result === false) {
                    $this->returnAPI($model->getError());
                }
                $this->returnAPI('操作成功', 0);
            }
            $this->returnAPI($validate);
        }
        $model = model($this->tn)->get($id);
        if (is_null($model)) {
            $data = $this->table_fields($this->tn);
        } else {
            $data = $model->getData();
        }
        return $this->fetch('', ['data' => $data, 'category' => db('goods_category')->order(['sort' => 'desc', 'id' => 'desc'])->column('name', 'id')]);
    }
    public function comment($goods_id)
    {
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $db = db('goods_comment')->alias('c')->join('user u', 'c.user_id = u.id')
                ->field(['u.head', 'u.nick', 'c.id', 'c.score', 'c.content', 'c.images', 'c.create_time'])
                ->where('c.score', '>', 0)->where('c.goods_id', $goods_id)->where('c.comment_del', 0);
            if (isset($this->get['score']) && $this->get['score'] > 0) {
                $score = $this->get['score'];
                if ($score == 1) {
                    $db->where('c.score', 'in', [4, 5]);
                } else if ($score == 2) {
                    $db->where('c.score', 'in', [2, 3]);
                } else if ($score == 3) {
                    $db->where('c.score', 1);
                }
            }
            $count = $db->count();
            $list  = $db->order('create_time', 'desc')->page($this->get['page'])->limit($this->get['limit'])->select();
            foreach ($list as $k => $v) {
                $images = json_decode($v['images'], true);
                foreach ($images as $key => $value) {
                    $images[$key] = '<img class="upload-image show-image" src="' . $value . '"/>';
                }
                $list[$k]['images']      = $images;
                $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
            }
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit']]);
        }
        return $this->fetch();
    }
}
