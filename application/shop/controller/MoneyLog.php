<?php
namespace app\shop\controller;

use think\Db;

class MoneyLog extends Base
{
    public function index()
    {
        $shop_id      = session('shop_id');
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $list = db('shop_wallet')
                ->where('shop_id',$shop_id);
            if (isset($this->get['type'])) {
                $list->where('type', $this->get['type']);
            }
            $count = $list->count();
            $list  = $list
                ->order('create_time', 'desc')
                ->page($this->get['page'])
                ->limit($this->get['limit'])
                ->select();
            foreach ($list as &$v) {

                if($v['order_id']){
                    //订单信息
                    $find_order = db('order')
                        ->where('id', $v['order_id'])
                        ->field('id,orderno,pay_true,refund_amount')
                        ->find();
                    if($find_order){
                        $v['orderno']       = $find_order['orderno'];
                        $v['order_pay']     = $find_order['pay_true'];     //订单实付金额
                        $v['refund_amount'] = $find_order['refund_amount']; //订单退款金额
                    }else{
                        $v['orderno'] = '';
                        $v['order_pay']     = '';     //订单实付金额
                        $v['refund_amount'] = ''; //订单退款金额
                    }
                }else{
                    $v['orderno'] = '';
                }

                /*if($v['order_refund_id']){
                    //订单信息
                    $find_order = db('order')
                        ->where('id', $v['order_refund_id'])
                        ->field('id,orderno')
                        ->find();
                    if($find_order){
                        $v['refund_orderno'] = $find_order['orderno'];
                    }else{
                        $v['refund_orderno'] = '';
                    }
                }else{
                    $v['refund_orderno'] = '';
                }*/

                if ($v['type'] == 1) {

                    $v['type_name'] = '加额';

                } else {

                    $v['type_name'] = '减额';
                }

                /*if ($v['status'] == 1) {

                    $v['status_name'] = '已结算';

                } else {

                    $v['status_name'] = '未结算';
                }

                if ($v['freeze'] == 1) {

                    $v['freeze_name'] = '已结算';

                } else {

                    $v['freeze_name'] = '未结算';
                }*/

                $v['create_time'] = date('Y-m-d H:i:s', $v['create_time']);

            }
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit']]);
        }
        return $this->fetch('');
    }
}
