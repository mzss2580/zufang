<?php
namespace app\shop\controller;

class Shop extends Base
{
    private $tn = 'coupon';
/*
优惠券
 */
    public function index()
    {
        if (isset($this->get['page']) && isset($this->get['limit'])) {
            $db    = db($this->tn)->alias('c')->leftJoin('shop s', 's.id=c.shop_id')->where('c.shop_id', $this->shop->id);
            $count = $db->count();
            $list  = $db->field(['c.*'])->page($this->get['page'])->limit($this->get['limit'])->order('c.id', 'desc')->select();
            foreach ($list as $k => $v) {
                $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
                $list[$k]['start_time']  = date('Y-m-d H:i:s', $v['start_time']);
                $list[$k]['end_time']    = date('Y-m-d H:i:s', $v['end_time']);
            }
            $this->returnAPI('', 0, $list, ['count' => $count, 'limit' => $this->get['limit']]);
        }
        return $this->fetch();
    }

    public function edit($id = 0)
    {
        if ($this->post) {
            if ($id === 0) {
                $this->post['couponno'] = $this->new_invite_code();
                $this->post['shop_id']  = $this->shop->id;
                $model                  = model($this->tn);
            } else {
                $model = model($this->tn)->get($id);
                if (is_null($model)) {
                    $this->returnAPI('信息有误，请稍后重试');
                }
            }
            $validate = $this->validate(['id' => $id] + $this->post, $this->tn);
            if ($validate === true) {
                $this->post['start_time'] = strtotime($this->post['start_time']);
                $this->post['end_time']   = strtotime($this->post['end_time']);
                if ($this->post['end_time'] <= $this->post['start_time']) {
                    $this->returnAPI('失效时间需大于生效时间');
                }

                if ($this->post['money_max'] <= $this->post['money']) {
                    $this->returnAPI('满减金额需大于优惠金额');
                }
                $result = $model->allowField(true)->save($this->post);
                if ($result === false) {
                    $this->returnAPI($model->getError());
                }
                $this->returnAPI('操作成功', 0);
            }
            $this->returnAPI($validate);
        }
        $model = model($this->tn)->get($id);
        if (is_null($model)) {
            $data = $this->table_fields($this->tn);
        } else {
            $data                = $model->getData();
            $data['create_time'] = date('Y-m-d H:i:s', $data['create_time']);
            $data['start_time']  = date('Y-m-d H:i:s', $data['start_time']);
            $data['end_time']    = date('Y-m-d H:i:s', $data['end_time']);
        }
        return $this->fetch('', ['data' => $data]);
    }
    /*
    商户资料
     */
    public function profile()
    {
        $shop = model('shop')->get(['id' => $this->shop->id]);
        if ($this->post) {
            $this->post['ziti'] = input('post.ziti', 0);
            $this->post['fuwu'] = input('post.fuwu', 0);
            $validate           = $this->validate(['id' => $this->shop->id] + $this->post, 'Shop.add');
            if ($validate === true) {
                $result = $shop->allowField(true)->save($this->post);
                if ($result === false) {
                    $this->returnAPI($model->getError());
                }
                $this->returnAPI('操作成功', 0);
            }
            $this->returnAPI($validate);
        }
        return $this->fetch('', ['data' => $shop]);
    }
    protected function new_invite_code()
    {
        $invite_code = substr(strtoupper(md5(uniqid(microtime(true), true))), 6, 6);
        if (!is_null(db('coupon')->where('couponno', $invite_code)->find())) {
            return $this->new_invite_code();
        }
        return $invite_code;
    }
}
