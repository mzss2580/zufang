基于  
ThinkPHP 5.1.* [ThinkPHP文档](https://www.kancloud.cn/manual/thinkphp5_1/353946)  
Layui 2.2.6 [Layui文档](https://www.layui.com/doc/)  
开发的后台模板  

 **开始开发之前设置config\app.php app_debug 项为true**   
 **开发完毕之后设置config\app.php app_debug 项为false**   

## 代码生成
1. 生成模块  
后台模块管理添加或者删除模块,路径与前缀区分管理,根据模块名生成目录结构  
```
├─application
│  ├─(route)           模块目录
│  │  ├─controller      控制器目录
│  │  │  ├─Base.php     基础控制器(后台模块包含后台权限控制部分)
│  │  ├─view            页面目录
│  │  │  ├─layout.html  模板布局(后台模块引入common模块layout/header.html和layout/footer.html)

在后续生成代码中会在common模块下以下目录生成相关内容
│  │  ├─model           模型目录
│  │  ├─validate        验证器目录

后台模块额外生成目录
├─public
│  ├─static
│  │  ├─module
│  │  │  ├─(route)     模块静态文件目录
```
2. 生成代码  
根据模块选择对应前缀表  
![输入图片说明](https://images.gitee.com/uploads/images/2018/1013/144204_4b85373d_1780445.png)  
![输入图片说明](https://images.gitee.com/uploads/images/2018/1013/144216_82ae18af_1780445.png)  
生成页面类似  
![输入图片说明](https://images.gitee.com/uploads/images/2018/1013/144259_fb13cd96_1780445.png)  
![输入图片说明](https://images.gitee.com/uploads/images/2018/1013/144309_e5f31de4_1780445.png)  
根据不是null勾选前后台添加required判断,并['int', 'decimal', 'float', 'double']类型添加数字判断  
若有create_time,update_time字段自动model类添加属性自动完成
```
    protected $insert = ['create_time'];

    protected function setCreateTimeAttr()
    {
        return time();
    }

    protected $update = ['update_time'];

    protected function setUpdateTimeAttr()
    {
        return time();
    }
```
编辑页面页面标签一般为
```
<input autocomplete="off" class="layui-input" lay-verify="required" name="name" type="text" value="{$data['name']}"/>
```
数据类型若为varchar且长度为233自动设置图片选择
```
<div class="layui-form-item">
    <label class="layui-form-label">
        图片
    </label>
    <div class="layui-input-block">
        <button class="layui-btn layui-btn-sm upload-image" type="button">
            <i class="fa fa-image">
            </i>
            图片
        </button>
        <input lay-verify="uploadimg" name="image" type="hidden" value="{$data['image']}"/>
        <div class="upload-image">
            <span>
            </span>
            <img class="upload-image" src="{$data['image']}"/>
        </div>
    </div>
</div>
```
select内容自行绑定数据并设置页面标签类似
```
<select lay-verify="required" name="module">
    {foreach $modules as k=>$v}
    {if $k == $data['module']}
    <option selected="" value="{$k}">
        {$v}
    </option>
    {else}
    <option value="{$k}">
        {$v}
    </option>
    {/if}
    {/foreach}
</select>
```
3. 代码生成之后添加菜单类似  
![输入图片说明](https://images.gitee.com/uploads/images/2018/1013/144439_334bf6b3_1780445.png)  

## 权限管理
1. 菜单管理
2. 用户角色  
用户根据角色分组,角色确定用户后台访问权限  
3. 禁用  
菜单、角色、用户均可禁用  

## 扩展
```
public\static\js\tool.js  
- show_big_image            layui.layer弹出图片层
- layer_iframe_close        layui.layer iframe层关闭自身
- layer_iframe              layui.layer弹出iframe层
- _ajax_update              ajax更新数据库内容
- _ajax_remove              ajax删除数据库内容
- _ajax_upload              ajax上传文件
- _ajax                     ajax简化方法
- stop_propagation          阻止冒泡事件  
public\static\plugin  
- font_awesome              字体图标
- layer_mobile              layer手机版
- layui                     layui2.2.6
extend(example.php/js)  
- alipay\Alipay             支付宝支付:APP支付、电脑网页支付、手机网页支付
- alisms\Alisms             阿里大鱼短信发送
- phpexcel\PHPExcel         excel(xlsx)导出下载
- region                    国内行政区划
- wxpay                     微信支付示例:APP支付、扫码支付、H5支付、公众号/小程序支付
```
