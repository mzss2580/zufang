<?php
return [

    //用户充值
    'appid'   => 'wxff323aae2a6bc752',
    'mch_id'  =>'1666826706',
    'api_key' =>'81Bd1fddfbb02ab287be0f7e27e1B4dC',

    //之前商城证书
    'debug'        => true,
    'app_id'       => 'wxff323aae2a6bc752',
    'secret'       => '4b9b10dc988b4f6a785699cc36fa8cb4',
    'token'        => 'Token',
    'aes_key'      => 'EncodingAESKey，安全模式、兼容模式必填',
    'mini_program' => [
        'app_id'  => 'AppID',
        'secret'  => 'AppSecret',
        'token'   => 'Token',
        'aes_key' => 'EncodingAESKey，安全模式、兼容模式必填',
    ],
    'payment'      => [
        'merchant_id' => '1666826706',
        'key'         => '81Bd1fddfbb02ab287be0f7e27e1B4dC',
        'cert_path'   => env('ROOT_PATH') . 'public' . DIRECTORY_SEPARATOR . 'cert' . DIRECTORY_SEPARATOR . 'apiclient_cert.pem',
        'key_path'    => env('ROOT_PATH') . 'public' . DIRECTORY_SEPARATOR . 'cert' . DIRECTORY_SEPARATOR . 'apiclient_key.pem',
    ],
    'oauth'        => [
        'scopes'   => ['snsapi_userinfo'],
        'callback' => '/weixin/oauth/callback',
    ],
    'log'          => [
        'level'      => 'debug',
        'permission' => 0777,
        'file'       => env('ROOT_PATH') . 'runtime' . DIRECTORY_SEPARATOR . 'easywechat' . DIRECTORY_SEPARATOR . 'easywechat_' . date('Ymd') . '.log',
    ],
    'guzzle'       => [
        'timeout' => 3,
    ],
];
