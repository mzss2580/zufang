<?php
return [
    'order_timeout' => 60 * 15,
    'order_finish'  => 86400 * 7,
    'order_refund'  => 86400 * 15,
];
