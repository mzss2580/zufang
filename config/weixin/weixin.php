<?php
return [
    'debug'        => true,
    'app_id'       => 'wxff323aae2a6bc752',
    'secret'       => '4b9b10dc988b4f6a785699cc36fa8cb4',
    'token'        => 'Token',
    'aes_key'      => 'EncodingAESKey，安全模式、兼容模式必填',
    'mini_program' => [
        'app_id'  => 'AppID',
        'secret'  => 'AppSecret',
        'token'   => 'Token',
        'aes_key' => 'EncodingAESKey，安全模式、兼容模式必填',
    ],
    'payment'      => [
        'merchant_id' => '商户号',
        'key'         => 'API密钥',
        'cert_path'   => env('ROOT_PATH') . 'public' . DIRECTORY_SEPARATOR . 'cert' . DIRECTORY_SEPARATOR . 'apiclient_cert.pem',
        'key_path'    => env('ROOT_PATH') . 'public' . DIRECTORY_SEPARATOR . 'cert' . DIRECTORY_SEPARATOR . 'apiclient_key.pem',
    ],
    'oauth'        => [
        'scopes'   => ['snsapi_userinfo'],
        'callback' => '/weixin/kanjia/callback',
    ],
    'log'          => [
        'level'      => 'debug',
        'permission' => 0777,
        'file'       => env('ROOT_PATH') . 'runtime' . DIRECTORY_SEPARATOR . 'easywechat' . DIRECTORY_SEPARATOR . 'easywechat_' . date('Ymd') . '.log',
    ],
    'guzzle'       => [
        'timeout' => 3,
    ],
];
