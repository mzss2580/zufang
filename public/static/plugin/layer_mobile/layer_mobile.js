/**
 * layer_mobile prompt层
 * @param  {string}     title 提示信息
 * @param  {function}   yes   确定回调,当输入信息不为空且不等于默认值时回调
 * @param  {string}     value 默认值,默认''
 */
function layer_prompt(title, yes, value) {
    if (typeof value === 'undefined') {
        value = '';
    }
    layer.open({
        content: '<input id="layer_prompt" type="text" value="' + value + '" style="width: 80%; margin: 0; padding: 0 5px;"/>',
        btn: ['确定', '取消'],
        title: title,
        yes: function(index) {
            var text = $('#layer_prompt').val().trim();
            if (text !== '' && text !== value) {
                layer.close(index);
                yes(text);
            }
        }
    });
}
/**
 * layer_mobile加载层
 * @param  {string}     content    加载层显示内容,默认''
 * @param  {boolean}    shadeClose 遮罩层点击关闭,默认false
 * @return {index}                 layer该层索引
 */
function layer_load(content, shadeClose) {
    if (typeof shadeClose === 'undefined') {
        shadeClose = false;
        if (typeof content === 'undefined') {
            content = '';
        }
    }
    return layer.open({
        type: 2,
        content: content,
        shadeClose: shadeClose
    });
}
/**
 * layer_mobile提示层
 * @param  {string}  content 显示内容,必传
 * @param  {string}  skin    弹层显示风格,'msg'|'footer',默认:'msg'
 * @param  {integer} time    显示时间,单位:秒,默认:1
 * @return {index}           layer该层索引
 */
function layer_msg(content, time, skin) {
    if (typeof content === 'undefined') {
        return false;
    }
    if (typeof skin === 'undefined') {
        skin = 'msg';
        if (typeof time === 'undefined') {
            time = 1;
        } else if (typeof time === 'string') {
            skin = time;
            time = 1;
        }
    }
    return layer.open({
        content: content,
        skin: skin,
        time: time
    });
}
/**
 * layer_mobile信息层
 * @param  {string}   content 显示内容,必传
 * @param  {function} yes     确定点击回调,默认关闭当前弹层
 * @param  {string}   btn     确定按钮内容,默认'确定'
 * @return {index}            layer该层索引
 */
function layer_alert(content, yes, btn) {
    if (typeof content === 'undefined') {
        return false;
    }
    if (typeof btn === 'undefined') {
        btn = '确定';
        if (typeof yes === 'undefined') {
            yes = function(index) {
                layer.close(index);
            }
        } else if (typeof yes === 'string') {
            yes = function(index) {
                layer.close(index);
            }
            btn = yes;
        }
    }
    return layer.open({
        content: content,
        btn: btn,
        yes: yes
    });
}
/**
 * layer_mobile确认层
 * @param  {string}     content 显示内容,必传
 * @param  {function}   yes     确定点击回调
 * @param  {string}     skin    弹层显示风格,'msg'|'footer',默认:'footer'
 * @param  {array}      btn     按钮组,默认['确定','取消']
 * @return {index}              layer该层索引
 */
function layer_cfm(content, yes, btn, skin) {
    if (typeof content === 'undefined') {
        return false;
    }
    if (typeof yes === 'undefined') {
        yes = function(index) {
            layer.close(index);
        }
    }
    if (typeof btn === 'undefined') {
        btn = ['确定', '取消'];
    } else if (typeof btn === 'string') {
        btn = [btn, '取消'];
    }
    var obj = {
        content: content,
        btn: btn,
        yes: yes
    };
    if (skin === 'footer') {
        obj.skin = skin;
    }
    return layer.open(obj);
}
