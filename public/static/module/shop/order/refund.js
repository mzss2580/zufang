$(function() {
    laydate.render({
        elem: '#start'
    });
    laydate.render({
        elem: '#end'
    });
    $('.layui-plus').click(function() {
        layer_iframe('添加', 'edit');
    });
    layform.on('submit(reasonform)', function(data) {
        _ajax({
            url: '/shop/order/refuse_refund',
            data: data.field,
            success: function(res) {
                if (res.code) {
                    layer.msg(res.msg, {
                        icon: 2
                    });
                } else {
                    layer.msg(res.msg, {
                        icon: 1
                    });
                    layer_iframe_close(333, true);
                }
            },
            error: function() {
                layer.msg('拒绝失败,请稍后重试', {
                    icon: 0
                });
            }
        });
        return false;
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            window.options.page.curr = curr;
            if (!res.data && curr > 1) {
                window.options.page = {
                    curr: curr - 1
                }
                window.tableIns.reload(window.options);
            }
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
                field: 'orderno',
                title: '订单号',
                unresize: true,
                align: 'center'
            }, {
                field: 'refund_type_desc',
                title: '退款方式',
                unresize: true,
                align: 'center'
            }, {
                field: 'refund_money',
                title: '退款金额',
                unresize: true,
                align: 'center'
            }, {
                field: 'status_desc',
                title: '退款状态',
                unresize: true,
                align: 'center'
            }, {
                field: 'goods_state_desc',
                title: '货物状态',
                unresize: true,
                align: 'center'
            }, {
                field: 'create_time',
                title: '申请时间',
                unresize: true,
                align: 'center',
                width: 160
            }, {
                title: '协商历史',
                unresize: true,
                align: 'center',
                toolbar: '#refund_history'
            }, {
                title: '操作',
                unresize: true,
                align: 'center',
                toolbar: '#toolbar_normal',
                width: 220
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event == 'refund_info') {
            layer_iframe('订单详情', 'refund_info?id=' + obj.data.id);
        } else if (obj.event == 'agree') {
            layer.confirm('确认同意退款吗？', function(index) {
                _ajax({
                    url: '/shop/order/agree_refund',
                    data: {
                        id: obj.data.id,
                    },
                    success: function(res) {
                        layer.close(index);
                        if (typeof success === 'function') {
                            success(res);
                        } else {
                            if (res.code) {
                                layer.msg(res.msg, {
                                    icon: 2
                                });
                            } else {
                                layer.msg(res.msg, {
                                    icon: 1
                                });
                                setTimeout(function() {
                                    window.tableIns.reload(window.options);
                                }, 333);
                            }
                        }
                    },
                    error: function() {
                        layer.close(index);
                    }
                });
            });
        } else if (obj.event == 'refuse') {
            layer_iframe('拒绝申请', '/shop/order/refuse_reason?id=' + obj.data.id);
        } else if (obj.event == 'address') {
            layer_iframe('选择地址', '/shop/order/address?id=' + obj.data.id);
        } else if (obj.event == 'history') {
            layer_iframe('协商历史', '/shop/order/refund_history?id=' + obj.data.id);
        } else if (obj.event == 'over') {
            layer.confirm('确定确认收货吗？', function(index) {
                _ajax({
                    url: '/shop/order/returnd_over',
                    data: {
                        id: obj.data.id,
                    },
                    success: function(res) {
                        layer.close(index);
                        if (typeof success === 'function') {
                            success(res);
                        } else {
                            if (res.code) {
                                layer.msg(res.msg, {
                                    icon: 2
                                });
                            } else {
                                layer.msg(res.msg, {
                                    icon: 1
                                });
                                setTimeout(function() {
                                    window.tableIns.reload(window.options);
                                }, 333);
                            }
                        }
                    },
                    error: function() {
                        layer.close(index);
                    }
                });
            });
        }
    });
});