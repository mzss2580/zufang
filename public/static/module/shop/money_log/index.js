$(function() {
    laydate.render({
        elem: '#start'
    });
    laydate.render({
        elem: '#end'
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            window.options.page.curr = curr;
            if (!res.data && curr > 1) {
                window.options.page = {
                    curr: curr - 1
                }
                window.tableIns.reload(window.options);
            }
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
                field: 'id',
                title: 'ID',
                unresize: true,
                align: 'center',
                width: 80
            }, {
                field: 'type_name',
                title: '类型',
                unresize: true,
                align: 'center',
                width: 100
            }, {
                field: 'money',
                title: '余额变化',
                unresize: true,
                align: 'center',
                width: 100
            }/*, {
                field: 'balance',
                title: '余额',
                unresize: true,
                align: 'center',
                width: 100
            }*/, {
                field: 'orderno',
                title: '订单号',
                unresize: true,
                align: 'center',
                width: 180
            }, {
                field: 'order_pay',
                title: '订单金额',
                unresize: true,
                align: 'center',
                width: 180
            }, {
                field: 'refund_amount',
                title: '退款金额',
                unresize: true,
                align: 'center',
                width: 180
            }, {
                field: 'platform_profit',
                title: '平台抽成',
                unresize: true,
                align: 'center',
                width: 100
            }, {
                field: 'create_time',
                title: '创建时间',
                unresize: true,
                align: 'center',
                width: 180
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
});