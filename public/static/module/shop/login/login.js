$(function() {
    if (self !== top) {
        top.location.href = location.href;
    }
    $('.layui-btn-sm,.layui-form-item img').click(function() {
        refresh_captcha();
    });
    layform.on('radio(remember)', function(data) {
        if (data.value === '0') {
            $('#remember').val('1');
            $('#remember').prop('checked', true);
        } else {
            $('#remember').val('0');
            $('#remember').prop('checked', false);
        }
        layform.render('radio');
    });
    layform.on('submit(loginform)', function(data) {
        data.field.pwd = $.md5(data.field.pwd);
        _ajax({
            data: data.field,
            success: function(res) {
                if (res.code) {
                    layer.msg(res.msg, {
                        icon: 2,
                        offset: 0
                    });
                    refresh_captcha();
                } else {
                    layer.msg(res.msg, {
                        icon: 1,
                        offset: 0
                    });
                    setTimeout(function() {
                        location.href = '/shop/index/index';
                    }, 333)
                }
            },
            error: function() {
                layer.msg('系统繁忙,请稍后重试', {
                    icon: 0,
                    offset: 0
                });
                refresh_captcha();
            }
        });
        return false;
    });

    function refresh_captcha() {
        $('img.captcha').attr('src', '/captcha.html');
    }
});
