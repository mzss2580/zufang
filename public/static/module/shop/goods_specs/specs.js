$(function() {
    var index, data_value, prop_checked, upload_image,
        specs_type_checked = new Array(),
        goods_id = location.search.replace('?goods_id=', ''),
        init_specs_table = function() {
            var specs_type = $(':checkbox.specs_type:checked');
            $('#specs_table thead').empty();
            $('#specs_table tbody').empty();
            if (specs_type.length === 0) {
                $('#specs_table').hide().next().hide();
            } else {
                var tbody = new Array();
                if (specs_type.length === 1) {
                    $.each(specs_type.parent().next().find(':checkbox.specs:checked'), function(i, item) {
                        tbody.push([
                            [$(item).attr('data-specs'), $(item).attr('title')]
                        ]);
                    });
                } else {
                    $.each($(specs_type[0]).parent().next().find(':checkbox.specs:checked'), function(i0, item0) {
                        $.each($(specs_type[1]).parent().next().find(':checkbox.specs:checked'), function(i1, item1) {
                            tbody.push([
                                [
                                    $(item0).attr('data-specs'),
                                    $(item0).attr('title')
                                ],
                                [
                                    $(item1).attr('data-specs'),
                                    $(item1).attr('title')
                                ]
                            ]);
                        });
                    });
                }
                laytpl($('#specs_type_tr').html()).render(specs_type.map(function(i, item) {
                    return $(item).attr('title');
                }).get(), function(thead_html) {
                    $('#specs_table thead').append(thead_html);
                });
                laytpl($('#specs_tr').html()).render(tbody, function(tbody_html) {
                    $('#specs_table tbody').append(tbody_html);
                });
                $('#specs_table').show().next().show();
            }
        };
    layform.render('checkbox');
    layform.on('checkbox(specs_type)', function(data) {
        index = $.inArray($(data.elem).attr('data-specs-type'), specs_type_checked);
        data_value = '0';
        prop_checked = false;
        if (data.value === '0') {
            if (index === -1) {
                if (specs_type_checked.length === 2) {
                    layer.msg('最多选择两种规格类型', {
                        icon: 0
                    });
                } else {
                    specs_type_checked.push($(data.elem).attr('data-specs-type'));
                    data_value = '1';
                    prop_checked = true;
                }
            }
        } else {
            if (index !== -1) {
                specs_type_checked.splice(index, 1);
            }
        }
        $(data.elem).prop('checked', prop_checked).val(data_value).parent().next().find(':checkbox.specs').prop('checked', prop_checked).val(data_value);
        layform.render('checkbox');
        init_specs_table();
    });
    layform.on('checkbox(specs)', function(data) {
        index = $.inArray($(data.elem).attr('data-specs-type'), specs_type_checked);
        if (data.value === '1') {
            $(data.elem).prop('checked', false).val('0');
            if ($(data.elem).siblings(':checkbox.specs:checked').length === 0) {
                specs_type_checked.splice(index, 1);
                $(data.elem).parent().prev().find(':checkbox.specs_type').prop('checked', false).val('0');
            }
        } else {
            data_value = '1';
            prop_checked = true;
            if (index === -1) {
                if (specs_type_checked.length === 2) {
                    data_value = '0';
                    prop_checked = false;
                    layer.msg('最多选择两种规格类型', {
                        icon: 0
                    });
                } else {
                    specs_type_checked.push($(data.elem).attr('data-specs-type'));
                }
            }
            $(data.elem).prop('checked', prop_checked).val(data_value).parent().prev().find(':checkbox.specs_type').prop('checked', prop_checked).val(data_value);
        }
        layform.render('checkbox');
        init_specs_table();
    });
    $('#specs_table').on('click', '.remove-specs', function() {
        $(this).parent().parent().remove();
    });
    $('#specs_table').on('click', 'button.upload-image', function() {
        upload_image = $(this);
        $('#upload_form input').click();
    });
    $('#specs_table').on('click', 'div.upload-image span', function() {
        $(this).parent().next().next().val('');
        $(this).parent().remove();
    });
    $('#upload_form input').change(function() {
        var _this = $(this);
        var data = new FormData();
        data.append('file', _this[0].files[0]);
        data.append('domain', 0);
        _ajax({
            contentType: false,
            data: data,
            processData: false,
            url: '/ajax/index/upload',
            success: function(res) {
                if (res.code) {
                    layer.msg(res.msg, {
                        icon: 0
                    });
                } else {
                    if (!res.data.thumb) {
                        res.data.thumb = res.data.src;
                    }
                    if (upload_image.next().val() === '') {
                        upload_image.before('<div class="upload-image"><span></span><img class="upload-image" src="' + res.data.thumb + '"/></div>');
                    } else {
                        upload_image.prev().find('img').attr('src', res.data.thumb);
                    }
                    upload_image.next().val(res.data.src);
                }
            }
        });
        _this.val('');
    });
    $('.save-goods-spce').click(function() {
        if (specs_type_checked.length === 0) {
            layer.msg('最少选择一种规格类型', {
                icon: 0
            });
        } else {
            var goods_specs = new Array(),
                error_str = '';
            $('#specs_table tbody tr').each(function(i, item) {
                var tds = $(item).find('td'),
                    specs = tds.eq(0).attr('data-specs'),
                    price = tds.eq(-4).find('input').val(),
                    image = tds.eq(-3).find('input').val(),
                    kucun = tds.eq(-2).find('input').val();
                if (specs_type_checked.length === 2) {
                    specs += ',' + tds.eq(1).attr('data-specs');
                }
                if (isNaN(price)) {
                    error_str += '价格只能填写数字<br/>';
                }
                if (kucun === '') {
                    error_str += '请填写库存<br/>';
                } else if (isNaN(kucun)) {
                    error_str += '库存只能填写整数<br/>';
                } else if (kucun % 1 !== 0) {
                    error_str += '库存只能填写整数<br/>';
                }
                goods_specs.push({
                    goods_id: goods_id,
                    specs: specs,
                    price: price == '' ? 0 : price,
                    image: image,
                    kucun: kucun,
                });
            });
            if (error_str === '') {
                layer.confirm('价格、图片默认为商品价格、图片<br/>之前存在规格则会先清空<br/>确认生成商品规格吗?', function(index) {
                    layer.close(index);
                    _ajax({
                        data: {
                            goods_specs: goods_specs
                        },
                        success: function(res) {
                            layer.msg('操作成功', {
                                icon: 1
                            });
                            layer_iframe_close(333, true);
                        },
                        error: function() {
                            layer.close(index);
                        }
                    });
                });
            } else {
                layer.alert(error_str);
            }
        }
    });
});
