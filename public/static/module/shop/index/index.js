$(function() {
    if (self !== top) {
        top.location.href = location.href;
    }
    var lay_id, tab_array = new Array();
    $(document).click(function() {
        tab_tool_hide();
    });
    $('.layui-layout-left .layui-nav-item').eq(0).addClass('layui-this');
    $('.tab-tool').click(function(e) {
        stopPropagation(e);
        $('.tab-tool-body').toggle();
        $(this).find('i').toggleClass('fa-rotate-180');
    });
    $('.tab-tool-body li').click(function(e) {
        stopPropagation(e);
        if ($(this).index() == 0) {
            $('.layui-tab-content .layui-show iframe').attr('src', $('.layui-tab-content .layui-show iframe').attr('src'));
        } else if ($(this).index() == 2) {
            tab_delete($('.layui-tab-title .layui-this').attr('lay-id'));
        } else if ($(this).index() == 4) {
            $('.layui-tab-title li').not('.layui-this').each(function() {
                tab_delete($(this).attr('lay-id'));
            });
        } else if ($(this).index() == 6) {
            $('.layui-tab-title li').each(function() {
                tab_delete($(this).attr('lay-id'));
            });
        }
        tab_tool_hide();
    });
    layelem.on('nav(layui_nav_left)', function(elem) {
        $('.layui-nav-tree').hide();
        $('.layui-nav-tree').eq(elem.parent().index()).show();
    });
    layelem.on('nav(layui_nav_right)', function(elem) {
        if ($(elem).parent().hasClass('layui-this')) {
            layer.confirm('确定退出登录吗?', function(index) {
                layer.close(index);
                _ajax({
                    url: '/shop/login/logout',
                    success: function(res) {
                        if (!res.code) {
                            layer.msg(res.msg, {
                                icon: 1
                            });
                            setTimeout(function() {
                                location.href = '/shop/login/login';
                            }, 333)
                        }
                    }
                });
            });
        }
    });
    layelem.on('nav(layui_nav_menu)', function(elem) {
        lay_id = elem.attr('lay-id');
        if (lay_id) {
            if ($.inArray(lay_id, tab_array) === -1) {
                tab_array.push(lay_id);
                layelem.tabAdd('laytab', {
                    title: elem.find('span').html(),
                    content: '<iframe src="' + elem.attr('lay-url') + '"></iframe>',
                    id: lay_id
                });
            }
            layelem.tabChange('laytab', lay_id);
            elem.parent().removeClass('layui-this');
        }
    });

    function tab_delete(lay_id) {
        tab_array.splice($.inArray(lay_id, tab_array), 1);
        layelem.tabDelete('laytab', lay_id);
    }
});

function tab_tool_hide() {
    $('.tab-tool-body').hide();
    $('.tab-tool').find('i').removeClass('fa-rotate-180');
}
