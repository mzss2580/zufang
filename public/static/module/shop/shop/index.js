$(function() {
    $('.layui-plus').click(function() {
        layer_iframe('添加优惠劵', 'edit');
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            if (!res.data && curr > 1) {
                window.options.page.curr = curr - 1;
                window.tableIns.reload(window.options);
            }
            window.options.page.curr = curr;
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
                field: 'couponno',
                title: '编号',
                unresize: true,
                align: 'center'
            }, {
                field: 'money_max',
                title: '满减金额',
                unresize: true,
                align: 'center'
            }, {
                field: 'money',
                title: '优惠金额',
                unresize: true,
                align: 'center'
            }, {
                field: 'start_time',
                title: '生效时间',
                unresize: true,
                align: 'center'
            }, {
                field: 'end_time',
                title: '失效时间',
                unresize: true,
                align: 'center'
            }, {
                field: 'create_time',
                title: '添加时间',
                unresize: true,
                align: 'center'
            }, {
                title: '操作',
                unresize: true,
                align: 'center',
                toolbar: '#toolbar_normal'
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event === 'edit') {
            layer_iframe('编辑优惠劵', 'edit?id=' + obj.data.id);
        } else if (obj.event === 'del') {
            _ajax_remove('确定删除该优惠劵吗?', 'coupon', obj.data.id);
        }
    });
});