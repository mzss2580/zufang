$(function() {
    layform.on('switch(login_captcha)', function(data) {
        if (data.elem.checked) {
            $(data.elem).val('1');
        } else {
            $(data.elem).val('0');
        }
    });
    var about_us = layedit.build('about_us', {
        uploadImage: {
            url: '/ajax/index/upload'
        }
    });
    var contact_us = layedit.build('contact_us', {
        uploadImage: {
            url: '/ajax/index/upload'
        }
    });
    var agreement = layedit.build('agreement', {
        uploadImage: {
            url: '/ajax/index/upload'
        }
    });
    var hundred = layedit.build('hundred', {
        uploadImage: {
            url: '/ajax/index/upload'
        }
    });
    layform.on('submit(configform)', function(data) {
        if (!data.field.login_captcha) {
            data.field.login_captcha = '0';
        }
        data.field.about_us = $.trim(layedit.getContent(about_us));
        data.field.contact_us = $.trim(layedit.getContent(contact_us));
        data.field.agreement = $.trim(layedit.getContent(agreement));
        data.field.hundred = $.trim(layedit.getContent(hundred));
        _ajax({
            data: data.field,
            success: function(res) {
                if (!res.code) {
                    layer.msg(res.msg, {
                        icon: 1
                    });
                }
            }
        });
        return false;
    });
});