$(function() {
    layform.on('submit(pwdform)', function(data) {
        if (data.field.newpwd !== data.field.cfmpwd) {
            layer.msg('两次输入密码不一致', {
                icon: 2,
                anim: 6
            });
            return false;
        }
        if (data.field.newpwd === data.field.oldpwd) {
            layer.msg('与原密码相同', {
                icon: 2,
                anim: 6
            });
            return false;
        }
        _ajax({
            data: {
                oldpwd: data.field.oldpwd,
                newpwd: data.field.newpwd
            },
            success: function(res) {
                if (res.code) {
                    layer.msg(res.msg, {
                        icon: 2,
                        anim: 6
                    });
                } else {
                    layer.msg(res.msg, {
                        icon: 1
                    });
                    setTimeout(function() {
                        location.reload();
                    }, 333);
                }
            },
            error: function() {
                layer.msg('系统繁忙,请稍后重试', {
                    icon: 0
                });
            }
        });
        return false;
    });
});
