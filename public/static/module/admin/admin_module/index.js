$(function() {
    $('.layui-plus').click(function() {
        layer_iframe('添加模块', 'edit');
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            if (!res.data && curr > 1) {
                window.options.page.curr = curr - 1;
                window.tableIns.reload(window.options);
            }
            window.options.page.curr = curr;
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
                field: 'type',
                title: '是否后台模块',
                unresize: true,
                align: 'center'
            }, {
                field: 'route',
                title: '模块路径',
                unresize: true,
                align: 'center'
            }, {
                field: 'name',
                title: '模块名称',
                unresize: true,
                align: 'center'
            }, {
                title: '操作',
                unresize: true,
                align: 'center',
                toolbar: '#toolbar_normal'
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event === 'edit') {
            layer_iframe('编辑模块', 'edit?id=' + obj.data.id);
        } else if (obj.event === 'del') {
            layer.confirm('删除模块将删除模块下所有内容,确定删除吗?', function(index) {
                layer.close(index);
                _ajax({
                    url: 'remove',
                    data: {
                        id: obj.data.id
                    },
                    success: function(res) {
                        if (res.code) {
                            layer.msg(res.msg, {
                                icon: 2
                            });
                        } else {
                            layer.msg(res.msg, {
                                icon: 1
                            });
                            window.tableIns.reload(window.options);
                        }
                    }
                });
            });
        }
    });
});
