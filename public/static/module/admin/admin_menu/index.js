$(function() {
    $('.layui-plus').click(function() {
        layer_iframe('添加菜单', 'edit');
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            if (!res.data && curr > 1) {
                window.options.page.curr = curr - 1;
                window.tableIns.reload(window.options);
            }
            window.options.page.curr = curr;
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
                field: 'parent_id',
                title: '父级菜单',
                unresize: true,
                align: 'center'
            }, {
                field: 'module',
                title: '所属模块',
                unresize: true,
                align: 'center'
            }, {
                field: 'name',
                title: '菜单名称',
                unresize: true,
                align: 'center'
            }, {
                field: 'route',
                title: '访问路径',
                unresize: true,
                align: 'center',
                width: 250
            }, {
                field: 'icon',
                title: '图标',
                unresize: true,
                align: 'center'
            }, {
                title: '状态',
                unresize: true,
                align: 'center',
                toolbar: '#toolbar_disabled'
            }, {
                field: 'sort',
                title: '排序',
                unresize: true,
                align: 'center'
            }, {
                title: '操作',
                unresize: true,
                align: 'center',
                toolbar: '#toolbar_normal'
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event === 'edit') {
            layer_iframe('编辑菜单', 'edit?id=' + obj.data.id);
        } else if (obj.event === 'del') {
            _ajax_remove('确定删除该菜单吗?', 'admin_menu', obj.data.id);
        } else if (obj.event === 'disabled') {
            _ajax_update('确定禁用该菜单吗?', 'admin_menu', obj.data.id, {
                disabled: 1
            });
        } else if (obj.event === 'enabled') {
            _ajax_update('确定启用该菜单吗?', 'admin_menu', obj.data.id, {
                disabled: 0
            });
        }
    });
});
