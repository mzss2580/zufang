$(function() {
    layform.on('checkbox(menu)', function(data) {
        if (data.value === '1') {
            $(data.elem).prop('checked', false).val('0').parent().next().find('.sub').prop('checked', false).val('0');
        } else {
            $(data.elem).prop('checked', true).val('1').parent().next().find('.sub').prop('checked', true).val('1');
        }
        layform.render('checkbox');
    });
    layform.on('checkbox(sub)', function(data) {
        if (data.value === '1') {
            $(data.elem).prop('checked', false).val('0');
            if ($(data.elem).siblings('.sub:checked').length === 0) {
                $(data.elem).parent().prev().find('.menu').prop('checked', false).val('0');
            }
        } else {
            $(data.elem).prop('checked', true).val('1').parent().prev().find('.menu').prop('checked', true).val('1');
        }
        layform.render('checkbox');
    });
    layform.on('submit(authform)', function() {
        var auth = $(':checkbox:checked').map(function() {
            return $(this).attr('lay-id');
        }).get().join(',');
        if (auth === $('#auth').val()) {
            layer_iframe_close(333);
            return false;
        }
        _ajax_update('确定保存权限吗?', 'admin_role', $('#id').val(), {
            auth: auth
        }, function(res) {
            if (res.code) {
                layer.msg(res.msg, {
                    icon: 2
                });
            } else {
                layer.msg(res.msg, {
                    icon: 1
                });
                layer_iframe_close(333);
            }
        });
        return false;
    });
});
