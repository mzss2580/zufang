$(function() {
    $('.layui-plus').click(function() {
        layer_iframe('添加角色', 'edit');
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            if (!res.data && curr > 1) {
                window.options.page.curr = curr - 1;
                window.tableIns.reload(window.options);
            }
            window.options.page.curr = curr;
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
                field: 'name',
                title: '角色名称',
                unresize: true,
                align: 'center'
            }, {
                title: '状态',
                unresize: true,
                align: 'center',
                toolbar: '#toolbar_disabled'
            }, {
                title: '操作',
                unresize: true,
                align: 'center',
                toolbar: '#toolbar_normal'
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event === 'edit') {
            layer_iframe('编辑角色', 'edit?id=' + obj.data.id);
        } else if (obj.event === 'del') {
            _ajax_remove('确定删除该角色吗?', 'admin_role', obj.data.id);
        } else if (obj.event === 'disabled') {
            _ajax_update('确定禁用该角色吗?', 'admin_role', obj.data.id, {
                disabled: 1
            });
        } else if (obj.event === 'enabled') {
            _ajax_update('确定启用该角色吗?', 'admin_role', obj.data.id, {
                disabled: 0
            });
        } else if (obj.event === 'auth') {
            layer_iframe('分配权限', 'auth?id=' + obj.data.id);
        }
    });
});
