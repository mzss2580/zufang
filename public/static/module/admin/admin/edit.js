$(function() {
    layform.on('submit(adminform)', function(data) {
        if (data.field.newpwd) {
            if (data.field.newpwd !== data.field.cfmpwd) {
                layer.msg('两次密码必须相同', {
                    icon: 0,
                    anim: 6
                });
                return false;
            }
            data.field.password = $.md5(data.field.newpwd);
        }
        _ajax({
            data: data.field,
            success: function(res) {
                if (res.code) {
                    layer.msg(res.msg, {
                        icon: 2
                    });
                } else {
                    layer.msg(res.msg, {
                        icon: 1
                    });
                    layer_iframe_close(333, true);
                }
            },
            error: function() {
                layer.msg('系统繁忙,请稍后重试', {
                    icon: 0
                });
            }
        });
        return false;
    });
});
