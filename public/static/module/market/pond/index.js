$(function() {
    $('.layui-plus').click(function() {
        layer_iframe('添加', 'edit');
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            if (!res.data && curr > 1) {
                window.options.page.curr = curr - 1;
                window.tableIns.reload(window.options);
            }
            window.options.page.curr = curr;
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
                field: 't_credi2',
                title: '今日销毁易贝数',
                unresize: true,
                align: 'center'
            }, {
                field: 'sum_credit2',
                title: '全网销毁',
                unresize: true,
                align: 'center'
            }, {
                field: 't_yb',
                title: '每天发放的易贝',
                unresize: true,
                align: 'center'
            }, {
                field: 'sum_yb',
                title: '总共发放易贝量',
                unresize: true,
                align: 'center'
            }, {
                title: '操作',
                unresize: true,
                align: 'center',
                toolbar: '#toolbar_normal'
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event === 'edit') {
            layer_iframe('编辑', 'edit?id=' + obj.data.id);
        } else if (obj.event === 'del') {
            _ajax_remove('确定删除该吗?', 'pond', obj.data.id);
        }
    });
});
