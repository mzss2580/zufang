$(function() {
    // $('.layui-plus').click(function() {
    //     layer_iframe('添加', 'edit');
    // });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            if (!res.data && curr > 1) {
                window.options.page.curr = curr - 1;
                window.tableIns.reload(window.options);
            }
            window.options.page.curr = curr;
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
                field: 'mid',
                title: '挂售id',
                unresize: true,
                align: 'center'
            }, {
                field: 'pay_uid',
                title: '购买用户',
                unresize: true,
                align: 'center'
            }, {
                field: 'voucher',
                title: '购买凭证',
                unresize: true,
                align: 'center'
            }, {
                field: 'pay_num',
                title: '收购数量',
                unresize: true,
                align: 'center'
            }, {
                field: 'type',
                title: '支付方式',
                unresize: true,
                align: 'center'
            }, {
                field: 'status',
                title: '状态',
                unresize: true,
                align: 'center'
            }, {
                title: '操作',
                unresize: true,
                align: 'center',
                toolbar: '#toolbar_normal'
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event === 'edit') {
            layer_iframe('编辑', 'edit?id=' + obj.data.id);
        } else if (obj.event === 'del') {
            _ajax_remove('确定删除该吗?', 'market_order', obj.data.id);
        }
    });
});
