$(function() {
    $('.layui-plus').click(function() {
        layer_iframe('添加求购订单列表	', 'edit');
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            if (!res.data && curr > 1) {
                window.options.page.curr = curr - 1;
                window.tableIns.reload(window.options);
            }
            window.options.page.curr = curr;
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
                field: 'aid',
                title: '求购id',
                unresize: true,
                align: 'center'
            }, {
                field: 'sell_uid',
                title: '卖出用户id',
                unresize: true,
                align: 'center'
            }, {
                field: 'create_time',
                title: '订单创建时间',
                unresize: true,
                align: 'center'
            }, {
                field: 'true_time',
                title: '订单完成时间',
                unresize: true,
                align: 'center'
            }, {
                field: 'voucher',
                title: '凭证',
                unresize: true,
                align: 'center'
            }, {
                field: 'status',
                title: '状态1已提交2求购者打款卖家待确认,3已完成',
                unresize: true,
                align: 'center'
            }, {
                title: '操作',
                unresize: true,
                align: 'center',
                toolbar: '#toolbar_normal'
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event === 'edit') {
            layer_iframe('编辑求购订单列表	', 'edit?id=' + obj.data.id);
        } else if (obj.event === 'del') {
            _ajax_remove('确定删除该求购订单列表	吗?', 'acquisition_order', obj.data.id);
        }
    });
});
