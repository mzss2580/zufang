$(function() {
    $('.layui-plus').click(function() {
        layer_iframe('添加求购表', 'edit');
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            if (!res.data && curr > 1) {
                window.options.page.curr = curr - 1;
                window.tableIns.reload(window.options);
            }
            window.options.page.curr = curr;
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
                field: 'uid',
                title: '收购者id',
                unresize: true,
                align: 'center'
            }, {
                field: 'num',
                title: '求购数量',
                unresize: true,
                align: 'center'
            }, {
                field: 'price',
                title: '价格',
                unresize: true,
                align: 'center'
            }, {
                field: 'create_time',
                title: '创建时间',
                unresize: true,
                align: 'center'
            }, {
                field: 'status',
                title: '状态 1发布中 2 已完成 3已取消',
                unresize: true,
                align: 'center'
            }, {
                title: '操作',
                unresize: true,
                align: 'center',
                toolbar: '#toolbar_normal'
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event === 'edit') {
            layer_iframe('编辑求购表', 'edit?id=' + obj.data.id);
        } else if (obj.event === 'del') {
            _ajax_remove('确定删除该求购表吗?', 'acquisition', obj.data.id);
        }
    });
});
