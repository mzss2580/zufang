$(function() {
    $('.layui-plus').click(function() {
        layer_iframe('添加app易贝池', 'edit');
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            if (!res.data && curr > 1) {
                window.options.page.curr = curr - 1;
                window.tableIns.reload(window.options);
            }
            window.options.page.curr = curr;
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
            field: 'market_credit2',
                    title: '易贝总量池',
                    unresize: true,
                    align: 'center'
                }, {
                field: 't_yb',
                title: '易贝释放池',
                unresize: true,
                align: 'center'
            }
            , {
                field: 'market_credit2_sy',
                title: '易贝总量池剩余',
                unresize: true,
                align: 'center'
            },{
                field: 't_credi2',
                title: '今日销毁易贝数',
                unresize: true,
                align: 'center'
            }, {
                field: 'sum_credit2',
                title: '全网销毁',
                unresize: true,
                align: 'center'
            }
            , {
                field: 'sum_credit1',
                title: '全网总积分',
                unresize: true,
                align: 'center'
            }, {
                title: '操作',
                unresize: true,
                align: 'center',
                toolbar: '#toolbar_normal'
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event === 'edit') {
            layer_iframe('编辑app易贝池', 'edit?id=' + obj.data.id);
        } else if (obj.event === 'del') {
            _ajax_remove('确定删除该app易贝池吗?', 'market_pond', obj.data.id);
        }
    });
});
