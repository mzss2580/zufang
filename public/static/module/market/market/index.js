$(function() {
    $('.layui-plus').click(function() {
        layer_iframe('添加市场表', 'edit');
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            if (!res.data && curr > 1) {
                window.options.page.curr = curr - 1;
                window.tableIns.reload(window.options);
            }
            window.options.page.curr = curr;
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
                field: 'id',
                title: 'id',
                unresize: true,
                align: 'center'
            }, {
                    field: 'head',
                    title: '用户头像',
                    unresize: true,
                    align: 'center'
                },
                {
                field: 'nick',
                title: '挂卖用户',
                unresize: true,
                align: 'center'
            }, {
                field: 'num',
                title: '易贝数量',
                unresize: true,
                align: 'center'
            }, {
                field: 'price',
                title: '挂卖价格',
                unresize: true,
                align: 'center'
            }, {
                field: 'phone',
                title: '手机号',
                unresize: true,
                align: 'center'
            }
            , {
                field: 'statuss',
                title: '挂售状态',
                unresize: true,
                align: 'center'
            }, {
                field: 'create_time',
                title: '创建时间',
                unresize: true,
                align: 'center'
            }, {
                field: 'true_time',
                title: '完成时间',
                unresize: true,
                align: 'center'
            }, {
                    field: 'false_time',
                    title: '取消时间',
                    unresize: true,
                    align: 'center'
        }]
        ],
    };
    //
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event === 'edit') {
            layer_iframe('编辑市场表', 'edit?id=' + obj.data.id);
        } else if (obj.event === 'del') {
            _ajax_remove('确定删除该市场表吗?', 'market', obj.data.id);
        }
        if (obj.event === 'log') {
            layer_iframe('购买日志', 'log?id=' + obj.data.id);
        }
        if (obj.event === 'cancel') {
            layer.confirm('确定取消挂售吗', function(index) {
                _ajax({
                    url: 'cancel',
                    data: {
                        id: obj.data.id
                    },
                    success: function(res) {
                        layer.close(index);
                        if (typeof success === 'function') {
                            success(res);
                        } else {
                            if (res.code) {
                                layer.msg(res.msg, {
                                    icon: 2
                                });
                            } else {
                                layer.msg(res.msg, {
                                    icon: 1
                                });
                                setTimeout(function() {
                                    window.tableIns.reload(window.options);
                                }, 333);
                            }
                        }
                    },
                    error: function() {
                        layer.close(index);
                    }
                });
            });
        }
    });
});
