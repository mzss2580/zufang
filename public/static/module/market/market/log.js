$(function() {

    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            if (!res.data && curr > 1) {
                window.options.page.curr = curr - 1;
                window.tableIns.reload(window.options);
            }
            window.options.page.curr = curr;
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
                field: 'id',
                title: 'ID',
                unresize: true,
                width: 80,
                align: 'center'
            }, {
                field: 'nick',
                title: '昵称',
                width: 100,
                unresize: true,
                align: 'center'
            }, {
                field: 'head',
                title: '头像',
                width: 80,
                unresize: true,
                align: 'center'
            }, {
                field: 'pay_num',
                title: '购买数量',
                width: 150,
                unresize: true,
                align: 'center'
            }
                , {
                field: 'phone',
                title: '手机号',
                unresize: true,
                width: 200,
                align: 'center'
            } , {
                field: 'statuss',
                title: '购买状态',
                width:200,
                unresize: true,
                align: 'center'
            }, {
                field: 'pay_time',
                title: '支付时间',
                unresize: true,
                width:200,
                align: 'center'
            }, {
                field: 'true_time',
                title: '完成时间',
                unresize: true,
                width:200,
                align: 'center'
            }
            ]
        ],
    };
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event === 'queren') {
            layer.confirm('确认放行吗?', function(index) {
                _ajax({
                    url: 'queren',
                    data: {
                        id: obj.data.id
                    },
                    success: function(res) {
                        layer.close(index);
                        if (typeof success === 'function') {
                            success(res);
                        } else {
                            if (res.code) {
                                layer.msg(res.msg, {
                                    icon: 2
                                });
                            } else {
                                layer.msg(res.msg, {
                                    icon: 1
                                });
                                setTimeout(function() {
                                    window.tableIns.reload(window.options);
                                }, 333);
                            }
                        }
                    },
                    error: function() {
                        layer.close(index);
                    }
                });
            });
        }
    });
});
