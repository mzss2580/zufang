$(function() {
    $('.layui-plus').click(function() {
        layer_iframe('添加市场参数设置', 'edit');
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            if (!res.data && curr > 1) {
                window.options.page.curr = curr - 1;
                window.tableIns.reload(window.options);
            }
            window.options.page.curr = curr;
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
                field: 'price',
                title: '价格',
                unresize: true,
                align: 'center'
            }, {
                field: 'after_price',
                title: '之前价格',
                unresize: true,
                align: 'center'
            }
            , {
                field: 'service',
                title: '挂售手续费',
                unresize: true,
                align: 'center'
            }
            , {
                field: 'market_service',
                title: '强制挂售比例',
                unresize: true,
                align: 'center'
            }
            , {
                field: 'market_num',
                title: '强制挂售限制数量',
                unresize: true,
                align: 'center'
            }
            , {
                field: 'update_time',
                title: '修改时间',
                unresize: true,
                align: 'center'
            }
            , {
                title: '挂售状态',
                unresize: true,
                align: 'center',
                toolbar: '#gold_medal',
            }
            ,{
                title: '操作',
                unresize: true,
                align: 'center',
                toolbar: '#toolbar_normal'
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event === 'edit') {
            layer_iframe('编辑市场参数设置', 'edit?id=' + obj.data.id);
        } else if (obj.event === 'del') {
            _ajax_remove('确定删除该市场参数设置吗?', 'market_config', obj.data.id);
        }else if (obj.event === 'gold_enabled') {
            _ajax_update('确定重启强制挂售吗?', 'market_config', obj.data.id, {
                flag: 1
            });
        }
        else if (obj.event === 'gold_disabled') {
            _ajax_update('确定关闭强制挂售吗?', 'market_config', obj.data.id, {
                flag: 0
            });
        }
    });

});
