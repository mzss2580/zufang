$(function() {
    $('.layui-plus').click(function() {
        layer_iframe('添加会员等级奖励制度表', 'edit');
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            if (!res.data && curr > 1) {
                window.options.page.curr = curr - 1;
                window.tableIns.reload(window.options);
            }
            window.options.page.curr = curr;
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
                field: 'name',
                title: '等级名称',
                unresize: true,
                align: 'center'
            }, {
                field: 'integral',
                title: '购物积分(%)',
                unresize: true,
                align: 'center'
            }, {
                field: 'g_integral',
                title: '打赏/易债(易贝)',
                unresize: true,
                align: 'center'
            }, {
                title: '操作',
                unresize: true,
                align: 'center',
                toolbar: '#toolbar_normal'
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event === 'edit') {
            layer_iframe('编辑会员等级奖励制度表', 'edit?id=' + obj.data.id);
        } else if (obj.event === 'del') {
            _ajax_remove('确定删除该会员等级奖励制度表吗?', 'grade', obj.data.id);
        }
    });
});
