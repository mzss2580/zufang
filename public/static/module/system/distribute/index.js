
$(function() {
    $('.layui-plus').click(function() {
        layer_iframe('添加每天发放易贝数量和比例', 'edit');
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            if (!res.data && curr > 1) {
                window.options.page.curr = curr - 1;
                window.tableIns.reload(window.options);
            }
            window.options.page.curr = curr;
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
                field: 'green_points',
                title: '总额度',
                unresize: true,
                align: 'center'
            }, {
                field: 'surplus',
                title: '剩余数量',
                unresize: true,
                align: 'center'
            }, {
                field: 'probability',
                title: '递减百分比',
                unresize: true,
                align: 'center'
            }, {
                field: 'every',
                title: '每天发放',
                unresize: true,
                align: 'center'
            }, {
                title: '操作',
                unresize: true,
                align: 'center',
                toolbar: '#toolbar_normal'
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event === 'edit') {
            layer_iframe('编辑每天发放易贝数量和比例', 'edit?id=' + obj.data.id);
        } else if (obj.event === 'del') {
            _ajax_remove('确定删除该每天发放易贝数量和比例吗?', ' distribute', obj.data.id);
        }
    });
});