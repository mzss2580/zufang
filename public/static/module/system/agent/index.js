$(function() {
    $('.layui-plus').click(function() {
        layer_iframe('添加会员等级奖励制度表', 'edit');
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            if (!res.data && curr > 1) {
                window.options.page.curr = curr - 1;
                window.tableIns.reload(window.options);
            }
            window.options.page.curr = curr;
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
                field: 'id',
                title: 'id',
                unresize: true,
                align: 'center'
            }, {
                field: 'nick',
                title: '代理人',
                unresize: true,
                align: 'center'
            }, {
                field: 'head',
                title: '代理人头像',
                unresize: true,
                width: 80,
                align: 'center'
            }, {
                field: 'phone',
                title: '代理人手机号',
                unresize: true,
                align: 'center'
            }, {
                field: 'address',
                title: '代理区域',
                unresize: true,
                width:400,
                align: 'center'
            }, {
                field: 'grade',
                title: '代理等级',
                unresize: true,
                align: 'center'
            }, {
                    field: 'proportion',
                    title: '抽成比例',
                    unresize: true,
                    align: 'center'
            }
            , {
                    field: 'remarks',
                    title: '备注',
                    unresize: true,
                    align: 'center'
                }
                , {
                field: 'create_time',
                title: '备注',
                unresize: true,
                align: 'center'
            },{
                title: '操作',
                unresize: true,
                align: 'center',
                toolbar: '#toolbar_normal'
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event === 'edit') {
            layer_iframe('编辑代理', 'edit?id=' + obj.data.id);
        } else if (obj.event === 'del') {
            _ajax_remove('确定删除代理?', 'agent', obj.data.id);
        }
    });
});
