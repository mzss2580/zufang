$(function() {
    $('.layui-plus').click(function() {
        layer_iframe('添加', 'edit');
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            if (!res.data && curr > 1) {
                window.options.page.curr = curr - 1;
                window.tableIns.reload(window.options);
            }
            window.options.page.curr = curr;
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
                field: 'user_id',
                title: '用户名',
                unresize: true,
                align: 'center'
            }, {
                field: 'content',
                title: '内容',
                unresize: true,
                align: 'center'
            }, {
                field: 'image',
                title: '图片',
                unresize: true,
                align: 'center'
            }, {
                field: 'reply',
                title: '回复内容',
                unresize: true,
                align: 'center'
            }, {
                field: 'create_time',
                title: '创建时间',
                unresize: true,
                align: 'center'
            }, {
                field: 'reply_time',
                title: '回复时间',
                unresize: true,
                align: 'center'
            }, {
                title: '操作',
                unresize: true,
                align: 'center',
                toolbar: '#toolbar_normal'
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event === 'edit') {
            layer_iframe('编辑', 'edit?id=' + obj.data.id);
        } else if (obj.event === 'del') {
            _ajax_remove('确定删除该吗?', 'hundred', obj.data.id);
        }
    });
});
