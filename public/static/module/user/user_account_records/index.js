$(function() {
    $('.layui-plus').click(function() {
        var uid = $('#uid').val();
        layer_iframe('编辑会员钱包', 'edit?user_id='+uid);
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            if (!res.data && curr > 1) {
                window.options.page.curr = curr - 1;
                window.tableIns.reload(window.options);
            }
            window.options.page.curr = curr;
            window.page = curr;
            window.limit = res.limit;
            //$('#uid').val(res.uid);
        },
        cols: [
            [{
                field: 'laiyuan',
                title: '来源',
                unresize: true,
                align: 'center'
            }, {
                field: 'account_type',
                title: '类型',
                unresize: true,
                align: 'center'
            }, {
                field: 'number',
                title: '金额',
                unresize: true,
                align: 'center'
            }, {
                field: 'text',
                title: '描述',
                unresize: true,
                align: 'center'
            }, {
                field: 'create_time',
                title: '创建时间',
                unresize: true,
                align: 'center'
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event === 'edit') {
            layer_iframe('编辑会员流水账表', 'edit?id=' + obj.data.id);
        } else if (obj.event === 'del') {
            _ajax_remove('确定删除该会员流水账表吗?', 'admin_user_account_records', obj.data.id);
        }
    });
});
