$(function() {
    $('.layui-plus').click(function() {
        layer_iframe('添加店铺', 'edit');
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            if (!res.data && curr > 1) {
                window.options.page.curr = curr - 1;
                window.tableIns.reload(window.options);
            }
            window.options.page.curr = curr;
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
                field: 'sort',
                title: '排序(可编辑)',
                unresize: true,
                align: 'center',
                edit: 'text',
            }, {
                field: 'user_id',
                title: '所属用户',
                unresize: true,
                align: 'center'
            }, {
                field: 'name',
                title: '店铺名称',
                unresize: true,
                align: 'center'
            }, {
                field: 'logo',
                title: '店铺logo',
                unresize: true,
                align: 'center'
            }, {
                field: 'tel',
                title: '店铺电话',
                unresize: true,
                align: 'center'
            }, {
                field: 'yyzz',
                title: '营业执照',
                unresize: true,
                align: 'center'
            }, {
                field: 'id_card_rx',
                title: '身份证人像面',
                unresize: true,
                align: 'center'
            },{
                field: 'id_card_gh',
                title: '身份证国徽面',
                unresize: true,
                align: 'center'
            },
                /*{
                field: 'jyxkz',
                title: '经营许可证',
                unresize: true,
                align: 'center'
            },*/ {
                title: '审核状态',
                unresize: true,
                align: 'center',
                toolbar: '#toolbar_disabled'
            }, {
                field: 'create_time',
                title: '注册时间',
                unresize: true,
                align: 'center'
            }, {
                title: '操作',
                unresize: true,
                align: 'center',
                toolbar: '#toolbar_normal',
                width: 200
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event === 'edits') {
            layer_iframe('编辑店铺', 'edits?id=' + obj.data.id);
        } else if (obj.event === 'del') {
            _ajax_remove('确定删除该店铺吗?', 'shop', obj.data.id);
        } else if (obj.event === 'disabled') {
            _ajax_update('确定不通过审核吗?', 'shop', obj.data.id, {
                status: 2
            });
        } else if (obj.event === 'enabled') {
            _ajax_update('确定通过审核吗?', 'shop', obj.data.id, {
                status: 1
            });
        } else if (obj.event === 'no') {
            _ajax_update('确定禁用该店铺吗?', 'shop', obj.data.id, {
                is_disable: 1
            });
        } else if (obj.event === 'yes') {
            _ajax_update('确定解封该店铺吗?', 'shop', obj.data.id, {
                is_disable: 0
            });
        }
    });
    laytable.on('edit(data_table)', function (obj) {
        // $('input').keyup(function () {
        //     value = value.replace(/[^d.]/g, '');
        // });
        var value = obj.value, // 得到修改后的值
            id = obj.data.id, // 得到当前修改行的id
            field = obj.field; // 得到当前修改的字段名
        var data = {},
            key = field;
        data[key] = value;
        data['id'] = id;
        $.ajax({
            url: 'editSort?id=' + obj.data.id,
            type: 'POST',
            data: data,
            success: function (data) {
                if (data.code == 1) {
                    layer.msg(data.msg, {
                        icon: 6,time: 500
                    });
                } else {
                    layer.msg(data.msg, {
                        icon: 5,time: 500
                    });
                }
            }
        });
    });
    
});