$(function() {
    laydate.render({
        elem: '#start'
    });
    laydate.render({
        elem: '#end'
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            if (!res.data && curr > 1) {
                window.options.page.curr = curr - 1;
                window.tableIns.reload(window.options);
            }
            window.options.page.curr = curr;
            window.page = curr;
            window.limit = res.limit;
            $('.condition_money').html(res.condition_money);
        },
        cols: [
            [{
                field: 'id',
                title: 'ID',
                unresize: true,
                align: 'center',
                width: '60'
            },{
                field: 's_name',
                title: '商户姓名',
                unresize: true,
                align: 'center',
                width: '130'
            },{
                field: 's_tel',
                title: '商户手机号',
                unresize: true,
                align: 'center',
                width: '130'
            },{
                field: 'type_name',
                title: '类型',
                unresize: true,
                align: 'center',
                width: '130'
            },{
                field: 'order_sn',
                title: '订单号',
                unresize: true,
                align: 'center',
                width: '200'
            },{
                field: 'order_refund_sn',
                title: '退款订单号',
                unresize: true,
                align: 'center',
                width: '200'
            },{
                field: 'money',
                title: '余额变化',
                unresize: true,
                align: 'center',
                width: '130'
            },{
                field: 'freeze_money',
                title: '实际冻结金额',
                unresize: true,
                align: 'center',
                width: '180'
            },{
                field: 'status_name',
                title: '状态',
                unresize: true,
                align: 'center',
                width: '180'
            },{
                field: 'freeze_name',
                title: '冻结状态',
                unresize: true,
                align: 'center',
                width: '130'
            }, {
                field: 'reason',
                title: '驳回原因',
                unresize: true,
                align: 'center',
                width: '180'
            },{
                field: 'refund_cont',
                title: '退款内容',
                unresize: true,
                align: 'center',
                width: '150'
            }, {
                field: 'status_name',
                title: '状态',
                unresize: true,
                align: 'center',
                width: '110'
            }, {
                field: 'desc',
                title: '描述',
                unresize: true,
                align: 'center',
                width: '220'
            }, {
                field: 'create_time',
                title: '创建时间',
                unresize: true,
                align: 'center',
                width: '190'
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
});