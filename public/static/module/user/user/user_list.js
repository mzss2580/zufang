$(function() {
    $('.layui-plus').click(function() {
        var uid = $('#uid').val();
        layer_iframe('添加用户', 'edit?user_id='+uid);
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            if (!res.data && curr > 1) {
                window.options.page.curr = curr - 1;
                window.tableIns.reload(window.options);
            }
            window.options.page.curr = curr;
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
                field: 'id',
                title: 'ID',
                unresize: true,
                align: 'center'
            }, {
                field: 'nick',
                title: '昵称',
                unresize: true,
                align: 'center'
            }, {
                field: 'head',
                title: '头像',
                unresize: true,
                align: 'center'
            }, {
                field: 'phone',
                title: '手机号',
                unresize: true,
                align: 'center'
            },{
                field: 'gid',
                title: '级别',
                unresize: true,
                align: 'center'
            }, {
                field: 'credit1',
                title: '购物积分',
                unresize: true,
                align: 'center'
            }, {
                field: 'credit2',
                title: '易贝',
                unresize: true,
                align: 'center'
            }, {
                field: 'create_time',
                title: '注册时间',
                unresize: true,
                align: 'center'
            }/*, {
                title: '操作',
                unresize: true,
                align: 'center',
                toolbar: '#toolbar_normal',
                width: 250
            }*/]
        ],
    };
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event === 'list') {
            //layer_iframe('推荐列表', '/user/user_account_records/index?id=' + obj.data.id,0,0,1);
            layer_iframe('推荐列表', '/user/user/user_list?id=' + obj.data.id,0,0,1);
        }
        if (obj.event === 'edit') {
            layer_iframe('编辑用户', 'edit?id=' + obj.data.id);
        } else if (obj.event === 'del') {
            _ajax_remove('确定删除该用户吗?', 'user', obj.data.id);
        }else if (obj.event === 'edit_code') {
            layer_iframe('编辑邀请码', 'edit_code?id=' + obj.data.id);
        }
    });
});
