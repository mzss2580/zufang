$(function() {
    $('.layui-plus').click(function() {
        layer_iframe('添加用户', 'edit');
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            if (!res.data && curr > 1) {
                window.options.page.curr = curr - 1;
                window.tableIns.reload(window.options);
            }
            window.options.page.curr = curr;
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
                field: 'id',
                title: 'ID',
                unresize: true,
                width: 80,
                align: 'center'
            }, {
                field: 'nick',
                title: '昵称',
                unresize: true,
                width: 100,
                align: 'center'
            }, {
                field: 'head',
                title: '头像',
                width: 80,
                unresize: true,
                align: 'center'
            }, {
                field: 'phone',
                title: '手机号',
                width: 150,
                unresize: true,
                align: 'center'
                }
                , {
                field: 'text',
                title: '原由  ',
                unresize: true,
                align: 'center'
            }
                , {
                field: 'content',
                title: '记录',
                unresize: true,
                align: 'center'
            }, {
                field: 'create_time',
                title: '消费时间',
                width: 200,
                unresize: true,
                align: 'center'
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event === 'list') {
            //layer_iframe('推荐列表', '/user/user_account_records/index?id=' + obj.data.id,0,0,1);
            layer_iframe('推荐列表', '/user/user/user_list?id=' + obj.data.id,0,0,1);
        }
        if (obj.event === 'edit') {
            layer_iframe('编辑用户', 'edit?id=' + obj.data.id);
        } else if (obj.event === 'del') {
            _ajax_remove('确定删除该用户吗?', 'user', obj.data.id);
        }else if (obj.event === 'edit_code') {
            layer_iframe('编辑邀请码', 'edit_code?id=' + obj.data.id);
        }else if (obj.event === 'credit1'){
            layer_iframe('编辑购物积分', 'credit1?id=' + obj.data.id);
        }else if (obj.event === 'remake'){
            layer_iframe('备注', 'remark?id=' + obj.data.id);
        }
    });
});
