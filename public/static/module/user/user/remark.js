$(function() {
    var remark = layedit.build('remark', {
        uploadImage: {
            url: '/ajax/index/upload'
        }
    });
    layform.on('submit(configform)', function(data) {
        data.field.remark = $.trim(layedit.getContent(remark));
        _ajax({
            data: data.field,
            success: function(res) {
                if (!res.code) {
                    layer.msg(res.msg, {
                        icon: 1
                    });
                    layer_iframe_close(333, true);
                }
            }
        });
        return false;
    });
});