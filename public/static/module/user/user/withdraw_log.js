$(function() {
    laydate.render({
        elem: '#start'
    });
    laydate.render({
        elem: '#end'
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            if (!res.data && curr > 1) {
                window.options.page.curr = curr - 1;
                window.tableIns.reload(window.options);
            }
            window.options.page.curr = curr;
            window.page = curr;
            window.limit = res.limit;
            $('.condition_money').html(res.condition_money);
        },
        cols: [
            [{
                field: 'id',
                title: 'ID',
                unresize: true,
                align: 'center',
                width: '60'
            },{
                field: 'nick',
                title: '昵称',
                unresize: true,
                align: 'center',
                width: '130'
            },{
                field: 'phone',
                title: '商户手机号',
                unresize: true,
                align: 'center',
                width: '130'
            },/*{
                field: 'type_name',
                title: '提现类型',
                unresize: true,
                align: 'center',
                width: '130'
            },*/{
                field: 'alipay_acc',
                title: '支付宝账号',
                unresize: true,
                align: 'center',
                width: '130'
            },{
                field: 'alipay_name',
                title: '支付宝姓名',
                unresize: true,
                align: 'center',
                width: '130'
            },/*{
                field: 'name',
                title: '提现姓名',
                unresize: true,
                align: 'center',
                width: '130'
            },{
                field: 'bank_name',
                title: '提现银行名称',
                unresize: true,
                align: 'center',
                width: '150'
            },{
                field: 'bank_card',
                title: '提现银行卡号',
                unresize: true,
                align: 'center',
                width: '180'
            },{
                field: 'bank_address',
                title: '开户行地址',
                unresize: true,
                align: 'center',
                width: '180'
            },*/{
                field: 'money',
                title: '申请余额',
                unresize: true,
                align: 'center',
                width: '130'
            }, {
                field: 'reason',
                title: '驳回原因',
                unresize: true,
                align: 'center',
                width: '180'
            }, {
                field: 'status_name',
                title: '状态',
                unresize: true,
                align: 'center',
                width: '110'
            }, {
                title: '审核状态',
                unresize: true,
                align: 'center',
                toolbar: '#toolbar_disabled',
                width: '140'
            }, {
                field: 'create_time',
                title: '创建时间',
                unresize: true,
                align: 'center',
                width: '190'
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event === 'disabled') {
            layer.confirm('确定驳回审核吗?', function(index) {
                layer.close(index);
                var reason = prompt('请输入驳回原因');
                if(reason != null){
                    $.ajax({
                        url: '/user/user/withdraw_nopass',
                        data: {
                            id: obj.data.id,
                            reason:reason
                        },
                        success: function(res) {

                            if (res.code) {
                                layer.msg(res.msg, {
                                    icon: 2
                                });
                            } else {
                                layer.msg(res.msg, {
                                    icon: 1
                                });
                                window.tableIns.reload(window.options);
                            }
                        }
                    });
                }

            });
        } else if (obj.event === 'enabled') {
            var type = obj.data.type;
            if(0 && type == 1){
                var msg = '确定通过审核吗并打款给用户?';
            }else{
                var msg = '确定通过审核吗并已打款给用户?';
            }
            layer.confirm(msg, function(index) {
                layer.close(index);
                $.ajax({
                    url: '/user/user/withdraw_ispass',
                    data: {
                        id: obj.data.id
                    },
                    success: function(res) {
                        if (res.code) {
                            layer.msg(res.msg, {
                                icon: 2
                            });
                        } else {
                            layer.msg(res.msg, {
                                icon: 1
                            });
                            window.tableIns.reload(window.options);
                        }
                    }
                });
            });
        }
    });
});