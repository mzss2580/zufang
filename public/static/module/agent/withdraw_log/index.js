$(function() {
    laydate.render({
        elem: '#start'
    });
    laydate.render({
        elem: '#end'
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            window.options.page.curr = curr;
            if (!res.data && curr > 1) {
                window.options.page = {
                    curr: curr - 1
                }
                window.tableIns.reload(window.options);
            }
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
                field: 'id',
                title: 'ID',
                unresize: true,
                align: 'center'
            }/*, {
                field: 'name',
                title: '姓名',
                unresize: true,
                align: 'center'
            }, {
                field: 'bank_name',
                title: '银行名称',
                unresize: true,
                align: 'center'
            }, {
                field: 'bank_card',
                title: '银行卡号',
                unresize: true,
                align: 'center'
            }, {
                field: 'bank_address',
                title: '开户行',
                unresize: true,
                align: 'center'
            }*/, {
                field: 'alipay_acc',
                title: '支付宝账号',
                unresize: true,
                align: 'center'
            }, {
                field: 'alipay_name',
                title: '支付宝名称',
                unresize: true,
                align: 'center'
            }, {
                field: 'money',
                title: '余额变化',
                unresize: true,
                align: 'center'
            }, {
                field: 'status_name',
                title: '状态',
                unresize: true,
                align: 'center'
            }, {
                field: 'reason',
                title: '描述',
                unresize: true,
                align: 'center',
                width: 180
            }, {
                field: 'create_time',
                title: '创建时间',
                unresize: true,
                align: 'center',
                width: 180
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
});