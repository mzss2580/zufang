$(function() {
    $('.layui-plus').click(function() {
        layer_iframe('添加商品轮播图', 'edit' + location.search);
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            window.options.page.curr = curr;
            if (!res.data && curr > 1) {
                window.options.page = {
                    curr: curr - 1
                }
                window.tableIns.reload(window.options);
            }
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
                field: 'image',
                title: '轮播图',
                unresize: true,
                align: 'center'
            }, {
                field: 'sort',
                title: '排序',
                unresize: true,
                align: 'center'
            }, {
                title: '操作',
                unresize: true,
                align: 'center',
                toolbar: '#toolbar_normal'
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event == 'del') {
            _ajax_remove('确定删除该商品轮播图吗?', 'goods_images', obj.data.id);
        } else if (obj.event == 'edit') {
            layer_iframe('编辑商品轮播图', 'edit' + location.search + '&id=' + obj.data.id);
        }
    });
});
