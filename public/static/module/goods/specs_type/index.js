$(function() {
    $('.layui-plus').click(function() {
        layer_iframe('添加规格类型', 'edit');
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            window.options.page.curr = curr;
            if (!res.data && curr > 1) {
                window.options.page = {
                    curr: curr - 1
                }
                window.tableIns.reload(window.options);
            }
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
                field: 'shop_name',
                title: '店铺',
                unresize: true,
                align: 'center'
            }, {
                field: 'name',
                title: '类型名称',
                unresize: true,
                align: 'center'
            }, {
                field: 'sort',
                title: '排序',
                unresize: true,
                align: 'center'
            }, {
                title: '操作',
                unresize: true,
                align: 'center',
                toolbar: '#toolbar_normal'
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event == 'del') {
            layer.confirm('确定删除该规格类型吗?', function(index) {
                _ajax({
                    url: 'remove',
                    data: {
                        id: obj.data.id
                    },
                    success: function(res) {
                        if (res.code) {
                            layer.msg(res.msg, {
                                icon: 2
                            });
                        } else {
                            layer.msg(res.msg, {
                                icon: 1
                            });
                            setTimeout(function() {
                                window.tableIns.reload(window.options);
                            }, 333);
                        }
                    },
                    error: function() {
                        layer.close(index);
                    }
                });
            });
        } else if (obj.event == 'edit') {
            layer_iframe('编辑规格类型', 'edit?id=' + obj.data.id);
        } else if (obj.event == 'specs') {
            layer_iframe('规格明细(' + obj.data.name + ')', '/goods/specs/index?type=' + obj.data.id);
        }
    });
});
