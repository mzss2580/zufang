$(function() {
    $('.layui-plus').click(function() {
        layer_iframe('添加', 'edit');
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            if (!res.data && curr > 1) {
                window.options.page.curr = curr - 1;
                window.tableIns.reload(window.options);
            }
            window.options.page.curr = curr;
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
                field: 'head',
                title: '头像',
                unresize: true,
                align: 'center'
            }, {
                field: 'name',
                title: '名称',
                unresize: true,
                align: 'center'
            }, {
                field: 'contacts',
                title: '面积',
                unresize: true,
                align: 'center'
            }, {
                field: 'phone',
                title: '联系方式',
                unresize: true,
                align: 'center'
            }, {
                field: 'type',
                title: '分类',
                unresize: true,
                align: 'center'
            }
            , {
                field: 'end_time',
                title: '结束时间',
                unresize: true,
                align: 'center'
            }, {
                field: 'create_time',
                title: '创建时间',
                unresize: true,
                align: 'center'
            }  , {
                title: '热门',
                unresize: true,
                align: 'center',
                toolbar: '#is_hot'
            }, {
                title: '是否上架',
                unresize: true,
                align: 'center',
                toolbar: '#is_shows'
            }, {
                title: '操作',
                unresize: true,
                align: 'center',
                toolbar: '#toolbar_normal'
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event === 'edit') {
            layer_iframe('编辑', 'edit?id=' + obj.data.id);
        } else if (obj.event === 'del') {
            _ajax_remove('确定删除该吗?', 'shops', obj.data.id);
        }else if (obj.event === 'fist_enabled') {
            _ajax_update('确定设置推荐商家?', 'shops', obj.data.id, {
                is_recommend: 1
            });
        }
        else if (obj.event === 'fist_disabled') {
            _ajax_update('确定取消推荐商家?', 'shops', obj.data.id, {
                is_recommend: 0
            });
        }else if (obj.event === 'fist_shows') {
            _ajax_update('确定上架商家吗?', 'shops', obj.data.id, {
                is_show: 1
            });
        }
        else if (obj.event === 'fist_showss') {
            _ajax_update('确定下架商家吗?', 'shops', obj.data.id, {
                is_show: 0
            });
        }
    });
});
