$(function() {
    $('.layui-plus').click(function() {
        layer_iframe('添加', 'edit');
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            if (!res.data && curr > 1) {
                window.options.page.curr = curr - 1;
                window.tableIns.reload(window.options);
            }
            window.options.page.curr = curr;
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
                field: 'duration',
                title: '时长(月)',
                unresize: true,
                align: 'center'
            }, {
                field: 'price',
                title: '价格',
                unresize: true,
                align: 'center'
            }, {
                title: '操作',
                unresize: true,
                align: 'center',
                toolbar: '#toolbar_normal'
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event === 'edit') {
            layer_iframe('编辑', 'edit?id=' + obj.data.id);
        }
               else if (obj.event === 'del') {
            // layer_iframe('编辑', 'edit?id=' + obj.data.id);
            layer.confirm('确定删除该价格吗?', function(index) {
                _ajax({
                    url: '/goods/shop_join/del',
                    data: {
                       id: obj.data.id
                    },
                    success: function(res) {
                        layer.close(index);
                        if (typeof success === 'function') {
                            success(res);
                        } else {
                            if (res.code) {
                                layer.msg(res.msg, {
                                    icon: 2
                                });
                            } else {
                                layer.msg(res.msg, {
                                    icon: 1
                                });
                                setTimeout(function() {
                                    window.tableIns.reload(window.options);
                                }, 333);
                            }
                        }
                    },
                    error: function() {
                        layer.close(index);
                    }
                });
            });
            // _ajax({id: obj.id},)
            // _ajax_remove('确定删除该吗?', 'shop_join', obj.data.id);
        }
            // _ajax_remove('确定删除该吗?', 'shop_join', obj.data.id);
        
    });
});
