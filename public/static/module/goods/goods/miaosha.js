$(function() {
    $('.layui-plus').click(function() {
        layer_iframe('添加秒杀商品', 'edit_ms');
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            window.options.page.curr = curr;
            if (!res.data && curr > 1) {
                window.options.page = {
                    curr: curr - 1
                }
                window.tableIns.reload(window.options);
            }
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
                field: 'name',
                title: '名称',
                unresize: true,
                align: 'center'
            }, {
                field: 'image',
                title: '图片',
                unresize: true,
                align: 'center'
            }, {
                field: 'price',
                title: '秒杀价',
                unresize: true,
                align: 'center'
            }, {
                field: 'del_price',
                title: '原价',
                unresize: true,
                align: 'center'
            }, {
                field: 'sale',
                title: '销量',
                unresize: true,
                align: 'center'
            }, {
                field: 'sort',
                title: '排序',
                unresize: true,
                align: 'center'
            }, {
                field: 'create_time',
                title: '发布时间',
                unresize: true,
                align: 'center',
                width: 160
            }, {
                title: '销售状态',
                unresize: true,
                align: 'center',
                toolbar: '#toolbar_on_sale'
            }, {
                title: '操作',
                unresize: true,
                align: 'center',
                toolbar: '#toolbar_normal'
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event == 'edit') {
            layer_iframe('编辑秒杀商品', 'edit_ms?id=' + obj.data.id);
        } else if (obj.event == 'sold_out') {
            _ajax_update('确定下架该秒杀商品吗?', 'goods', obj.data.id, {
                on_sale: 0
            });
        } else if (obj.event == 'open_sell') {
            _ajax_update('确定上架该秒杀商品吗?', 'goods', obj.data.id, {
                on_sale: 1
            });
        } else if (obj.event == 'image') {
            layer_iframe('商品轮播图', '/goods/goods_images/index?goods_id=' + obj.data.id);
        }
    });
});
