$(function() {
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            window.options.page.curr = curr;
            if (!res.data && curr > 1) {
                window.options.page = {
                    curr: curr - 1
                }
                window.tableIns.reload(window.options);
            }
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
                field: 'nick',
                title: '用户',
                unresize: true,
                align: 'center'
            }, {
                field: 'score',
                title: '评分',
                unresize: true,
                align: 'center'
            }, {
                field: 'content',
                title: '评价内容',
                unresize: true,
                align: 'center'
            }, {
                field: 'images',
                title: '评价图片',
                unresize: true,
                align: 'center'
            }, {
                field: 'create_time',
                title: '评价时间',
                unresize: true,
                align: 'center'
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event == 'image') {
            layer_iframe('评价图片', '/goods/goods/comment_images?id=' + obj.data.id);
        }
    });
});