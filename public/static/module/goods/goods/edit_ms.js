$(function() {
    var content = layedit.build('content', {
        uploadImage: {
            url: '/ajax/index/upload'
        }
    });
    laydate.render({
        elem: '#ms_start_time',
        min: 1
    });
    laydate.render({
        elem: '#ms_end_time',
        min: 1
    });
    layform.on('submit(layeditform)', function(data) {
        if ((data.field.ms_end_time + data.field.ms_end_times) <= (data.field.ms_start_time + data.field.ms_start_times)) {
            layer.msg('结束时间需大于开始时间', {
                icon: 2,
                anim: 6
            });
            return false;
        }
        data.field.content = $.trim(layedit.getContent(content));
        _ajax({
            data: data.field,
            success: function(res) {
                if (res.code) {
                    layer.msg(res.msg, {
                        icon: 2
                    });
                } else {
                    layer.msg(res.msg, {
                        icon: 1
                    });
                    layer_iframe_close(333, true);
                }
            },
            error: function() {
                layer.msg('系统繁忙,请稍后重试', {
                    icon: 0
                });
            }
        });
        return false;
    });
});
