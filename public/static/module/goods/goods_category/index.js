$(function() {
    $('.layui-plus').click(function() {
        layer_iframe('添加商品分类', 'edit');
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            if (!res.data && curr > 1) {
                window.options.page.curr = curr - 1;
                window.tableIns.reload(window.options);
            }
            window.options.page.curr = curr;
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
                field: 'name',
                title: '名称',
                unresize: true,
                align: 'center'
            }, {
                field: 'sort',
                title: '排序(可编辑)',
                unresize: true,
                align: 'center',
                edit: 'text',
            }, {
                field: 'create_time',
                title: '添加时间',
                unresize: true,
                align: 'center'
            }, {
                title: '操作',
                unresize: true,
                align: 'center',
                width:250,
                toolbar: '#toolbar_normal'
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event === 'edit') {
            layer_iframe('编辑商品分类', 'edit?id=' + obj.data.id);
        } else if (obj.event === 'del') {
            // _ajax_remove('确定删除该商品分类吗?', 'goods_category', obj.data.id);
            layer.confirm('确定删除该商品分类吗?', function(index) {
                _ajax({
                    url: 'remove',
                    data: {
                        id: obj.data.id
                    },
                    success: function(res) {
                        if (res.code) {
                            layer.msg(res.msg, {
                                icon: 2
                            });
                        } else {
                            layer.msg(res.msg, {
                                icon: 1
                            });
                            setTimeout(function() {
                                window.tableIns.reload(window.options);
                            }, 333);
                        }
                    },
                    error: function() {
                        layer.close(index);
                    }
                });
            });
        }else if (obj.event === 'watch') {
            layer_iframe('查看', 'index?category=' + obj.data.id);
        }
    });
    //检测排序编辑
    laytable.on('edit(data_table)', function (obj) {
        var value = obj.value, // 得到修改后的值
            id = obj.data.id, // 得到当前修改行的id
            field = obj.field; // 得到当前修改的字段名
        var data = {},
            key = field;
        data[key] = value;
        data['id'] = id;
        $.ajax({
            url: 'editSort?id=' + obj.data.id,
            type: 'POST',
            data: data,
            success: function (data) {
                if (data.code == 1) {
                    layer.msg(data.msg, {
                        icon: 6,time: 500
                    });
                } else {
                    layer.msg(data.msg, {
                        icon: 5,time: 500
                    });
                }
            }
        });
    });
});