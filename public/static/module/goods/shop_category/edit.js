$(function() {
    var content = layedit.build('content', {
        uploadImage: {
            url: '/ajax/index/upload'
        }
    });
    layform.on('submit(layeditform)', function(data) {
        _ajax({
            data: data.field,
            success: function(res) {
                if (res.code) {
                    layer.msg(res.msg, {
                        icon: 2
                    });
                } else {
                    layer.msg(res.msg, {
                        icon: 1
                    });
                    layer_iframe_close(333, true);
                }
            },
            error: function() {
                layer.msg('系统繁忙,请稍后重试', {
                    icon: 0
                });
            }
        });
        return false;
    });
});
