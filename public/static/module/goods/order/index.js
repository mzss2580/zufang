$(function() {
    laydate.render({
        elem: '#start'
    });
    laydate.render({
        elem: '#end'
    });
    $('.layui-plus').click(function() {
        layer_iframe('添加', 'edit');
    });
    window.page = 1;
    window.limit = 10;
    window.options = {
        id: 'data_table',
        elem: '.layui-table.data-table',
        url: location.pathname + location.search,
        where: {},
        page: {
            curr: 1
        },
        limits: [10, 25, 50, 100],
        done: function(res, curr, first) {
            window.options.page.curr = curr;
            if (!res.data && curr > 1) {
                window.options.page = {
                    curr: curr - 1
                }
                window.tableIns.reload(window.options);
            }
            window.page = curr;
            window.limit = res.limit;
        },
        cols: [
            [{
                field: 'shop_name',
                title: '店铺',
                unresize: true,
                align: 'center'
            },{
                field: 'orderno',
                title: '订单号',
                unresize: true,
                align: 'center'
            },{
                field: 'union_orderno',
                title: '合并付款单号',
                unresize: true,
                align: 'center'
            }, {
                field: 'nick',
                title: '下单用户',
                unresize: true,
                align: 'center'
            }, {
                field: 'person',
                title: '收货人',
                unresize: true,
                align: 'center'
            }, {
                field: 'phone',
                title: '收货人手机号',
                unresize: true,
                align: 'center'
            }, {
                field: 'total',
                title: '订单总价',
                unresize: true,
                align: 'center'
            }, {
                field: 'pay_true',
                title: '实付金额',
                unresize: true,
                align: 'center'
            }
            , {
                field: 'pay_type',
                title: '支付类型',
                unresize: true,
                align: 'center'
            }, {
                field: 'status_desc',
                title: '状态',
                unresize: true,
                align: 'center'
            }, {
                field: 'create_time',
                title: '下单时间',
                unresize: true,
                align: 'center',
                width: 160
            }, {
                title: '操作',
                unresize: true,
                align: 'center',
                toolbar: '#toolbar_normal',
                width: 220
            }]
        ],
    };
    window.tableIns = laytable.render(window.options);
    laytable.on('tool(data_table)', function(obj) {
        if (obj.event == 'info') {
            layer_iframe('订单详情', 'info?id=' + obj.data.id);
        } else if (obj.event == 'send') {
            layer_iframe('订单发货', '/goods/order_goods/send?order_id=' + obj.data.id);
        } else if (obj.event == 'goods') {
            layer_iframe('订单商品', '/goods/order_goods/index?order_id=' + obj.data.id);
        } else if (obj.event == 'hurry') {
            var mydate = new Date();
            var times=mydate.toLocaleString();
            _ajax_update('确定催促吗?', 'order', obj.data.id, {
                hurry: times
            });
        }
    });
});