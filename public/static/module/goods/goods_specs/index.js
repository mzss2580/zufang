$(function() {
    var goods_id = location.search.replace('?goods_id=', ''),
        goods_specs_on_sale = function(confirm, on_sale, _this) {
            _ajax_update('确定' + confirm + '架该商品规格吗?', 'goods_specs', _this.parent().parent().attr('data-id'), {
                on_sale: on_sale
            }, function(res) {
                if (res.code) {
                    layer.msg(res.msg, {
                        icon: 2
                    });
                } else {
                    layer.msg(res.msg, {
                        icon: 1
                    });
                    setTimeout(function() {
                        location.reload();
                    }, 333);
                }
            });
        };
    $('.sold_out').click(function() {
        goods_specs_on_sale('下', 0, $(this));
    });
    $('.open_sell').click(function() {
        goods_specs_on_sale('上', 1, $(this));
    });
    $('.edit_specs').click(function() {
        layer_iframe('编辑商品规格', '/goods/goods_specs/edit?id=' + $(this).parent().parent().attr('data-id'));
    });
    $('.remove-goods-specs').click(function() {
        _ajax_remove('确认清空商品规格吗?', 'goods_specs', {
            goods_id: goods_id
        }, function(res) {
            layer.msg('操作成功', {
                icon: 1
            });
            layer_iframe_close(333, true);
        });
    });
});
